define("firebaseSettings", ["jquery"], function($) {
	
	var firebaseRoot = _global_setup.firebaseAddress;
	var firebaseIP  = "https://" + firebaseRoot + ".firebaseio.com";
 
 
		  return {  			
			///////////////////////////////////////
			setDefaultUserData: function(callback){

			 	//SET DEFAULT USER DATA - INIT IN HEADER
			 	var logState = localStorage.getItem("logState");
				var user = localStorage.getItem("user");	
			 	if (logState == null || logState == '' || user == null || user == '' || logState == "false"){								
					localStorage.clear();
					setTimeout(function(){
						localStorage.setItem("logState", false);					
						callback();
					}, 0);
			 	}
			 	else{
		 			callback();	
			 	};
			},
			///////////////////////////////////////		
	
		
			///////////////////////////////////////
			setLoginData: function(data, refresh){
					// PULL INFO 
					new Firebase(firebaseIP + '/users/' + data.id + "/info/").once('value', function(snap) {					   
						var data  = snap.val();
						
						// UPDATE SECURITY HASH
						var infoEntry = new Firebase(firebaseIP + '/users/' + data.id + "/info/");
						var securityHash = "";
						for (i = 0; i < 10; i++){	securityHash += Math.random().toString(36).substring(7);};
						infoEntry.update({auth: securityHash});						
						
						// SET LOCAL DATA
						defaultData = {
							id: data.id,
							auth: securityHash
						};
						
						
						localStorage.setItem("user", JSON.stringify(defaultData) );				
						if (refresh == null){refresh = false};
						if (refresh){
							location.reload();
						}
						else{
							// refresh controller 			
							setTimeout(function(){
								angular.element($('body')).scope().initCore.masterRefresh();
							}, 0);
						}				
					});	
			},
			///////////////////////////////////////	
			
			///////////////////////////////////////
			setLogout: function(){			
				localStorage.clear();
				location.reload();		
			},
			///////////////////////////////////////		
			
			
			///////////////////////////////////////	
			getUserData:function(callback){

					passCheck = true;	
						
					// CHECK USER ID AND AUTH
					var user = JSON.parse(localStorage.getItem("user"));
					if (user != null){
						if(user.id == null || user.id == undefined || user.auth == null || user.auth == null){
								passCheck = false;				
						}					
					}
					else{
						passCheck = false;
					}					
					
	
					if (passCheck){
						// PULL INFO 
						
						
						new Firebase(firebaseIP + '/users/' + user.id + "/info/").once('value', function(snap) {					   
							var data  = snap.val();
								if (data != null){
									// IF AUTHENTICATION MATCHES, PULL DATA
									if (data.auth == user.auth){							
											userData = {
												id: data.id,
												email: data.email,
												permission: data.permission,
												firstName: data.firstName,
												lastName: data.lastName,
												userName: data.userName
											};
											callback(true, userData)
									}
									
									// FAILED
									else{
										//returnFailData(true, "Authentication has failed.");
									}
								}
								else{
									//returnFailData(true, "Authentication failure.  Just like you.");
								}							
						});				
					}	
					else{
						callback(false);
					}	
				
			},
			///////////////////////////////////////	
			
			///////////////////////////////////////
			getPermissionLevel: function(callback){
				
				this.getUserData(function(returnState, data){
					
					
					function allowedToVisit(type){
						
						_array = [];
						for (var g = 0; g < _global_setup.pages.length; g++){
							var label = _global_setup.pages[g].label,
								permissions = _global_setup.pages[g].permissions;
								// superadmin has access to everything
								if (type == 'superadmin'){	
									_array.push(label);
								}
								// else filter for pages
								else{
									for (p = 0; p < permissions.length; p++){	
										if (type == permissions[p]){									
											_array.push(label);
											p = permissions.length; 	
										}
									}
								}
						}
						return _array;
					};
					

					if (!returnState){
						var type = "guest";
					}else{
						var type = data.permission.toLowerCase();
					}
					

					
					// ACCESS DEFINITIONS	
					for (i = 0; i < _global_setup.permissionTypes.length; i++){
						var permission = _global_setup.permissionTypes[i];
						if (type == permission.label.toLowerCase() ){
							
			  				var accessLevel = {
					  					isUser: true,
					  					permission:  permission.label.toLowerCase(),
					  					level:  permission.level,
					  					allowed: allowedToVisit(type)
					  		};	
					  		callback( true, accessLevel );														
						}								
					}							
							
					
				});

				
				
			},
			///////////////////////////////////////
			
			
			///////////////////////////////////////
			checkUserData: function(callback){

				var logState	= localStorage.getItem("logState");
				var passCheck = true; 
				var returnCheck = [false, false];
				var returnData = [
					{user: ""},
					{image: ""}				
				];
				
				// NOT LOGGED IN							
				if (logState == "false"){
					returnFailData(false, "Not logged in."); 
				}
				
				// CHECK USER ID AND AUTH
				var user = JSON.parse(localStorage.getItem("user"));
				if (user != null){
					if(user.id == null || user.id == undefined || user.auth == null || user.auth == null){
							passCheck = false; 
					}
				}
				else{
					passCheck = false; 
				}
				


				// RETRIEVE DATA AND CHECK AUTH
				if (passCheck){

					// PULL INFO 
					new Firebase(firebaseIP + '/users/' + user.id + "/info/").once('value', function(snap) {					   
						var data  = snap.val();
							if (data != null){
								// IF AUTHENTICATION MATCHES, PULL DATA
								if (data.auth == user.auth){	
									
																
										userData = {
											id: data.id,
											email: data.email,
											permission: data.permission,
											firstName: data.firstName,
											lastName: data.lastName,
											userName: data.userName
										};
										returnData.user = userData;																
										returnSuccessData(0);	
								}
								
								// FAILED
								else{
									returnFailData(true, "Authentication has failed.");
								}
							}
							else{
								returnFailData(true, "Authentication failure.  Just like you.");
							}							
					});
	
					// PULL IMAGE DATA
					new Firebase(firebaseIP + '/users/' + user.id + "/images/profile/").once('value', function(snap) {					   
						var data  = snap.val();
							if (data != null){
								imageData = {
									small: data.small,
									standard: data.standard,
									thumbnail: data.thumbnail,
								};
								
								returnData.image = imageData;																
								returnSuccessData(1);	
							}
							else{
								returnFailData(true, "Authentication failure.  Just like you.");
							}
					});					
				}; 		 

				// RETURN SUCCESS
				function returnSuccessData(arrayCheck){
					returnCheck[arrayCheck] = true;
					var pass = true;
					
					// CHECK FOR QUERIES
					for (i = 0; i < returnCheck.length; i++){
						if (returnCheck[i] == false){
							pass = false;
						}
					}
					//  WAITS FOR BOTH CALLS TO BE MADE BEFORE RETURNING
					if (pass){
						callback(true, returnData);	
					};
				};

				// DEFAULT RETURN FAIL DEFAULT
				function returnFailData(isError, errorType){
															
					callback(false, [isError, errorType]);
				}

			},
			///////////////////////////////////////	


			///////////////////////////////////////
			firebaseBase: function(){
				return firebaseRoot;		
			},
			///////////////////////////////////////
			
		  	///////////////////////////////////////
		    firebaseRoot: function() {
				return firebaseIP;
		    },
		    ///////////////////////////////////////	
		    	 
    
  		};
 
});
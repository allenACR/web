define("custom", ["jquery", "sharedData"], function($, sharedData) {//startwrapper
  	

	var firebaseRoot = "sizzling-fire-8858";
	var firebaseIP  = "https://" + firebaseRoot + ".firebaseio.com";
 
 
		  return {
		  	
		  	///////////////////////////////////////
		  	databaseInit:function(callback){

					var checkCount = 0; 
					
					// DATABASE TIMEOUT 
					var checkTimeout = function(){
						callback(false);				
					};								
					
					var checkConnection = function(){
						setTimeout(function(){
						 	var phpStatus = sharedData.getAll().phpStatus;	
						    if (phpStatus == undefined){							    	
						    	checkCount++;
						    	if (checkCount > 50){
						    		checkTimeout();
						    	}
						    	else{
						    		checkConnection();
						    	}							    	
						    }
						    else{
						    	callback(true);										
						    }
					   }, 200);
					};
					checkConnection();	
					
		  	 },
		  	///////////////////////////////////////
		  	
		  	
		  	///////////////////////////////////////		  	
		  	backstretchBG:function(image, d, f){
		  	 	$.backstretch([image], {duration: d, fade: f});
		  	 },
		  	   	 
		  	backstretchElement:function(target, image, d, f){
		  	 	$(target).backstretch([image], {duration: d, fade: f});
		  	 },
		  	 
		  	removeBackstretch:function(){		  	 	
		  	 	if($.find('.backstretch')){
  					  $('.backstretch').remove();
  				}
		  	 },
		  	///////////////////////////////////////
		  	 
		  	///////////////////////////////////////		  	
		  	equalize:function(element, delay, checkLimit){
		  		
				var largestHeight = 0,
					count = 0;
					
				if (delay == undefined){ delay = 1000; }
				if (checkLimit == undefined){ checkLimit = 1;}
				function eq(){	
						
					setTimeout(function(){ 
						$(element).each(function(){
							if ($(this).height() > largestHeight){largestHeight = $(this).height();}
						});
						$(element).each(function(index){
							
								$(this).animate({
									height: largestHeight								
								}, 250 );
							
							//$(this).height(largestHeight);
						});	
						count++;
						
						if (count < checkLimit){ eq(); }							
					}, delay);
				
				}
				eq();
		  	},
		  	///////////////////////////////////////		  	 
		  	 
		  	///////////////////////////////////////
		  	parseUrlString:function(url){
		  	 	
					var parser = document.createElement('a');
					parser.href = url;
					 
					parser.protocol; // => "http:"
					parser.hostname; // => "example.com"
					parser.port;     // => "3000"
					parser.pathname; // => "/pathname/"
					parser.search;   // => "?search=test"
					parser.hash;     // => "#hash"
					parser.host;     // => "example.com:3000"	
					
					var _return = {
						full: parser.href,
						protocol: parser.protocol,
						hostname: parser.hostname,
						port: parser.port,
						pathname: parser.pathname,
						search: parser.search,
						hash: parser.hash,
						host: parser.host
					};
					
					return _return; 	  	 	
		  	 },
		  	///////////////////////////////////////

		  	
		  	///////////////////////////////////////
		  	imageToDataUri:function(img, size, callback) {
						    // create an off-screen canvas
						    var canvas = document.createElement('canvas'),
						        ctx = canvas.getContext('2d');
							var image = new Image();
							image.src = img;
							image.onload = function() {	
								var aspectRatio = this.width / this.height,
									adjustedHeight = size / aspectRatio;
									var dimensions = {
										width: size,
										height: adjustedHeight,
										aspectRatio: aspectRatio
									};	
							    // set its dimension to target size
							    canvas.width = size;
							    canvas.height = adjustedHeight;									
							    ctx.drawImage(image, 0, 0, size, adjustedHeight);
							    callback( canvas.toDataURL(), dimensions ); 
							};
			},
			///////////////////////////////////////	
			
		  	///////////////////////////////////////
		  	resizeImageToDataUri:function(img, size, callback) {
		  		
		            var image = new Image();
		            image.onload = function (imageEvent) {
	
					        var maxWidth	= size; 		 // Max width for the image
					        var maxHeight 	= size;     	 // Max height for the image	
					        var ratio 		= 0;  			 // Used for aspect ratio
     					    var width 		= image.width;   // Current image width
       						var height 		= image.height;  // Current image height
       													var aspectRatio = image.width / image.height,
								adjustedHeight = size / aspectRatio;
							
							// Check if the current width is larger than the max
					        if(width > maxWidth){
					            ratio = maxWidth / width;   // get ratio for scaling image
					            height = height * ratio;    // Reset height to match scaled image
					            width = width * ratio;    // Reset width to match scaled image
					        }
        					// Check if the current width is larger than the max
					        if(height > maxHeight){
					            ratio = maxHeight / height; // get ratio for scaling image
					            width = width * ratio;    // Reset width to match scaled image
					            height = height * ratio;    // Reset height to match scaled image
					         }

			                // canvas

							var dimensions = {
								width: width,
								height: height,
								aspectRatio: aspectRatio
							};				                
			                
			                var canvas = document.createElement('canvas');
			             		canvas.width = width;
			                	canvas.height = height;
			                	canvas.getContext('2d').drawImage(image, 0, 0, width, height);
			                
			                //thumbnail
			                var thumbnail = canvas.toDataURL('image/jpeg');
							_return = {
								original: image.src, 
								original_dimensions: {aspectRatio: aspectRatio, width: image.width, height: image.height }, 
								thumbnail: thumbnail, 
								thumbnail_dimensions: dimensions
							};
							callback (_return);


			                
		            };
		            // fullsize image
					image.src = img.target.result;
					
			},
			///////////////////////////////////////				
			
			
			///////////////////////////////////////
			urlToBase64: function(url, callback) {
				
			    var data, canvas, ctx;
			    var img = new Image();
			    img.onload = function(){
			        // Create the canvas element.
			        canvas = document.createElement('canvas');
			        canvas.width = img.width;
			        canvas.height = img.height;
			        // Get '2d' context and draw the image.
			        ctx = canvas.getContext("2d");
			        ctx.drawImage(img, 0, 0);
			        // Get canvas data URL
			        try{
			            data = canvas.toDataURL();
			            callback({status: "success", data:data});
			        }catch(e){
			            callback({status: "error", data: e});
			        }
			    };
			    // Load image URL.
			    try{
			        img.src = url;
			    }catch(e){
			        error(e);
			    }
	
			},
			///////////////////////////////////////	
			
						
		  	
		  	///////////////////////////////////////
		  	parseFile:function(file, callback){
		  		Papa.parse(file, {
					download: true,
					complete: function(csv) {	
													
		 				var tableInfo = csv.data;
		                var headers = [];
		                var finalObj = {};
		
		                // convert headers into obj key
		                var assemblyList = [];
		                for (i = 0; i < tableInfo[0].length; i++){
		                  value = tableInfo[0][i].toLowerCase();
		                  finalObj[value] = {};
		                  assemblyList.push(value);
		                }
		
		                // grab first left hand column - the keys
		                var keys = [];
		                for (i = 0; i < tableInfo.length; i++){
		                   field = tableInfo[i][0].toLowerCase();
		                   if (field == ''){
		                     keys.push( ("key_" + i) );
		                   }
		                   else{
		                     keys.push(field);
		                   }
		                }
		
		                // assemble into object with key/values
		                for (i = 0; i < tableInfo.length; i++){
		                  for (m = 0; m < tableInfo[i].length; m++){
		                    var value = tableInfo[i][m];
		                    var name = assemblyList[m];
		                    var key = keys[i];
		                    finalObj[name][key] = value;
		                  }
		                }						
						
                    	callback(finalObj);											
					}
				});
		  	},
		  	///////////////////////////////////////
		  	
			///////////////////////////////////////
			buildObjectFromSpreadsheet:function(csv, callback ){

                var tableInfo = csv.data;
                var headers = [];
                var finalObj = {};

                // convert headers into obj key
                var assemblyList = [];
                for (i = 0; i < tableInfo[0].length; i++){
                  value = tableInfo[0][i].toLowerCase();
                  finalObj[value] = {};
                  assemblyList.push(value);
                };

                // grab first left hand column - the keys
                var keys = [];
                for (i = 0; i < tableInfo.length; i++){
                   field = tableInfo[i][0].toLowerCase();
                   if (field == ''){
                     keys.push( ("key_" + i) );
                   }
                   else{
                     keys.push(field);
                   }
                }

                // assemble into object with key/values
                for (i = 0; i < tableInfo.length; i++){
                  for (m = 0; m < tableInfo[i].length; m++){
                    var value = tableInfo[i][m];
                    var name = assemblyList[m];
                    var key = keys[i];
                        finalObj[name][key] = value;
                  }
                }

                callback(finalObj);
            },
			///////////////////////////////////////
		
			///////////////////////////////////////
			fetchTimestamp: function(){
					var dateObj = new Date();
					var month = dateObj.getUTCMonth();
					var day = dateObj.getUTCDate();
					var year = dateObj.getUTCFullYear();
					var time = dateObj.getTime(); 
					
					dateObject = {
						month: month,
						day: day,
						year: year,
						time: time
					};
					
					return dateObject;
				
			},
			///////////////////////////////////////
			
			
		  	///////////////////////////////////////
		  	offCanvasReset: function(){
		  		$('#offCanvasMain').addClass('canvas-initial-hide');
		  		$('#offCanvasMain .offcanvas-wrapper').addClass(' offcanvas-hide');
		  	},
		  	
		    offcanvas: function(action) {
		
					var canvas = $('#offCanvasMain'); 
								 canvas.removeClass('canvas-initial-hide');
					
					if (action == "show"){
							$('.offcanvas-wrapper').transition({  opacity: 1, x: '-110%' }, 0)
												   .transition({ opacity: 1, x: '0' }, 500, 'ease');							
							canvas.addClass('offcanvas-active');
							canvas.removeClass('offcanvas-hide');							
							$('body').css('overflow-y', 'hidden');
							return(true);		
					};
					
					if (action == "hide"){
							$('.offcanvas-wrapper').transition({  opacity: 1, x: '-110%' }, 500, 'ease');
							canvas.removeClass('offcanvas-active');
							canvas.addClass('offcanvas-hide');
							$('body').css('overflow-y', 'auto');
							return(false);
					};			
				
					if (action == "toggle"){
					
						if (canvas.hasClass('offcanvas-active')){
							$('.offcanvas-wrapper').transition({  opacity: 1, x: '0%' }, 0)
												   .transition({ opacity: 1, x: '-100%' }, 500, 'ease');							
							canvas.removeClass('offcanvas-active');
							canvas.addClass('offcanvas-hide');							
							$('body').css('overflow-y', 'hidden');
							return(false);	
							
						}else{
							$('.offcanvas-wrapper').transition({  opacity: 1, x: '-110%' }, 0)
												   .transition({ opacity: 1, x: '0' }, 500, 'ease');	
							canvas.addClass('offcanvas-active');
							canvas.removeClass('offcanvas-hide');							
							$('body').css('overflow-y', 'auto'); 							
							return(true);
						}
						
					};
				
		    },
		    ///////////////////////////////////////
		
		    ///////////////////////////////////////		
			transitIn:function(){
				
						$('body .row').not('.no-animation').each(function(index){
							$(this)
								.transition({   x: -50, scale: .95, opacity: 0}, 0)
								.transition({   x: 0, scale: 1, opacity: 1, delay: index*150}, 1000);	
						});		
	
			},
		    ///////////////////////////////////////
			
		
		  	///////////////////////////////////////
		    transitionStart: function() {

		
		    },
		    ///////////////////////////////////////
		
		  	///////////////////////////////////////
		    transitionEnd: function() {		
				
		    },
		    ///////////////////////////////////////
		
		  	///////////////////////////////////////
		    logger: function(msg) {
				console.log(msg);
		    },
		    ///////////////////////////////////////
		    
		  	///////////////////////////////////////
		    kcode: function() {
				alert("here")
		    },
		    ///////////////////////////////////////    
		  	
		  	///////////////////////////////////////
		    truncateText: function(text, limit ) {
				var myText = text.toString();
				len = myText.length;
				if(len>limit)
				{
					return myText.substr(0,limit)+'...';
				}	
				else{	
					return myText;
				}
		    },
		    ///////////////////////////////////////

			///////////////////////////////////////
			getRandomHash: function(length) {
			    var text = "";
			    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			
			    for( var i=0; i < length; i++ )
			        text += possible.charAt(Math.floor(Math.random() * possible.length));
			
			    return text;
			},			
			///////////////////////////////////////

			///////////////////////////////////////
			getRandomNumber: function(min, max) {
				return Math.floor( Math.random() * (max - min) + min );
			},			
			///////////////////////////////////////
			
			///////////////////////////////////////
			commaSeparateNumber: function(val){
			    while (/(\d+)(\d{3})/.test(val.toString())){
			      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
			    }
			    return val;
			},			
			///////////////////////////////////////
			
			///////////////////////////////////////
			checkNumberType: function(val){
			    var unit = "";
			    var fixedNum = 0; 
			    
			    if (val >= 100){
			    	unit = "Hundred";
			    	fixedNum = val; 			    	
			    };
			    if (val >= 1000){
			    	unit = "Thousand";
			    	fixedNum = (val * .001).toFixed(1); 
			    };
			    if (val >= 1000000){
			    	unit = "Million";
			    	fixedNum = (val * .000001).toFixed(1); 
			    };	
			    if (val >= 1000000000){
			    	unit = "Billion";
			    	fixedNum = (val * .000000001).toFixed(1); 
			    };	
			    return {unit: unit, number: fixedNum};			    		    
			},			
			///////////////////////////////////////			

			

            //////////////////////////////////
            //
            checkForNullsInObject:function(objects){

              for(var key in objects) {
                var value = objects[key];
                if (value == null || value == undefined || value == ''){
                  objects[key] = '';
                }
              }

              return objects;

            },
            //
            //////////////////////////////////


            //////////////////////////////////
            //  combines sort and page into one function
            // 
            //  var packet = {
			//			objToPaginate: objectToPaginate,
			//			filterBy: "id",
			//			reverse: true,
			//			filterSize: 10,
			//		};
            //
            //
            //  utilityjs.sortAndPage(packet)
            sortAndPage:function(packet){

                // sort array with objects
                sorted = this.sortArrayWithObjects(packet.objToPaginate, packet.filterBy, packet.reverse);

                // sort into pagination
                var pager = this.newPagination(sorted, packet.filterSize);
                

                return pager;

            },
            //
            //////////////////////////////////

	
	            //////////////////////////////////
	            /*  HOW TO USE
	            //  creates a pagination system for ANY array
	            //
	                var pager = utilityjs.newPagination(arrayData);
	              console.log(pager.page(x) )
	              console.log(pager.next() )
	              console.log(pager.prev() )
	              console.log(pager.hasNext() )
	              console.log(pager.hasPrev() )
	              console.log(pager.perPage, pager.totalPages, pager.currentPage)
	            */
	            //////////////////////////////////
	
	
	            //////////////////////////////////
	            //
	            newPagination:function(theArray, pPage){
	
	                function Paginate (data, perPage) {
	                  if (!data) throw new Error('Required Argument Missing');
	                  if (!(data instanceof Array)) throw new Error('Invalid Argument Type');
	
	                  this.data = data;
	                  this.perPage = perPage || 10;
	                  this.currentPage = 0;
	                  this.totalPages = Math.ceil(this.data.length / this.perPage);
	                };
	
	                Paginate.prototype.offset = function () {
	
	                  return ((this.currentPage - 1) * this.perPage);
	                };
	                Paginate.prototype.page = function (pageNum) {
	
	                  if (pageNum < 1) pageNum = 1;
	                  if (pageNum > this.totalPages) pageNum = this.totalPages;
	
	                  this.currentPage = pageNum;
	
	                  var start = this.offset()
	                    , end = start + this.perPage;
	
	                  return this.data.slice(start, end);
	                };
	                Paginate.prototype.next = function () {
	
	                  return this.page(this.currentPage + 1);
	                };
	                Paginate.prototype.prev = function () {
	
	                  return this.page(this.currentPage - 1);
	                };
	                Paginate.prototype.hasNext = function () {
	
	                  return (this.currentPage < this.totalPages);
	                };
	                Paginate.prototype.hasPrev = function () {
	
	                  return (this.currentPage > 1 );
	                };
	                if (typeof module !== 'undefined') module.exports = Paginate;
	
	
	                var pager = new Paginate(theArray, pPage);
	                return pager;
	          },
	          //
	          //////////////////////////////////
	
	
	
	          //////////////////////////////////
	          //  SORT ARRAY BY FIELD
	          //  sort array with objects
	          //  testData = [{name: "Bee"}, {name: "Allen"}, {name: "Cow"}]
	          //  sorted = utilityjs.sortArrayWithObjects($scope.tableData, 'name', true)
	          //  sort into pagination
	          //  var pager = utilityjs.newPagination(sorted);
	          //  console.log(pager)
	          sortArrayWithObjects:function(theArray, field, reverse){
	            theArray.sort(this.sort_by(field, reverse, function(a){return a.toUpperCase();}));
	            return theArray;
	          },
	
	
	          sort_by:function(field, reverse, primer){
	             var key = primer ?
	                 function(x) {return primer(x[field]);} :
	                 function(x) {return x[field];};
	
	             reverse = [1, -1][+!!reverse];
	
	             return function (a, b) {
	                 return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
	              };
	          },
	
	
	
	          sortArray:function(theArray, reverse){
	              var a = theArray.sort();
	              if (reverse){
	                a = theArray.reverse();
	              }
	
	              return a;
	          },
	          //////////////////////////////////
			
							
			
			///////////////////////////////////////
			selfReference: function(scripts){
			    filepath = scripts[ scripts.length-1 ].src; 
				var fileNameIndex = filepath.lastIndexOf("/") + 1;
				var filename = filepath.substr(fileNameIndex);
				filename = filename.replace(/\.[^/.]+$/, "");
				return filename;		
			},
			///////////////////////////////////////
			
			///////////////////////////////////////
			fillArray:function(length){				
				return Array.apply(null, new Array(length)).map(Number.prototype.valueOf,0);
			},
			///////////////////////////////////////
		
			///////////////////////////////////////
			//  will return the fields in an object
			//  object = { one: {id: "1", name: "Allen" },
			//			   two: {id: "2", name: "Angie" }
			//			 }
			//  custom.returnPropertyInObject(object, ["Id", "Name", "parameter3"];)
			//  
			//  will return as { id: [1, 2], name: ["Allen", "Angie"] } 
    		returnPropertyInObject: function(_object, field){
				
				
				var _return = {};
				for (i = 0; i < field.length; i++){
					_return[field[i]] = [];
				};
				
				
				var p = _object;
				for (var key in p) {
				  if (p.hasOwnProperty(key)) {				    
				    for (i = 0; i < field.length; i++){
				    	_return[field[i]].push(p[key][field[i]]);
				    };
				  }
				}
				
				return _return; 

			},	
			///////////////////////////////////////
			
			///////////////////////////////////////
			returnAllPropertiesInObject: function(obj){
				_return = [];
				for (var key in obj) {
				    if (obj.hasOwnProperty(key)) {				        
				         _return.push(key);
				    }
				}			
				
				return _return;
			},	
			///////////////////////////////////////
			
			///////////////////////////////////////
			//  search in array object to find matching index
			//  var Data = [
    		//		{id_list: 2, name: 'John', token: '123123'},
    		//		{id_list: 1, name: 'Nick', token: '312312'}
			//  ];
			//
			//  custom.findWithAttr(Data, 'name', 'John');  // returns 0
			//  custom.findWithAttr(Data, 'name', 'John');  // return 1
			//  custom.findWithAttr(Data, 'id_list', '10');   // returns undefined
			findWithAttr:function(array, attr, value) {
			    for(var i = 0; i < array.length; i += 1) {
			        if(array[i][attr] === value) {
			            return i;
			        }
			    }
			},	
			///////////////////////////////////////	
						
			///////////////////////////////////////
			//  will return the fields in an object
			//  object = { one: {id: "1", name: "Allen" },
			//			   two: {id: "2", name: "Angie" }
			//			 }
			//  custom.returnLengthOfObject(object)
			//  
			//  will return 2 
    		returnLengthOfObject: function(_object){
				
			    var size = 0, key;
			    for (key in _object) {
			        if (_object.hasOwnProperty(key)) size++;
			    }							
				return size;

			},	
			///////////////////////////////////////		
			
			
			///////////////////////////////////////
			//  will return the fields in an object
			//  object = { one: {id: "1", name: "Allen" },
			//			   two: {id: "2", name: "Angie" }
			//			 }
			//  custom.returnLengthOfObject(object)
			//  
			//  will return 2 
    		sanitizePacketsForDatabase: function(packetData, callback){
					
				 	var submitReady = [];
			 		var p = packetData.fields;
					for (var key in p) {
					  if (p.hasOwnProperty(key)) {
					   	if (packetData.inputData[key] != undefined || packetData.inputData[key] != null){							   								   		
								
					   		var type = p[key].Type;
					   		var inputType = p[key].inputType;
					   		var field = packetData.inputData[key]; //.replace(/"/g, "&quot;").replace(/'/g, "&lsquo;");
					   		// wrap any of these text fields otherwise it creates a crash 
					   		if (type.indexOf("char") > -1 || 
					   		    type.indexOf("text") > -1 ||
					   		    type.indexOf("tinytext") > -1 ||
					   		    type.indexOf("mediumtext") > -1 ||
					   		    type.indexOf("largetext") > -1 ||
					   		    type.indexOf("binary") > -1 ||
					   		    type.indexOf("blob") > -1 ||
					   		    type.indexOf("enum") > -1 ||
					   		    type.indexOf("set") > -1 ||
					   		    type.indexOf("date") > -1 ||
					   		    type.indexOf("time") > -1
					   		){
					   			field = "\"" + field + "\"";
					   		}
					   		submitReady.push({catagory: p[key].Field, field: field, type: type, inputType: inputType});
					   	};
					  };
					};	
								
					function stripEndQuotes(s){
						var t=s.length;
						if (s.charAt(0)=='"') s=s.substring(1,t--);
						if (s.charAt(--t)=='"') s=s.substring(0,t);
						return s;
					}									
								
					processed = 0;
					submitPacket = {};
					function loopCycle(){
						if (processed < submitReady.length){
							var encryptString = submitReady[processed].field;
							var inputType = submitReady[processed].inputType; 
							var catagory = submitReady[processed].catagory; 
							
							if (inputType == 'adv' || inputType == "imageUpload"){											
								phpJS.encodeHtmlEntities(stripEndQuotes(encryptString), function(state, data){
									submitPacket[catagory] = "\"" + data + "\"";
									processed++;
									loopCycle();
								});	
							}
							else{
								submitPacket[catagory] =  encryptString;
								processed++;
								loopCycle();
							}										
						}	
						//  loop complete
						else{
							loopComplete(submitPacket)
						}	
					};
					loopCycle();
					
					function loopComplete(submitPacket){
						callback( submitPacket );
					}


			},	
			///////////////////////////////////////						
			
			///////////////////////////////////////
			returnEditString: function(table, data, id){
	
				var buildString = "";
				
				var p = data; 
				for (var key in p) {
				  if (p.hasOwnProperty(key)) {
				    buildString += [key] + "=" + p[key] + ", ";
				  }
				}
				buildString = buildString.replace(/,\s*$/, "");
				
				
				return "UPDATE " + table + " SET " + buildString + " WHERE id=" + id;
					
			},	
			///////////////////////////////////////
			
			///////////////////////////////////////
			compareArrayForDifferences: function(arr1, arr2){
	
		   		var ret = [];
				for(i in arr1) {
				        if(arr2.indexOf( arr1[i] ) <= -1){
				            ret.push( arr1[i] );
				        }
				}
				return ret;
					
			},	
			///////////////////////////////////////			
			
			///////////////////////////////////////
    		lockBody: function(isMobile){
    			if(sharedData.fetch("browserDetails").mobile){
					$('body').css('overflow','hidden');
					$('body').css('position','fixed');	
				}
			},	
			///////////////////////////////////////		
			
			///////////////////////////////////////
    		unlockBody: function(isMobile){
    			if(sharedData.fetch("browserDetails").mobile){
					$('body').css('overflow','visible');
					$('body').css('position','relative');		
				}
			},	
			///////////////////////////////////////		
			
			///////////////////////////////////////
			parseManagerImages: function(data, folderName){
				var thumbnails = [],
					full = [];
				
			 		for (v = 0; v < data.files.length; v++){
			 			if(data.files[v].includes("_thumbnail")){
		 					thumbnails.push({src: "./media/manager/images/" + folderName + "/" +  data.files[v], name: data.files[v], active: true});
		 					
			 			}
			 			else{
			 				full.push({src: "./media/manager/images/" + folderName + "/" + data.files[v], name: data.files[v], active: true});
			 				
			 			};
			 		};	
			 		
			 		_return = {thumbnails: thumbnails, full: full};
			 		return (_return);		
			},
			
			///////////////////////////////////////		
			
			///////////////////////////////////////			
			initSlick: function(callback){
				
				
		    	$('.fade-slider').each(function(){
		    		
		    		if ($(this).hasClass('slider-inactive')){
		    			$(this).removeClass('slider-inactive').removeClass('hidden').addClass('slider-active').
				    	slick({
						  dots: true,
						  infinite: true,
						  speed: 500,
						  fade: true,
						  slide: 'div',
						  cssEase: 'linear',
		  					autoplay: true,
		  					autoplaySpeed: 2000,				  
					    });		
		    		}
		    	});
		    	
		    	$('.slideshow-slider').each(function(){
		    		if ($(this).hasClass('slider-inactive')){
		    			$(this).removeClass('slider-inactive').removeClass('hidden').addClass('slider-active').
				    	slick({
							dots: true,
							infinite: true,
							speed: 200,
							slide: 'div',
							cssEase: 'linear',
			  					autoplay: true,
			  					autoplaySpeed: 2000,				  
				    	});	 	
		    		}
		    	});		
		    	if (callback != null){
		    		callback();
		    	}		
			},	
			///////////////////////////////////////			
			
			///////////////////////////////////////
    		parallaxStart: function(){
				// Cache the Window object
			   $window = $(window);			         
			   $('div[data-type="background"]').each(function(){
			     var $bgobj = $(this); // assigning the object
			                   
			      $(window).scroll(function() {
			                    
					// Scroll the background at var speed
					// the yPos is a negative value because we're scrolling it UP!								
					var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
					
					var vOffset = $bgobj.data('offset');
					
					
					// Put together our final background position
					var coords = '50% '+ (yPos + vOffset) + 'px';
			
					// Move the background
					$bgobj.css({ backgroundPosition: coords });
					
					}); // window scroll Ends
			
			 });	
			///////////////////////////////////////
			
			


			
    		}
    
    
  		};
  
 
});//endwrapper



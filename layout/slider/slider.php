<div id='<?php $path = basename(__DIR__);echo $path;?>_id' class="" ng-init="initCore.init()">
    <div ng-if="masterData.browserSize == 'small'">
        <div class="small-slider"></div>
    </div>
    <div ng-if="masterData.browserSize == 'medium' || masterData.browserSize == 'large'">
        <div class="large-slider"></div>
    </div>
</div>
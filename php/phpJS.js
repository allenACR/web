

	////////////////////////
	// 	
	
	phpJS = {
		
		url: 'php/phpCore.php',

		/////////////////
		encodeHtmlEntities: function(htmlString, callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "encodeHtmlEntities", htmlString: htmlString},
	                 type: 'post',
	                 success: function(data) {
	                 	_returnString = data.replace(/&quot;/g, "~`").replace(/&lsquo;/g, "`~");
						callback(true, _returnString);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 		
		},
		/////////////////
		
		/////////////////
		dencodeHtmlEntities: function(htmlString, callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "dencodeHtmlEntities", htmlString: htmlString},
	                 type: 'post',
	                 success: function(data) {
	                 	_returnString = data.replace(/~`/g, "\"").replace(/`~/g, "'");
						callback(true, _returnString);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 		
		},
		/////////////////		

		/////////////////
		getIPAddress: function(callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getIPAddress"},
	                 type: 'post',
	                 success: function(data) {
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////
		
		
		/////////////////
		getSystemData:function(callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getSystemData"},
	                 type: 'post',
	                 success: function(data) {
	                 	var results = JSON.parse(data); 
	                 	if (results.status == "error"){
	                 		callback(  true, {hasData: false} );
	                 	}
	                 	else{
		                 	var obj = results[0]; 
		                 	phpJS.dencodeHtmlEntities(obj.systemdata, function(state, data){	                		
		                 		obj.systemdata  = JSON.parse(data);		                 		
		                 		obj.hasData = true;
		                 		callback(true, obj);
		                 	});
	                 	}
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////
		
		/////////////////
		createNewTable:function(tableName, callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "createNewTable", tableName: tableName},
	                 type: 'post',
	                 success: function(data) {	  
	                 	
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 	
		},
		/////////////////

		/////////////////
		renameTable:function(packet, callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "renameTable", oldName: packet.oldName, newName: packet.newName},
	                 type: 'post',
	                 success: function(data) {	  
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 	
		},
		/////////////////	
		
		/////////////////
		deleteTable:function(tableName, callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "deleteTable", tableName: tableName},
	                 type: 'post',
	                 success: function(data) {	  
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 	
		},
		/////////////////		

		/////////////////
		//phpJS.returnListOfTables("codeandl_example", function(state, data){console.log(data)})
		returnListOfTables: function(database, callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "returnListOfTables", database: database},
	                 type: 'post',
	                 success: function(data) {
	                 	var results = JSON.parse(data); 
	                 	_return = {user: [], system: []};
	                 	for (i = 0; i < results.length; i++){
	                 		if(results[i].indexOf('_') === -1){
	                 			_return.user.push(results[i]);
	                 		}
	                 		else{
	                 			_return.system.push(results[i]);
	                 		}
	                 	}
	                 	
	                 	
	                 	callback(true, _return);
	                 	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////	

		
		/////////////////
		getPageData: function(pageName, callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getPageData", pageName: pageName},
	                 type: 'post',
	                 success: function(data) {
	                 	var results = JSON.parse(data); 	                 	
	                 	if (results.status == "error"){
	                 		callback(  {hasData: false} );
	                 	}
	                 	else{
		                 	obj = results[0]; 
		                 	phpJS.dencodeHtmlEntities(obj.pageSettings, function(state, data){
		   
		                 		obj.pageSettings  = JSON.parse(data);		                 	
		                 		obj.hasData = true;		                 		
		                 		callback(obj);
		                 	});
	                 	}
	                 	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////	
		
		/////////////////
		updatePageSettings: function(packet, callback){			   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "updatePageSettings", pageName: packet.pageName, settings: packet.settings, desktopTable: packet.desktopTable, mobileTable: packet.mobileTable},
	                 type: 'post',
	                 success: function(data) {	                 
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////	
		
		/////////////////
		renameTablesInPageSettings:function(packet, callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "renameTablesInPageSettings", oldname: packet.oldname, newname: packet.newname},
	                 type: 'post',
	                 success: function(data) {	                 
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	
	       
		},
		/////////////////
		
		/////////////////
		updateSystemSettings: function(packet, callback){	
	       $.ajax({ url: phpJS.url,
	                 data: {action: "updateSystemSettings", settings: packet.settings},
	                 type: 'post',
	                 success: function(data) {	                 
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////		
				
			
		
		/////////////////
		getFullUrl: function(callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getFullUrl"},
	                 type: 'post',
	                 success: function(data) {
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////		
		
		/////////////////
		getPHPStatus: function(callback){
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getPHPStatus"},
	                 type: 'post',
	                 success: function(data) {
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	 
		},
		/////////////////
		
		// test
	    testFunction: function(callback) {   	
	       $.ajax({ url: phpJS.url,
	                 data: {action: "testFunction"},
	                 type: 'post',
	                 success: function(data) {
						callback(true, data);	
	                 },
					 error: function(data){
						callback(false, data) ;  
					 }
	        });	        
	    },   
	    /////////////////
	    
		// create new entry
	    createNewEntry: function(packet, values, callback) { 	 	 
		    $.ajax({ url: phpJS.url,
		                 data: {action: "createNewEntry", database: packet.database, table: packet.table, values: values},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data) );	
		                 },
						 error: function(data){
							callback(false, JSON.parse(data) ) ;  
						 }
		    });	 
	    },  
	     /////////////////	    
	    
		// create blank entry - return id
	    createBlankEntry: function(packet, callback) { 	 
	    		  
		    $.ajax({ url: phpJS.url,
		                 data: {action: "createBlankEntry", database: packet.database, table: packet.table},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data) );	
		                 },
						 error: function(data){
							callback(false, JSON.parse(data) ) ;  
						 }
		    });	 
	    },    
	     /////////////////
	    
	    
		// get table data
		// var packet={database: "codeandl_example", table: "entries"}
		// phpJS.getTableFields(packet, function(state, data){ console.log(data)})
	    getTableFields: function(packet, callback) { 	 	 
	 
	       $.ajax({ url: phpJS.url,
	                 data: {action: "getTableFields", database: packet.database, table: packet.table},
	                 type: 'post',
	                 success: function(data) {
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },  
	    	 
		// create query
	    queryDatabase2: function(packet, callback) { 			
	       $.ajax({ url: phpJS.url,
	                 data: {action: "queryDatabase2", database: packet.database, query: packet.query},
	                 type: 'post',
	                 success: function(data) {	                 	
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data)) ;  
					 }
	        });	        
	    },   
	    /////////////////	 
	    	 
		// create query
		// var packet = {database: 'codeandl_example', query: 'SELECT * FROM ' }
		// phpJS.queryDatabase(packet, function(state, data){console.log()})
	    queryDatabase: function(packet, callback) { 

	       $.ajax({ url: phpJS.url,
	                 data: {action: "queryDatabase", database: packet.database, query: packet.query},
	                 type: 'post',
	                 success: function(data) {	          
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data)) ;  
					 }
	        });	        
	    },   
	    /////////////////	    
	    
		// create query
	    queryNumberOfEntries: function(packet, callback) { 
	       $.ajax({ url: phpJS.url,
	                 data: {action: "queryNumberOfEntries", database: packet.database, table: packet.table},
	                 type: 'post',
	                 success: function(data) {	      
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data)) ;  
					 }
	        });	        
	    },   
	    /////////////////	  	    
	       
	       

		// send email
		// phpJS.listOfFiles("../themes/templates/", function(state, data){ console.log(data) })
	    listOfFiles: function(folder, callback) { 
	    	   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "listOfFiles", folder: folder},
	                 type: 'post',
	                 success: function(data) {
	                 	_data = JSON.parse(data); 
	                 	if (_data.status != "error"){
		                 	for (var i = 0; i < _data.files.length; i++){
		                 		
		                 		if (_data.files[i] == ".DS_Store"){
		                 			_data.files.splice(i, 1);
		                 		};
	                 	}
	                 	}
	                 
	                 	callback(true, _data );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////	
	    
	    
		// send email
	    listOfFiles_noExtensions: function(folder, callback) { 
	    	   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "listOfFiles", folder: folder},
	                 type: 'post',
	                 success: function(data) {
	                 	var d = JSON.parse(data); 
	                 	if (d.status != "error"){
		                 	if (d.files.length > 0){
		                 		for (var i = 0; i < d.files.length; i++){
		                 			d.files[i] = d.files[i].replace(/\.[^/.]+$/, "");
		                 		}
		                 	}
		                 	callback(true, d);	
	                 	};
	                 	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////		    
	    
	    
		// send email
	    swapPageNames: function(packet, callback) {
	    	 
	       $.ajax({ url: phpJS.url,
	                 data: {action: "swapPageNames", database: packet.database, oldName: packet.oldName, newName: packet.newName},
	                 type: 'post',
	                 success: function(data) {
	                 	callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////		    
	   
	   /////////////////	
	   // obj = {name: "something"}
	   // phpJS.returnJsonToObject("../production/json/test.json", function(state, data){ console.log(data)})	
	   returnJsonAsObject: function(loadLocation, callback){
					
	       $.ajax({ url: phpJS.url,
	                 data: {action: "returnJsonAsObject", 	                 		 
	                 		loadLocation: loadLocation
	                 		},
	                 type: 'post',
	                 success: function(data) {	    
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	
	   },
	   /////////////////	
	   
	   /////////////////	
	   // obj = {name: "something"}
	   // phpJS.convertObjectToJson(obj, "../production/json/test.json", function(state, data){ console.log(data)})	
	   convertObjectToJson: function(object, saveLocation, callback){
	   	
			var sendObj = JSON.stringify(object)
	   		
	       $.ajax({ url: phpJS.url,
	                 data: {action: "convertObjectToJson", 
	                 		object: sendObj, 
	                 		saveLocation: saveLocation
	                 		},
	                 type: 'post',
	                 success: function(data) {	             	
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });		
	   	
	   },
	   /////////////////		
		// generate SEO json
		// var packet = {title: "Title of Site", description: "I am the description", meta: "meta, tags, etc", author: "you, author"}
		// phpJS.generateSEOjson(packet, function(statem, data){console.log(data)})
	    generateSEOjson: function(packet, callback) { 
	    	   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "generateSEOjson", 
	                 		title: packet.title, 
	                 		description: packet.description, 
	                 		meta: packet.meta, 
	                 		author: packet.author},
	                 type: 'post',
	                 success: function(data) {	                 	
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////	
	    
		// generate SEO json
		//var packet = {local: "localhost", remote: "example.justhost.com", user: "the_admin", key:"something", db: "the_database", fb: "firebaseAddress"}
		//phpJS.generateSetupjson(packet, function(statem, data){console.log(data)}) 
	    generateSetupjson: function(packet, callback) { 
	    	   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "generateSetupjson", 
	                 		local: packet.local, 
	                 		remote: packet.remote, 
	                 		user: packet.user, 
	                 		key: packet.key, 
	                 		db: packet.db,
	                 		fb: packet.fb,
	                 		},
	                 type: 'post',
	                 success: function(data) {	                 	
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////	
	    
	    
	    /////////////////
	    restoreSetupConfig: function(callback) {    	   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "restoreSetupConfig"},
	                 type: 'post',
	                 success: function(data) {	                 	
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////	
	    
	    /////////////////
		//  phpJS.retrieveSetupConfig(function(state, data){
  		//  	console.log(data)
		//  })	    
	    retrieveSetupConfig: function(callback) {    	   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "retrieveSetupConfig"},
	                 type: 'post',
	                 success: function(data) {	                 	
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////		    	    
	    	    
	    	 
	    	    
	       
		// send email
	    sendEmail: function(emailData, callback) { 
	    	   
	       $.ajax({ url: phpJS.url,
	                 data: {action: "sendEmail", sendTo: emailData.sendTo, subject: emailData.subject, content: emailData.content, replyTo: emailData.replyTo},
	                 type: 'post',
	                 success: function(data) {
						callback(true, JSON.parse(data) );	
	                 },
					 error: function(data){
						callback(false, JSON.parse(data) ) ;  
					 }
	        });	        
	    },   
	    /////////////////	
	    
		// basic upload (file already in base64 format)
	    imageUploadBase64: function(base64, nameOfFile, fileLocation, callback) {	
	    	
	    	if (fileLocation == null || fileLocation == undefined){
	    		fileLocation = "";
	    	};
	    	 	
		       $.ajax({ url: phpJS.url,
		                 data: {action: "basicUpload", base64: base64, nameOfFile: nameOfFile, fileLocation: fileLocation},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 

	    },   
	    /////////////////		    
	 
	 	/////////////////		    
	    videoUpload: function(file, nameOfFile, fileLocation, callback) {	
	    	/*
	    	if (fileLocation == null || fileLocation == undefined){
	    		fileLocation = "";
	    	};
			    	
			var reader = new FileReader();
	        reader.onload = function(evt) {				   
		       $.ajax({ url: phpJS.url,
		                 data: {action: "videoUpload", videoData: evt.target.result, nameOfFile: nameOfFile, fileLocation: fileLocation},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 		        
			};
			reader.readAsDataURL(file)
			*/
	    },  
	 	/////////////////		     
	 	
	 
		// basic upload
	    basicUpload: function(file, nameOfFile, fileLocation, callback) {	
	    	
	    	if (fileLocation == null || fileLocation == undefined){
	    		fileLocation = "";
	    	};
	    	
			var reader = new FileReader();
	        reader.onload = function(readerEvt) {
	            var base64 = readerEvt.target.result;
		       $.ajax({ url: phpJS.url,
		                 data: {action: "basicUpload", base64: base64, nameOfFile: nameOfFile, fileLocation: fileLocation},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
	        };
	        reader.readAsDataURL(file);

	    },   
	    /////////////////	

		// delete files
		//  var packet = [];
		//	packet.push({folder: "../themes/templates/" + _global_setup.theme + "/background/", file: name});						
		//  phpJS.deleteFiles(packet, function(state, data){console.log(data) })
	    deleteFiles: function(packet, callback) {	

		       $.ajax({ url: phpJS.url,
		                 data: {action: "deleteFiles", packet: packet},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
			
			
	    },   
	    /////////////////	
	    

		// rename files
		// var packet = {oldName: '../text.txt', newName: '../new.txt''}
		// phpJS.renameFile(packet, function(state, data){console.log(data)})
	    renameFile: function(packet, callback) {	

		       $.ajax({ url: phpJS.url,
		                 data: {action: "renameFile", oldName: packet.oldName, newName: packet.newName},
		                 type: 'post',
		                 success: function(data) {
		                 	callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
			
			
	    },   
	    /////////////////		
	    
	    
		// rename files
		// var packet = {source: '../text.txt', newName: '../new.txt''}
		// phpJS.copyFile(packet, function(state, data){console.log(data)})
	    copyFile: function(packet, callback) {	

		       $.ajax({ url: phpJS.url,
		                 data: {action: "copyFile", source: packet.source, newName: packet.newName},
		                 type: 'post',
		                 success: function(data) {
		                 	callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
			
			
	    },   
	    /////////////////			        
	    

		// rename folder
		// var packet = {oldFolder: "../oldFolder", newFolder: "../newFolder"}
		// phpJS.renameFolder(packet, function(state, data){ console.log(data) })
	    renameFolder: function(packet, callback) {	
	    	
		       $.ajax({ url: phpJS.url,
		                 data: {action: "renameFolder", oldFolder: packet.oldFolder, newFolder: packet.newFolder},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
			
			
	    },   
	    /////////////////	
	    
		// delete folder
		// phpJS.deleteFolder("themes/structure/something", function(state, data){console.log(data)})				
	    deleteFolder: function(nameOfFolder, callback) {	

		       $.ajax({ url: phpJS.url,
		                 data: {action: "deleteFolder", folder: nameOfFolder},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
			
			
	    },   
	    /////////////////	
	    
		// create folder
		// var packet = {newFolder: "../themes/structure/folderName"}
		// phpJS.createFolder(packet, function(state, data){console.log(data)})		
	    createFolder: function(packet, callback) {	
				
				
		       $.ajax({ url: phpJS.url,
		                 data: {action: "createFolder", newFolder: packet.newFolder},
		                 type: 'post',
		                 success: function(data) {
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
			
			
	    },   
	    /////////////////		

		// create folder
		// var packet = {location: "../themes/structure/something.html"}
		// phpJS.retrieveFileContents(packet, function(state, data){console.log(data)})		
	    retrieveFileContents: function(packet, callback) {	
				
				
		       $.ajax({ url: phpJS.url,
		                 data: {action: "retrieveFileContents", location: packet.location},
		                 type: 'post',
		                 success: function(data) {		                 	
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
			
			
	    },   
	    /////////////////			


		// create folder
		// var packet = {location: "../file.txt", content: "Hello world!"}
		// phpJS.saveFileContents(packet, function(state, data){console.log(data)})		
	    saveFileContents:function(packet, callback){
	    	
		       $.ajax({ url: phpJS.url,
		                 data: {action: "saveFileContents", location: packet.location, content: packet.content},
		                 type: 'post',
		                 success: function(data) {		                 	
							callback(true, JSON.parse(data));	
		                 },
						 error: function(data){
							callback(false, data) ;  
						 }
		        });	 
				
	    }        
	    /////////////////	    	            
   
	};
	//
	////////////////////////		
	
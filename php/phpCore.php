<?php


require_once 'settings.php';

//  PARSE THE CALL	
//
if(isset($_POST['action']) && !empty($_POST['action'])) {

    $action 			= $_POST['action'];

    switch($action) {
		case 'testFunction'					: testFunction();break;
		case 'getPHPStatus'					: getPHPStatus();break;
					
		case 'getIPAddress'					: getIPAddress(); break;
		case 'getFullUrl'					: getFullUrl(); break;
		case 'getSystemData'				: getSystemData(); break;
		case 'getPageData'					: getPageData($_POST['pageName']); break;
		case 'updatePageSettings'			: updatePageSettings($_POST['pageName'], $_POST['settings'], $_POST['desktopTable'], $_POST['mobileTable']); break;
		case 'updateSystemSettings' 		: updateSystemSettings($_POST['settings']);break;
		case 'renameTablesInPageSettings'	: renameTablesInPageSettings($_POST['oldname'], $_POST['newname']);break;
		case 'createNewTable'				: createNewTable($_POST['tableName']);break;
		case 'renameTable'					: renameTable($_POST['oldName'], $_POST['newName']);break;
		case 'deleteTable'					: deleteTable($_POST['tableName']);break;
		
		
		case 'returnListOfTables'			: returnListOfTables($_POST['database']); break;
		case 'encodeHtmlEntities'			: encodeHtmlEntities($_POST['htmlString']);break;
		case 'dencodeHtmlEntities'			: dencodeHtmlEntities($_POST['htmlString']);break;
		case 'createBlankEntry'				: createBlankEntry($_POST['database'], $_POST['table']); break;
		case 'createNewEntry'				: createNewEntry($_POST['database'], $_POST['table'], $_POST['values']); break;
		case 'getTableFields'				: getTableFields($_POST['database'], $_POST['table']); break;
		case 'queryDatabase'				: queryDatabase($_POST['database'], $_POST['query']);break;
		case 'queryDatabase2'				: queryDatabase2($_POST['database'], $_POST['query']);break;
		case 'queryNumberOfEntries'			: queryNumberOfEntries($_POST['database'], $_POST['table']);break;
		case 'sendEmail'					: sendEmail($_POST['sendTo'], $_POST['subject'], $_POST['content'], $_POST['replyTo']);break;
		
		case 'convertObjectToJson'			: convertObjectToJson($_POST['object'], $_POST['saveLocation']);break;	
		case 'returnJsonAsObject'			: returnJsonAsObject($_POST['loadLocation']);break;
		case 'generateSEOjson'				: generateSEOjson($_POST['title'], $_POST['description'], $_POST['meta'], $_POST['author']);break; 
		case 'generateSetupjson'			: generateSetupjson($_POST['local'], $_POST['remote'], $_POST['user'], $_POST['key'], $_POST['db'], $_POST['fb']);break;
		case 'restoreSetupConfig'			: restoreSetupConfig();break;
		case 'retrieveSetupConfig'			: retrieveSetupConfig(); break;
		
		case 'basicUpload'					: basicUpload($_POST['base64'], $_POST['nameOfFile'], $_POST['fileLocation']); break;
		case 'videoUpload'					: videoUpload($_POST['videoData'], $_POST['nameOfFile'], $_POST['fileLocation']); break;
				
		case 'deleteFiles'					: deleteFiles($_POST['packet']);break;
		case 'renameFile'					: renameFile($_POST['oldName'], $_POST['newName']);break;
		case 'copyFile'						: copyFile($_POST['source'], $_POST['newName']);break;
		
		case 'renameFolder'					: renameFolder($_POST['oldFolder'], $_POST['newFolder']);break;
		case 'deleteFolder'					: deleteFolder($_POST['folder']); break;		
		case 'createFolder'					: createFolder($_POST['newFolder']); break;
		
		case 'listOfFiles'					: listOfFiles($_POST['folder']); break;
		case 'swapPageNames'				: swapPageNames($_POST['database'], $_POST['oldName'], $_POST['newName']); break;
		
		case 'retrieveFileContents'			: retrieveFileContents($_POST['location']); break;
		case 'saveFileContents'				: saveFileContents($_POST['location'], $_POST['content']); break;
    }
}	
//
///////////


		////////////////////////
		//phpJS.testFunction(function(state, data){console.log(data)})
		function testFunction(){
			
			echo "PHP Test script successful.";		

		}
		//
		////////////////////////
		
		////////////////////////
		//
		function encodeHtmlEntities($htmlString){			
			echo htmlentities($htmlString);		
		}
		//
		////////////////////////		

		////////////////////////
		//
		function dencodeHtmlEntities($htmlString){		
			echo html_entity_decode($htmlString);		
		}
		//
		////////////////////////
		
		
		
		////////////////////////
		//
		function getIPAddress(){
				$fileData = array(
						"ipAddress" => get_client_ip()						
				);	
				echo json_encode($fileData);				
		}
		//
		////////////////////////
		
		////////////////////////
		//		
		function returnUrl(){
				$path = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				$scheme = parse_url($path);
				$tokens = explode('/', $path);
				$rootFolder =  $tokens[sizeof($tokens)-3];	
				$base =  $tokens[sizeof($tokens)-4];						
				
				$full = $_SERVER['HTTP_HOST'] . "/" . $base . "/" . $rootFolder . "/" ;
				if ($base == "localhost:8888" || $base == "localhost" ){
					$full = $_SERVER['HTTP_HOST'] . "/" . $rootFolder . "/"; 
				}
				
				$fileData = array(
						"relative" => $base . "/" . $rootFolder . "/",
						"full" => $full,
						"base" => $base,
						"folder" => $rootFolder,
						"uploadFolder" => $full . "uploads/"
				);
				
				return $fileData;			
		}
		
		function getFullUrl(){

				echo json_encode( returnUrl() );	
		}
		//
		////////////////////////
		
		////////////////////////
		//		
		function getSystemData(){
			global $link; 
			global $usingDatabase; 
			
				// SELECT THE BLOG DATABASE
				$table = "_systems";
				$query = "SELECT * FROM $table";
				
		
				$db_selected = mysql_select_db($usingDatabase);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){


					// DO QUERY  
					$json = sql2json($query);
					// QUERY IS VALID
					if ($json != ''){		
						echo $json;    							
					}				
					// QUERY IS INVALID		
					else{
						returnFalse();
						exit;
					};


				}
				else{
					returnFalse();
					exit;
				}	
		}
		//
		////////////////////////		
		

		////////////////////////
		//		
		function createNewTable($tableName){
			
			global $link; 
			global $usingDatabase;

				// SELECT THE BLOG DATABASE
				$query = "CREATE TABLE `$tableName` (`id` TINYINT NOT NULL AUTO_INCREMENT COMMENT 'noshow',PRIMARY KEY (`id`)) COLLATE='latin1_swedish_ci' ENGINE=MyISAM";
				

				$db_selected = mysql_select_db($usingDatabase);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){
					$fileData = array(
						"status" => "good"
					);	
					echo json_encode($fileData);
					exit;
				}
				else{
					returnFalse();
					exit;
				}			

		}
		//
		////////////////////////	


		////////////////////////
		//		
		function renameTable($oldName, $newName){
			
			global $link; 
			global $usingDatabase;

				// SELECT THE BLOG DATABASE
				$query = "RENAME TABLE `$oldName` TO `$newName`";
				
				$db_selected = mysql_select_db($usingDatabase);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){
					$fileData = array(
						"status" => "success"
					);	
					echo json_encode($fileData);
					exit;
				}
				else{
					returnFalse();
					exit;
				}			

		}
		//
		////////////////////////	

		////////////////////////
		//		
		function deleteTable($tableName){
			
			global $link; 
			global $usingDatabase;

				// SELECT THE BLOG DATABASE
				$query = "DROP TABLE `$tableName`";
				
				$db_selected = mysql_select_db($usingDatabase);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){
					$fileData = array(
						"status" => "success"
					);	
					echo json_encode($fileData);
					exit;
				}
				else{
					returnFalse();
					exit;
				}			

		}
		//
		////////////////////////	
		
		

		////////////////////////
		//		
		function returnListOfTables($database){
			
			
			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				$db_selected = mysql_select_db($database);  // 			
				$query = "SHOW TABLES"; 
				$results = mysql_query($query);
				
				if ($results){
					
				
					$json = sql2json($query);
					// QUERY IS VALID
					if ($json != ''){		
						echo $json;    							
					}				
					// QUERY IS INVALID		
					else{
						returnFalse();
						exit;
					};				
					
				}
				
			}			

		}
		//
		////////////////////////		
				
		
		////////////////////////
		//
		function getPageData($pageName){
			global $link; 
			global $usingDatabase; 
			
				// SELECT THE BLOG DATABASE
				$table = "_pagedata";
				$query = "SELECT * FROM $table WHERE name = '$pageName'";
				
		
				$db_selected = mysql_select_db($usingDatabase);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){


					// DO QUERY  
					$json = sql2json($query);
					// QUERY IS VALID
					if ($json != ''){		
						echo $json;    							
					}				
					// QUERY IS INVALID		
					else{
						returnFalse();
						exit;
					};


				}
				else{
					returnFalse();
					exit;
				}	
						
			
		}
		//
		////////////////////////
		
		
		////////////////////////
		//		
		function updatePageSettings($pageName, $newSettings, $desktopTable, $mobileTable){
			global $link; 
			global $usingDatabase;

				// SELECT THE BLOG DATABASE
				$table = "_pagedata";
				$query = "UPDATE $table SET pageSettings='$newSettings', desktopTable='$desktopTable', mobileTable='$mobileTable' WHERE name='$pageName'";

				$db_selected = mysql_select_db($usingDatabase);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){
					$fileData = array(
						"status" => "good"
					);	
					echo json_encode($fileData);
					exit;
				}
				else{
					returnFalse();
					exit;
				}	
				
		}
		
		function renameTablesInPageSettings($oldname, $newname){
			global $link; 
			global $usingDatabase;
			
				$table  = "_pagedata";	
				$query1  = "UPDATE $table SET desktopTable='$newname' WHERE desktopTable='$oldname'";
				$query2 = "UPDATE $table SET mobileTable='$newname' WHERE mobileTable='$oldname'";
				
				$db_selected = mysql_select_db($usingDatabase);  // 
				$results1 = mysql_query( $query1, $link ); 
				$results2 = mysql_query( $query2, $link ); 
				if ($results1){ $results1 = "success"; }else{$results1 = "failure";};
				if ($results2){ $results2 = "success"; }else{$results2 = "failure";};
				
				$fileData = array(
					"desktop" => "$results1",
					"mobile" => "$results2"
				);	
				echo json_encode($fileData);
				exit;				
								
		}
		//
		////////////////////////	
		
		////////////////////////
		//		
		function updateSystemSettings($newSettings){
			global $link; 
			global $usingDatabase;
			

				// SELECT THE BLOG DATABASE
				$table = "_systems";
				$query = "UPDATE $table SET systemdata='$newSettings' WHERE id='1'";
				

				$db_selected = mysql_select_db($usingDatabase);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){
					$fileData = array(
						"status" => "good"
					);	
					echo json_encode($fileData);
					exit;
				}
				else{
					returnFalse();
					exit;
				}	
				
		}
		//
		////////////////////////		
			
		
		////////////////////////
		//	
		function createNewEntry($database, $dbTable, $values){
			
			global $link; 
			
			if(connectToDatabase($database) ){
				
				$db_selected = mysql_select_db($database);  // 											
				$values = json_decode($values, true);	
				
				//CONVERT ARRAY TO TEXT
				reset($values);
				$keys = array();
				$vals = array();
				while (list($key, $val) = each($values)) {
				    //echo "$key => $val\n";
					$keys[] = "$key";
					$vals[] = "$val";
				}
				$keyString = implode(',', $keys);
				$valueString = implode(',', $vals);
				
				// INSERT INTO DATABASE
				$sql = 	"INSERT INTO $dbTable" . 
					 	"(" . $keyString . ")" . 
						"VALUES (". $valueString . ")";
				if (mysql_query( $sql, $link )){
					$fileData = array(
						"statement" => $sql,
						"status" => "good",
						"createdId" => mysql_insert_id()
					);	
					echo json_encode($fileData);	
				}
				else{
					echo returnFalse();	
				}
			}		
		}
		//
		////////////////////////
				
		////////////////////////
		//	
		function getTableFields($database, $dbTable){
			
			
			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				$db_selected = mysql_select_db($database);  // 			
				$result = mysql_query("SHOW FULL COLUMNS FROM $dbTable");
				
				if (!$result) {
				   	returnFalse();
				    exit;
				}
				if (mysql_num_rows($result) > 0) {
					$resultArray = array();
				    while ($row = mysql_fetch_assoc($result)) {
				       
				       $resultArray[] = $row;
				    };
					
					echo json_encode($resultArray);
				}
				
			}
			
		}
		//
		////////////////////////
		
				
		////////////////////////
		//
		function getPHPStatus(){
			
			global $link;
			global $status; 
			global $usingDatabase;
				 
				/* CHECK DATABASE	 */
				if ($link != false){
			
					if(connectToDatabase($usingDatabase) ){				
						$dbStatus = "true";		
					}
					else{
						$dbStatus = "false";
					}	
			
				};

				$fileData = array(
					"dbName" => "$usingDatabase",
				    "type" => "$status",		   
				    "status" => "$dbStatus"
				);			
				echo json_encode($fileData);				
				
		}
		//
		////////////////////////
		
				

		////////////////////////
		//
		function queryNumberOfEntries($database, $table){

			global $link;

			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				$query = "SELECT * FROM $table";
				
				
				$db_selected = mysql_select_db($database);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){
                    
					$fileData = array(
						"table" => "$table",
						"db" => "$database",
						"rows" => mysql_num_rows($results)
					);			
					echo json_encode($fileData);				
				


				}
				else{
					returnFalse();
					exit;
				}	
	

			}
			else{
				returnFalse();
				exit;
			}	
			
		}		
		//
		////////////////////////
		
		////////////////////////
		//
		function queryDatabase2($database, $query){

			global $link;

			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				
				$db_selected = mysql_select_db($database);  // 
				if (mysql_query( $query, $link )){
					returnTrue();
					exit;
				}
				else{
					returnFalse();
					exit;
				}	
	

			}
			else{
				returnFalse();
				exit;
			}	
			
		}		
		//
		////////////////////////
		

				
		////////////////////////
		//
		function queryDatabase($database, $query){

			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				$db_selected = mysql_select_db($database);  // 
				
						// DO QUERY  
						$json = sql2json($query);
						// QUERY IS VALID
						if ($json != ''){
							echo $json;
						}				
						// QUERY IS INVALID		
						else{
							returnFalse();
							exit;
						};

			}
			else{
				returnTrue();
				exit;
			}	
			 
			
		}
		//
		////////////////////////	
		
		
		////////////////////////
		//
		function createBlankEntry($database, $table){
			

			if(connectToDatabase($database) ){
				
				global $link;
				
				// CREATE NEW ENTRY IN DATABASE
				$query = "INSERT INTO $table (`id`) VALUES (NULL);";
				$db_selected = mysql_select_db($database);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){

					$fileData = array(
						"id" => mysql_insert_id(),
					);			
					echo json_encode($fileData);		

		
				}
				else{
					returnFalse();
					exit;
				}	
	

			}
			else{
				returnFalse();
				exit;
			}	
		
			 
			
		}
		//
		////////////////////////				
		
		////////////////////////
		//
		function videoUpload($videoData, $nameOfFile, $fileLocation){
			
			//global $status; 			
			// read base64 image 
			
			/*			
			// image data	
			$fileName = $nameOfFile;
			$extension = ".mp4"; 						
			$directory = '../' . $fileLocation . '/';
			$relativeFileLocation = $directory . $fileName . $extension;
			
			
			// create directory if it does not exist
			$dirCheck = is_dir($directory);
			if(!$dirCheck){
				if (!mkdir($directory, 0777, true)) {
				    die('Failed to create folders...');
				}				
			}
			/*
			// place into location on server
			move_uploaded_file($_FILES['photo']['tmp_name'], 'uploads/'.$new_file_name);
			
			file_put_contents($relativeFileLocation, $fullsize);
			
			// get image
			$dimensions = getimagesize($relativeFileLocation);
			$size = filesize($relativeFileLocation);
			$absPath = realpath($relativeFileLocation);
			$pathing = returnUrl(); 
			
			// if server vs local
			if ($status == "remote"){ // is local
				$src =  "/" . $pathing["folder"] . "/" . $fileLocation . '/' . $fileName . $extension;
			}
			if ($status == "server"){ // server
				$src = "/" . $pathing["relative"] . $fileLocation . '/' . $fileName . $extension;
			}
			*/
			
			
			// return an array with image data
			$fileData = array(
				"return" => "return"
			);			
			echo json_encode($fileData);
	
		}
		//
		////////////////////////	
		
				
		////////////////////////
		//
		function basicUpload($base64, $nameOfFile, $fileLocation){
			
			global $status; 
			
			// read base64 image 
			list($type, $base64) = explode(';', $base64);
			list(, $base64)      = explode(',', $base64);
			$fullsize = base64_decode($base64);	
						
			// image data	
			$fileName = $nameOfFile;
			$extension = ".png"; 						
			$directory = '../' . $fileLocation . '/';
			$relativeFileLocation = $directory . $fileName . $extension;
			
			// create directory if it does not exist
			$dirCheck = is_dir($directory);
			if(!$dirCheck){
				if (!mkdir($directory, 0777, true)) {
				    die('Failed to create folders...');
				}				
			}
			
			// place into location on server
			file_put_contents($relativeFileLocation, $fullsize);
			
			// get image
			$dimensions = getimagesize($relativeFileLocation);
			$size = filesize($relativeFileLocation);
			$absPath = realpath($relativeFileLocation);
			$pathing = returnUrl(); 
			
			// if server vs local
			if ($status == "remote"){ // is local
				$src =  "/" . $pathing["folder"] . "/" . $fileLocation . '/' . $fileName . $extension;
			}
			if ($status == "server"){ // server
				$src = "/" . $pathing["relative"] . $fileLocation . '/' . $fileName . $extension;
			}
			
			
			// return an array with image data
			$fileData = array(
				"fileLocation" => $fileLocation,
			    "name" => $fileName,
			    "ext" => $extension,			   
			    "dimensions" => $dimensions,
			    "size" => $size,
			    "src" => $src, 
			    "absolutePath" => $absPath
			);			
			echo json_encode($fileData);
	
		}
		//
		////////////////////////	



		////////////////////////
		//	
		function listOfFiles($folder){
			
		    if (is_dir($folder) === true){
				$listOfFiles = array();
				$files = array_values(array_filter(scandir($folder), function($file) {
				    return !is_dir($file);
				}));
				foreach($files as $file){
				    $listOfFiles[] = $file;
				}
		        $status = true;
				// return an array with image data
				$fileData = array(
					"folder" => $folder, 
					"status" => $status, 
					"files" => $listOfFiles
				);			
				echo json_encode($fileData);				
				
		    }
		
		    else{
		    	returnFalse();
				exit;   
		    }	
			



		}	
		//
		////////////////////////	
		
		////////////////////////
		//	
		function swapPageNames($database, $oldname, $newname){
			
	
			global $link;
			

			if(connectToDatabase($database) ){
								
				// SELECT THE BLOG DATABASE
				$query = "update _pagedata set name = replace(name, '$oldname', '$newname') WHERE name='$oldname'";
			
				$db_selected = mysql_select_db($database);  // 
				$results = mysql_query( $query, $link ); 
				if ($results){
                    
					$fileData = array(
						"old" => "$oldname",
						"new" => "$newname"
					);			
					echo json_encode($fileData);				
				
				}
				else{
					returnFalse();
					exit;
				}					

	

			}
			else{
				returnFalse();
				exit;
			}	
		
		
		}	
		//
		////////////////////////			


		////////////////////////phpListOfFiles
		//	
		function deleteFiles($packet){
			
			$deleted = 0; $errors = 0;
			$return = array();
			foreach ($packet as $key) {
			    
			   $file = $key['file'];
			   $folder = $key['folder'];
				
					    if (is_dir($folder) === true){
					        if (unlink($folder . "/" . $file)){
					        	$deleted++;
								$return["deleted"] = $file;
					        }
							else{
								$errors ++;
								$return["error"] = $file;
							}			       
					    }
					
					    else{
					    	$return["notInDirectory"] = $file;
					    	$errors ++;      
					    }		
				
			}
					
			
			// return an array with image data
			$fileData = array(
				"errors" => $errors,
				"success" => $deleted,
				"complete" => $return
			);			
			echo json_encode($fileData);
		}	
		//
		////////////////////////	
		
		////////////////////////
		//	
		function renameFile($oldname, $newname){
			
			if (file_exists($oldname)) {
				rename($oldname, $newname);
				returnTrue();
			}
			else{
				returnFalse();
			}
		}	
		//
		////////////////////////	
		
		////////////////////////
		//	
		function copyFile($source, $newname){
			
			if (file_exists($source)) {
				copy($source,$newname);
				returnTrue();
			}
			else{
				returnFalse();
			}
		}	
		//
		////////////////////////				
		
		
		////////////////////////
		//	
		function deleteFolder($folder){
			
			$status = '';
			$numberOfFiles = 0;
			$fileLocation = '';
			
		    if (is_dir($folder)){
		        $files = array_diff(scandir($folder), array('.', '..'));
		
		        foreach ($files as $file){
		            $fileLocation = realpath($folder) . '/' . $file; 
					unlink($fileLocation);
		            $numberOfFiles++;
		        }
				rmdir ( $folder );
		
		        $status = "folder deleted";
		    }
		
		    else{
		    	$status = "no folder to delete";		      
		    }			
			
			// return an array with image data
			$fileData = array(
				"numberOfFiles" => $numberOfFiles, 
				"status" => $status, 
			    "folder" => $folder
			);			
			echo json_encode($fileData);
		}	
		//
		////////////////////////
		
		////////////////////////
		//			
		function renameFolder($oldFolder, $newFolder){

				// check if folder already exists
				if(is_dir( $newFolder )){
					$fileData = array(
					    "status" => "folder already exists",
					);			
					echo json_encode($fileData);	
				}
				// if it doesn't, rename it
				else{
					
					// folder does exists, rename it
					if (file_exists($oldFolder)) {
						rename($oldFolder, $newFolder);		
						$fileData = array(
						    "status" => "success",
						);			
						echo json_encode($fileData);							
					}
					// folder does not exist, create it
					else{
						mkdir($newFolder, 0700);							
						$fileData = array(
						    "status" => "created",
						);			
						echo json_encode($fileData);
					}

					
				}			

		}
		//
		////////////////////////
					
		
		////////////////////////
		//	
		function createFolder($newFolder){
					

			// check if it exists
			if( is_dir( $newFolder ) ) {
				
				$fileData = array(
				    "status" => "folder already exists"
				);			
				echo json_encode($fileData);					
		
			}
			// creates new folder
			else{
			    mkdir($newFolder);
				$fileData = array(
				    "status" => "created"
				);			
				echo json_encode($fileData);			    	
			};							
		

		}	
		//
		////////////////////////				

		////////////////////////
		//			
		function convertObjectToJson($object, $saveLocation){

			$json = json_decode($object);
			
			$fp = fopen($saveLocation, 'w');
			fwrite($fp, json_encode($json, JSON_PRETTY_PRINT));
			fclose($fp);				
			
			$fileData = array(
			    "status" => "good",
			    "saveLocation" => "$saveLocation",	    
			);				
			
			echo json_encode($fileData);			
	

		}
		//
		////////////////////////	
		
		////////////////////////
		//	
		function returnJsonAsObject($loadLocation){
		
				$json = @file_get_contents(dirname(__FILE__) . $loadLocation);
				$object = json_decode($json, true);
				echo json_encode($object);

		}		
		//
		////////////////////////			
		
		
		////////////////////////
		//			
		function generateSEOjson($title, $description, $meta, $author){

			$fileData = array(
			    "websiteTitle" => "$title",
			    "websiteDescription" => "$description",
			    "metatags" => "$meta",		
			    "author" => "$author"		    
			);			
			
			
			$fp = fopen('../production/settings/seo/seo.json', 'w');
			fwrite($fp, json_encode($fileData, JSON_PRETTY_PRINT));
			fclose($fp);		
			
			echo json_encode($fileData);			
				
			

		}
		//
		////////////////////////	
		
		////////////////////////
		//			
		function generateSetupjson($local, $remote, $user, $key, $db, $fb){

			$fileData = array(
			    "localname" => "$local",
			    "remotename" => "$remote",
			    "username" => "$user",		
			    "password" => "$key",	
			    "assignedDB" => "$db",
			    "firebase" => "$fb"		    
			);			
			
			
			$fp = fopen('../setup/json/setup.json', 'w');
			fwrite($fp, json_encode($fileData, JSON_PRETTY_PRINT));
			fclose($fp);		
			
			echo json_encode($fileData);			
				
			

		}
		//
		////////////////////////	

		////////////////////////
		//	
		function restoreSetupConfig(){
			
			$settingsArray = file_get_contents(dirname(__FILE__) . '/../setup/json/backup.json');
			$settings = json_decode($settingsArray, true); 

			$fileData = array(
			    "localname" => $settings["localname"],
			    "remotename" => $settings["remotename"],
			    "username" => $settings["username"],		
			    "password" => $settings["password"],	
			    "assignedDB" => $settings["assignedDB"],
			    "firebase" => $settings["firebase"]	    
			);	
			
			$fp = fopen('../setup/json/setup.json', 'w');
			fwrite($fp, json_encode($fileData, JSON_PRETTY_PRINT));
			fclose($fp);		
			
			echo json_encode($fileData);		
		}		
		//
		////////////////////////	
		
		////////////////////////
		//	
		function retrieveSetupConfig(){
			
			$settingsArray = file_get_contents(dirname(__FILE__) . '/../setup/json/setup.json');
			$settings = json_decode($settingsArray, true); 

			$fileData = array(
			    "localname" => $settings["localname"],
			    "remotename" => $settings["remotename"],
			    "username" => $settings["username"],		
			    "password" => $settings["password"],	
			    "assignedDB" => $settings["assignedDB"],
			    "firebase" => $settings["firebase"]	    
			);	
				
			echo json_encode($fileData);		
		}		
		//
		////////////////////////			
		
		
				
		////////////////////////
		//	
		function sendEmail($sendTo, $subject, $content, $replyTo){
			
			$message	= 	"<!DOCTYPE html><html><head></head><body>" . 
							"<div>" .
							$content . 
							"</div>" .
							"</body></html>";
			
			$headers = "From: " . strip_tags($replyTo) . "\r\n";
			$headers .= "Reply-To: ". strip_tags($replyTo) . "\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			
			@mail($sendTo,$subject,$message,$headers);

			$fileData = array(
			    "status" => "sent",
			    "sendTo" => $sendTo,
			    "subject" => $subject,			   
			    "replyTo" => $replyTo,
			);			
			echo json_encode($fileData);

			
		}
		//
		////////////////////////
	
	
		
		////////////////////////
		//	
		function retrieveFileContents($location){
		
			if (file_exists($location)) {
			$content = file_get_contents($location);
				$fileData = array(
				    "status" => "success",
				    "content" => $content
				);			
				echo json_encode($fileData);
			}
			else{
				returnFalse();
			}
		}
		//
		////////////////////////	
		

				
		////////////////////////
		//	
		function saveFileContents($location, $content){
				
			
			file_put_contents($location, $content);

			$fileData = array(
			    "status" => "success"
			);			
			echo json_encode($fileData);
		}
		//
		////////////////////////			
			

?>
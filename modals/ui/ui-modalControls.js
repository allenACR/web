define(['custom'], function(custom) {


	angular.module('uiModalCtrl', []).	
	factory('uiModalCtrl', function(){
		
		return {

			//////////////////////
			/* TEST CONTROLLER */
			ping: function(amount){								
				var test = "ponged: " + amount;
				return 	test;			
			},
			/* END MODAL CONTROLLER */
			//////////////////////
			
			//////////////////////
			/* MODAL CONTROLLER */
			confirmModalCtrl: ['items', '$scope', '$modalInstance', function(items, $scope, $modalInstance){		
				
				$scope.data = items; 
				
				$scope.yes = function () {				  
				    $modalInstance.close(true);				  	
				};
				  
				$scope.no = function () {
				    $modalInstance.close(false);
				};
				  
				$scope.cancel = function(){				  	
				  	$modalInstance.dismiss();
				};
				
				$scope.dismiss = function(){				  	
				  	;
				  	$modalInstance.dismiss();
				};					
			
			}],
			/* END MODAL CONTROLLER */	
			//////////////////////	
			
			//////////////////////
			/* MODAL CONTROLLER */
			pictureModalCtrl: ['items', '$scope', '$modalInstance', '$timeout', 
						function(items, $scope, $modalInstance, $timeout){		
				
				$scope.listOfImages = items.list;
				$scope.currentSlide = items.index;

				
				$scope.next = function(){
					
					if ($scope.currentSlide + 1 >= $scope.listOfImages.length){
						$scope.currentSlide = 0;
					}
					else{
						$scope.currentSlide ++; 
					}
				};
				
				$scope.prev = function(){
					
						
					if ($scope.currentSlide - 1 < 0){
						$scope.currentSlide = $scope.listOfImages.length - 1;					
					}
					else{
						$scope.currentSlide --; 
					}
				};				

				$scope.cancel = function(){				  	
				  	$modalInstance.dismiss();
				};
				
				$scope.dismiss = function(){				  	
				  	;
				  	$modalInstance.dismiss();
				};					
			
			}],
			/* END MODAL CONTROLLER */	
			//////////////////////							
			
			
			//////////////////////
			/* CHOICE CONTROLLER */
			choiceModalCtrl: ['items', '$scope', '$modalInstance', function(items, $scope, $modalInstance){	
										
			  $scope.data = items;
			  
			  $scope.selected = {
			    item: $scope.data[0]
			  };
			 
			  $scope.ok = function () {	
			    $modalInstance.close($scope.selected.item);
			  };
			
			  $scope.cancel = function () {
			    $modalInstance.dismiss('cancel');
			  };			
			}],
			/* END MODAL CONTROLLER */
			//////////////////////			
				
				
			//////////////////////
			/* SPLASH CONTROLLER */
			splashModalCtrl: function(){	

									
				var cntrl = ['$scope', '$modalInstance', function ($scope, $modalInstance) {
					
					var randomQuotes = [
						"Loading all the important stuff for you!",
						"Remember how important you are today.",
						"Keep it sexy, internet.",
					];
					
					$scope.randomQuote = randomQuotes[Math.floor((Math.random() * randomQuotes.length) + 0)];					
				}];	
				return cntrl;		
			},
			/* END MODAL CONTROLLER */	
			//////////////////////		
				
			//////////////////////
			/* THINKING CONTROLLER */
			thinkingModalCtrl: function(){								
				var cntrl = ['$scope', '$modalInstance', function ($scope, $modalInstance) {
	
					
				}];	
				return cntrl;		
			},
			/* END MODAL CONTROLLER */	
			//////////////////////			
				


			
			
			
			/* MODAL CONTROLLER */
			browserInfoCtrl: function(){	
				var cntrl = ['$scope', '$modalInstance', '$detection',  'browserInfo', function ($scope, $modalInstance, $detection,  browserInfo) {

					///////////////////////	 DETECTION   
				  	if ($detection.isAndroid()){
				  		$scope.detected = "production/images/detection/androidIcon.jpg";
				  		$scope.detectedType = "Android OS";
				  	}
				  	else if($detection.isiOS()){
				  		$scope.detected = "production/images/detection/iosIcon.jpg";
				  		$scope.detectedType = "IOS";
				  	}
				  	else if($detection.isWindowsPhone()){
				  		$scope.detected = "production/images/detection/windowsIcon.jpg";
				  		$scope.detectedType = "Windows 8 (Mobile)";
				  	}
				  	else{
				  		$scope.detected = "production/images/detection/desktopIcon.jpg";
				  		$scope.detectedType = "Desktop";
				  	}
					///////////////////////	   						
					
					$scope.details = browserInfo.giveMeAllYouGot(); 
					  
					$scope.dismiss = function(){				  	
					  	;
					  	$modalInstance.dismiss();
					};
				}];	
		
				return cntrl;		
			},				
			/* END MODAL CONTROLLER */										
						
			
		};
		

	
	});

});
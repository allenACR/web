define(['custom'], function(custom) {

	angular.module('editorModalCtrl', []).	
	factory('editorModalCtrl', function(){
		
		return {

			//////////////////////
			/* TEST CONTROLLER */
			ping: function(amount){								
				var test = "ponged: " + amount;
				return 	test;			
			},
			/* END MODAL CONTROLLER */
			//////////////////////
			
			//////////////////////
			/* MODAL CONTROLLER */
			scriptMediaCtrl: ['items', '$scope', '$timeout', '$modalInstance', 'TransactionManager', 'toaster', 'lrFileReader',
							function(items, $scope, $timeout, $modalInstance, TransactionManager, toaster, lrFileReader){		
	
				// variables
				$scope.tabs = [
					{title: "Images"},
					{title: "Video"},
					{title: "Files"},									
				];		
				
				$scope.select = {
					images: null,
					videos: null,
					files: null
				};
				$scope.edit  ={
					images: false,
					videos: false,
					files: false
				};
				
				$scope.galleryType = "thumbnail";
				$scope.canDelete = false;
				$scope.canDownload = false;
				
				$scope.section = {
					images: "gallery"
				};
				
				$scope.loading = {
					initCheck: true
				};
				
				$scope.fileData = [];
				
				//--------- 	
				$scope.init = function(){
					
					$scope.checkForMediaFolder(function(){
						$scope.checkForImageFolder(function(){
							$scope.checkForVideoFolder(function(){
								$scope.checkForFileFolder(function(){
									$scope.initComplete();
								});
							});
						});
					
					});
									
				};
				//--------- 
				
				//--------- 	
				$scope.initComplete = function(){
					$timeout(function(){
						$scope.loading.initCheck = false;
					});
				};
				//--------- 				
				
				//---------
				$scope.checkForMediaFolder = function(callback){
					// check if manager folder exists - if not, create it
					phpJS.listOfFiles("../media/manager/", function(state, data){ 
					 	if (data.status == "error"){							
							phpJS.createFolder({newFolder: "../media/manager/"}, function(state, data){										
								callback();
							});		
					 	}else{	
					 			callback();					 	
					 	};
					});	
				};
				//---------
				
				//--------- 
				$scope.checkForImageFolder = function(callback){
					phpJS.listOfFiles("../media/manager/images", function(state, data){ 
					 	if (data.status == "error"){							
							phpJS.createFolder({newFolder: "../media/manager/images"}, function(state, data){		
								callback();
							});		
					 	}else{	
					 		$scope.listOfImageFolders = data.files;
					 		$scope.listOfImageFolders.push("Create New Folder...");
					 		callback();
					 	};
					});	
				};
				//--------- 
				
				//--------- 
				$scope.checkForVideoFolder = function(callback){
					phpJS.listOfFiles("../media/manager/videos", function(state, data){ 
					 	if (data.status == "error"){							
							phpJS.createFolder({newFolder: "../media/manager/videos"}, function(state, data){		
								callback();
							});		
					 	}else{	
					 		$scope.listOfVideoFolders = data.files; 
					 		callback();
					 	};
					});	
				};
				//--------- 
				
				//--------- 
				$scope.checkForFileFolder = function(callback){
					phpJS.listOfFiles("../media/manager/files", function(state, data){ 
					 	if (data.status == "error"){							
							phpJS.createFolder({newFolder: "../media/manager/files"}, function(state, data){		
								callback();
							});		
					 	}else{	
					 		$scope.listOfFileFolders = data.files; 
					 		callback();
					 	};
					});	
				};
				//--------- 
				
				//--------- 
				$scope.loadImagesInFolder = function(folderName){
					
					if(folderName == "Create New Folder..."){
						$scope.createFolder();
					}
					else{
						if (folderName != null || folderName != undefined){
							$scope.listOfThumbnails = [];
							$scope.listOfFullImages = []; 
							phpJS.listOfFiles("../media/manager/images/" + folderName + "/", function(state, data){ 
								$timeout(function(){
									var img = custom.parseManagerImages(data, folderName); 
									$scope.listOfThumbnails = img.thumbnails;
									$scope.listOfFullImages = img.full;
							 					
								});
							});
						}
					};
				};	
				
				$scope.loadFirstImages = function(folder){
					if ($scope.listOfImageFolders.length > 1){
						$scope.select.images = $scope.listOfImageFolders[0]; 
						$scope.loadImagesInFolder($scope.listOfImageFolders[0]);
					}
				};						
				//--------- 
				
				//---------
				$scope.createFolder = function(newFolder){
							var folderName = prompt("Folder name: ", "New Folder").toLowerCase();					
							if (folderName != null) {
								var packet = {newFolder: "../media/manager/images/" + folderName + "/"},
									validName = true;
								
								for (i = 0; i < $scope.listOfImageFolders.length; i++){	
									if(folderName == $scope.listOfImageFolders[i]){
										validName = false;
									}
								}
								
								if(validName){
									phpJS.createFolder(packet, function(state, data){ 										
											$scope.checkForImageFolder(function(){
												$timeout(function(){						
													toaster.pop('success', "Folder name updated...", "");
													$scope.select.images = folderName; 
													$scope.loadImagesInFolder(folderName);													
													
												});
											});
										
									});
								}else{
									alert("Folder name is already taken.");
								}
							};					
				};
				
				
				$scope.deleteFolder = function(type, newName){
					
					if (confirm("Deleting this folder will also delete everything in it.  Are you sure you want to continue?")) {
						if (confirm("Are you sure you want to DELETE ALL THE FILES in this folder?")) {
							phpJS.deleteFolder("../media/manager/" + type + "/" + $scope.select.images, function(state, data){
								$scope.checkForImageFolder(function(){
									$timeout(function(){					
										toaster.pop('success', "Folder deleted!", "");	
										if($scope.listOfImageFolders.length > 1){	
											$scope.select.images = $scope.listOfImageFolders[0]; 
											$scope.loadImagesInFolder($scope.listOfImageFolders[0]);
										}
									});	
								});								
							});					
						};
					}	
				};
				
				$scope.renameFolder = function(type, newName){
					
						var validName = true;					
						for (i = 0; i < $scope.listOfImageFolders.length; i++){
							
							if(newName == $scope.listOfImageFolders[i]){
								validName = false;
							}
						}					
					
						if(validName){
							var packet = {
								oldFolder: "../media/manager/" + type + "/" + $scope.select.images, 
								newFolder: "../media/manager/" + type + "/" + newName + "/"
							};					
	
							phpJS.renameFolder(packet, function(state, data){
									
									if (data.status == "success"){	
										$scope.checkForImageFolder(function(){
											$timeout(function(){		
												$scope.edit.images = !$scope.edit.images;							
												toaster.pop('success', "Folder name updated...", "");
												$scope.select.images = newName; 
												$scope.loadImagesInFolder(newName);
											});		
										});
									}
									else{
										
									}
							});
						}
						else{
							
							alert("Folder name is already taken.");
						}
					
				};
				
				$scope.deleteImage = function(index){
					
					var location = "../media/manager/images/" + $scope.select.images + "/"; 
					var packet = []; 
					
					// DELETE THUMBNAIL
					packet.push({folder: location, file: $scope.listOfThumbnails[index].name});											
					phpJS.deleteFiles(packet, function(state, data){ 
						$scope.listOfThumbnails[index].active = false;	
					});					
					
					// DELETE FULL IMAGE
					packet.push({folder: location, file: $scope.listOfFullImages[index].name});						
					phpJS.deleteFiles(packet, function(state, data){
						$timeout(function(){
							$scope.listOfFullImages[index].active = false;	
							toaster.pop('success', "Image deleted.", "");	
						});
					});							
					
					
				};
				//---------
				

				
	    		//---------
				$scope.previewImage = function($files, $index) {
							
						    var imageCount = 0; 
						    var totalImages = $files.length;
	    					

						    function processLoop(){		
						    	 	
						        var reader = new FileReader();						
						        reader.onload = function (image) {		
						 			   	
						 			  
										// will resize and return thumbnail and original size data
										custom.resizeImageToDataUri(image, 200, function(data){
											
											var fullSize  = data.original; 
											var thumbnail = data.thumbnail; 
											var fileName = custom.getRandomHash(10); 
											
										
											// populate arrays for preview and upload	
								            $scope.fileData.push( {name: fileName, src: fullSize, thumbnail: thumbnail, parse: true}	);
								            
								            
								            // upload
								            $scope.imgPreview = thumbnail;
							               
							                // loop and apply
								            imageCount++;
								            if (imageCount < totalImages){
								            	processLoop();
								            }
								            else{
								            	$timeout(function(){
								            		processLoopComplete();
								            	});
								            }
								           
								           
										});
										
								   			            
						        };
						       

						        //validate image type
						        var fileExt = ($files[imageCount].name).split('.').pop();
								if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif'){						       					
						        	reader.readAsDataURL($files[imageCount]);
						      	};
						      	
						    };
						    
						    function processLoopComplete(){	
						    	console.log();
						    	$scope.$apply(); 
						    }
						    
						    // start loop
						    processLoop();
						    
						  
				};				
				//---------
				
				//---------
				$scope.uploadMultiImages = function(){
					
					 
					count = 0;  
					
					 
					function upload(){
						
						randomName = custom.getRandomHash(10);
			        	//upload thumbnails first
						phpJS.imageUploadBase64($scope.fileData[count].thumbnail, randomName + "_thumbnail", "./media/manager/images/" + $scope.select.images + "/", function(state, data){						
							// upload full image
							phpJS.imageUploadBase64($scope.fileData[count].src, randomName, "./media/manager/images/" + $scope.select.images + "/", function(state, data){
									if (count < $scope.fileData.length - 1){										
										count++;
										upload();
									}
									else{
										loopComplete();
									}
							});			
						});		
					};	
					
					function loopComplete(){
						$timeout(function(){
							toaster.pop('success', "Images uploaded!", "");
							$scope.section.images = "gallery";
							$scope.fileData = [];
							$scope.loadImagesInFolder($scope.select.images);	
						});			
						
					}
					upload();			 
					 
				};
				//---------
				
				//---------
				$scope.resetSingleImage = function(){
					$scope.singlePreview = {
						thumbnail: null,
						tnW: null,
						tnH: null,
						full: null,
						fW: null,
						fH: null
					};
				};
				
		        $scope.image_handleFileSelect = function(evt) {
		      
					var file = evt.files[0];	
					$scope.resetSingleImage();
						    	
				    lrFileReader(file)
				        .on('progress',function(event){
				            $scope.progress = event.loaded/event.total;
				        })
				        .on('load',function(event){
				        })
				        .readAsDataURL()
				        .then(function(raw){
				        	
				        	var image = new Image();
					        image.onload = function(evt) {
					            var width = this.width,
					            	height = this.height;
					            	
					            	// get thumbnail 
						        	custom.imageToDataUri(raw, 200,  function(imgData, dimensions){		
						        		$timeout(function(){
						        			$scope.singlePreview.thumbnail = imgData;
						        			$scope.singlePreview.tnW = dimensions.width;
						        			$scope.singlePreview.tnH = dimensions.height;
						        		});
						        									
						        	});		
						        	
					            	// get full 
						        	custom.imageToDataUri(raw, width,  function(imgData, dimensions){	
						        		$timeout(function(){
						        			$scope.singlePreview.full = imgData;
						        			$scope.singlePreview.fW = dimensions.width;
						        			$scope.singlePreview.fH = dimensions.height;
						        		}); 
						        		
						        						
						        	});								        					            
					            
					        };
					        image.src = raw;			        	
				        	
				        });		

		        };	
		        //---------	
		        
		        //---------	
		        $scope.uploadSingleImage = function(){
		        	randomName = custom.getRandomHash(10);
		        	//upload thumbnails first
					phpJS.imageUploadBase64($scope.singlePreview.thumbnail, randomName + "_thumbnail", "./media/manager/images/" + $scope.select.images + "/", function(state, data){						
						// upload full image
						phpJS.imageUploadBase64($scope.singlePreview.full, randomName, "./media/manager/images/" + $scope.select.images + "/", function(state, data){
							$timeout(function(){
								toaster.pop('success', "Image uploaded!", "");
								$scope.section.images = "gallery";
								$scope.resetSingleImage();
								$scope.loadImagesInFolder($scope.select.images);			
							});
						});			
					});	

		        };			
				//---------	

				//--------- change tab
				$scope.changeFocus = function(to){	
				
					$scope.focusOn = to.toLowerCase();
				};
				$scope.changeFocus($scope.tabs[0].title);
				//--------- 		

				$scope.cancel = function(){				  	
				  	$modalInstance.dismiss();
				};
				
				$scope.dismiss = function(){				  	
				  	;
				  	$modalInstance.dismiss();
				};	
				//--------- end button behavior	
			
			}],
			/* END MODAL CONTROLLER */	
			//////////////////////		
			
			//////////////////////
			/* MODAL CONTROLLER */
			scriptCreatorCntrl: ['items', '$scope', '$timeout', '$modalInstance', 'TransactionManager', 'toaster',
							function(items, $scope, $timeout, $modalInstance, TransactionManager, toaster){		
	
				// variables
				$scope.tabs = [
					{title: "Create"},
					{title: "Edit"}										
				];		
			 	$scope.tinymceOptions = {
			        theme: "modern",
			        plugins : "paste, autoresize",
			        theme_advanced_buttons3_add : "pastetext,pasteword,selectall",
			        paste_auto_cleanup_on_paste : true,
			        paste_preprocess : function(pl, o) {
			            // Content string containing the HTML from the clipboard
			          	//o.content = "-: CLEANED :-\n" + o.content;
			        },
			        paste_postprocess : function(pl, o) {
			            // Content DOM node containing the DOM structure of the clipboard
			            //o.node.innerHTML = o.node.innerHTML + "\n-: CLEANED :-";
			        },
			    };	
				$scope.listOfTemplates = [];
				$scope.collections = [];			    
			    $scope.isLoading = true;				
				
				$scope.resetVariables = function(){			 
					$scope.editScript = {
						code: "", 
						preview: "",
						mce: ""
					};
					$scope.select = {
						collection: null,
						template: null,
						currentRoot: null,
						hasFolders: true,
					};
					$scope.load = {
						templateSelect: true,				
					};				
					$scope.show = {
						// create scripts
						createType: true,
						createMode: false,
						isType: null,
						afterCreate: false,
						
						// edit scripts
						preview: true,												
						templateSelect: false,
						loadBtn: false,
						editScript: false,
						editor: false,
						
					};	
					$scope.active = {
						editor: true,
						rename: false,
						merge: true
					};
					$scope.currentFile = null;					
					$scope.isSaving = false;
					
					$scope.media = {
						using: null,
						folder: null
					}
					$scope.listOfSubfolders = null;
					$scope.listOfThumbnails = [];
					$scope.listOfFullImages = []; 
				};
				$scope.createTitle = "Create new template...";
				$scope.resetVariables();
			    //---------

				//---------
				$scope.init = function(){
										
					$scope.setFocusOn("collection");
					
				};
				//---------
				
				//---------
				$scope.setFocusOn = function(type){
					$timeout(function(){
						$scope.load.templateSelect = true;	
						$scope.listOfTemplates = [];					
						$scope.show.isType = type;	
						$scope.newTemplateName = "new_" + type;
					});
					
					var location = "../templates/" + type + "/";

					phpJS.listOfFiles(location, function(state, data){ 
						$timeout(function(){
							
							if (data.status != "error"){
								$scope.select.hasFolders = true;
								$scope.collections = data.files;
								$scope.isLoading = false; 	
								$scope.select.collection = $scope.collections[0]; //;
								$scope.select.currentRoot = location;
								$scope.updateTemplates($scope.select.collection);
							}
							else{
								var packet = {newFolder: location};
								phpJS.createFolder(packet, function(state, data){
									$scope.setFocusOn($scope.show.isType);										
								});										
							}
							
						});	
					});
	
				};
				//---------
				
				
				//---------
				$scope.renameTemplate = function(name){
					var isMatch = false;
					var writefile = true;
					for (i = 0; i < $scope.listOfTemplates.length; i++){
						if ($scope.listOfTemplates[i].toLowerCase() == name.toLowerCase()){
							isMatch = true;
						};
					}
					if(isMatch){
						var r = confirm("That filename already exists.  Do you want to overwrite it?");
						if (!r) {
						    writefile = false;
						}
					}
					if(writefile){
						
						var packet = {oldName: $scope.currentFile, newName:  $scope.select.currentRoot  + $scope.currentTheme + "/" + name + ".html"};
						phpJS.renameFile(packet, function(state, data){
							$timeout(function(){
								$scope.currentFile = packet.newName;
								$scope.currentTemplate = name; 
								$scope.active.rename = false;
								if (data.status == "good"){
									toaster.pop('success', "Script renamed.", "");
								}
								else{
									toaster.pop('alert', "Could not rename file.", "");
								};
							});
						});
					}
				};
				//---------
				
				
				//---------
				$scope.loadMediaFolders = function(type, callback){
					$scope.listOfSubfolders = [];

					phpJS.listOfFiles("../media/manager/" + type, function(state, data){ 
					 	if (data.status == "error"){}
					 	else{	
					 		$timeout(function(){
					 			$scope.listOfSubfolders = data.files;
					 			if(callback != null){callback();};
					 		})
					 	};
					});	
		
					
				};
				//---------
				
				//---------
				$scope.loadMediaFiles = function(folderName, callback){
					$scope.listOfThumbnails = [];
					$scope.listOfFullImages = [];
					$scope.mediaVariations = [];
					$scope.variationSelection = {selection: null};
					var root = "../media/manager/" + $scope.media.using + "/" + folderName; 
					phpJS.listOfFiles(root, function(state, data){ 
					 	if (data.status == "error"){}
					 	else{	
					 		
					 		$timeout(function(){
								var img = custom.parseManagerImages(data, folderName); 
								$scope.listOfThumbnails = img.thumbnails;
								$scope.listOfFullImages = img.full;
						 		$scope.mediaVariations = [
						 			{folder: folderName, name: "Slider Gallery", version: "slider_gallery"},
						 			{folder: folderName, name: "Fade Gallery", version: "fade_gallery"},
						 		]
						 		
					 		})
					 	};
					 	
					});	

				};
				//---------
				
				//---------
				$scope.updateTemplates = function(templateName, callback){		
					$timeout(function(){
						$scope.load.templateSelect = true;
					});
					
					phpJS.listOfFiles_noExtensions($scope.select.currentRoot + templateName + "/", function(state, data){	
							
						if (data.status != "error"){
							$scope.listOfTemplates = data.files;				
						}
						
						$timeout(function(){
							$scope.load.templateSelect = false;
							$scope.show.templateSelect = true;	
							$scope.collectionSelected = templateName;						
							$scope.listOfTemplates.push($scope.createTitle);					
							$scope.select.template = $scope.listOfTemplates[0];										
						});
						
						
						if (callback != undefined){
							callback();
						}						
						
					});
				};
				//---------
				
				//---------
				$scope.loadTemplate = function(c, t){

						$scope.show.editor = false;
						$scope.currentFile = $scope.select.currentRoot + c + "/" + t + ".html";
						$scope.currentTemplate = t;
						$scope.currentTheme = c; 
						var packet = {location: $scope.currentFile};					
						phpJS.retrieveFileContents(packet, function(state, data){
														
								if (data.status != "error"){
									$timeout(function(){
										$scope.show.editor = true;
										$scope.load.templateLoaded = true;
										$scope.editScript.code = data.content;
										$scope.editScript.preview = data.content;
										$scope.editScript.mce = data.content;
										TransactionManager.snapshot($scope.editScript);									
									}); 
								}
								else{
									$timeout(function(){
										$scope.load.templateLoaded = false;
									});	
								}
						});
						
						var packet = {database: items.system.systemdata.setup.db, table: $scope.select.collection};
						phpJS.getTableFields(packet, function(state, data){ 
							$timeout(function(){	
								$scope.componentList = custom.returnPropertyInObject(data, ["Field", "Type", "Comment"]);
							});
						});						
						
				
				
				};
				//---------
				
				//---------
				$scope.pasteMergeToken = function(){
					$timeout(function(){
						$scope.editScript.code += "\n\n";
						$scope.editScript.code += $scope.useTemplate; 						
						toaster.pop('success', "Added to script", "");
					});
				};
				
				$scope.updateVariations = function(index){
					var comment = $scope.componentList.Comment[index],
						inputType = null;
						$scope.variationList = [];
					
		        	if (comment.indexOf("inputType") > -1){ 
		        		    var matches = comment.match(/inputType=\"(.*?)\"/);    												
		        			inputType = matches[1];							        	
		        	}						
					
					$timeout(function(){
						switch(inputType) {
						    case 'imageUpload':
						    	$scope.variationList = [
						    		{version: 1, type: "Basic Display"},
						    		{version: 2, type: "Photo Gallery (fade)"},
						    		{version: 3, type: "Photo Gallery (slideshow)"}
						    	];
						        break;	
						    case 'date':
						    	$scope.variationList = [
						    		{version: 1, type: "YYYY-MM-DD"},
						    		{version: 2, type: "Day of Week, Month DD YYYY"},
						    		{version: 3, type: "Month DD, YYYY"},
						    		{version: 4, type: "Relative to now"},
						    	];
						        break;	
						    case 'time':
						    	$scope.variationList = [
						    		{version: 1, type: "HH:mm:ss (AM/PM)"},
						    		{version: 2, type: "HH:mm:ss (24 Hour)"},
						    		{version: 3, type: "HH:mm (AM/PM)"},
						    		{version: 4, type: "HH:mm (24 Hour)"}
						    	];
						        break;							        						        
						    default:
						        $scope.variationList = [
						    		{version: 1, type: "Default"}
						    	];
						 }	
						 
						 // set defaults
						 $scope.useVersion = $scope.variationList[0].version; 
						 $scope.getComponentToken(index, $scope.variationList[0].version);
					 });
					
				};
				
				$scope.getMediaToken = function(obj, type){
					
					
					switch(type) {
						case 'images':
							if(obj.version == "slider_gallery"){
								var randomHash = custom.getRandomHash(5),
									id = obj.version + "_" + randomHash;
								
								$scope.useTemplate = "<div id='" + id +  "'  class='fade-slider slider-inactive' ng-init=\"tokenInits.gallery('" + obj.folder + "', '" + id + "')\"></div>";	
							}
						break;
					}
					
					
					$timeout(function(){$scope.useTemplate;});
				};
				
				$scope.getComponentToken = function(index, version){
					
					var comment = $scope.componentList.Comment[index],
						field = $scope.componentList.Field[index], 
						type = $scope.componentList.Type[index].replace(/[, 0-9()]/g,'').toLowerCase();
						inputType = null;
						
		        	if (comment.indexOf("inputType") > -1){ 
		        		    var matches = comment.match(/inputType=\"(.*?)\"/);    												
		        			inputType = matches[1];							        	
		        	}	
		        	
		        	
		        	
					switch(inputType) {
					    case 'input':
					    	if(version == 1){
					        	$scope.useTemplate = "{{entry." + field + "}}";
					        }
					        break;
					    case 'adv':
					    	if(version == 1){
					        	$scope.useTemplate = "<div ng-bind-html=\"entry." + field + "| trusthtml\"></div>";
					        }
					        break;
					    case 'imageUpload':
					    	// regular display
					    	if(version == 1){
								 $scope.useTemplate =  "<div class='small-3 columns left' ng-repeat=\"image in entry." + field + "\"> \n";
								 $scope.useTemplate += "  <img ng-src=\"{{image.thumbnail}}\" class=\"img-responsive\" ng-click=\"mainCore.pictureBox(entry." + field + ", $index)\"/> \n";
					 			 $scope.useTemplate += "  <p><small><em>{{image.caption | characters:15}}</em></small></p> \n";						
							 	 $scope.useTemplate += "</div>	";		
							}		
							// fade slider
							if(version == 2){
								 $scope.useTemplate =  "<div class='small-10 small-offset-1 fade-slider slider-inactive'> \n";
								 $scope.useTemplate += "  <div ng-repeat=\"image in entry." + field + "\">  \n";
					 			 $scope.useTemplate += "  <img ng-src=\"{{image.src}}\" class='img-responsive'>  \n";	
					 			 $scope.useTemplate += "  <p><small><em>{{image.caption | characters:15}}</em></small></p> \n";	
					 			 $scope.useTemplate += "  </div> \n"; 			
							 	 $scope.useTemplate += "</div>	";		
							}	
							// slideshow slider
							if(version == 3){
								 $scope.useTemplate =  "<div class='small-10 small-offset-1 slideshow-slider slider-inactive'> \n";
								 $scope.useTemplate += "  <div ng-repeat=\"image in entry." + field + "\">  \n";
					 			 $scope.useTemplate += "  <img ng-src=\"{{image.src}}\" class='img-responsive' >  \n";	
					 			 $scope.useTemplate += "  <p><small><em>{{image.caption | characters:15}}</em></small></p> \n";	
					 			 $scope.useTemplate += "  </div> \n"; 			
							 	 $scope.useTemplate += "</div>	";		
							}								
							
					        break;		
					    case 'date':
					    	if(version == 1){
					        	$scope.useTemplate = "{{entry." + field + "}}";
					        }
					    	if(version == 2){
					        	$scope.useTemplate = "{{entry." +field + " | amDateFormat:'dddd, MMMM Do YYYY'}}";
					        }
					        if(version == 3){
					        	$scope.useTemplate = "{{entry." +field + " | amDateFormat:'MMMM Do, YYYY'}}";
					        }	
					        if(version == 4){
					        	$scope.useTemplate = "<span am-time-ago='entry." + field + "'></span>";
					        }						        						        
					        break;	
					    case 'time':
					    	if(version == 1){
					        	$scope.useTemplate = "{{entry." + field + " | timeOnly | amDateFormat:'h:mm:ss a'}}";
					        }
					    	if(version == 2){
					        	$scope.useTemplate = "{{entry." + field + " | timeOnly | amDateFormat:'HH:mm:ss' }}";
					        }		
					    	if(version == 3){
					        	$scope.useTemplate = "{{entry." + field + " | timeOnly | amDateFormat:'h:mm a'}}";
					        }
					    	if(version == 4){
					        	$scope.useTemplate = "{{entry." + field + " | timeOnly | amDateFormat:'HH:mm' }}";
					        }							        			        						        
					        break;						        				        			        
					    default:
					        $scope.useTemplate = "{{entry." + field + "}}";
					}		        	
		        	
		        	$timeout(function(){$scope.useTemplate;});
					
				};
				//---------



	
	  
	

				//---------
				$scope.swapFormats = function(raw){
						if (raw){
							$scope.editScript.mce = $scope.editScript.code;
						}
						else{				
							$scope.editScript.code = $scope.editScript.mce;
						}
				};
				//---------
				
				//---------
				$scope.revertChanges = function(){
					var r = confirm("Discard your changes?");
					if (r) {
					   $scope.show.editor = false;
					}								
				};
				//---------
				
				
				//---------
				$scope.deleteSubFolder = function(){
					if ( confirm("Delete this folder and all files in it?") ) {
						if ( confirm("This action will delete ALL FILES IN THIS FOLDER.  Are you sure you want to continue?") ) {	
							phpJS.deleteFolder($scope.select.currentRoot + $scope.select.collection, function(state, data){
								$timeout(function(){
									toaster.pop('success', "Folder Created!", "");
									$scope.setFocusOn($scope.show.isType);
								});									
							});									
												
						};
					};
				};
				
				$scope.newSubFolder = function(){
					var folderName = prompt("Folder name: ", "New Folder");
					
					if (folderName != null) {
						var packet = {newFolder: $scope.select.currentRoot  + folderName};
						
						phpJS.createFolder(packet, function(state, data){ 
							$timeout(function(){
								toaster.pop('success', "Folder Created!", "");
								$scope.setFocusOn($scope.show.isType);
							});
						});
					};
										
					
					
				};
				//---------
				
				//---------
				$scope.createTemplate = function(){		
					
					var templateName = prompt("File name: ", "new_script");
					if (templateName != null) {
								
						var full = (templateName + ".html").toLowerCase();
						var file = $scope.select.currentRoot + $scope.select.collection + "/" + full;
						var blankTemplate = "<p>Edit this script</p>";
						var packet = {location: file, content: blankTemplate};					
						var alreadyExists = false;
						$scope.currentFile = full;
						
						
						
						for (i = 0; i < $scope.listOfTemplates.length; i++){
							name = $scope.listOfTemplates[i].toLowerCase();
							
							if (templateName == name){
								alreadyExists = true;
							}
						};			
	
						function createFile(){
							$scope.isSaving = true;
							phpJS.saveFileContents(packet, function(state, data){
								$scope.updateTemplates($scope.select.collection, function(){
									$timeout(function(){
										$scope.isSaving = false;
										$scope.show.afterCreate = true;	
										toaster.pop('success', "Script Created!", "File is ready to edit.");								
									});
								});
							});
						};
	
						if (alreadyExists){
							if ( confirm("This file already exists.  Do you wish to override?") ) {
								createFile();
							};
						}
						else{
							createFile();
						};
					}
						
				};
				//---------
				
				$scope.removeExt = function(str){
					return str.replace(/\.[^/.]+$/, "");
				};
				
				//---------
				$scope.editFromNew = function(){
					$timeout(function(){						
						$scope.changeFocus("edit");						
						$scope.show.editor = true;
					});
				};
				//---------
				
				
				//--------- save changes
				$scope.saveTemplate = function(closeIt){
					
					$scope.isSaving = true;
					var packet = {location: $scope.currentFile, content: $scope.editScript.code};
					phpJS.saveFileContents(packet, function(state, data){
						$timeout(function(){
							$scope.isSaving = false;
							toaster.pop('success', "Script has been saved!", "");
							if (closeIt){
								$scope.show.editor = false;
							}
						});
					});
				};
				//---------
				
				//---------
				$scope.deleteTemplate = function(name){
					$scope.load.templateSelect = true; 
					if ($scope.select.template != $scope.createTitle){
						if (confirm("Delete this template?")) {
							var packet = [];
							packet.push({folder: $scope.select.currentRoot  + $scope.select.collection + "/", file: name + ".html"});
	
							phpJS.deleteFiles(packet, function(state, data){
								$scope.updateTemplates($scope.select.collection, function(){
									$timeout(function(){
										toaster.pop('success', "Template Deleted", "Refreshing in a moment...");
										$scope.show.editor = false;
										$scope.load.templateSelect = false; 
									});
								}); 

							});
							
						}
					}


				};
				//---------

				//--------- change tab
				$scope.changeFocus = function(to){	
					if ($scope.editScript.code != ""){


						if ( confirm("Discard your changes?") ) {
						   $scope.resetVariables();						
						}							
						
											
					}					
					$scope.focusOn = to.toLowerCase();
				};
				$scope.changeFocus($scope.tabs[0].title);
				//--------- 		

				$scope.cancel = function(){				  	
				  	$modalInstance.dismiss();
				};
				
				$scope.dismiss = function(){				  	
				  	;
				  	$modalInstance.dismiss();
				};	
				//--------- end button behavior	
			
			}],
			/* END MODAL CONTROLLER */	
			//////////////////////		
			
	
			//////////////////////
			/* MODAL CONTROLLER */
			pageSettingsModalCtrl: ['items', '$scope', '$timeout', '$modalInstance', 'lrFileReader',
							function(items, $scope, $timeout, $modalInstance, lrFileReader){		
				
				
				$scope.data = items;	
					
				$scope.listOfLayouts = []; 							
				$scope.currentTables = {desktop: '', mobile: ''};		
				$scope.getComplete = false; 
				$scope.buildFor = {};
				$scope.tabs = [
					{title: "Info"},
					{title: "Presentation"},
					{title: "Layout"},
					{title: "Animation"},
					{title: "Visuals"},
					{title: "Other"},											
				];
				$scope.animations = [
					{title: "Slide Left"},
					{title: "Slide Right"},
					{title: "Fade Left"},
					{title: "Fade Right"},
					{title: "Fade Up"},
					{title: "Fade Down"},				
					{title: "Zoom In"},
					{title: "Shrink Out"},
					{title: "Skew Left"},
					{title: "Skew Right"},					
				];		
				$scope.loading = {
					templateFiles: false
				};
				$scope.preheaderTemp = [];
				$scope.postheaderTemp = [];
				$scope.prescriptsTemp = [];
				$scope.postscriptsTemp = [];
				$scope.prefooterTemp = [];
				$scope.postfooterTemp = [];
				$scope.customOptions = {
					columns: 3,
					size: 40,
					roundCorners: true
				};	
					
				
					
					
				//---------					
				$scope.init = function(){


					var location = "../templates/general/";
					phpJS.listOfFiles(location, function(state, data){ 
						if (data.files.length > 0){
							$scope.templateFolders = data; 
							console.log(data);
						}
						else{
							var empty = {files: ["None Available"]};
							$scope.templateFolders = empty;
						}
					});

					phpJS.returnListOfTables($scope.data.database, function(state, data){
						$scope.data.collection = data; 	
						$scope.formData = $.extend(true, {}, $scope.data );						
						$scope.data.collection.user.unshift("none");

						phpJS.getPageData($scope.data.pagename, function(data){	
							$timeout(function(){
								$scope.currentTables.desktop = data.desktopTable;
								$scope.currentTables.mobile  = data.mobileTable;	
								$scope.getComplete = true;		
								
								if ($scope.formData.settings[$scope.data.displayAs].currentTable == false){
									$scope.formData.settings[$scope.data.displayAs].currentTable = "none";
								}																							
											
							});
						});		
					
						if ($scope.data.settings[$scope.data.displayAs].currentTable != false){
							$scope.updateLayouts($scope.data.settings[$scope.data.displayAs].currentTable);
						}
						else{
							$scope.listOfLayouts.push("----");
						}
										
							
					});				
				};
				//---------
				
				//---------
				$scope.getTemplateFiles = function(type, folderName, index){
					$timeout(function(){
						$scope.loading.templateFiles = true;
						$scope.templateFiles = [];
					});
					phpJS.listOfFiles("../templates/general/" + folderName + "/", function(state, data){
						$timeout(function(){
							$scope.loading.templateFiles = false;
							if (data.files.length > 0){
								$scope.templateFiles = data;
								if (type == "preheaders"){
									$scope.preheaderTemp[index] = data.files[0];
								}
								if (type == "postheaders"){
									$scope.postheaderTemp[index] = data.files[0];
								}								
							} 
							else{
								
								$scope.preshowTemp[index] = null;
								
							}
						});
					});
				};
				//---------
				
				//---------
				$scope.addNewScript = function(type){
					var newPacket = {deploy: true, folder: $scope.templateFolders.files[0], template: null};
					$scope.formData.settings[$scope.buildFor.type][type].push(newPacket);				
				};
				//---------
				
				//---------
				$scope.updateLayouts = function(checkTable){
						$scope.listOfLayouts = [];		
						
									
						$scope.loadingLayouts = true;	
							if (checkTable != false){
								phpJS.listOfFiles("../templates/collection/" + checkTable + "/", function(state, data){

									if (data.status != "error"){
										if (data.files.length > 0){
											$timeout(function(){
												for (i = 0; i < data.files.length; i++){
													$scope.listOfLayouts.push(data.files[i].replace(/\.[^/.]+$/, ""));
												};												
											});
											$scope.updateLayoutSelectors();										
										}
										else{
											$timeout(function(){
												$scope.listOfLayouts.push("none");
											});
											$scope.updateLayoutSelectors();
										}																				
									}
									else{
										$timeout(function(){
											$scope.listOfLayouts.push("none");
										});
										$scope.updateLayoutSelectors();
									}		
								});	
							}	
							else{
								$timeout(function(){
									$scope.listOfLayouts.push("none");
								});
								$scope.updateLayoutSelectors();
							}	
				};
				//---------
				
				//---------
				$scope.updateLayoutSelectors = function(checkTable){
									
					var isMatched = {
						abstract: { 
							desktop: false, 
							mobile: false
						},
						full: { 
							desktop: false, 
							mobile: false																
						}
					};
					
					
					
					$timeout(function(){
						for (i = 0; i < $scope.listOfLayouts.length; i++){
							if($scope.listOfLayouts[i] == $scope.formData.settings["desktop"].abstractLayout){isMatched.abstract.desktop = true;}
							if($scope.listOfLayouts[i] == $scope.formData.settings["mobile"].abstractLayout){isMatched.abstract.mobile = true;}
							if($scope.listOfLayouts[i] == $scope.formData.settings["desktop"].fullLayout){isMatched.full.desktop = true;}
							if($scope.listOfLayouts[i] == $scope.formData.settings["mobile"].fullLayout){isMatched.full.mobile = true;}							
						};
						
						if (!isMatched.abstract.desktop){
							$scope.formData.settings["desktop"].abstractLayout = $scope.listOfLayouts[0];
						}
						if (!isMatched.abstract.mobile){
							$scope.formData.settings["mobile"].abstractLayout = $scope.listOfLayouts[0];
						}
						if (!isMatched.full.desktop){
							$scope.formData.settings["desktop"].fullLayout = $scope.listOfLayouts[0];
						}
						if (!isMatched.full.mobile){
							$scope.formData.settings["mobile"].fullLayout = $scope.listOfLayouts[0];
						}						
							
						
						$scope.loadingLayouts = false;
					});	
				};
				//---------				
				
				//---------
				//optionsGradient.start = formData.settings.solidColor
				$scope.staticImages = [];		
				$scope.listOfStaticImages = function(){			
					phpJS.listOfFiles("../themes/available/" + _global_setup.theme + "/background/", function(state, data){
							
 							if (state){
 								$timeout(function(){
 									$scope.staticFolder = data.folder;
 									$scope.staticImages = data; 
 								});
 							}
 							else{
 								
 							};	
					});					
				};
				//---------
												
				//---------
				//optionsGradient.start = formData.settings.solidColor
				$scope.allParallax = [];		
				$scope.listOfParallaxImages = function(callback){
					phpJS.listOfFiles("../themes/available/" + _global_setup.theme + "/parallax/", function(state, data){
							
 							if (state){
 								$timeout(function(){
 									$scope.parallaxFolder = data.folder;
 									$scope.allParallax = data; 
 								});
 							}
 							else{
 								
 							};	
					});					
				};			
				//---------	
				
				//---------
				$scope.allVideos = {};		
				$scope.listOfVideos = function(callback){
					phpJS.listOfFiles("../themes/available/" + _global_setup.theme + "/video/", function(state, data){
							var posters = [];
							var files = [];
							for (i = 0; i < data.files.length; i++){	
								var myFileName = data.files[i].replace(/.[^.]+$/,'');
								var myFileExt  = data.files[i].replace(/^.*\./,'');

								if (myFileExt == "jpg" || myFileExt == "png" || myFileExt == "gif"){
									posters.push(data.files[i]);
								};
								if(myFileExt == "mp4"){
									files.push(data.files[i]);
								};
							}
							
 							if (state){
 								$timeout(function(){
 									$scope.allVideos.folder = data.folder;
 									$scope.allVideos.videos = files;
 									$scope.allVideos.posters = posters;
 								});
 							}
 							else{
 								
 							};	
					});					
				};
				//---------

				//---------
		        $scope.handleFileSelect = function(evt, type) {
		        	

					var file = evt.files[0];	
									        	
				    lrFileReader(file)
				        .on('progress',function(event){
				            $scope.progress = event.loaded/event.total;
				        })
				        .on('load',function(event){
				        })
				        .readAsDataURL()
				        .then(function(result){
				        	custom.imageToDataUri(result, 1920,  function(imgData, dimensions){
								randomName = custom.getRandomHash(10);
								
								phpJS.imageUploadBase64(imgData, randomName, "themes/available/" + _global_setup.theme + "/" + type +  "/", function(state, data){
									
										if (type == "background"){
											$scope.listOfStaticImages();
										}
										if (type == "parallax"){
											$scope.listOfParallaxImages();
										}
									
								});
							
							

				        	});		      
				        });		

		        };	
		        //---------
		        
		        //---------
		        $scope.handleFileSelect_video = function(evt) {
		        	alert("This feature is still in beta and unfortunately unavailable.");
					/*
					var file = evt.files[0],						
						randomName = custom.getRandomHash(10);
						
						phpJS.videoUpload(file, randomName, "media/video/", function(state, data){
							//console.log(state, data)
						});	
					*/
		        };			        	
		        //---------
		        
		        //---------
		        $scope.deleteStaticImage = function(name, index, $event){
		        	
						var btn = $event.target;
							btnText = $(btn).text(),
							btnRaw = $(btn).html();	
							$(btn).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);		        	
		        	
						var packet = [],
							name = $scope.staticImages.files[index]; 
								
						packet.push({folder: "../themes/available/" + _global_setup.theme + "/background/", file: name});						
				
						phpJS.deleteFiles(packet, function(state, data){
							$timeout(function(){
								if (!state){
									alert("Could not delete background image.");
								}
								else{
									$scope.staticImages.files.splice(index, 1);
								}								
							});	
						});		
						    	
		        };
				//--------- 
				
		        //---------
		        $scope.deleteParallaxImage = function(name, index, $event){
		        		
						var btn = $event.target;
							btnText = $(btn).text(),
							btnRaw = $(btn).html();	
							$(btn).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);		        	
		        	
						var packet = [],
							name = $scope.allParallax.files[index]; 
								
						packet.push({folder: "../themes/available/" + _global_setup.theme + "/parallax/", file: name});						
				
						phpJS.deleteFiles(packet, function(state, data){
							$timeout(function(){
								if (!state){
									alert("Could not delete background image.");
								}
								else{
									$scope.allParallax.files.splice(index, 1);
								}								
							});	
						});
						    	
		        };
				//--------- 				
				
				
				//--------- change tab
				$scope.changeFocus = function(to){	
					$scope.focusOn = to.toLowerCase();
				};
				$scope.changeFocus($scope.tabs[0].title);
				//--------- 				
				
				
				//--------- 
				$scope.checkDeployedForNulls = function(index){
					if (formData.system.systemdata.pages[index].deployed == null){
						formData.system.systemdata.pages[index].deployed = true;
					};
				};
				//--------- 
				
				//---------
				$scope.copySettings = function(startWith){
					
					var endWith = '';
					if (startWith == "desktop"){
						endWith = "mobile";
					}
					if (startWith == "mobile"){
						endWith = 'desktop';
					}
					var r = confirm("This process will overwrite your current " + endWith + " settings.  Are you sure you want to continue?");
					if (r) {
						
						var p = $scope.formData.settings[startWith]; 						
						for (var key in p) {
						  if (p.hasOwnProperty(key)) {
						  	$scope.formData.settings[endWith][key] = p[key];						    
						  }
						}						
						
					}
				};
				//---------
				
				//---------
				$scope.checkAbstract = function(){
					var enable = $scope.formData.settings[$scope.buildFor.type].useAbstract;
					if(!enable){
						$timeout(function(){
							$scope.formData.settings[$scope.buildFor.type].abstractLayout = $scope.formData.settings[$scope.buildFor.type].fullLayout;
						});
					};
				};
				//---------
				
				//--------- button behavior
				$scope.save = function($event){
					
					var btn = $event.target;								
							  $(btn).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);						
					
					var saveObj = {
				    	name: $scope.formData.pagename,
				    	desktopTable: $scope.formData.settings.desktop.currentTable,
				    	mobileTable:  $scope.formData.settings.mobile.currentTable,				    	
				    	pageSettings: $scope.formData.settings
				    };	
				    
				    
				    // SAVE AS JSON
					phpJS.convertObjectToJson(saveObj, "../production/settings/pagedata/" + $scope.formData.pagename + ".json", function(state, data){});
					
					
					// SAVE IN DATABASE (as backup)
					var jsonString = JSON.stringify($scope.formData.settings);
						phpJS.encodeHtmlEntities(jsonString, function(state, encodedString){
							var packet = {	pageName: $scope.data.pagename, settings: encodedString, 
											desktopTable: $scope.currentTables.desktop, 
											mobileTable:$scope.currentTables.mobile};
								
							phpJS.updatePageSettings(packet, function(state, data){								
								setTimeout(function(){ location.reload();	}, 3000);
							});				
						});
					
				};
				
				
				$scope.cancel = function(){				  	
				  	$modalInstance.dismiss();
				};
				
				$scope.dismiss = function(){				  	
				  	;
				  	$modalInstance.dismiss();
				};	
				//--------- end button behavior	
			
			}],
			/* END MODAL CONTROLLER */	
			//////////////////////	
			
			
			//////////////////////
			/* MODAL CONTROLLER */
			pageSystemModalCtrl: ['items', '$scope', '$timeout', '$modalInstance',
							function(items, $scope, $timeout, $modalInstance){		
				

				//--------- variables declared
				$scope.getComplete = false; 
				$scope.tabs = [										
					{title: "Pages"},
					{title: "Theme"},
					{title: "Permissions"},
					{title: "SEO"},
					{title: "Database"},
				];
				$scope.buildTypes = [
					"Desktop", "Mobile"
				];
				$scope.listOfThemes = [];
	
				
				$scope.showPermissions = [];
				$scope.accessTable = [];
				var oldPages = [];
				var newPages = [];
				
				//--------- change tab
				$scope.init = function(){
						
					// grab current pages
					for (i = 0; i < items.system.systemdata.pages.length; i++){
						oldPages.push( items.system.systemdata.pages[i].label.toLowerCase() );						
					};	

					phpJS.listOfFiles("../themes/available/", function(state, data){
						$scope.listOfThemes = data.files; 						  				
					});
													
					phpJS.returnListOfTables(items.system.usingdb, function(state, data){	
						$timeout(function(){
							$scope.data = items;	
							$scope.data.tables = data; 	
							$scope.formData = $.extend(true, {}, $scope.data );					
							for (var i = 0; i < data.user.length; i++){
								table = data.user[i];
								blank = {tableName: table, canRead: false, canWrite: false, canDelete: false, canCreate: false};
								$scope.accessTable.push(blank);
							}
							
														
							$scope.changeFocus($scope.tabs[0].title);									
							$scope.getComplete = true;
						});
					});
					
				};
				//--------- 
								
				//--------- change tab
				$scope.changeFocus = function(to){		
					$scope.setAllEditsToFalse();		
					$timeout(function(){$scope.focusOn = to.toLowerCase();});
				};
				//--------- 
				
				
				
				//---------
				$scope.pullSetupJson = function(){
					$scope.dbSettings = {};
					phpJS.retrieveSetupConfig(function(state, data){
						$timeout(function(){
	  		  				$scope.dbSettings.local = data.localname;
	  		  				$scope.dbSettings.remote = data.remotename;
	  		  				$scope.dbSettings.user = data.username;
	  		  				$scope.dbSettings.key = data.password;
	  		  				$scope.dbSettings.db = data.assignedDB;
	  		  				$scope.dbSettings.fb = data.firebase;
  		  			});
		  			});					
				};
				//---------
				
				//---------
				$scope.validateLevel = function(permission){
					if (permission.level == null || permission.level == undefined || permission.level == '' ){
						permission.level = 0;
					};					
				};
				//---------
				
				//--------- 
				$scope.validateSuperAdminAccess = function(name, access, part){
					if (name.toLowerCase() == 'superadmin'){
						alert("Cannot alter SUPERADMIN permissions.  They are locked.");
						access[part] = true;												
					}
				};
				
				$scope.validatePermissions = function(index){
					var raw = $scope.formData.system.systemdata.pages[index].raw,
						permissionTypes = $scope.formData.system.systemdata.permissionTypes;
					
					// remove permission types that don't exists
					for (var i = 0; i < raw.length; i++){
						var label = raw[i].label,
							isAType = custom.findWithAttr(permissionTypes, 'label', label);
							if (isAType == undefined){ 
								raw.splice(i, 1);
							}	 	
					}
					
					// add permission types that are new					
					for (var i = 0; i < permissionTypes.length; i++){
						var label = permissionTypes[i].label;
							isAType = custom.findWithAttr(raw, 'label', label);
							if (isAType == undefined){
								permissionTypes[i].ticked = false; 								
								raw.push(permissionTypes[i]);
							}								 
					}
					 

				};
				//--------- 
				
				//--------- 
				$scope.setAllEditsToFalse = function(){
					for (var i = 0; i < $scope.formData.system.systemdata.permissionTypes.length; i++){
						$scope.formData.system.systemdata.permissionTypes[i].isEdit = false; 	
						$scope.formData.system.systemdata.permissionTypes[i].level = $scope.formData.system.systemdata.permissionTypes[i].level.toString(); 	
					};
					
				};
				//--------- 
				
				//--------- 
				$scope.makeEditable = function(permission){					
					var isPermanent = permission.permanent;
					if (!isPermanent){						
						permission.isEdit = !permission.isEdit;
					}
					
				};
				//--------- 
				
				
				//--------- 
				$scope.addPermission = function(){
					var access = {};		
					for (var i = 0; i < $scope.data.tables.user.length; i++){
						var table =  $scope.data.tables.user[i];
						var blankPermissions = {tableName: table, canRead: false, canWrite: false, canCreate: false, canDelete: false, canComment: false};
						access[table] = blankPermissions;	
					}	
					newPermission = {label: "New", permanent: false, isEdit: false, level: 1, access: access};					
					$scope.formData.system.systemdata.permissionTypes.push(newPermission);					
				};
				//--------- 
				
				//---------
				$scope.deletePermission = function(permission){					
					index = custom.findWithAttr($scope.formData.system.systemdata.permissionTypes, 'label', permission.label);
					$scope.formData.system.systemdata.permissionTypes.splice(index, 1);
					
				};
				//---------

				//--------- button behavior
				$scope.save = function($event){
					var btn = $event.target;								
							  $(btn).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);					
					
					// strip out extra information for page security - replace with just labels
					var pages = $scope.formData.system.systemdata.pages;
							 
					for (i = 0; i < pages.length; i++){
						newPages.push(pages[i].label.toLowerCase());
						var justLabels = [],							
							permissions = pages[i].permissions;
						for (n = 0; n < permissions.length; n++){
							if (permissions[n].ticked){
								justLabels.push(permissions[n].label);
							};
						}	
						pages[i].permissions = justLabels;															
					};

					// updates page names if they are different
					function updatePageNames(oldName, newName){
						packet = {database: items.system.usingdb, oldName: oldName, newName: newName};
						// update database
						phpJS.swapPageNames(packet, function(state, data){});	
						// update jsons
						var packet = {oldName: '../production/settings/pagedata/' + oldName + ".json", newName: '../production/settings/pagedata/' + newName + ".json"};
						phpJS.renameFile(packet, function(state, data){
							// if doesn't exists, create from template
							if(data.status == "error"){
								var packet = {source: '../production/settings/defaults/newpagedata.json', newName: '../production/settings/pagedata/' + newName + '.json'};
								phpJS.copyFile(packet, function(state, data){});
							}
						});						
									
					}
					for (i = 0; i < oldPages.length; i++){
						if (oldPages[i] != newPages[i]){
							updatePageNames(oldPages[i], newPages[i]);
						}
					}
					
					// GENERATE SEO packet
					var packet = {
						title: $scope.formData.system.systemdata.seo.title,
						description: $scope.formData.system.systemdata.seo.description,
						meta: $scope.formData.system.systemdata.seo.metatags,
						author: $scope.formData.system.systemdata.seo.author,
					};
					phpJS.generateSEOjson(packet, function(state, data){   });
					
					// GENERATE SETUP JSON
					if ($scope.dbSettings != undefined){
						var packet = {
								local: $scope.dbSettings.local, 
								remote: $scope.dbSettings.remote,
								user:  $scope.dbSettings.user,
								key: $scope.dbSettings.key,
								db:  $scope.dbSettings.db,
								fb:  $scope.dbSettings.fb,
						};
						phpJS.generateSetupjson(packet, function(statem, data){ });
					}	
								
								
					// SAVE AS JSON
					phpJS.convertObjectToJson($scope.formData.system, "../production/settings/systemdata/system.json", function(state, data){});
				
					// UPDATE DATABASE (as backup)
					$timeout(function(){
						$scope.setAllEditsToFalse();
						var jsonString = JSON.stringify($scope.formData.system.systemdata);
						
						phpJS.encodeHtmlEntities(jsonString, function(state, encodedString){
							var packet = {settings: encodedString};					
							phpJS.updateSystemSettings(packet, function(state, data){
								setTimeout(function(){ location.reload();	}, 3000);
							});													
						});
					}, 1000);
					
				};
				
				
				$scope.cancel = function(){				  	
				  	$modalInstance.dismiss();
				};
				
				$scope.dismiss = function(){				  	
				  	;
				  	$modalInstance.dismiss();
				};	
				//--------- end button behavior	
			
			}],
			/* END MODAL CONTROLLER */	
			//////////////////////				
				
			//////////////////////
			/* MODAL CONTROLLER */
			collectionSettingsModalCtrl: ['items', '$scope', '$timeout', '$modalInstance', 'TransactionManager', 
									function(items, $scope, $timeout, $modalInstance, TransactionManager){		
				
				
				//--------- variables declared
				$scope.resetVariables = function(){	
					$scope.formData = $.extend(true, {}, $scope.data );
					$scope.isLoading = null; 
					$scope.getComplete = false;
					$scope.collectionData;   
					$scope.editMode = [];
					$scope.showMe = [];
					$scope.newTableData = {};
					$scope.newSubmitCheck = false;
					$scope.newFieldObj = {checkPass: false};
					$scope.showMeActive = true;
					$scope.tableData = {
						current: '',
						renamed: ''
					};				
					var usingTable = '';
									
					$scope.tabs = [
						{title: "Create New"},
						{title: "Edit"}
					];
	
					$scope.dataTypes = [
						{name: "Text", 				category: "User Input",	value: 'text'},
						{name: "Boolean",			category: "User Input",	value: 'boolean'},
						{name: "Number",			category: "User Input",	value: 'number'},
						{name: "Decimal",			category: "User Input",	value: 'decimal'},
						{name: "Date",				category: "User Input",	value: 'date'},
						{name: "Time",				category: "User Input",	value: 'time'},
						{name: "Image Upload",		category: "User Input",	value: 'imageUpload'},				
						{name: "User First Name",	category: "Read Only",	value: '_fname'},
						{name: "User Last Name",	category: "Read Only",	value: '_lname'},
						{name: "User Username",		category: "Read Only",	value: '_username'},
						{name: "User Access Level",	category: "Read Only",	value: '_access'},
						{name: "User Email",		category: "Read Only",	value: '_email'},
						{name: "User ID",			category: "Read Only",	value: '_id'},
						{name: "Timestamp",			category: "Archival",	value: 'timestamp'},											
					];
					
					$scope.systemDataTypes = {
						text: {property: 'TEXT'},
						boolean: {property: 'TINYINT(0)'},
						number: {property: 'INT(0)'},
						decimal: {property: 'DECIMAL(10, 2)'},
						date: {property: 'DATE'},
						time: {property: 'TIME'},
						timestamp: {property: 'TIMESTAMP'},
						imageUpload: {property: 'TEXT'},
						_fname: {property: "TEXT"},
						_lname: {property: "TEXT"},
						_username: {property: "TEXT"},
						_access: {property: "TEXT"},
						_email: {property: "TEXT"},
						_id: {property: "TINYINT(0)"},		
											
					};
					
					$scope.dataTypeSelected = [];
				};
				$scope.resetVariables();
				
				//--------- change tab
				$scope.init = function(){
					
					$scope.resetVariables();
					
					phpJS.returnListOfTables(items.system.usingdb, function(state, data){
						$timeout(function(){
							$scope.data = {
								tables: data,
								system: items.system
							};
							$scope.newTableData.name = "new" + $scope.data.tables.user.length; 
							$scope.checkExistingNames($scope.newTableData.name);
							$scope.getComplete = true;
						});
					});		
				};
				//---------
				
				//--------- change tab
				$scope.returnCategory = function(n){
					
					
					
					for (var i = 0; i < $scope.dataTypes.length; i++){
						var v = $scope.dataTypes[i].value;
						if (n == v){
							return $scope.dataTypes[i].category.toLowerCase();
						};
					};
				};
				
				
				$scope.checkDefault = function(type, actual){
					if (type == "set1"){
						if (actual == "none" || actual == "required" ){
							return actual;
						}
						else{
							return "none";
						}
					};
					if (type == "set2"){
						if (actual == "hidden" || actual == "disabled" ){
							return actual;
						}
						else{
							return "hidden";
						}
					}					
				};
				
				$scope.cleanNumber = function(n){
					if (n != undefined){
						return(parseFloat(n));
					}
				};
				
				$scope.cleanBoolean = function(b, setDefault){
					if (b == "true"){
						return true;
					}
					else if (b == "false"){
						return false;
					}	
					else{
						if (b == undefined){
							if (setDefault == undefined){
								return true;
							}
							else{
								return setDefault; 
							}
						}
						else{
							return b;
						}
					}				
				};
				//--------- change tab				
					
				//--------- change tab
				$scope.changeFocus = function(to){				
					$timeout(function(){$scope.focusOn = to.toLowerCase();});
				};
				$scope.changeFocus($scope.tabs[0].title);
				//--------- 
				
				//--------- 
				$scope.createNewCollection = function(){
					var name = $scope.newTableData.name.toLowerCase();
					var p = $scope.data.system.systemdata.permissionTypes;
					for (var key in p) {
					  if (p.hasOwnProperty(key)) {
						switch(p[key].label) {
						    case "superadmin":
						        defaultPermission = {tableName: name, canRead: true, canWrite: true, canCreate: true, canDelete: true, canComment: true};
						        break;
						    case "admin":
						        defaultPermission = {tableName: name, canRead: true, canWrite: true, canCreate: true, canDelete: true, canComment: true};
						        break;
						    case "user":
						        defaultPermission = {tableName: name, canRead: true, canWrite: true, canCreate: true, canDelete: false, canComment: true};
						        break;
						    case "guest":
						        defaultPermission = {tableName: name, canRead: true, canWrite: false, canCreate: false, canDelete: false, canComment: true};
						        break;						        						        
						    default:
						    	defaultPermission = {tableName: $scope.newTableData.name, canRead: true, canWrite: false, canCreate: false, canDelete: false, canComment: true};
						       	break;
						}					  	
						// add new table permissions to system data
						p[key].access[p[key].access.length] = defaultPermission;
						

					  }
					}
					
	   
					var packet = {newFolder: "../templates/collection/" + name};
					phpJS.createFolder(packet, function(state, data){ 
							var copyPacket = {source: '../production/settings/defaults/newlayout.html', newName: "../templates/collection/" + name + '/default.html'};																					
							phpJS.copyFile(copyPacket, function(state, data){																					
								
							});		
					});
	   				
	
	   				phpJS.convertObjectToJson($scope.data.system, "../production/settings/systemdata/system.json", function(state, data){ 

	   				});					
					// create new table in database
					phpJS.createNewTable(name ,function(state, data){
						
						if (data.status == "good"){							
							// update system data with new permissions
							var jsonString = JSON.stringify($scope.data.system.systemdata);					
							phpJS.encodeHtmlEntities(jsonString, function(state, encodedString){
								var packet = {settings: encodedString};					
								phpJS.updateSystemSettings(packet, function(state, data){
									setTimeout(function(){ location.reload();	}, 3000);
								});													
							});		
							
											
						};						 
					});
					
				};
				//--------- 	
				

				//-------------------							
				$scope.checkAvailablity = function(name){
					checkPass = true;			
					for (i = 0; i < $scope.collectionData.length; i++){
						var n = $scope.collectionData[i].Field.toLowerCase();
						if (name.toLowerCase() == n.toLowerCase()){ checkPass = false; }	
					}
					$scope.newFieldObj.passCheck = !checkPass;		
	
				};																		
				//-------------------						
				
				//---------
				$scope.checkExistingNames = function(name){
					if ($scope.tableData.renamed != undefined){
						$.trim($scope.tableData.renamed);
					}
					checkPass = true;
					for (i = 0; i < $scope.data.tables.user.length; i++){
						var n = $scope.data.tables.user[i].toLowerCase();
						if (name == n){ checkPass = false; }	
					}
					$scope.newSubmitCheck = !checkPass;					
				};
				//---------
				
				//---------
				$scope.loadCollection = function(selected, showLoader){
					$scope.tableData = {
						current: selected,
						renamed: selected
					};
					usingTable = selected;	
					var packet = {database: 'codeandl_example', table: selected};
					$scope.isLoading = showLoader; 	
					
					//-------------------
					$scope.editBtn = function(index){
						$scope.showMeActive = false;
						var p = $scope.showMe.active; 
						
						for (var key in p) {
						  if (p.hasOwnProperty(key)) {
						    p[key] = false; 
						  }
						}
						
						// for autoCollect 
						
						if($scope.collectionData[index].Comment.hasOwnProperty("autoCollect")){
						    $scope.dataTypeSelected[index] = $scope.collectionData[index].Comment.autoCollect;						
						}
							
											
						
						$scope.showMe.active[index] = true;
						$scope.editMode.active[index] = true;
						TransactionManager.snapshot($scope.collectionData[index]);
										
					};
					//-------------------
					
					//-------------------
					$scope.deleteTable = function(name){
						
						
						var r = confirm("Are you sure you want to delete this table?  ALL INFORMATION ASSOCIATED WITH IT WILL BE LOST.");
						if (r) {
							var u = confirm("Are you absolutely sure?");
							if (u) {
								var count = 0,
									required = 5;
								
								
								function waitForChanges(){
									count ++;
									if (count == required){	
										location.reload();
									};
								}

			
		
								// delete table
								phpJS.deleteTable(name, function(state, data){
									waitForChanges();
								});
								
								
								// update systemdata
								var i = $scope.data.system.systemdata.permissionTypes.length;
								while(i--){
									var p = $scope.data.system.systemdata.permissionTypes[i];
									var n = p.access.length;
									var newArray = [];
									while(n--){			
										if (p.access[n].tableName != name){
											newArray.push(p.access[n]);
										};
									};
									p.access = newArray; 
								};
								phpJS.convertObjectToJson($scope.data.system, "../production/settings/systemdata/system.json", function(state, data){ 
									waitForChanges();
								});								
		
								var jsonString = JSON.stringify($scope.data.system.systemdata);					
								phpJS.encodeHtmlEntities(jsonString, function(state, encodedString){
									var packet = {settings: encodedString};			
									phpJS.updateSystemSettings(packet, function(state, data){
										if (state){
											waitForChanges();
										}
										else{
											alert("Error when updating tables.  Please refresh and try again.");
										}
									});				
								});		
								
								// delete folder and files inside
								phpJS.deleteFolder("../templates/collection/" + name + "/", function(state, data){
										if (state){
											waitForChanges();
										}
										else{
											alert("Error when updating tables.  Please refresh and try again.");
										}
								});		
								
								// rename page table in page settings						
								var packet = {oldname: name, newname: ''};
								phpJS.renameTablesInPageSettings(packet, function(state, data){
								  waitForChanges();
								});	
							
								
								
							}								
									
						}		

					};
					//-------------------
					
					//-------------------
					$scope.renameTable = function(){
						var count = 0,
							required = 5;
							
						function waitForChanges(){
							count ++;
							if (count == required){
								location.reload();
							};
						}
			
						// rename table						
						var packet ={ oldName: $scope.tableData.current, newName: $scope.tableData.renamed }; 
						phpJS.renameTable(packet, function(state, data){
								if (state){									
									waitForChanges();
								}
								else{
									alert("Error when renaming table.  Please refresh and try again.");
								}
						});	

						// update systemdata
						var i = $scope.data.system.systemdata.permissionTypes.length;
						while(i--){
							var p = $scope.data.system.systemdata.permissionTypes[i];
							var n = p.access.length;
							while(n--){			
								if (p.access[n].tableName == $scope.tableData.current){
									p.access[n].tableName = $scope.tableData.renamed;
								};
							};
						};
						
						// update systemdata.json
						phpJS.convertObjectToJson($scope.data.system, "../production/settings/systemdata/system.json", function(state, data){ 
							 waitForChanges();
						});						
						
						// update database to match
						var jsonString = JSON.stringify($scope.data.system.systemdata);					
						phpJS.encodeHtmlEntities(jsonString, function(state, encodedString){
							var packet = {settings: encodedString};					
							phpJS.updateSystemSettings(packet, function(state, data){
								if (state){
									waitForChanges();
								}
								else{
									alert("Error when updating tables.  Please refresh and try again.");
								}
							});					
						});
						
						// rename theme/structure folders
						var packet = {
							oldFolder: "../templates/collection/" + $scope.tableData.current, 
							newFolder: "../templates/collection/" + $scope.tableData.renamed
						};					
						phpJS.renameFolder(packet, function(state, data){

								// if newly created, add default page template
								if (data.status == "success"){										
										var copyPacket = {source: '../production/settings/defaults/newlayout.html', newName: "../templates/collection/" + $scope.tableData.renamed + '/default.html'};																					
										phpJS.copyFile(copyPacket, function(state, data){																					
											waitForChanges();
										});		
								}
								else{
									if (state){
										waitForChanges();
									}
									else{
										alert("Error when renaming folders.  Please refresh and try again.");
									}
								}
						});
						
						// rename page table in page settings						
						var packet = {oldname: $scope.tableData.current, newname: $scope.tableData.renamed};
						phpJS.renameTablesInPageSettings(packet, function(state, data){
						  waitForChanges();
						});						
									
						


					};
					//-------------------
					
					//-------------------
					$scope.revertChanges = function(index){
						hasHistory = TransactionManager.canRollback($scope.collectionData[index]);
						if(hasHistory){
							// rollback
							r = window.confirm("Discard your changes?");
							if (r){
								TransactionManager.rollback($scope.collectionData[index]);
								$scope.completeChanges(index);	
												
							}
						}
					};
					//-------------------
					
					//-------------------
					$scope.completeChanges = function(index){
							
							var p = $scope.showMe.active; 
							
							// go back
							for (var key in p) {
							  if (p.hasOwnProperty(key)) {
							    p[key] = true; 
							  }
							}
							$timeout(function(){
								$scope.loadCollection(usingTable, false);
								$scope.showMe.active[index] = true;
								$scope.editMode.active[index] = false;
								$scope.showMeActive = true;						
							});		
					};
					//-------------------
								
								
					//-------------------			
					$scope.addNewField = function(){
							buildString = 	"ALTER TABLE " + usingTable + " ADD " + $scope.newFieldObj.name + " TEXT";
							var packet ={ database: "codeandl_example", query: buildString }; 
							phpJS.queryDatabase2(packet, function(state, data){													 
								$scope.loadCollection(usingTable, false);
							});	
					};
					//-------------------	
					
					//-------------------
					$scope.deleteField = function(name){
							$('.deleteBtn, .editBtn').attr('disabled', true);
							r = window.confirm("Delete this table.  \n\n** Warning:  ALL DATA IN THIS FIELD WILL BE DELETED.  THIS CANNOT BE UNDONE.  \n\nAre you sure?");
							if (r){
								
								buildString = 	"ALTER TABLE " + usingTable + " DROP " + name;
								var packet ={ database: "codeandl_example", query: buildString }; 
								phpJS.queryDatabase2(packet, function(state, data){													 
									$scope.loadCollection(usingTable, false);
								});						
							}
							else{
								$('.deleteBtn, .editBtn').attr('disabled', false);
							}
					};
					//-------------------

										
					//-------------------
					$scope.moveField = function(index, dir){
						TransactionManager.snapshot($scope.collectionData[index]);
						$scope.saveChanges(index, dir);
					};
					
					$scope.saveChanges = function(index, dir){
						$scope.isLoading = true;
						$('.deleteBtn, .editBtn').attr('disabled', true);
						$('#saveBtn').attr('disabled', true);
						$('#cancelBtn').attr('disabled', true);


						var	type 				= $scope.dataTypeSelected[index],
							entry				= $scope.collectionData[index]; 
							c 					= entry.Comment,
							comments 			= "", 
							oldField 			= entry.snapshot[0].Field, 
							newField 			= entry.Field;
							
							// parse data types
							oldDataType 		= $scope.systemDataTypes[entry.UseType].property,
							currentDataType 	= $scope.systemDataTypes[type].property;
							if(oldDataType.indexOf('(') != -1){
								oldDataType 		= oldDataType.substring(0, oldDataType.indexOf('('));
							}
							if(currentDataType.indexOf('(') != -1){
								currentDataType 	= currentDataType.substring(0, currentDataType.indexOf('('));
							}

						// check data type compatability 
						if (oldDataType != currentDataType){
							if (confirm('You\'re about to switch data types that are not compatable with one another. This might clear out existing data for ALL entries and will be unrecoverable.  Continue?')) {
							   
							   buildString = 	"ALTER TABLE "+ usingTable +" CHANGE COLUMN " + oldField + " " + oldField + " " + $scope.systemDataTypes[type].property; 
								var packet ={ database: "codeandl_example", query: buildString }; 
								phpJS.queryDatabase2(packet, function(state, data){						  
									updateData(dir);
								});								  
								  
							   
							} else {
							   updateData(dir);
							}
						}
						else{
							updateData(dir);
						}
						

						
						// update dataType 
						function updateData(dir){

							statement = 	"ALTER TABLE "+ usingTable +" CHANGE COLUMN " + oldField + " " + newField + " " +  $scope.systemDataTypes[type].property + " " + entry.Default + " " + " COMMENT '";
							
							// get non specific comments											
							comments += " | label=\"" + c.label + "\"";
							
							// user data specific
							if( $scope.dataTypeSelected[index] == '_fname' || $scope.dataTypeSelected[index] == '_lname' ||
								$scope.dataTypeSelected[index] == '_username' || $scope.dataTypeSelected[index] == '_email' ||
								$scope.dataTypeSelected[index] == '_id' || $scope.dataTypeSelected[index] == '_access'){	
								comments += " | autoCollect=\"" + $scope.dataTypeSelected[index] + "\"";	
								comments += " | displayAs=\"" + c.displayAs + "\"";							
							}

							
							// text specific
							if($scope.dataTypeSelected[index] == 'text'){			
								// inputType
								comments += " | inputType=\"" + c.inputType + "\"";			
								// placeholder
								comments += " | placeholder=\"" + c.placeholder + "\"";
								// use restrictions
								comments += " | useRestriction=\"" + c.useRestriction + "\"";
								
								// if not advanced text capture
								if (c.inputType != "adv" && c.useRestriction == true){						
									
										comments += " | allowupper="+c.allowupper+" "; 			
										comments += " | allowlower="+c.allowlower+" "; 			
										comments += " | allownumbers="+c.allownumbers+" "; 		
										comments += " | allowspaces="+c.allowspaces+" "; 		
										comments += " | allowspecials="+c.allowspecials+" "; 	
										
										var regexp = "^$|^["; // start
											if (c.allowupper == true || c.allowupper == "true"){	 	regexp += "A-Z"; }
											if (c.allowlower == true || c.allowlower == "true"){	 	regexp += "a-z";  }
											if (c.allownumbers == true || c.allownumbers == "true"){ 	regexp += "0-9"; }
											if (c.allowspaces == true || c.allowspaces == "true"){ 		regexp += " "; 	}
											if (c.allowspecials == true || c.allowspecials == "true"){ 	regexp += ".:,!@#$%"; }										
										regexp += "]*$";  // end
										
									comments += " | regexp=\"" + regexp + "\"";
									
									
								}	
																									
							}							
							


							// timestamp specific
							if($scope.dataTypeSelected[index] == 'timestamp'){
								comments += " | inputType=\"" + $scope.dataTypeSelected[index] + "\""; 
							};	
							

							// time specific
							if($scope.dataTypeSelected[index] == 'time'){
								comments += " | inputType=\"" + $scope.dataTypeSelected[index] + "\""; 
							};	
							
							// date specific
							if($scope.dataTypeSelected[index] == 'date'){
								comments += " | inputType=\"" + $scope.dataTypeSelected[index] + "\""; 
							};	

							// imageUpload specific
							if($scope.dataTypeSelected[index] == 'imageUpload'){
								comments += " | inputType=\"" + $scope.dataTypeSelected[index] + "\""; 
								
								if(c.useCaptions){
									comments += " | captions ";
								} 
								comments += " | limit=\"" + c.limit + "\""; 
							};	
									
							// boolean specific
							if($scope.dataTypeSelected[index] == 'boolean'){
								comments += " | inputType=\"" + $scope.dataTypeSelected[index] + "\""; 
								// booleanAs
								comments += " | booleanAs=\"" + c.booleanAs + "\""; 
							};		
	
							// number specific
							if(	$scope.dataTypeSelected[index] == 'number' ||
								$scope.dataTypeSelected[index] == 'decimal'){	

								// capture method
								comments += " | numberType=\"" + c.numberType + "\"";
							
								// allowNegative
								comments += " | allowNeg=\"" + c.allowNeg + "\""; 
								
								// is negative
								if (c.allowNeg){
									if ($scope.dataTypeSelected[index] == 'number'){
										comments += " | regexp=\"^-?[0-9]*$\"";
									}
									if ($scope.dataTypeSelected[index] == 'decimal'){
										if (entry.Comment.decimalPlace == undefined){ entry.Comment.decimalPlace = 0; };
										comments += " | regexp=\"^$|^-?~escape~d+(~escape~.~escape~d{0," + entry.Comment.decimalPlace + "})?$\"";
									}									
								}
								// is NOT negative
								if (!c.allowNeg){
										if ($scope.dataTypeSelected[index] == 'number'){
										comments += " | regexp=\"^[0-9]*$\"";
									}
									if ($scope.dataTypeSelected[index] == 'decimal'){
										if (entry.Comment.decimalPlace == undefined){ entry.Comment.decimalPlace = 0; };
										comments += " | regexp=\"^$|^~escape~d+(~escape~.~escape~d{0," + entry.Comment.decimalPlace + "})?$\"";
									}	
								}								
								
								if ($scope.dataTypeSelected[index] == 'decimal'){									
									comments += " | decimalPlace=\""+ c.decimalPlace +"\"";
								}
								
								// if currency
								if (c.useCurrency){
									comments += "| useCurrency=\"true\" | currencyType=\"" + c.currencyType + "\"";
								}
								
								// has stepvalue
								if (c.stepValue == undefined){
										c.stepValue = 1.00;
								}
								comments += "| stepValue=\"" + c.stepValue + "\"";
								
								
								
								// min/max
								if (c.rangeMax != undefined){							
									comments += " | rangeMax=\"" + c.rangeMax + "\"";
								} 
								if (c.rangeMin != undefined){
									comments += " | rangeMin=\"" + c.rangeMin + "\"";
								} 
							};
													

							// isSearchable
							if(c.isSearchable){
								comments += " | isSearchable ";
							}
							
							// isRequired | Behavior default							
							if($scope.collectionData[index].Behavior != "none"){
								comments += " | " + $scope.collectionData[index].Behavior;
							}
						
							

							// closing
							comments += "'"; 
							
							
							
							// get closing statement
							afterStatement = '';
							if (dir == "same"){
								if (index == 0){
									afterStatement = " FIRST";
								}else{
									afterStatement = " AFTER " + $scope.collectionData[index - 1].Field + "";
								}
							};
							if(dir == "up"){
								if (index + 1 < $scope.collectionData.length ){
									afterStatement = " AFTER " + $scope.collectionData[index + 1].Field + "";
								}
								else{
									afterStatement = " AFTER " + $scope.collectionData[0].Field + "";
								}
							}
							if(dir == "down"){
								if (index - 2 >= 0 ){
									afterStatement = " AFTER " + $scope.collectionData[index - 2].Field + "";
								}
								else{
									afterStatement = " AFTER " + $scope.collectionData[$scope.collectionData.length - 1].Field + "";
								}
							}
							// compile
							buildString = statement + comments + afterStatement;
							buildString = buildString.replace(/"/g , "\\\"");
							
							
							// complete and update
							var packet ={ database: "codeandl_example", query: buildString }; 
							phpJS.queryDatabase2(packet, function(state, data){							  								
								$timeout(function(){
									$scope.loadCollection(usingTable, false);
									$scope.showMeActive = true;
								});
							});
						
						};
						
					};
					//-------------------
										
										
					//-------------------						
					phpJS.getTableFields(packet, function(state, data){
						$timeout(function(){		
						$scope.isLoading = false; 
							if (state){
								$scope.collectionData = data;
	
								
								for (var i = 0; i < $scope.collectionData.length; i++){

										var collectionComments = {}, 
											useType = '';
										
								
										var strippedType = $scope.collectionData[i].Type.replace(/[, 0-9()]/g,'').toLowerCase();
						        		
						        		// ALL TEXT INPUT
						        		if (strippedType == "input" || strippedType == "varchar" || strippedType == "text" || strippedType == "tinytext" || strippedType == "mediumtext" || strippedType == "longtext"){
						        			useType = "text";
						        		};
						        		
						        		// ALL DEFAULT NUMBERS
						        		if (strippedType == "tinyint" || strippedType == "smallint" || strippedType == "mediumint" || strippedType == "int" || strippedType == "bigint" || strippedType == "bit"){
						        			useType = "number";
						        		};										        		
						        		
						        		// ALL DECIMAL NUMBERS * needs to be redone - direct regexp work best
						        		if (strippedType == "double" || strippedType == "decimal" || strippedType == "float"){
						        			useType = "decimal";
						        		};	
						        		
						        		// TIME * need to be redone to match mysql database - direct regexp work best
						        		if (strippedType == "date"){
						        			useType = "date";
						        		};	
						        		if (strippedType == "time"){	
						        			useType = "time";
						        		};	
						        		if (strippedType == "timestamp"){	
						        			useType = "timestamp";
						        		};							        		
										        		
	
										// parse comments into properties and/or objects
										var strippedComment = $scope.collectionData[i].Comment.split("|");
										for (var n = 0; n < strippedComment.length; n++){
											strippedComment[n] = $.trim(strippedComment[n]);
											if ( strippedComment[n].indexOf("=") > -1 ){
												var strippedProperty = strippedComment[n].split("="),
													key = strippedProperty[0],
													value = strippedProperty[1].replace(/['"]+/g, '');	
													var obj = {};
													obj[key] = value;
												collectionComments[key] = value;
											}
											else{
												if (strippedComment[n] != ''){
													var obj = {};
													
													collectionComments[strippedComment[n]] = true;
												}
											}
											
											$scope.collectionData[i].Comment = collectionComments;
										}
										
										var p = $scope.collectionData[i].Comment; 
										var useBehavior = "none";	
										for (var key in p) {
										  if (p.hasOwnProperty(key)) {
										    
										    if (key == "inputType"){
												type = p[key];
												if (type == "boolean"){
													useType = "boolean";
												}
												if (type == "imageUpload"){
													useType = "imageUpload";
												}	
										    }
											    
											if (key == "getFirebaseId"){
												useType = "_id";
											}
											
											// text images specific
											if (key == "captions"){
												$scope.collectionData[i].Comment.useCaptions = true;
											}											
											
											// group behaviors (there can only be one)									
											if (key == "hidden"){
												useBehavior = "hidden";
											}
											if (key == "disabled"){												
												useBehavior = "disabled";
											}
											if (key == "required"){
												useBehavior = "required";
											}												    
											    
										  }
										}										
										
										
										$scope.collectionData[i].Behavior = useBehavior;										
										$scope.collectionData[i].UseType = useType;
										$scope.dataTypeSelected[i] = useType;
			
								};	
								
							}	
						});	
						
						
													
					});
					//-------------------	

				};
				//---------


				//--------- button behavior
				$scope.save = function($event){
					var btn = $event.target;								
							  $(btn).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);					
					
					/*
					$scope.setAllEditsToFalse();
					var jsonString = JSON.stringify($scope.formData.system.systemdata);					
					phpJS.encodeHtmlEntities(jsonString, function(state, encodedString){
						var packet = {settings: encodedString};					
						phpJS.updateSystemSettings(packet, function(state, data){
							setTimeout(function(){ location.reload();	}, 500);
						});													
					});
					*/
	
				};
				
				
				$scope.cancel = function(){				  	
				  	$modalInstance.dismiss();
				};
				
				$scope.dismiss = function(){				  	
				  	;
				  	$modalInstance.dismiss();
				};	
				//--------- end button behavior	
			
			}],
			/* END MODAL CONTROLLER */	
			//////////////////////	

		};
		

	
	});

});
define(['custom', 'sharedData', 'firebaseSettings'], function(custom, sharedData, firebaseSettings) {

	

	angular.module('formsModalCtrl', []).	
	factory('formsModalCtrl', function(){
		
		return {

			//////////////////////
			/* TEST CONTROLLER */
			ping: function(amount){								
				var test = "ponged: " + amount;
				return 	test;			
			},
			/* END MODAL CONTROLLER */
			//////////////////////
			
	
			//////////////////////
			/* POST CONTROLLER */
			postModalCtrl: ['items', '$timeout', '$interval', '$scope', '$modalInstance', 
					function(items, $timeout, $interval, $scope, $modalInstance){		
			  

			  // variables
			  $scope.editMode = false; if (items.editData != null){$scope.editMode = true;}			 
			  $scope.fileData = [];
			  $scope.inputData = {};	
			  $scope.uploadValidate = {};
			  $scope.tableFields = items;
			  $scope.masterData = sharedData.getAll();
			  $scope.imageUploadParameters = {
			  	limit: null,
			  	thumbnailSize: 200
			  };	    	   
			  $scope.tinymceOptions = {
			        theme: "modern",
			        plugins : "paste, autoresize",
			        theme_advanced_buttons3_add : "pastetext,pasteword,selectall",
			        paste_auto_cleanup_on_paste : true,
			        paste_preprocess : function(pl, o) {
			            // Content string containing the HTML from the clipboard
			          	//o.content = "-: CLEANED :-\n" + o.content;
			        },
			        paste_postprocess : function(pl, o) {
			            // Content DOM node containing the DOM structure of the clipboard
			            //o.node.innerHTML = o.node.innerHTML + "\n-: CLEANED :-";
			        },
			    };		
			    
			  $scope.init = function(){	
			  	checkForEdit();			  	
			  	$scope.checkFormValidation_initialLoad();
				$scope.checkFormValidation();			  	
			  };

			  // detect if in edit mode or not  
			  var checkForEdit = function(){ 
				  if ($scope.editMode){
				  		var p = items.editData;
				  		var i = 0; 
						for (var key in p) {
						  if (p.hasOwnProperty(key)) {
						  	$scope.inputData[i] = p[key].
			 		  				replace(/&quot;/g, "\"").replace(/&lsquo;/g, "'").
			 		  				replace(/~`/g, "\"").replace(/`~/g, "'"); 
						  	i++;					  					   
						  }
						}	
							   
				  };		  
			  };	
			   
			  // correct for date  	  
			  $scope.setDate = function(type, index){
			  	var m = new Date();
			  	if (type == "today"){ 									$scope.inputData[index] = moment(m).format(); }
			  	if (type == "tomorrow"){ 	m.setDate(m.getDate() + 1); $scope.inputData[index] = moment(m).format(); }
			  	if (type == "yesterday"){ 	m.setDate(m.getDate() - 1); $scope.inputData[index] = moment(m).format(); }	
			  };

			  // set time
			  $scope.setTime = function(type, index){		  	
			  	var timeEntry =  moment().format( "H:mm:ss").split(':');
			  	$scope.inputData[index] = new Date( 0000, 0, 0, timeEntry[0], timeEntry[1], timeEntry[2]);				  	
			  };
			  
			  // correct for time
			  $scope.correctTime = function(index, time){ 	
			  	if ($scope.inputData[index] == undefined){
			  		$timeout(function(){
			  		  $scope.inputData[index] = time;
			  		});
			  	}else{ 
			  		 var timeEntry =  $scope.inputData[index].split(':');
			  		 $scope.inputData[index] = new Date( 0000, 0, 0, timeEntry[0], timeEntry[1], timeEntry[2]);
			  	}
			  	
			  };
 
			 // correct for time
			 $scope.decodeAdvFields = function(index){
				  // correct for advanced fields
				  var decodeEntities = (function() {
					  // this prevents any overhead from creating the object each time
					  var element = document.createElement('div');
					
					  function decodeHTMLEntities (str) {
					    if(str && typeof str === 'string') {
					      // strip script/html tags
					      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
					      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
					      element.innerHTML = str;
					      str = element.textContent;
					      element.textContent = '';
					    }
						str = str.replace(/"/g, "&quot;").replace(/'/g, "&lsquo;");
						
					    return str;
					  }
					
					  return decodeHTMLEntities;
				  })();				 	
			 	
			 	if ($scope.editMode){
			 		$timeout(function(){
			 		  $scope.inputData[index] = decodeEntities($scope.inputData[index]);
			 		});
			 	};			 	
			 };

			 // parse images that are drag and dropped
			 $scope.parseImageUpload = function(index, fieldData){		
				
			 	$scope.imageUploadParameters.limit = fieldData.limit; 	
			 	var isBlank = false; 
			 	var imageString =  $scope.inputData[index]; 
			 	if (imageString == undefined || imageString == null || imageString == ""){
			 		$scope.inputData[index] = [];
			 		isBlank = true;
			 	}
			 	if ($scope.editMode){			 		
			 		$timeout(function(){
			 		  if (!isBlank){
			 		  	
			 		  	var obj = JSON.parse($scope.inputData[index]);
								 		  				
			 		  	$scope.inputData[index] = obj;
			 		  }
			 		}); 				 		
			 	}
			 	
			 };  

	    	 // preview images 
			 $scope.previewImage = function($files, $index) {

						    var imageCount = 0; 
						    var totalImages = $files.length;
	    
						    function processLoop(){		
						    	 	
						        var reader = new FileReader();						
						        reader.onload = function (image) {		
						 			if ($scope.inputData[$index].length < $scope.imageUploadParameters.limit){     	
											
											// will resize and return thumbnail and original size data
											custom.resizeImageToDataUri(image, 200, function(data){
												
												var fullSize  = data.original; 
												var thumbnail = data.thumbnail; 
												var fileName = custom.getRandomHash(10); 
												
												// populate arrays for preview and upload
								        		$scope.inputData[$index].push( 	{name: fileName, src: fullSize, thumbnail: thumbnail, parse: true}	);		
									            $scope.fileData.push( 			{name: fileName, src: fullSize, thumbnail: thumbnail, parse: true}	);
									            
									            
									            // upload
									            $scope.imgPreview = thumbnail;
								               
								                // loop and apply
									            imageCount++;
									            if (imageCount < totalImages){
									            	processLoop();
									            }
									            else{
									            	$timeout(function(){
									            		processLoopComplete();
									            	});
									            }
											});
								    }					            
						        };
						       

						        //validate image type
						        var fileExt = ($files[imageCount].name).split('.').pop();
								if (fileExt == 'jpg' || fileExt == 'png' || fileExt == 'gif'){						       					
						        	reader.readAsDataURL($files[imageCount]);
						      	};
						      	
						    };
						    
						    function processLoopComplete(){	
						    	$scope.$apply(); 
						    }
						    
						    // start loop
						    processLoop();
						  
			 };
			
					
			 // remove image 
			 $scope.removeImage = function(index, parentIndex){
				$scope.inputData[parentIndex].splice(index, 1);					
			  };

			 // validate (initial)
			 $scope.checkFormValidation_initialLoad = function(){
			  	var count = 0;
					$timeout(function(){	
						$('#php-form .ng-invalid-required').each(function(index){ 
							count++; 
						});						
						$('#php-form .ng-invalid-time').each(function(index){
							var isRequired = $(this).attr('isRequired'); 
							if (isRequired == "true"){count++;}
							
						});
						$("#formSubmitBtn").removeClass('hidden');
					}, 1000); // second delay to ensure all fields are present
						
			  };
			  
			 // validate (subsequent)
			 $scope.checkFormValidation = function(){
				var count = 0;
					$timeout(function(){	
						$('#php-form .ng-invalid-required').each(function(index){ 
							count++; 
						});
						$('#php-form .ng-invalid-time').each(function(index){
							var isRequired = $(this).attr('isRequired'); 
							if (isRequired == "true"){count++;} 
						});					
						$scope.tableFields.meta.required = count;						
					}, 500);
			  };
			  
			  
			 // validate image upload if required
			 $scope.checkOnFileUpload = function(index){
				function isEmpty(obj) {
    				return Object.keys(obj).length === 0;
				}			  	 	
			  	var checkPromise = $interval(function () { 
			  		if ($scope.inputData[index].length > 0){
			  			delete $scope.uploadValidate[index];			  			
			  		}
			  		else{
			  			$scope.uploadValidate[index] = false;
			  		}	
			  		$scope.imageValidation = isEmpty($scope.uploadValidate); 		  		
			  	}, 500); 
			  	$scope.$on('$destroy', function () { $interval.cancel(checkPromise); });
			  };
			 
			 // buttons
			 $scope.submitPost = function () {	
						    $modalInstance.close({returnData: $scope.inputData, isEdit: $scope.editMode});
			 };
						
			 $scope.close = function(){
						  	$modalInstance.dismiss('');
			 };
			 $scope.cancel = function () {
						    $modalInstance.dismiss('cancel');
			 };			
			}],
			/* END MODAL CONTROLLER */
			//////////////////////			
				
				
				
				

			//////////////////////
			/* ADVANCED FILTER CONTROLLER */
			advSearchModalCtrl: ['items', '$timeout', '$scope', '$modalInstance', '$q', 
			function(items, $timeout, $scope, $modalInstance, $q){		
			  
			  // organize data
			  $scope.formData = items;
			  $scope.results = {message: null, length: null, data: null};
			 
			  
			  // easy reference object/array 
			  $scope.fieldData = {};
			  $scope.typeData = [];
			  for (i = 0; i < $scope.formData.fields.length; i++){
			  	$scope.fieldData[$scope.formData.fields[i]] = i;
			  } 
			  for (i = 0; i < $scope.formData.types.length; i++){
			  	$scope.typeData.push($scope.formData.types[i]);
			  } 			  
			  $scope.inputData = [];
		  	  $scope.inputData.push({type: {name: null, type: null}, operator: null, input: null});			 
			

			  // get operators
			  $scope.typeOfOperator = [
			  	{label: "Is Equal To", value: 0, actual: "="},
			  	{label: "Is Not Equal To", value: 1, actual: "!="},
			  	{label: "Is Greater Than", value: 2, actual: ">"},
			  	{label: "Is Great or Equal Than", value: 3, actual: ">="},
			  	{label: "Is Less Than", value: 4, actual: "<"},
			  	{label: "Is Less or Equal Than", value: 5, actual: "<="}
			  ];

			  $scope.typeOfOperator_text = [
			  	{label: "Contains",		 	 value: 0, actual: " LIKE "},
			  	{label: "Does Not Contain",	 value: 1, actual: " NOT LIKE "}
			  ];
			
			  
			  // get all the fields
			  var obj = $scope.formData.objToQuery[0];
			  var objFields = custom.returnAllPropertiesInObject(obj); 			  

			  // parse time and date fields
			  var locationCapture = [];			  
			  for (var i = 0; i < $scope.formData.types.length; i++){
			  	 var _type = $scope.formData.types[i]; 
			  	 if (_type == "time" || _type == "date"){
			  	 	locationCapture.push(i);	
			  	 }			  	 
			  };	
			  
			  var parseList = [];
			  for (var i = 0; i < locationCapture.length; i++){
			  	parseList.push(objFields[locationCapture[i]]);
			  };
			  			 
			  
			  for (var i = 0; i < $scope.formData.objToQuery.length; i++){
			  	var _obj = $scope.formData.objToQuery[i];
			  		for (var n = 0; n < parseList.length; n++){
			  			_obj[parseList[n]] = _obj[objFields[locationCapture[n]]].replace(/\D/g,'');
			  		}
			  }
			  
	  
			  // run search
			  $scope.runSearch = function(){
			  	
				  	var queryString = 'SELECT * FROM ' + $scope.formData.table + ' WHERE ',
				  		isFirst = true,
				  		count  = 0 ;
				  		
				  	for (var i = 0; i < $scope.inputData.length; i++){
				  		var type = $scope.inputData[i].type.name,
				  			dataType = $scope.inputData[i].type.type,
				  			operator = $scope.inputData[i].operator,
				  			input = $scope.inputData[i].input;
				  		
				  		// SANITIZE DATA TYPES
				  		if (dataType == 'time'){
				  			input = moment(input).format( "H:mm:ss");
				  		}
				  		
				  		if (type != null && operator != null && input != null){
				  			if (count != 0){ queryString += " AND ";}
				  			if ($.trim(operator).toLowerCase() == 'like' || $.trim(operator).toLowerCase() == "not like"){
				  				queryString += (type + " " +  operator + " \"%" + input + "%\""); 
				  			}
				  			else{
				  				queryString += (type + " " +  operator + " \"" + input + "\""); 	
				  			}
				  			count++;
				  		}
				  		
				  		
				  		
				  	}

					var packet = {
						database: $scope.formData.database, 
						query: queryString
					};
					
					
				    phpJS.queryDatabase(packet, function(state, data){
				    	if (state){
				    		$timeout(function(){
					    		if (data.status == "error"){
					    			$scope.results.length = 0;
					    			$scope.results.message = "No results.";
					    			
					    		}
					    		else{
					    			$scope.results.data = data;
					    			$scope.results.length = data.length;
					    			$scope.results.message = data.length + " results.";
					    		};
				    		});	
				    	};
				    	
				    });
				  	
  			};
  			
  			  $scope.useResults = function(){	
  			  	var idString = "";
  			  	for (i = 0; i < $scope.results.data.length; i++){
  			  		if (i != 0){ idString += ", "; }
  			  		idString += $scope.results.data[i].id;
  			  	}
  			  	$modalInstance.close({returnData: idString, length: $scope.results.data.length});
  			  };
			  
			  

			  $scope.close = function(){
			  	$modalInstance.dismiss('');
			  };
			
			  $scope.cancel = function () {
			    $modalInstance.dismiss('cancel');
			  };			
			}],
			/* END MODAL CONTROLLER */
			//////////////////////	

		};
		

	
	});

});
console.log("Production enviroment");

// ESTABLISH REQUIREJS PATHING
require.config({
	
	
	// URL
	baseUrl: '',
	
	// LOCATION OF SCRIPTS
	paths: {
		
		// APP				
		"app": "require/app",	
		
		// ANGULAR
		"angular": "vendor/angular/js/angularjs-1.3.14",

		// JQUERY DEPENDENT		
		"plugins": "production/js/plugins.min",		
		"jqDep":   "production/js/jqDep.min",								
						
		// ANGULAR DEPENDENT
		"ngDep": "production/js/ngDep.min",									
	
		// FIREBASE //			
		"firebaseCore": "production/js/fb.min",		
		"firebaseSettings": "production/js/firebaseSettings.min",	
					
		// ESSENTIAL BASE MODULES //
		"production-module": "production/js/module.min",
				
		// SLICK 		
		"slickCore": "vendor/core/slick-1.5.min",													//http://kenwheeler.github.io/slick/
		"slick": "vendor/angular/module/angular-slick",												//https://github.com/vasyabigi/angular-slick/tree/master/dist
		
		// ADVANCED EDITOR 				
		"tinymceCore": "vendor/tinymce/tinymce.min",
		"ui.tinymce": "vendor/tinymce/ui.tinymce",


		// PAGE CONTROLLERS //
		"appCtrl": "production/controllers/combined/controllers.min",			

		// MODALS //
		"editorModalCtrl": "production/modals/editor-modalControls.min",		
		"uiModalCtrl": "production/modals/ui-modalControls.min",									
		"accountModalCtrl": "production/modals/account-modalControls.min",
		"formsModalCtrl": "production/modals/forms-modalControls.min",
		

		// FACTORIES //
		"custom": "production/js/custom.min",
		"sharedData": "production/js/shared.min",
		"konami": "production/js/konami.min",


		// ADD TO GRUNT LATER
		/*
		"papaParse": "vendor/papaparse/papaparse.min",											//http://papaparse.com/			
		"contenteditable": "vendor/angular/module/angular-contenteditable",						//https://github.com/akatov/angular-contenteditable
		"ui.utils": "vendor/angular/module/ui-utils.min",										//http://angular-ui.github.io/
		"FBAngular": "vendor/angular/module/angular-fullscreen",								//https://github.com/fabiobiondi/angular-fullscreen		
		"dcbImgFallback": "vendor/angular/module/angular.dcb-img-fallback.min",					//http://ngmodules.org/modules/angular-img-fallback							
		"gd.ui.jsonexplorer": "vendor/jsonexplorer/js/gd-ui-jsonexplorer",						//https://github.com/Goldark/ng-json-explorer	
		"checklist-model": "vendor/angular/module/checklist-model",								//https://github.com/vitalets/checklist-model		
		"ngClickSelect": "vendor/angular/module/ng-click-select",								//https://github.com/adjohnson916/ng-click-select
		"angular-table": "vendor/angular/module/smartTable.min",								//http://lorenzofox3.github.io/smart-table-website/	
		"ngIdle": "vendor/angular/module/angular-idle",											//http://ngmodules.org/modules/ng-idle
		"angulartexteditor": "vendor/angular/module/angularTextEditor",                         //https://github.com/vitconte/angularTextEditor
		"ui.tree": "vendor/uiTree/js/angular-ui-tree.min",										//https://github.com/JimLiu/angular-ui-tree
		"ng-mfb": "vendor/floatingButtons/mfb-directive",										//https://github.com/nobitagit/ng-material-floating-button/blob/master/src/index.html
		"adaptive.youtube": "vendor/angular/module/angular-adaptive-youtube.min",				//https://github.com/angular-adaptive/adaptive-youtube
		// +  D3 GRAPHS 
		"d3Core":"vendor/d3/js/d3.min",
		"angularCharts":"vendor/d3/js/angular-charts",											//http://chinmaymk.github.io/angular-charts/
		"ngDonut":"vendor/d3/js/donut",															//https://github.com/Wildhoney/ngDonut
		"n3-line-chart":"vendor/d3/js/lineCharts.min",											//https://github.com/n3-charts/line-chart
		*/
		

	},

    //DEPENDENCIES
    shim: {        
        
        // ANGULAR
        "angular": { 
        	deps: ['jqDep'],       	
            exports: "angular"
        },  
        
  
        // JQUERY DEPENDENT MODULES
        "ngDep": {
        	deps: ['angular', 'jqDep'],
            exports: "angular"
        },  
        
        // STANDALONE MODULES
        "production-module":{
			deps: ['angular', 'ngDep'],
        	exports: "angular" 	  
        },
                
    	//FIREBASE
        "firebaseSettings":{
        	deps: ['angular', 'ngDep', 'firebaseCore']
        },        
           
        //SHARED SERVICES
		"custom":{
        	deps: ['jqDep', 'ngDep']
        },  
		"sharedData":{
        	deps: ['angular', 'ngDep', 'firebaseCore']
        },            
        
        
        // SLICK 
        "slickCore":{
        	deps: ['jqDep']
        },
        "slick":{
        	deps: ['slickCore', 'angular'],
        	exports: "angular"
        },
        
        // EDITOR
		"tinymceCore":{
			deps: ['jqDep']
		},
        "ui.tinymce":{
			deps: ['tinymceCore', 'angular'],
        	exports: "angular" 	       	
        },         
		

                      
      
        // MODALS    
        "uiModalCtrl":{
			deps: ['angular'],
        	exports: "angular"   	
        },
        "accountModalCtrl":{
			deps: ['angular'],
        	exports: "angular"   	
        },     
        "formsModalCtrl":{
			deps: ['angular'],
        	exports: "angular"      	
        },      
        "editorModalCtrl":{
			deps: ['angular'],
        	exports: "angular"   
        },
           

    }	
	
});
 	 


// INITALIZE ANGULAR IN CONTROLLERS
require(	['app', 'appCtrl'], function (	app, appCtrl ) {
	
	// PULL FROM DATABASE BEFORE INITIATLIZING ANGULAR
	if(_global_setup.databaseConnection){
		phpJS.returnJsonAsObject("/../production/settings/systemdata/system.json", function(state, data){  
			
			_global_setup.theme = data.theme;
			_global_setup.pages = data.systemdata.pages;	
			_global_setup.permissionTypes = data.systemdata.permissionTypes;				
			app.init();															
		});
	}

	
});



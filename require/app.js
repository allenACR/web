define("app", 	[	
					// FACTORIES
					"custom", "firebaseSettings", 

					// MODAL
					"editorModalCtrl",
					"uiModalCtrl", 
					"accountModalCtrl",
					"formsModalCtrl",					
						
					// ESSENTIAL
					"production-module",
										
					// UNIQUE
					"slick",
					"ui.tinymce",
							
					
				], function(custom, firebaseSettings) { 
	
	
	
	////////////////
	// INIT 
	var app = angular.module('app', [	
	
					
					// FIREBASE
					"firebase",
					
					// MODAL
					"editorModalCtrl",
					"uiModalCtrl", 
					"accountModalCtrl",
					"formsModalCtrl",
					
					
					// UNIQUE
					"slick",
					"ui.tinymce",
										
					// MODULES
					"ngStorage",
					"ngAnimate", 
					"ngSanitize",
					"SmoothScroll",
					"mm.foundation", 
					"angular-loading-bar", 	
					"ui.router", 
					"angularLoad", 
					"formly",					
					"readMore",					 
					"string", 
					"truncate", 
					"angular-centered",
					"angular-gap", 	
					"adaptive.detection", 	
					"simplifield.dragndrop",					
					"sticky", 
					"monospaced.elastic",
					"lrFileReader", 
					"ngBrowserInfo",
					"psResponsive",  
					"toaster",	
					"angularSpinkit", 	
					"angularMoment",
					"sbDateSelect",	
					"ps.inputTime",
					"ngjsColorPicker",
					"ngCovervid",
					"transactionManager",
					"ngPatternRestrict",
					"angularFileUpload",
					"multi-select", 
					"ui.utils",
					"infinite-scroll",
					"ngClickSelect"
				
					
					
	]);
	
										
	app.init = function () {
		
		if (_global_setup.databaseConnection){
			firebaseSettings.setDefaultUserData(function(){
				angular.bootstrap(document, ['app']);	
			});
		};
	           
	};
	////////////////
	
	// --- FACTORIES --
	app.factory('myOffCanvas', function (cnOffCanvas) {
	  return cnOffCanvas({
	    controller: 'headerCtrl',
	    templateUrl: 'components/offcanvas/offcanvas.php'
	  });
	}).
	// --- end factories --/
	
	// --- FILTERS --
	filter('trusthtml', ['$sce', function ($sce) {
            return function(t) {
                return $sce.trustAsHtml(t);
            };
    }]).
	
	filter('timeOnly', function() {
            return function(t) {
               date = "February 04, 2011 " + t;
               return date;
            };
    }).
    
	filter('removeExtension', function() {
            return function(str) {
               return str.replace(/\.[^/.]+$/, "");
            };
    }).    
    
  	filter('capitalize', function() {
		return function(input, all) {
      		return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    	};				
    }).  
		
	
	// example:  <input ng-model="entry.Field" nospace>   removes all white space when entering 
	filter('nospace', function () {
	    return function (value) {
	        return (!value) ? '' : value.replace(/ /g, '');
	    };
	}).  
	// --- end filters --  	
	
	//--- DIRECTIVES --
	directive('nospace', function() {
	  return {
	    require: 'ngModel',
	    link: function(scope, element, attrs, ngModelController) {
	      ngModelController.$parsers.push(function(value) {
	        return (!value) ? '' : value.replace(/ /g, '');
	      });
	      ngModelController.$formatters.push(function(value) {
	       return (!value) ? '' : value.replace(/ /g, '');
	      });
	    }
	  };
	}).
	
	directive('styleParent', function(){ 
	   return {
	     restrict: 'A',
	     link: function(scope, elem, attr) {
	         elem.on('load', function() {
	            var w = $(this).width(),
	                h = $(this).height();
					//$('#mainSlider').css('min-height', h + 'px');
					//$('#headerFiller').css('height', h + 'px'  ) ;
	                //check width and height and apply styling to parent here.
	         });
	     }
	   };
	}).		
	
	directive('imagefill', function ($compile) {
	    return {
	        restrict: 'AE',
	        scope:{ 
	        	fillsrc: '=',
	       		fillheight: '='
	       	},
	        link: function(scope, elem, attr) {
	        	var validate = false;
	        	if (scope.fillsrc.includes("png") || scope.fillsrc.includes("gif") || scope.fillsrc.includes("jpg")){
	        		validate = true;
	        	}	
	        	if (validate){
	        		$(elem).css({'width': '100%',  'height': scope.fillheight, 'background-color': 'black', 'margin-top': '10px'});	        	
	        		custom.backstretchElement($(elem), scope.fillsrc, 500, 500);
	        	};
	        },
	        
	    };
	}).
	
	// -- end directives
			
	//----- CONTROLLERS  
	controller('MyOffCanvasCtrl', function (myOffCanvas) {
	  this.toggle = myOffCanvas.toggle;
	}).
		
		

		
	////////////////  CONFIG
	//
	config(function($stateProvider, $urlRouterProvider) {
			
			// For any unmatched url, redirect to /state1
			$urlRouterProvider.otherwise("/" + _global_setup.pages[0].label);
		  
			for (var i = 0; i < _global_setup.pages.length; i++){
				var page = _global_setup.pages[i],
					label = page.label,
					actual = page.actual;
					
					// Now set up the states
					if (page.deployed){
					$stateProvider
					    .state(actual, {
					      url: "/" + label + "?search",
					      views: {
					        "overlay": { templateUrl: "layout/overlay/overlay.html",
					        			controller: "overlayController"
					        },				      				      	
					        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
					        			controller: "offcanvasController"
					        },	
					        /*
					        "slider": { templateUrl: "layout/slider/slider.php",
					        			controller: "sliderController"
					        },
					        */
					        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
					        			
					        },
					        "account-modals": { templateUrl: "modals/account/account-modals.html"
					        			
					        },	
					        "forms-modals": { templateUrl: "modals/forms/forms-modals.html"
					        			
					        },	
					        "editor-modals": { templateUrl: "modals/editor/editor-modals.html"
					        			
					        },			        			        			        			      	
					        "header": { templateUrl: "components/header/header.php",
					        			controller: "headerController"
					        },
					        "footer": { templateUrl: "components/footer/footer.php",
					        			controller: "footerController"
					        },	
					        
					        "body": { 	templateUrl: "components/shared/shared.php",
					        			controller: "sharedController"
					         },					        	
   		         
					      },	
						  onEnter: function(){
						   custom.transitionStart();
						  },
						  onExit: function(){
						    custom.transitionEnd();
						  },				  			      		      
					      
					    });	
					  };				
			};	

			//-- UNIQUE PAGES --//
			$stateProvider			    
			    .state('config', {
			      url: "/config",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },	
			        "editor-modals": { templateUrl: "modals/editor/editor-modals.html"
			        			
			        },			        
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },	
			        "forms-modals": { templateUrl: "modals/forms/forms-modals.html"
			        			
			        },				        			        					        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/config/config.php",
			        			controller: "configController"
			         },
			      },
			      onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },
			  	});			
			$stateProvider			    
			    .state('user', {
			      url: "/user",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },	
			        "editor-modals": { templateUrl: "modals/editor/editor-modals.html"
			        			
			        },			        
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },	
			        "forms-modals": { templateUrl: "modals/forms/forms-modals.html"
			        			
			        },				        			        					        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/user/user.php",
			        			controller: "userController"
			         },
			      },
			      onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },
			  	});
			$stateProvider
			    .state('firstLogin', {
			      url: "/firstLogin/:email/{tempPassword}",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },
			        "editor-modals": { templateUrl: "modals/editor/editor-modals.html"
			        			
			        },				        
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },
			        "forms-modals": { templateUrl: "modals/forms/forms-modals.html"
			        			
			        },				        				        						        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/firstLogin/firstLogin.php",
			        			controller: "firstLoginController"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			});
			$stateProvider
			    .state('redirect', {
			      url: "/redirect",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },
			        "editor-modals": { templateUrl: "modals/editor/editor-modals.html"
			        			
			        },				        	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },
			        "forms-modals": { templateUrl: "modals/forms/forms-modals.html"
			        			
			        },				        				        						        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/redirect/redirect.php",
			        			controller: "redirectController"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			});				
			//--------//		     
			    			    
			    

	    			       
		});
		//
		////////////////   
	
	return app;
	
});


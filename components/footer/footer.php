<div class="" style='' ng-init="initCore.init()">
	
	<div class='full-width' ng-repeat='script in pagedata.prefooter'>     		
		<ng-include  ng-if='script.deploy'  src="'templates/general/' + script.folder + '/' + script.template"></ng-include>
	</div>   	
	<div id='footer-div'>
		
		<div class="flat-secondary fonttype-1" >
			<centered>
				<div id='inner-footer'>
					<br><br>	
					
					<!-- FOOTER -->
					<small>Code & Logic is created by Allen Royston | 2010-2015</small><br>
					<small>Thanks for checking out my site!</small>
					<br><br><br>
					
					<!-- INFO -->
					<a class="button tiny" ng-click="mainCore.browserInfo()">Browser Info</a>		
				</div>
			</centered>
		</div>
		
	</div>
	<div class='full-width' ng-repeat='script in pagedata.postfooter'>     		
		<ng-include  ng-if='script.deploy'  src="'templates/general/' + script.folder + '/' + script.template"></ng-include>
	</div>  	
</div>
<div id='header-div' ng-init="initCore.init()">


				<div ng-class="{true: 'visible', false: 'hidden'}[page.isLoaded]">		
							<div id='headerGap'>
								
					        	<div class='full-width' ng-repeat='script in pagedata.preheaders'>     		
					        		<ng-include  ng-if='script.deploy'  src="'templates/general/' + script.folder + '/' + script.template"></ng-include>
					        	</div>   								
							</div>
							<div ng-if='page.gapLoaded && page.browserType != "mobile"'>				
								<div sticky id="headerMain"  style="width: 100%; z-index: 990000"> 
										
										<div class="header-bar">
											<div class="row full-width no-animation">
												
												
												<!-- LEFT SIDE -->
												<div class="small-8 columns pull-left" style='text-align: left'>
													
														<span>
															 
															 <i style="margin-right: 10px" ng-class="{	desktop: 'fa fa-desktop ', 
																			large: 'fa fa-laptop ', 
																			medium: 'fa fa-tablet ', 
																			small: 'fa fa-mobile ', 
																			mobile: 'fa fa-mobile'}[masterData.ios]">
														     </i> 
														    
														     
														     <span ng-repeat='entry in pages'>
															     <a style="margin-right: 10px" ng-if='entry.deployed' ui-sref="{{entry.actual}}">{{entry.label}}</a>
															 </span>
														</span>
													
												</div>
												<!-- end right side -->
					
												
												<!-- RIGHT SIDE -->
												<div class="small-1 columns pull-right">											
													<div ng-class="{false: 'hidden'}[page.isLoaded]">				
														<a ng-click="mainCore.login()" ng-class="{true: 'hidden', false: 'visible', null: 'visible'}[masterData.logState]">Login</a>					
														<a ng-click="mainCore.logout()" ng-class="{false: 'hidden', true: 'visible', null: 'hidden'}[masterData.logState]">Logout</a>
													</div>
												</div>		
												
												
												<div class="small-2 columns pull-right">			
													<div ng-class="{false: 'hidden'}[page.isLoaded]">				
														<a ui-sref="user">My Account</a>												
													</div>									
												</div>		
												<!-- end right side -->							
												
												
											</div>
												
								</div>	
									
							</div>
						</div>
			        	<div class='full-width' ng-repeat='script in pagedata.postheaders'>     		
			        		<ng-include  ng-if='script.deploy'  src="'templates/general/' + script.folder + '/' + script.template"></ng-include>
			        	</div>  			
			        	
				</div>
				
</div>



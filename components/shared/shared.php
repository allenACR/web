<div id='<?php $path = basename(__DIR__);echo $path;?>_id' class="content-default-height " ng-init="initCore.init()">
    <div id='nt-layout-button-group' class='hidden'>
        <div id='nt-search-btn' class='isPointer' ng-if='collection.pagination.showQuickSearch' ng-show='!page.settings.noTable' ng-click="premadeCollection.toggleSideButtons('search')" ng-class='{search: "nt-group-sidebar-btn-show", 
				 			advsearch: "nt-group-sidebar-btn-hide", 
				 			settings: "nt-group-sidebar-btn-hide",
				 			layout: "nt-group-sidebar-btn-hide",
				 			none: "nt-group-sidebar-btn-hide"}[page.sidebar]'>
            <i class="fa fa-search"></i>
        </div>
        <div id='nt-adv-search-btn' class='isPointer' ng-if='collection.pagination.showAdvancedSearch' ng-show='!page.settings.noTable' ng-click="premadeCollection.toggleSideButtons('advsearch');" ng-class='{search: "nt-group-sidebar-btn-hide", 
				 			advsearch: "nt-group-sidebar-btn-show", 
				 			settings: "nt-group-sidebar-btn-hide", 
				 			layout: "nt-group-sidebar-btn-hide",
				 			none: "nt-group-sidebar-btn-hide"}[page.sidebar]'>
            <i class="fa fa-search-plus"></i>
        </div>
        <div id='nt-editor-btn' class='isPointer' ng-if='masterData.accessLevel.level> 8' ng-click="premadeCollection.toggleSideButtons('settings')" ng-class='{search: "nt-group-sidebar-btn-hide", advsearch: "nt-group-sidebar-btn-hide", settings: "nt-group-sidebar-btn-show", layout: "nt-group-sidebar-btn-hide", none: "nt-group-sidebar-btn-hide"}[page.sidebar]'>
            <i class="fa fa-power-off"></i>
        </div>
        <div id='nt-layout-btn' class='isPointer' ng-if='masterData.accessLevel.level> 8 && !page.settings.noTable' ng-click="premadeCollection.toggleSideButtons('layout');" ng-class='{search: "nt-group-sidebar-btn-hide", advsearch: "nt-group-sidebar-btn-hide", settings: "nt-group-sidebar-btn-hide", layout: "nt-group-sidebar-btn-show", none: "nt-group-sidebar-btn-hide"}[page.sidebar]'>
            <i class="fa fa-newspaper-o"></i>
        </div>
    </div>
    <div class="covervid-wrapper" ng-if='page.settings.bgType == "video"'>
        <covervid class="covervid-video" autoplay loop>
            <source ng-src="{{'themes/available/' + page.theme + '/video/' +  page.settings.videoBG}}" type="video/mp4">
        </covervid>
    </div>
    <div id='parallax-container' data-speed="{{page.settings.parallaxSpeed * .1}}" data-offset="0" data-type="background"></div>
    <ng-include src="'layout/editor/editor.html'"></ng-include>
    <ng-include id='nt-search-bar' ng-if='collection.pagination.showAdvancedSearch' src="page.theming['searchbar.html']"></ng-include>
    <div ng-if="masterData.browserSize == 'small' || masterData.browserSize == 'medium' || masterData.browserSize == 'large'">
        <ng-include src="page.theming.main"></ng-include>
    </div>
</div>
module.exports = function(grunt) {
	

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

	
		//--------------------------------------------------------	
		concat: {
			build:{
				files:[
					// firebase
					{
						src: [
							'vendor/firebase/firebase.js',											//https://github.com/firebase/angularFire
							'vendor/firebase/firebase-simple-login.js'								//https://cdn.firebase.com/js/simple-login/1.6.3/firebase-simple-login.js	
						],
						dest: 'production/js/fb.js'
					},
					// jquery dependents
					{
		      			src: [		            		            
				            'vendor/core/jquery-2.1.3.min.js',										//jquery comes first - all others are dependent on it				            
				            'vendor/backstretch/backstretch.js',									//http://srobbin.com/jquery-plugins/backstretch/			            
				            'vendor/angular/module/transit.js',										//http://ricostacruz.com/jquery.transit/          		            
				            'vendor/foundation5/js/responsive-tables',								//http://zurb.com/playground/responsive-tables
				            'vendor/moment/moment.min.js',											//http://momentjs.com/
				            'vendor/foundation5/js/foundation.min.js',								//http://foundation.zurb.com/ 		            	
				            
				            'php/phpJS.js'															// php file	            		              																														
		       			],
		        		dest: 'production/js/jqDep.js'												
					},
					// angularDependent
					{
				        src: [
				            'vendor/angular/module/ngTouch.js',		
				            'vendor/angular/module/angular-moment.js',    		                		 			            																		
				        ],
				        dest: 'production/js/ngDep.js'
					},
					//  modules
					{
				        src: [
				            //'vendor/angular/module/*.js' // All JS in the libs folder
				            // modules		            
				            'vendor/angular/module/ang-date.js', 									//https://github.com/sambs/angular-sb-date-select						
				            'vendor/angular/module/ang-time.js',									//https://github.com/randallmeeker/ps-input-time
				            'vendor/angular/module/angular-adaptive-detection.min.js', 				//https://github.com/angular-adaptive/adaptive-detection,
				            'vendor/angular/module/angular-readmore.js',							//https://github.com/pattysnippets/angular-readmore
				            'vendor/angular/module/simplifield.dragndrop.js',						//http://ngmodules.org/modules/simplifield.dragndrop
				            'vendor/angular/module/sticky.js',										//https://github.com/d-oliveros/angular-sticky
				            'vendor/angular/module/textareaFit.js',									//https://github.com/monospaced/angular-elastic
				            'vendor/angular/module/ngStorage.min.js',								//https://github.com/gsklee/ngStorage
				            'vendor/angular/module/angular-sanitize.js',							//https://github.com/angular/bower-angular-sanitize
				            'vendor/angular/module/ng-string.min.js',								//https://github.com/goodeggs/ng-string
				            'vendor/angular/module/fileReader.js',									//https://github.com/matteosuppo/angular-filereader
				            'vendor/angular/module/truncate.js',									//https://github.com/sparkalow/angular-truncate
				            'vendor/angular/module/angular-centered.js',							//http://ngmodules.org/modules/angular-centered
				            'vendor/angular/module/toaster.js',										//https://github.com/jirikavi/AngularJS-Toaster
				            'vendor/angular/module/ngBrowserInfo.min.js',							//https://github.com/transferwise/ng-browser-info
				            'vendor/angular/module/ps-responsive.js',								//https://github.com/lavinjj/angular-responsive
				            'vendor/angular/module/angular-spinkit.js',								//https://github.com/urigo/angular-spinkit
				            'vendor/angular/module/angular-ui-router.js',							//https://github.com/angular-ui/ui-router
				            'vendor/angular/module/formly.js',										//https://github.com/nimbly/angular-formly
				            'vendor/angular/module/angular-load.min.js',							//https://github.com/urish/angular-load
				            'vendor/angular/module/ng-smoothscroll.min.js',							//https://github.com/gsklee/ngStorage
				            'vendor/angular/module/ngCovervid.min.js',								//https://github.com/jfeigel/ngCovervid
				            'vendor/angular/module/angular-transactionManager.js',					//https://github.com/marco64bit/Angular-Transaction-manager
				            'vendor/angular/module/ng-pattern-restrict.js',							//https://github.com/AlphaGit/ng-pattern-restrict
				            'vendor/angular/module/angular-file-upload.js',							//https://github.com/danialfarid/angular-file-upload
				            'vendor/angular/module/ui-utils.min.js',								//https://github.com/angular-ui/ui-utils
				            'vendor/angular/module/ngInfiniteScroll.js',							//https://github.com/sroze/ngInfiniteScroll
				            'vendor/angular/module/ng-click-select.js',								//https://github.com/adjohnson916/ng-click-select
				            
				            // firebase
				            'vendor/firebase/angularfire.min.js', 									//https://www.firebase.com/docs/web/libraries/angular/quickstart.html
				            
				            // modules with css
				            'vendor/angular/module/loading-bar.js', 								//http://chieffancypants.github.io/angular-loading-bar/
				            'vendor/foundation5/js/foundation5.js',									//http://madmimi.github.io/angular-foundation/		            
				            'vendor/multiSelect/js/angular-multi-select.js',						//http://isteven.github.io/angular-multi-select/#/main
				            'vendor/colorPicker/js/ngjs-color-picker.js',							//https://github.com/simeg/ngjs-color-picker		            
				            'vendor/angular/module/angular-gap.js',									
				            'vendor/angular/module/ang-animate.js',												 
				        ],
				        dest: 'production/js/module.js',						
					}					
					
				]				
			},	
		    css:{
		    	files: [
					{
				        src: [							
							// reset
							"css/reset.css",
							
							//vendor
							"vendor/foundation5/css/normalize.css",
							"vendor/centered/css/angular-centered.css",
							"vendor/foundation5/css/foundation.css",
							"vendor/foundation5/css/responsive-tables.css",
							"vendor/loadingBar/css/loading-bar.css",
							"vendor/toaster/css/toaster.css",
							"vendor/uiTree/css/angular-ui-tree.min.css",
							"vendor/jsonexplorer/css/gd-ui-jsonexplorer.css",
							"vendor/multiSelect/css/angular-multi-select.css",
							"vendor/floatingButtons/floating-button.css",
							"vendor/colorPicker/css/ngjs-color-picker.css",
				            
							// custom
							"css/editor.css",
							"css/hint.css",
							"css/slick.css",
							"css/style.css",
							"css/readmore-style.css",
							"css/custom.css",
							"css/animation.css",
							"css/dropzone.css",
							"css/parallax.css", 															
				        ],
				        dest: 'production/css/production.css',						
					}		    	
		    	
		    	]
		    },					
		    combine:{
		      files: [
		        {src: ['production/controllers/wrapper/open.js', 'production/controllers/stripped/**', 'production/controllers/wrapper/close.js'], dest: 'production/controllers/combined/controllers.js'},
		      ]
		    },

		    
		},	
		//--------------------------------------------------------	
			
		//--------------------------------------------------------	
		copy: {
		  
		  production:{ 
		  	files:[
			  	{src: 'components/config/controller.dev.js',   		dest: 'production/controllers/src/config.js'},
			  	{src: 'components/firstLogin/controller.dev.js',	dest: 'production/controllers/src/firstLogin.js'},		  			  	
			  	{src: 'components/footer/controller.dev.js',   		dest: 'production/controllers/src/footer.js'},
			  	{src: 'components/header/controller.dev.js',   		dest: 'production/controllers/src/header.js'},
			  	{src: 'components/master/controller.dev.js',   		dest: 'production/controllers/src/master.js'},
			  	{src: 'components/redirect/controller.dev.js',  	dest: 'production/controllers/src/redirect.js'},
			  	{src: 'components/shared/controller.dev.js',  		dest: 'production/controllers/src/shared.js'},
			  	{src: 'components/user/controller.dev.js',   		dest: 'production/controllers/src/user.js'},
			  	
			  	{src: 'layout/offcanvas/controller.dev.js',  		dest: 'production/controllers/src/offcanvas.js'},
			  	{src: 'layout/overlay/controller.dev.js',  			dest: 'production/controllers/src/overlay.js'},
			  	{src: 'layout/slider/controller.dev.js',   			dest: 'production/controllers/src/slider.js'}			  	
		  	],
		  }
		},	
		//--------------------------------------------------------		
		
		//--------------------------------------------------------	
		'string-replace': {
		  production: {
			files: [
				{src: 'production/controllers/src/config.js', 		dest: 'production/controllers/stripped/config.js'},
				{src: 'production/controllers/src/firstLogin.js', 	dest: 'production/controllers/stripped/firstLogin.js'},
				{src: 'production/controllers/src/footer.js', 		dest: 'production/controllers/stripped/footer.js'},
				{src: 'production/controllers/src/header.js', 		dest: 'production/controllers/stripped/header.js'},
				{src: 'production/controllers/src/master.js', 		dest: 'production/controllers/stripped/master.js'},
				{src: 'production/controllers/src/redirect.js', 	dest: 'production/controllers/stripped/redirect.js'},
				{src: 'production/controllers/src/shared.js', 		dest: 'production/controllers/stripped/shared.js'},
				{src: 'production/controllers/src/user.js', 		dest: 'production/controllers/stripped/user.js'},
				
				{src: 'production/controllers/src/offcanvas.js', 	dest: 'production/controllers/stripped/offcanvas.js'},
				{src: 'production/controllers/src/overlay.js', 		dest: 'production/controllers/stripped/overlay.js'},
				{src: 'production/controllers/src/slider.js', 		dest: 'production/controllers/stripped/slider.js'}				
			],
			options: {
			  replacements: [
			  {
			    pattern: "define(['custom', 'sharedData', 'firebaseSettings', 'app'], function(custom, sharedData, firebaseSettings, app){//startWrapper",
			    replacement: '//start'
			  },{
			    pattern: '});//endwrapper',
			    replacement: '//end'
			  }]
			}
		  }
		},		
		//--------------------------------------------------------	
		
		//--------------------------------------------------------	
		uglify: {	
			
			build:{
				files: [
					{src: 'vendor/core/slick-1.5.js',							dest: 'vendor/core/slick-1.5.min.js'}			
				]
			},
			
			
			development:{
				files: [
					// main
					{src: 'require/development/main.dev.js', 					dest: 'require/main.js'},	
				]	
			},
			
			
			production:{
				files: [
					// controllers
					{src: 'production/controllers/combined/controllers.js', 	dest: 'production/controllers/combined/controllers.min.js'},					
					// main/app
					{src: 'require/product/main.product.js', 					dest: 'require/main.js'},
					
					// ----- dependencies --------------------
					// jquery 
					{src: 'production/js/jqDep.js', 							dest: 'production/js/jqDep.min.js'},					
					// firebase
					{src: 'production/js/fb.js', 								dest: 'production/js/fb.min.js'},
					// module dependencies
					{src: 'production/js/ngDep.js', 							dest: 'production/js/ngDep.min.js'},	
					// modules
					{src: 'production/js/module.js',  					   		dest: 'production/js/module.min.js'},
					
					// ----- modals --------------------
					// forms
					{src: 'modals/forms/forms-modalControls.js',		    	dest: 'production/modals/forms-modalControls.min.js'},
					//editor
					{src: 'modals/editor/editor-modalControls.js',		    	dest: 'production/modals/editor-modalControls.min.js'},
					//account
					{src: 'modals/account/account-modalControls.js',	   		dest: 'production/modals/account-modalControls.min.js'},
					//ui		
					{src: 'modals/ui/ui-modalControls.js',		    			dest: 'production/modals/ui-modalControls.min.js'},
					
					//------ packages -------------------					
					{src: 'production/js/jqDep.js',	        					dest: 'production/js/jqDep.min.js'},		// jquery dependents
					{src: 'production/js/fb.js',		     			   		dest: 'production/js/fb.min.js'},			// firebase
					{src: 'production/js/ngDep.js',    							dest: 'production/js/ngDep.min.js'},		// angular dependents
					{src: 'production/js/module.js',	     			   		dest: 'production/js/module.min.js'},		// modules		
					
					//------ factories ------------------					
					{src: 'factory/custom.dev.js',	        					dest: 'production/js/custom.min.js'},				// custom 
					{src: 'factory/firebaseSettings.dev.js',		       	  	dest: 'production/js/firebaseSettings.min.js'},		// firebase
					{src: 'factory/konami.dev.js',		     			  	  	dest: 'production/js/konami.min.js'},				// konami code
					{src: 'factory/shared.dev.js',    							dest: 'production/js/shared.min.js'}				// shared					
				],
			}
		},
		//--------------------------------------------------------	
			
		
		//--------------------------------------------------------
		cssmin: {
		  production: {
			    files: [{
			      expand: true,
			      cwd: 'production/css',
			      src: ['*.css', '!*.min.css'],
			      dest: 'production/css',
			      ext: '.min.css'
			    }]
		  }
		},
		//--------------------------------------------------------


		//--------------------------------------------------------
		htmlclean: {                                     	
		    production: {  
		    	options:{
		    		
		    	}, 
		    	files:[                                 
			      {src: 'index.php',     											dest: 'index.php'},
			      
			      // components
			      {src: 'components/config/config.php',								dest: 'components/config/config.php'},
			      {src: 'components/shared/shared.php',								dest: 'components/shared/shared.php'},
			      
			      // layout
			      {src: 'layout/editor/editor.html',								dest: 'layout/editor/editor.html'},
			      {src: 'layout/offcanvas/offcanvas.html',							dest: 'layout/offcanvas/offcanvas.html'},
			      {src: 'layout/overlay/overlay.html',								dest: 'layout/overlay/overlay.html'},
			      {src: 'layout/slider/slider.php',									dest: 'layout/slider/slider.php'},
			      
			      // fragments
			      {src: 'fragments/components/advancedOptions.html',				dest: 'fragments/components/advancedOptions.html'},
			      {src: 'fragments/components/clearbutton.html',					dest: 'fragments/components/clearbutton.html'},
			      {src: 'fragments/components/createEntry.html',					dest: 'fragments/components/createEntry.html'},
			      {src: 'fragments/components/deleteEntry.html',					dest: 'fragments/components/deleteEntry.html'},
			      {src: 'fragments/components/editEntry.html',						dest: 'fragments/components/editEntry.html'},
			      {src: 'fragments/components/filterSearch.html',					dest: 'fragments/components/filterSearch.html'},
			      {src: 'fragments/components/loadingScreen.html',					dest: 'fragments/components/loadingScreen.html'},
			      {src: 'fragments/components/multiButtons.html',					dest: 'fragments/components/multiButtons.html'},
			      {src: 'fragments/components/multipButtons.html',					dest: 'fragments/components/multipButtons.html'},
			      {src: 'fragments/components/singleButtons.html',					dest: 'fragments/components/singleButtons.html'},
			      {src: 'fragments/components/totalEntries.html',					dest: 'fragments/components/totalEntries.html'},
			      
			      // templates
			      {src: 'themes/templates/default/layout/html/empty.html',			dest: 'themes/templates/default/layout/html/empty.html'},
			      {src: 'themes/templates/default/layout/html/layout.html',			dest: 'themes/templates/default/layout/html/layout.html'},
			      
			       
				]
		    },
		},	
		//--------------------------------------------------------
		
		//--------------------------------------------------------
		prettify: {
			development:{
			    options: {
				  "indent": 4,
				  "condense": true,				  
				  "indent_inner_html": true,
				  "brace_style": "collapse",
				  "unformatted": [
				    "a",
				    "pre"
				  ]
			    },
			    files: [
			      {src: 'index.php',     								dest: 'index.php'},
			      
			      // components
			      {src: 'components/config/config.php',					dest: 'components/config/config.php'},
			      {src: 'components/shared/shared.php',					dest: 'components/shared/shared.php'},
			      
			      // layout
			      {src: 'layout/editor/editor.html',								dest: 'layout/editor/editor.html'},
			      {src: 'layout/offcanvas/offcanvas.html',							dest: 'layout/offcanvas/offcanvas.html'},
			      {src: 'layout/overlay/overlay.html',								dest: 'layout/overlay/overlay.html'},
			      {src: 'layout/slider/slider.php',									dest: 'layout/slider/slider.php'},			      
			      
			      // fragments
			      {src: 'fragments/components/advancedOptions.html',				dest: 'fragments/components/advancedOptions.html'},
			      {src: 'fragments/components/clearbutton.html',					dest: 'fragments/components/clearbutton.html'},
			      {src: 'fragments/components/createEntry.html',					dest: 'fragments/components/createEntry.html'},
			      {src: 'fragments/components/deleteEntry.html',					dest: 'fragments/components/deleteEntry.html'},
			      {src: 'fragments/components/editEntry.html',						dest: 'fragments/components/editEntry.html'},
			      {src: 'fragments/components/filterSearch.html',					dest: 'fragments/components/filterSearch.html'},
			      {src: 'fragments/components/loadingScreen.html',					dest: 'fragments/components/loadingScreen.html'},
			      {src: 'fragments/components/multiButtons.html',					dest: 'fragments/components/multiButtons.html'},
			      {src: 'fragments/components/multipButtons.html',					dest: 'fragments/components/multipButtons.html'},
			      {src: 'fragments/components/singleButtons.html',					dest: 'fragments/components/singleButtons.html'},
			      {src: 'fragments/components/totalEntries.html',					dest: 'fragments/components/totalEntries.html'},
			      
			      // templates
			      {src: 'themes/templates/default/layout/html/empty.html',			dest: 'themes/templates/default/layout/html/empty.html'},
			      {src: 'themes/templates/default/layout/html/layout.html',			dest: 'themes/templates/default/layout/html/layout.html'},			      
			      
			    ]
		   }
		}
		//--------------------------------------------------------		
		
    });

    // 3. used pluginms
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-htmlclean');
	grunt.loadNpmTasks('grunt-ngmin');
	grunt.loadNpmTasks('grunt-prettify');
	

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', 	['ngAnnotate', 'concat', 'uglify', 'cssmin']);
    

	grunt.registerTask('css', 		['concat:css', 'cssmin:production']);		
    grunt.registerTask('build',		['concat:build', 'cssmin:production', 'uglify:production']);
    grunt.registerTask('dev', 		['uglify:development', 'prettify:development']);
    grunt.registerTask('product',   ['htmlclean:production', 'concat:build', 'copy:production', 'string-replace:production', 'concat:combine', 'uglify:production']);

};
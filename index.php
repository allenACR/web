<!DOCTYPE html>
<html>
    <head>
        <?php require_once (dirname(__FILE__) . '/php/settings.php'); ?>
        <title>
            <?php echo $seo[ 'websiteTitle'] ?>
        </title>
        <meta name="description" content="<?php echo $seo['websiteDescription'] ?>">
        <meta name="keywords" content="<?php echo $seo['metatags'] ?>">
        <meta name="author" content="<?php echo $seo['author'] ?>">
        <meta name="ROBOTS" content="INDEX, FOLLOW">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <?php setupJS()?>
        <link type="text/css" href='production/css/production.min.css' rel="stylesheet" media="screen">
        
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Arvo|Lora' rel="stylesheet" media="screen">
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600,400' rel='stylesheet' type='text/css'>
        
        
        <link type="text/css" href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel="stylesheet" media="screen">
        <link rel="apple-touch-icon" sizes="57x57" href="production/fav/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="production/fav/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="production/fav/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="production/fav/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="production/fav/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="production/fav/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="production/fav/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="production/fav/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="production/fav/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="production/fav/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="production/fav/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="production/fav/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="production/fav/favicon-16x16.png">
        <link rel="manifest" href="production/fav/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="production/fav/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
    </head>
    <div id='nt-colorContainer'></div>
    <body style='background-color: black' ng-controller="masterController" ng-init="initCore.init()">
        <div id="offCanvasMain" class='canvas-initial-hide'>
            <div ui-view="offcanvas" autoscroll="false" class="offcanvas-wrapper offcanvas-hide" ng-cloak></div>
        </div>
        <div style="position: fixed; bottom: 2%; left: 10px; z-index: 500">
            <div ui-view="overlay" autoscroll="false"></div>
        </div>
        <div ui-view="ui-modals" autoscroll="false" ng-cloak></div>
        <div ui-view="account-modals" autoscroll="false" ng-cloak></div>
        <div ui-view="forms-modals" autoscroll="false" ng-cloak></div>
        <div ui-view="editor-modals" autoscroll="false" ng-cloak></div>
        <toaster-container toaster-options="{'position-class': 'toast-bottom-right', 'time-out': 3000}"></toaster-container>
        <div>
            <div id='content-header' id="headerMain" ui-view="header" autoscroll="false" class="ng-ignore" ng-cloak></div>
            <div id="headerFiller"></div>
            <div id='content-slider' ui-view="slider" autoscroll="false" class="ng-ignore" ng-cloak></div>
            <div id='content-body' ui-view="body" autoscroll="false" ng-cloak></div>
            <div id='content-footer' ui-view="footer" autoscroll="false" class="ng-ignore" ng-cloak></div>
        </div>
    </body>
    <script data-main="require/main" src="require/require.js"></script>
    <?php require 'php/phpCore.php' ?>
    <script>
    if (!_global_setup.databaseConnection) {
        window.location.assign("setup/repair.php")
    }
    </script>
</html>
<?php

    /*  WEBSITE NAME */
    $websiteTitle = 'Template of Awesome';		/* WEBSITE TITLE */
	$websiteUrlName = 'web';					/* WEBSITE NAME */
	

	/*  CONFIGURE MYSQL INFORMATION */
	$localName = 'localhost';					/*  LOCAL NAME - probably don't need to change this setting */
	$remoteName = 'just65.justhost.com';  		/*  SERVER ADDRESS OF YOUR MYSQL DATABASE */
	$name	= 'codeandl_admin';					/*  LOGIN CREDENTIALS */				
	$key	= 'E^mLjf&8mV1r';					/*  LOGIN PASSWORD */		
	$usingDatabase = 'codeandl_example';		/*  NAME OF THE DATABASE YOU'LL BE UTILIZING */
	
	
	/*  FIREBASE SETTINGS */
	$firebaseAddress = 'blazing-torch-5667';	/*  FIREBASE ADDRESS */
	
    
    
    
	/////////////////////////////////////////////////
	//  LIKE M.C. HAMMER SAID - DON'T TOUCH THIS
	function setupJS(){
		
			global $firebaseAddress; 
			
			echo 
			"<script type='text/javascript'>" . 
			
				"var _global_setup = { " . 
				  "usePHP: true, " . 
				  "firebaseAddress: '$firebaseAddress'" .   
				"}" .  
				
			
			"</script>";

	}
	//
	/////////////////////////////////////////////////
	
	////////////////////////
	//  COMPRESS FILES
	/*** On-the-fly CSS Compression * Copyright (c) 2009 and onwards, Manas Tungare. * Creative Commons Attribution, Share-Alike.	
	/* Add your CSS files to this array*/	
	function compressFiles($files, $type){
		$compressedString = "";
		foreach ($files as $files) {
		  $compressedString .= file_get_contents($files);
		}
		
		// Remove comments
		$compressedString = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $compressedString);
		
		// Remove space after colons
		$compressedString = str_replace(': ', ':', $compressedString);
		
		// Remove whitespace
		$compressedString = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $compressedString);
		
		if ($type == "css"){
			echo('<style type="text/css" rel="stylesheet"	media="screen">' . $compressedString . '</style>');
		}
		if ($type == "js"){
			echo('<script type="text/javascript">' . $compressedString . '</script>');
		}		
	}
	//
	////////////////////////  	
	

?>
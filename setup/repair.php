<!DOCTYPE html>
<html>
  
  <head>

	<!-- MINIFIED CSS -->
	<link type="text/css" href='../production/css/production.min.css' rel="stylesheet" media="screen"></link>
	
	<!-- FONT AWESOME / GOOGLE FONTS -->	
	<link type="text/css" href='http://fonts.googleapis.com/css?family=Arvo|Lora' rel="stylesheet"	media="screen"></link>
	<link type="text/css" href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel="stylesheet"	media="screen"></link>
	
	
  </head>
  
  
  <body style="background-color: rgba(60, 60, 60, 1); color: white;"> 
  		
  		<div class='row'>
  			<div class='small-12 columns'>
	  			<center>
	  				<h1>Something has gone awry!</h1>
	  				<p>But relax, we can fix it.</p>
	  				<button id='repairBtn' onclick='restoreBackup()' class='button primary'>Repair Connection</button>
	  			</center>
  			</div>  			
  		</div>


		
  </body>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="../php/phpJS.js"></script>  	
	<script>
		
		function restoreBackup(){
		   
		   $('#repairBtn').text("Takes about a minute...").attr('disabled', true);
		   
	       $.ajax({ url: "../php/phpCore.php",
	                 data: {action: "restoreSetupConfig"},
	                 type: 'post',
	                 success: function(data) {	 
	                 	                	
						$('#repairBtn').text("Success!  Restarting in a moment...");
						setTimeout(function(){
							window.location.assign("../index.php")
						}, 3000);
	                 },
					 error: function(data){
					 	$('#repairBtn').text("Restore failed...");						  
					 }
	        });	


		}

	</script>	


	
  
</html>

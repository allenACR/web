//start

				app.controller('userController', [
						'$rootScope', '$scope', '$timeout', 'SmoothScroll', 'cfpLoadingBar', 'toaster',
				function($rootScope, $scope, $timeout, SmoothScroll, cfpLoadingBar, toaster) {	    

					var fileName  = 'user';

					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					
								// ---------------- VARIABLES 
							    resetVariables:function(){
							    	$scope.page = {
							    		loadComponents: custom.fillArray(3),  // number should equal # of load components below
							    		isLoaded: false,
							    	};
							    	$scope.content = "I am test content.";
							    },
							   	//-----------------		
					
								//----------------- REFRESH
								onRefresh:function(){
									sharedData.add("currentController", fileName);	// current controller
									$scope.masterData = sharedData.getAll();
									$scope.callCore.callController({who: "master", action: "hideThinking"});
									$scope.$apply();		
								},
								//-----------------						
					
								// ---------------- INIT
								init:function(){
									custom.offCanvasReset();
									$scope.initCore.resetVariables();
									cfpLoadingBar.start();
									SmoothScroll.$goTo(0);							
									$scope.initCore.loadComponents(function(){
										sharedData.add("currentController", fileName);	// current controller
										$scope.mainCore.start();
									});				
								},
								//-----------------        
					        
								// ---------------- LOAD COMPONENTS
								loadComponents:function(callback){
									$scope.initCore.checkMaster(function(state){
										if (state){
											$scope.masterData = sharedData.getAll();
											$scope.page.loadComponents[0] = true;
											$scope.initCore.animate(function(state){
												if (state){
													$scope.page.loadComponents[1] = true;
													$scope.initCore.checkLoad(callback);
												}
											});									
											
										}
									});
			
									$scope.initCore.doSomething(function(state){
										if (state){
											$scope.page.loadComponents[2] = true;
											$scope.initCore.checkLoad(callback);
										}
									});						
								},
								//-------------------		
								
								// ------------------ LOAD COMPONENTS
								checkMaster:function(callback){
									// wait for master.js to finish loading 	
									sharedData.request("masterReady", function(state, data){
										if(state){
											if(data.ready == true){
												callback(true);
											}
											else{
												alert(data.ready);
											}
										}
										else{
											alert(data);
										}	
									});							
			
								},
								animate:function(callback){
									if ($scope.masterData.browserDetails.mobile){
										$('#' + fileName + '_id').addClass('gray-gradient');							
									}
									// for desktop
									else{
										custom.backstretchBG("media/background.jpg", 0, 1000);
									}		
									$timeout(function(){	
										$('#content-page')
											.transition({   x: -20,  opacity: 0, delay: 0}, 0)
											.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
											.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
											
										callback(true);
									}, 500); // slight delay needed
								},
								
								// -- blank component for example
								doSomething:function(callback){
									callback(true);
								},
								//-----------------		
								
								//----------------- CHECK LOAD
								checkLoad:function(callback){
									var check = true,
										array = $scope.page.loadComponents; 					
									var i = array.length; while(i--){
										if ( array[i] == false){
											check = false; 
										};
									};
									// all loads completed
									if (check){
									// CONTENT PAGE IS READY - INSERT CODE HERE		
																		
										// check for existing modal
										var duration = 2000;
										var hasModalOpen = $('.reveal-modal').hasClass('in');
										if (!hasModalOpen){ duration = 0; };
										
										$timeout(function(){ 
											cfpLoadingBar.complete();
											$scope.page.isLoaded = true;																
											$scope.callCore.callController({who: "master", action: "hideSplash"});
																	
											callback();											
										}, duration);									
										
									}						
								},					
								//-----------------																	        
					        
					        
					    };
					})();
					//-----------------     


					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								console.log($scope.masterData);			
							},
							//-----------------


							//----------------- EDIT ACCOUNT
							editAccount:function(){
								$scope.callCore.callController({who: "master", action: "editProfileData"}, function(d){
									
									if(d.packet.data == "updated"){ 
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "showThinking"});
										}, 300);
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "refreshData"});
										}, 600);
									}
								
									
								});							
							},
							//-----------------	


							//----------------- EDIT ACCOUNT
							editPicture:function(){
								$scope.callCore.callController({who: "master", action: "editProfileImage"}, function(d){ 
									if(d.packet.data == "updated"){ 
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "showThinking"});
										}, 300);
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "refreshData"});
										}, 600);
									}
								});							
							},
							//-----------------		
							
							
							//----------------- CHANGE PASSWORD
							changePassword:function(){
								$scope.callCore.callController({who: "master", action: "changePassword"}, function(d){		
									if(d.packet.data == "updated"){ 
										toaster.pop('success', "Password has been updated!", '');
									}							
								});							
							},
							//-----------------	
							
							//----------------- RESET PASSWORD
							resetPassword:function(){
								$scope.callCore.callController({who: "master", action: "resetPassword"}, function(d){
									if(d.data == "updated"){ 
										toaster.pop('success', "Password has been reset!", 'Check your email for a new temporary password.');
									}							
								});							
							},
							//-----------------
							
							
							//----------------- new ACCOUNT
							createNewAccount:function(){
								$scope.callCore.callController({who: "master", action: "createNewAccount"}, function(d){
									if(d.packet.data == "updated"){ 
										toaster.pop('success', "Account has been created!", 'Check your email for a login link.');							
									}							
								});	
							},
							//-----------------	
							
							//----------------- new ACCOUNT
							deleteMyAccount:function(){
								$scope.callCore.callController({who: "master", action: "deleteMyAccount"}, function(d){
									if(d.packet.data == "deleted"){ 
										alert("Your account has been deleted.");
										localStorage.clear();
										location.reload();	
									}
								});	
							},
							//-----------------						
							
							//----------------- LOGIN
							adminLogin:function(){
								
								$scope.callCore.callController({who: "master", action: "login"}, function(d){
									if(d.packet.data == "updated"){ 
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "showThinking"});
										}, 300);
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "refreshData"});
										}, 600);
									}
									
								});
								
							},
							//-----------------								
								

						};
					})();
					//-----------------  




					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		console.log(data);
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 	    
				    
				   

				}]);  
	

//end
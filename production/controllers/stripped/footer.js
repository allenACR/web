//start

			
				app.controller('footerController', ['$rootScope', '$scope', '$timeout', '$location',
				function($rootScope, $scope, $timeout, $location) {	   
					
	    			// reference self via url
	    			var sharedName		= decodeURIComponent($location.url().replace('/','').toLowerCase());			
					if(sharedName.indexOf("?search=") > -1){
						sharedName	= sharedName.slice(0, sharedName.indexOf("?"));
					}  
					
					// ---------------- INIT CORE					
					$scope.initCore = (function () {
					    return {
					    	
							// ---------------- VARIABLES 
						    resetVariables:function(){
						    	$scope.page = {
						    		loadComponents: custom.fillArray(2),  // number should equal # of load components below
						    		isLoaded: false,
						    		browserType: null	
						    	};
						    },
						   	//-----------------							    	
					    	
							// ---------------- INIT
							init:function(){
								$scope.initCore.resetVariables();
							
								$scope.initCore.loadComponents(function(){					
									$scope.mainCore.start();
								});					
							},
							//-----------------
							
							//----------------- REFRESH
							onRefresh:function(callback){
								$scope.masterData = sharedData.getAll();
								$scope.$apply();		
							},
							//-----------------							
							
							
							// ---------------- LOAD COMPONENTS
							loadComponents:function(callback){
								$scope.initCore.checkMaster(function(state){
									if (state){
										$scope.page.loadComponents[0] = true;
										$scope.initCore.checkLoad(callback);
									}
								});
								$scope.initCore.doSomething(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										$scope.initCore.checkLoad(callback);
									}
								});							
							},
							//-------------------
							
							
							// ------------------ SAMPLE COMPONENTS
							checkMaster:function(callback){
								// wait for master.js to finish loading 		
								sharedData.request("masterReady", function(state, data){
									if(state){
										if(data.ready == true){
											callback(true);
										}
										else{
											alert(data.ready);
										}
									}
									else{
										alert(data);
									}	
								});	
							}, 
							
							doSomething:function(callback){
								// LOAD A COMPONENT						
								callback(true);
							},
							//-----------------
							
							
							//----------------- CHECK LOAD
							checkLoad:function(callback){
								var check = true,
									array = $scope.page.loadComponents; 					
								var i = array.length; while(i--){
									if ( array[i] == false){
										check = false; 
									};
								};
								// all loads completed
								if (check){		
									$timeout(function(){
										$scope.page.isLoaded = true;					
										$scope.masterData = sharedData.getAll();	
										callback();			
									});									
								}						
							},					
							//-----------------



					    };
					})(); 
				   	//-----------------
				      				    				      
				      

				
					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								
								// GET PAGE SETTINGS
								if (!$scope.masterData.browserDetails.mobile){
									$scope.page.browserType = "desktop";
								}
								else{
									$scope.page.browserType = "mobile";
								}			
								$scope.pagedata = [];
													
								sharedData.request("pagedata_" + sharedName, function(state, data){
									$scope.pagedata = data.pageSettings[$scope.page.browserType];
									 

									// GIVE SECOND FOR POTENTIAL IMAGES TO LOAD
									$timeout(function(){
										custom.initSlick();
									}, 1000);											
								});
										
							},
							//-----------------
							
							//-----------------
							browserInfo:function(){
									$scope.callCore.callController({who: "master", action: "browserInfo"}, function(data){
										
									});
							},
							//-----------------									
							
			
						};
					})();
					//-----------------  


					
					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: sharedName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(sharedName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: sharedName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: sharedName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 				
				    
				}]);
				


//end
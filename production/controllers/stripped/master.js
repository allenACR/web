//start

	
				var fileName  = 'master';
			
				app.controller('masterController', [
								'$state', '$stateParams', '$rootScope', '$scope', '$timeout', '$modal',  
								'$detection', '$localStorage', '$sessionStorage', 
								'SmoothScroll', 'angularLoad',  
								'browserInfo', 'cfpLoadingBar', 'browserInfo', 
								'accountModalCtrl', 'uiModalCtrl', 'formsModalCtrl', 'editorModalCtrl',
								
					function(	$state, $stateParams, $rootScope, $scope, $timeout, $modal,  
								$detection, $localStorage, $sessionStorage, 
								SmoothScroll, angularLoad,  
								browserInfo, cfpLoadingBar, browserInfo, 
								accountModalCtrl, uiModalCtrl, formsModalCtrl, editorModalCtrl) {

			
					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					    	
						    // ---------------- VARIABLES 
						    resetVariables:function(){
							    $scope.page = {
							    	loadComponents: custom.fillArray(9),				    	
							    	browserSettings: browserInfo.giveMeAllYouGot()
							    };
						    },
						   	//-----------------

							// ---------------- INIT
							init:function(){

								console.groupCollapsed("Init Sequence");
								console.time("Loadtime");
								$scope.initCore.resetVariables();					
								$scope.initCore.loadComponents(function(){
									$scope.mainCore.start();
								});
																												
							},
							//-----------------
							
							//----------------- REFRESH
							masterRefresh:function(){	
								    
									console.groupCollapsed("Refresh Sequence");
									console.time("Loadtime");
										
									var currentPage = sharedData.getAll().currentController; 
									$scope.callCore.callController({who: "master", action: "showThinking"});
									
		    						$scope.initCore.resetVariables();
		   						    sharedData.clearAll();	
									$scope.initCore.loadComponents(function(){
										
									
																	
										$timeout(function(){
											sharedData.add("masterReady", {ready: true});	
											
											// REFRESH CURRENT PAGE
											var packet = {
													info:{to: currentPage,	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);
											
											// REFRESH HEADER
											var packet = {
													info:{to: "header",	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);	
											
											// REFRESH FOOTER
											var packet = {
													info:{to: "footer",	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);	
											
											// REFRESH OVERLAY
											var packet = {
													info:{to: "offcanvas",	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);	
											
											// REFRESH OFF CANVAS
											var packet = {
													info:{to: "overlay",	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);
																				
											console.timeEnd("Loadtime");
											console.groupEnd("Refresh Sequence");
																		
										}, 2000);
											
									});
							},
							//-----------------							
							
							//----------------- LOAD COMPONENTS 
							loadComponents:function(_callback){
													
								$scope.initCore.initalizeDatabase(function(state){								
									if (state){
										
										if ($scope.page.loadComponents[0] != true){
										  $scope.page.loadComponents[0] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});
								
								$scope.initCore.fetchSystemData(function(state){
									if (state){
										if ($scope.page.loadComponents[1] != true){
									  	  $scope.page.loadComponents[1] = true;
										  $scope.initCore.checkLoad(_callback);
										}																		
									}										
								});
								
								
								$scope.initCore.fetchUrl(function(state){														
									if (state){
										
										if ($scope.page.loadComponents[2] != true){
									  	  $scope.page.loadComponents[2] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});
								
								
								$scope.initCore.fetchIPAddress(function(state){							
									if (state){
										
										if ($scope.page.loadComponents[3] != true){
										  $scope.page.loadComponents[3] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});	
								
									
								$scope.initCore.detectType(function(state){												
									if (state){
										
										if ($scope.page.loadComponents[4] != true){
										  $scope.page.loadComponents[4] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});	
								
																		
								$scope.initCore.userData(function(state){
									if (state){
										
										if ($scope.page.loadComponents[5] != true){
										  $scope.page.loadComponents[5] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});
								
								
								$scope.initCore.userDetails(function(state){											
									if (state){
										if ($scope.page.loadComponents[6] != true){
										  $scope.page.loadComponents[6] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});	
								
								
								$scope.initCore.getBrowserDetails(function(state){														
									if (state){
										
										if ($scope.page.loadComponents[7] != true){
										  $scope.page.loadComponents[7] = true;
										  $scope.initCore.checkLoad(_callback); 
										}
									}
								});	
								
								
								$scope.initCore.resolveOverflow(function(state){
									
									if (state){
										
										if ($scope.page.loadComponents[8] != true){
										  $scope.page.loadComponents[8] = true;
										  $scope.initCore.checkLoad(_callback); 	
										}
									}
								});	
									
							},
							//-------------------
							
							
							// LOAD COMPONENTS
							initalizeDatabase:function(callback){
								
								
								custom.databaseInit(function(status){
									if (status){								
										callback(true);
									}
									else{
										alert("Timeout: Database could not be reached.");
									}
								});
								
							},
							

							fetchSystemData:function(callback){
								
								
								phpJS.returnJsonAsObject("/../production/settings/systemdata/system.json", function(state, data){

									if (state){
										sharedData.add('system', data );
										callback(true);
									}else{
										alert("Timeout:  Could not fetch system data.");
									}									
									
										 
								});		
													
							},

							
							fetchUrl:function(callback){
								phpJS.getFullUrl(function(state, data){
									if (state){
										sharedData.add('url', JSON.parse(data) );
										callback(true);
									}else{
										alert("Timeout:  Could not fetch URL.");
									}
								});
							},
							
							fetchIPAddress:function(callback){					
								phpJS.getIPAddress(function(state, data){
									if (state){
										sharedData.add('ipAddress', JSON.parse(data) );
										callback(true);
									}else{
										alert("Timeout: IP Address could not be retrieved.");
									}	
								});						
							},
							
							detectType:function(callback){
							  	var type = '';
							  	if ($detection.isAndroid()){
							  		type = "android";
							  	}
							  	else if($detection.isiOS()){				  		
							  		type = "ios";
							  	}
							  	else if($detection.isWindowsPhone()){
							  		type = "windows-phone";
							  	}
							  	else{				  		
							  		type = "desktop";
							  	}
								sharedData.add('ios', type);					   											
								callback(true);						
							},
							
							userData:function(callback){
								
					  			firebaseSettings.getPermissionLevel(function(isUser, accessLevel){			  				
		
					  			
					  				sharedData.add("accessLevel", accessLevel);
					  				callback(true);					  				
					  			});	
					  			
							},
							
							userDetails:function(callback){
						  		var userData = [];	
		
							  		firebaseSettings.checkUserData(function(returnState, data){					  			
										if(returnState){
											// retrieved user data
											userData = data.user;	
											userData["avatar"] = data.image;
											userData["auth"] = JSON.parse(localStorage.getItem("user")).auth; 
											sharedData.add("logState", true);	
											localStorage.setItem("logState", true);
																											
										}
										else{
											// not logged in
											userData = {
												id: null,
												email: null,
												firstName: null,
												lastname: null,
												permission: 'guest',										
												userName: 'anonymous',
											};
											userData["avatar"] = {small: null, standard: null, thumbnail: null};
											userData["auth"] = null;
											sharedData.add("logState", false);
											localStorage.setItem("logState", false);
										};
										sharedData.add("userInfo", userData);
										callback(true);								
									});				
							},
							
							getBrowserDetails:function(callback){
								 var size = null,
								 	 width = browserInfo.giveMeAllYouGot().screenSize.width; 
								 
								 if (width < 550){
								 	size = "small";
								 }
								 if (width >= 550 && width < 1080){						
								 	size = "medium";
								 }
								 if (width >= 1080){						
								 	size = "large";
								 }		
								 sharedData.add('browserSize', size);				 
								 sharedData.add('browserDetails', browserInfo.giveMeAllYouGot() );
								 callback(true);
							},
							
							resolveOverflow:function(callback){
								
	
								sharedData.request('browserDetails', function(state, data){
	
											// LOAD THEME SPECIFIC CSS				
											var themeRoot = "themes/available/" + _global_setup.theme;
								
										  	angularLoad.loadCSS(themeRoot + "/css/theme-base.css")
										  	.then(function() {
											if (state){
												var isMobile = data.mobile;
												
												if(isMobile){
													
												   	angularLoad.loadCSS(themeRoot + "/css/theme-mobile.css")
									  				.then(function() {
									  					callback(true);	
									  				});												
											   	}
											   	else{
												   	angularLoad.loadCSS(themeRoot + "/css/theme-desktop.css")
									  				.then(function() {
									  					callback(true);	
									  				});	
											   	}
				
											}
											else{
												alert("Cannot load proper CSS.");
											}
								
									});								

								});
								
								
								
							},
							//-----------------		
							
							// CHECK LOAD -----
							checkLoad:function(callback){
								var check = true,
									array = $scope.page.loadComponents; 	
								
								var i = array.length; while(i--){
									if ( array[i] == false){
										check = false; 
									};
								};
								
								// all loads completed
								if (check){
									callback();
								}						
							},					
							// -----------------												
			        
					    };
					})();
					//-----------------   
					
					


					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								console.timeEnd("Loadtime");
								console.groupEnd("Init Sequence");
																			
								$timeout(function(){
									sharedData.add("masterReady", {ready: true});
									$scope.callCore.callController({who: "master", action: "showSplash"}, function(data){});
									$('body').transition({  opacity: 1}, 1000, 'ease').css('background-color', 'transparent');	
								}, 1000);	
							
							},
							//-----------------
							
							
						};
					})();
					//-----------------  
					
					//-----------------  
					$scope.tokenInits = (function () {
						return {
						
							gallery:function(folderName, id){
								
								phpJS.listOfFiles("../media/manager/images/" + folderName + "/", function(state, data){ 
									var img = custom.parseManagerImages(data, folderName),
										buildString = "";
									
									for (var i = 0; i < img.full.length; i++){
										buildString += "<img src='" + img.full[i].src + "'>";
									}
									
									$('#' + id).append(buildString);
									custom.initSlick();
								});
							}
						
						};
					})();
					//-----------------  

			    
				    // ---------------- PHP CALLS 
					$scope.phpCore = (function () {
					    return {

							// ----------------  RUN QUERY
							__phpRunQuery:function(data, callback){
									var queryString = data.query;
								   	var packet = {
								   		query: queryString,
								   		database: data.database
								   	};
								   	
								   	
								   	
									phpJS.queryDatabase(packet, function(state, data){
										if (state){
											$timeout(function(){
											  callback({status: "success", data: data});
											});
										}
									});					
							},
  							// ----------------
  							
							// ----------------  RUN QUERY
							__queryNumberOfEntries:function(packet, callback){
									
									phpJS.queryNumberOfEntries(packet, function(state, data){
										if (state){
											$timeout(function(){
											  callback({status: "success", data: data});
											});
										}
									});					
							},
  							// ----------------  							
  							
							// ----------------  SEND EMAIL
							__phpSendEmail:function(packet, callback){
									phpJS.sendEmail(packet, function(state, data){
										$timeout(function(){
											  callback({status: "success", data: data});
										});
									});
							}, 							
  							// ----------------
					    	
							// ----------------  SEND EMAIL
							__phpSendEmail:function(packet, callback){
									phpJS.sendEmail(packet, function(state, data){
										$timeout(function(){
											  callback({status: "success", data: data});
										});
									});
							},
							// ----------------			

							// ----------------  DELETE FOLDER
							__phpDeleteFolder:function(packet, callback){	
								if (packet.folder == undefined || packet.folder == null){
									callback({status: "fail", msg: "No file included."});
								}
								else{
								
									phpJS.deleteFolder(packet.folder, function(state, data){
										$timeout(function(){
											callback({status: "success", data: data});
										});
									});	
								}
							},		
							// ----------------	
							
							// ----------------  DELETE FOLDER
							__phpDeleteFiles:function(packet, callback){	
									phpJS.deleteFiles(packet, function(state, data){
										$timeout(function(){
											callback({status: "success", data: data});
										});
									});	
								
							},		
							// ----------------		
							
							// ----------------  DELETE FOLDER
							__phpCreateFolder:function(packet, callback){
									phpJS.createFolder(packet, function(state, data){
									  callback({status: "success", data: data});
									});										
							},		
							// ----------------															
							
							
															
							// ----------------  UPLOAD FILE
							__phpfileUpload:function(packet, callback){	
								if (packet.file == undefined || packet.file == null){
									callback({status: "fail", msg: "No file included."});
								}
								else{
								
									phpJS.basicUpload(packet.file, packet.name, packet.location, function(state, data){
										$timeout(function(){
											callback({status: "success", data: data});
										});
									});	
								}
							},		
							// ----------------	
							
							// ----------------  UPLOAD FILE
							__phpbase64Upload:function(packet, callback){
								
								if (packet.file == undefined || packet.file == null){
									callback({status: "fail", msg: "No file included."});
								}
								else{
									
									phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
										$timeout(function(){
											callback({status: "success", data: data});
										});
									});
									
								}
							},		
							// ----------------								
							
							// ----------------  BROWSER INFO	
							__phpGetTableDetails:function(packet, callback){												
								phpJS.getTableFields(packet, function(state, data){
									if (state){
										
										
		
										// strip numbers out of type
										var requiredCount = 0;
										
										for (var key in data) {
										   var obj = data[key];
										   for (var prop in obj) {
										      // important check that this is objects own property 
										      // not from prototype prop inherited
										      if(obj.hasOwnProperty(prop)){
										      	
										      	// parse the types
										      	if (prop == "Type"){
													var strippedType = $.trim(obj[prop].replace(/[, 0-9()]/g,'').toLowerCase());
													var strippedSize = $.trim(obj[prop].replace(/[A-Za-z()]/g,''));
													obj["Type"] = strippedType;
													obj["Size"] = strippedSize;
										        }
										        
										        // parse the types
										        if (prop == "Comment"){
										        	
													// ATTRIBUTES
										        	if (obj[prop].indexOf("noshow") > -1){ obj["noshow"] = true; }		else{	obj["noshow"] = false;		};
										        	if (obj[prop].indexOf("disabled") > -1){ obj["isDisabled"] = true;}	else{	obj["isDisabled"] = false;	};										        											        	
										        	if (obj[prop].indexOf("hidden") > -1){ obj["isHidden"] = true; }	else{	obj["isHidden"] = false;	};
										        	if (obj[prop].indexOf("required") > -1){ obj["isRequired"] = true;}	else{	obj["isRequired"] = false;	};
										        	// ----- END
										        	
										        	
										        	
										        	// FIELDS
										        	// general 
										        	if (obj[prop].indexOf("label") > -1){ 
										        		    var matches = obj[prop].match(/label=\"(.*?)\"/);    												
										        			obj["label"] = matches[1]; 								        	
										        	}else{	obj["label"] = '';	};
													if (obj[prop].indexOf("placeholder") > -1){ 
										        		    var matches = obj[prop].match(/placeholder=\"(.*?)\"/);    												
										        			obj["placeholder"] = matches[1]; 								        	
										        	}else{	obj["placeholder"] = '';	};
										        	
										        	// number specific
										        	if (obj[prop].indexOf("allowNeg") > -1){
										        		 var matches = obj[prop].match(/allowNeg=\"(.*?)\"/);    
										        		 obj["allowNeg"] = matches[1]; 											        		 
										        	}										        	
										        	if (obj[prop].indexOf("rangeMax") > -1){
										        		 var matches = obj[prop].match(/rangeMax=\"(.*?)\"/);    
										        		 obj["rangeMax"] = matches[1]; 	
										        	}	
										        	if (obj[prop].indexOf("rangeMin") > -1){
										        		 var matches = obj[prop].match(/rangeMin=\"(.*?)\"/);    
										        		 obj["rangeMin"] = matches[1]; 											        		 
										        	}	
										        	if (obj[prop].indexOf("stepValue") > -1){
										        		 var matches = obj[prop].match(/stepValue=\"(.*?)\"/);    
										        		 obj["stepValue"] = matches[1]; 											        		 
										        	}											        											        											        	
										        	// ----- END
										        											        	

													// GRAB FIELDS AUTOMATICALLY
													obj["autoFill"] = false;
													if (obj[prop].indexOf("autoCollect") > -1){
														obj["autoFill"] = true; 
														var matches = obj[prop].match(/autoCollect=\"(.*?)\"/);  
														obj["autoCollect"] = matches[1]																										
													}else{	
														obj["autoFill"] = false;	
													};
													// ----- END

										        	// DEFINE INPUT TYPE (input/textarea/advanced)
										        	if (obj[prop].indexOf("inputType") > -1){ 
										        		    var matches = obj[prop].match(/inputType=\"(.*?)\"/);    												
										        			obj["inputType"] = matches[1]; 								        	
										        	}
										        	// DEFAULT
										        	else{	
										        		obj["inputType"] = 'input';	
										        	};
										        	// ----- END									        	
										        	
										        	// DEFINE REGEXP
										        	if (obj[prop].indexOf("regexp") > -1){ 
										        		    var matches = obj[prop].match(/regexp=\"(.*?)\"/); // adds escape characters for strings if added   												
										        			obj["textRestrict"] = matches[1].replace(/~escape~/g, "\\");								        	
										        	}
										        	// DEFAULTS
										        	else{	
										        		var type = obj["Type"]; 
										        		
										        		// ALL TEXT INPUT
										        		if (type == "input" || type == "varchar" || type == "text" || type == "tinytext" || type == "mediumtext" || type == "longtext"){
										        			obj["textRestrict"] = '';	
										        		};
										        		
										        		// ALL DEFAULT NUMBERS
										        		if (type == "tinyint" || type == "smallint" || type == "mediumint" || type == "int" || type == "bigint" || type == "bit"){
										        			obj["textRestrict"] = '^[0-9]*$';	
										        		};										        		
										        		
										        		// ALL DECIMAL NUMBERS * needs to be redone - direct regexp work best
										        		if (type == "double" || type == "decimal" || type == "float"){
										        			obj["textRestrict"] = '^\\d+(\\.\\d{0,2})?$';	
										        		};	
										        		
										        		// TIME * need to be redone to match mysql database - direct regexp work best
										        		if (type == "date"){
										        			obj["textRestrict"] = "";
										        		};	
										        		if (type == "time"){	
										        			obj["textRestrict"] = new Date(moment().format());
										        		};		
										        												        												        											        		
										        	};
										        	// ----- END
										        	
										        	
										        	// ----- IMAGE UPLOADS										        			
										        	if (obj["inputType"] == "imageUpload"){	
										        			if (obj[prop].indexOf("limit") > -1){
											        		    var matches = obj[prop].match(/limit=\"(.*?)\"/);    																					        				 
										        				obj["limit"] = matches[1]; 	; }
										        			else{
										        				obj["limit"] = 10;	
										        			};
										        			
										        			if (obj[prop].indexOf("captions") > -1){  																					        				 
										        				obj["captions"] = true; }
										        			else{
										        				obj["captions"] = false;	
										        			};
										        	};	
										        	
										        }
										        
										      }
										   }
										}
										
										var meta = {
											required: 0
										};
										
										var _returnData = {
											formData: data,
											metaData: meta
										};
										
		
										$timeout(function(){
											callback({status: "success", data: _returnData});
										});
									}
									else{
										$timeout(function(){
											callback({status: "failure", msg: "Could not retrieve value keys."});
										});
									}
								});	
							},		
							// ----------------	
							
							
						    // ----------------  CREATE ENTRY
						    __phpCreateBlankEntry:function(packetData, callback){
						 		
					
									phpJS.createBlankEntry({database: packetData.database, table: packetData.table}, function(state, data){	
			 							if (state){
			 								$timeout(function(){
													callback({status: "success", data: data});
											});
			 							}
			 							else{
			 								// backend failure
											$timeout(function(){
												callback({status: "failure", msg: "Could not create blank entry."});
											});
			 							};						
									});
									
						
						    },
						    // ----------------								
							
						    // ----------------  CREATE ENTRY
						    __phpCreateNewEntry:function(packetData, callback){
						 		
						 		// SANITIZE PACKGES BEFORE SENDING
						 		custom.sanitizePacketsForDatabase(packetData, function(sanitizedData){	
						 			
							 		var valuePacket = JSON.stringify(sanitizedData);						
									phpJS.createNewEntry({database: packetData.database, table: packetData.table}, valuePacket, function(state, data){	
			 							if (state){
			 								// new entry created successfully
			 								if(data.status == "good"){
												$timeout(function(){
													callback({status: "success", data: data});
												});
											};
											// error in the input (typos, mismatched fields, etc)
											if(data.status == "error"){
												$timeout(function(){
													callback({status: "error", msg: "Could not create new entry.  Check for mismatched fields or required fields."});
												});
											};
			 							}
			 							else{
			 								// backend failure
											$timeout(function(){
												callback({status: "failure", msg: "Could not create new entry."});
											});
			 							};						
			
									});
									
									
								});
						    },
						    // ----------------	
						    	
						    
						    // ----------------  DELETE DENTRY
							__phpModifyEntry:function(packet, callback){
											
									phpJS.queryDatabase2(packet, function(state, data){
								
										
										if (state){
											if (data.status == "good"){
												$timeout(function(){
													callback({status: "success", msg: "Modification complete."});
												});	
											};
											if (data.status == "error"){
												$timeout(function(){
													callback({status: "error", msg: "Modification has failed."});
												});
											};
										}
										else{
											$timeout(function(){
												callback({status: "failure", msg: "Could not reach database."});
											});	
										};
									});
							},
							// ----------------		
							
							// ----------------	
							__phpListOfFiles:function(packet, callback){
								phpJS.listOfFiles("../" +  packet.location, function(state, data){
										
			 							if (state){
			 								$timeout(function(){
													callback({status: "success", data: data});
											});
			 							}
			 							else{
			 								// backend failure
											$timeout(function(){
												callback({status: "failure", msg: "Could not create blank entry."});
											});
			 							};	
								});
							}
							// ----------------			
									    	
					    	
						};
					})();    
					
	
					// -------------- STANDARD CORE
					$scope.standardCore = (function () {
					    return {
					    	
							// ----------------  PING/PONG	  		
							__pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},
							// ---------------- 
							
							
							// ---------------- 
							__editorCollectionSettings:function(data, callback){
								  
								  $scope.items = data;
								  custom.lockBody();
								  
								 

								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-collectionSettingsModal.html',
								      controller: editorModalCtrl.collectionSettingsModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 
							
							// ---------------- 
							__editorPageSettings:function(data, callback){
								  $scope.items = data;
								  
								  custom.lockBody();
		  
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-pageSettingsModal.html',
								      controller: editorModalCtrl.pageSettingsModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){
										custom.unlockBody();											
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){										
										custom.unlockBody();																			
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 
							
							
							// ---------------- 
							__editorSystemSettings:function(data, callback){
								
								
								  $scope.items = data;
								  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-pageSystemModal.html',
								      controller: editorModalCtrl.pageSystemModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 
											
							
							// ----------------  CREATE/EDIT/DELETE LAYOUTS	
							__scriptCreator:function(data, callback){
								  $scope.items = data;
		  							
		  						  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-scriptCreator.html',
								      controller: editorModalCtrl.scriptCreatorCntrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 
							
							// ---------------- 
							__mediaManager:function(data, callback){
								  $scope.items = data;
		  							
		  						  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-mediaManager.html',
								      controller: editorModalCtrl.scriptMediaCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 				
							
							// ----------------  BROWSER INFO		  		
							__browserInfo:function (callback) {
								
								    custom.lockBody();
									var control = uiModalCtrl.browserInfoCtrl(); 
									var modalInstance = $modal.open({
								      	templateUrl: 'browserInfoModal.html',
								  		controller: control,
									    resolve: {
									        data: function () {
									       
									        }
									    }
									});
									
									// modal animation in
									modalIn();			
								
									modalInstance.result.then(
										function (returnData) {  // CLOSE	
											custom.unlockBody();																								
											modalOut(function(){
												callback({status: "closed", msg: "closed"});
											});	
										}, 
										function () {			// DISMISS
											custom.unlockBody();
											modalOut(function(){											
								  				callback({status: "dismiss", data: "dismissed"});
								  			});	
								    	}
								    );						
							},
							// ---------------- 						
								
							// ----------------  CONFIRM		  					  
							__pictureBox:function (data, callback) {
								  
								  $scope.items = data;
		                        
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'pictureModal.html',
								      controller: uiModalCtrl.pictureModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
		
							},
							// ---------------- 								
								
								
							// ----------------  CONFIRM		  					  
							__choiceBox:function (data, callback) {
								  
								  $scope.items = data;
		  						  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'choiceModal.html',
								      controller: uiModalCtrl.choiceModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
		
							},
							// ---------------- 								
								
								
							// ----------------  CONFIRM		  					  
							__confirmBox:function (data, callback) {
								
								 $scope.items = data;
								 custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'confirmModal.html',
								      controller: uiModalCtrl.confirmModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
		
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData)
									{	
										custom.unlockBody();
										modalOut(function(){			
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){	
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
		
							},
							// ---------------- 
							
						 	// ---------------- LOGOUT
							__logout:function (callback) {						
				 				
				 				
				 				var checkOnce = false; 
				 				var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );
								var auth = new FirebaseSimpleLogin(fbLogin, function() {
										if (!checkOnce){
											checkOnce = true; 			
											firebaseSettings.setLogout();								
							 			} 	
								});	
				 				auth.logout();	
							},
							//-----------------		
							
						 	// ---------------- RESET PASSWORD
							__resetPassword:function (callback) {						
								
								var modalInstance = $modal.open({
							      	templateUrl: 'resetModal.html',
							  		controller: accountModalCtrl.resetEmailCtrl(),
								});
								
								// modal animation in
								modalIn();
								
								// VIA CLOSE
								modalInstance.result.then(function(returnData)
								{							 
									modalOut(function(){
										callback({status: "closed", data: "updated"});		
									});			  							 		
								}, 
								// VIA DISMISS
								function (){
									modalOut(function(){
							  			callback({status: "dimiss", data: "dismissed"});
							  		});
								});	
								
							},
							//-----------------							
												
															
						 	// ---------------- LOGIN
							__login:function (callback) {						
								custom.lockBody();
								var modalInstance = $modal.open({
							      	templateUrl: 'loginModal.html',
							  		controller: accountModalCtrl.loginModalCtrl(),
								});
								
								// modal animation in
								modalIn();
								
								// VIA CLOSE
								modalInstance.result.then(function(returnData)
								{		
									custom.unlockBody();					 
									modalOut(function(){
										callback({status: "closed", data: "closed"});
									});					  							 		
								}, 
								// VIA DISMISS
								function (){
									custom.unlockBody();
									modalOut(function(){
							  			callback({status: "dimiss", data: "dismissed"});
							  		});
								});	
								
							},
							//-----------------
							
							
							//----------------- EDIT PROFILE IMAGE					
							__editProfilePic:function(callback){						
									
									custom.lockBody();						
									var modalInstance = $modal.open({
								      	templateUrl: 'editProfileModal.html',
								  		controller:  accountModalCtrl.editProfileModalCntrl(),
									    resolve: {
									        data: function () {
									        // return $scope.deleteUserObj;
									        }
									    }
									});
									
									// modal animation in
									modalIn();
								
									// VIA CLOSE
									modalInstance.result.then(function(returnData)
									{	
										custom.unlockBody();	
										modalOut(function(){
											callback({status: "closed", data: "updated"});	
										});				  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
							
				
							},
							//-----------------									


							//-----------------	EDIT PROFILE IMAGE					
							__editProfileData:function(callback){						
										
									custom.lockBody();						
									var modalInstance = $modal.open({
								      	templateUrl: 'editProfileDataModal.html',
								  		controller: accountModalCtrl.editProfileDataModalCntrl(),
									    resolve: {
									       
									    }
									});
									
									// modal animation in
									modalIn();
								
									modalInstance.result.then(
										function (returnData) {  // CLOSE
											custom.unlockBody();	
											modalOut(function(){																																								
												callback({status: "closed", data: "updated"});
											});											
										}, 
										function () {			// DISMISS
											custom.unlockBody();
											modalOut(function(){								
								  				callback({status: "closed", data: "dismissed"});
								  			});						  			
								    	});
							
				
							},
							//-----------------		
							
							
							//----------------- CHANGE PASSWORD
							__changePassword:function(callback){
									
									custom.lockBody();
									var modalInstance = $modal.open({
								      	templateUrl: 'changePasswordModal.html',
								  		controller: accountModalCtrl.changePasswordCtrl(),
									    resolve: {
									        data: function () {
									        // return $scope.deleteUserObj;
									        }
									    }
									});
									
									// modal animation in
									modalIn();							
									
									modalInstance.result.then(
										function (returnData) {  // CLOSE
											custom.unlockBody();
											modalOut(function(){																															
												callback({status: "closed", data: "updated"});	
											});	
										}, 
										function () {			// DISMISS
											custom.unlockBody();
											modalOut(function(){	
								  				callback({status: "closed", data: "dismissed"});
								  			});	
								   		});
							},
							//-----------------		
							
							
							//-----------------	DELETE ACCOUNT
							__deleteMyAccount:function(callback){
												
								custom.lockBody();
								var modalInstance = $modal.open({
							      	templateUrl: 'deleteModal.html',
							  		controller: accountModalCtrl.deleteModalCtrl(),
								    resolve: {
								        data: function () {
								         // return $scope.deleteUserObj;
								        }
								    }
								});
							
								// modal animation in
								modalIn();						
							
								modalInstance.result.then(
									function (returnData) {  // CLOSE
										custom.unlockBody();
										modalOut(function(){																														
											callback({status: "closed", data: "deleted"});
										});	
									}, 
									function () {			// DISMISS
										custom.unlockBody();
										modalOut(function(){
							  				callback({status: "closed", data: "dismissed"});
							  			});	
							  		});
							},	
							//-----------------														
					
					
							//-----------------	 CREATE NEW ACCOUNT
							__createNewAccount:function(callback){
									
									custom.lockBody();		
									var modalInstance = $modal.open({
								      	templateUrl: 'createModal.html',
								  		controller: accountModalCtrl.createModalCtrl(),
									    resolve: {
									        data: function () {
									        // return $scope.deleteUserObj;
									        }
									    }
									});
									
									// modal animation in
									modalIn();	
									
									modalInstance.result.then(
										function (returnData) {  // CLOSE
											custom.unlockBody();
											modalOut(function(){	
												callback({status: "closed", data: "created"});	
											});
										}, 
										function () {			// DISMISS
											custom.unlockBody();
											modalOut(function(){									
								  				callback({status: "closed", data: "dismissed"});
								  			});
								    	});
				
							},
							//////////////////////		
							
							//----------------- RELOAD DOM
							__refreshPage:function(){						
								$state.transitionTo($state.current, $stateParams, {
									    reload: true,
									    inherit: false,
									    notify: true
								});								
							},
							//-----------------							
											
							//----------------- REFRESH DATA
							__refreshData:function(){
								$scope.initCore.masterRefresh();
							},
							//-----------------
							
							//----------------- SHOW/HIDE THINKING
							__showSplash:function(callback){
								var isActive = $(".reveal-modal").hasClass("in"); 
								if (!isActive){
															
									var modalInstance = $modal.open({
								      	templateUrl: 'splashModal.html',
								  		controller: uiModalCtrl.splashModalCtrl(),
									});
									
									modalIn();
									
									callback({active: isActive});
								};		
								callback({active: isActive});											
							},
							__hideSplash:function(callback){
							
								$(".reveal-modal").transition({ opacity: 0 }, 500);
								$(".reveal-modal-bg").transition({ opacity: 0 }, 500);
								var isActive = $(".reveal-modal").hasClass("in"); 
								
								modalOut();
								
								if (isActive){
									setTimeout(function(){
										$('.modal-body').addClass('hidden');
										$(".reveal-modal").removeClass();
										$(".reveal-modal-bg").removeClass();
										$('body').removeClass('modal-open');
										callback({active: isActive});
									}, 500);	
								}
								callback({active: isActive});
								
							},
							//-----------------								
							
							//----------------- SHOW/HIDE THINKING
							__showThinking:function(callback){
								var isActive = $(".reveal-modal").hasClass("in"); 
								if (!isActive){					
									var modalInstance = $modal.open({
								      	templateUrl: 'thinkingModal.html',
								  		controller: uiModalCtrl.thinkingModalCtrl(),
									});
									
									modalIn();
									
									callback({active: isActive});
								};	
								callback({active: isActive});												
							},
							
							__hideThinking:function(callback){
								$(".reveal-modal").transition({ opacity: 0 }, 500);
								$(".reveal-modal-bg").transition({ opacity: 0 }, 500);
								var isActive = $(".reveal-modal").hasClass("in"); 
								
								modalOut();
								
								if (isActive){
									setTimeout(function(){
										$('.modal-body').addClass('hidden');
										$(".reveal-modal").removeClass();
										$(".reveal-modal-bg").removeClass();
										$('body').removeClass('modal-open');
										callback({active: isActive});
									}, 500);	
								}
								callback({active: isActive});
							},
							//-----------------		
							
							// ----------------  POST MODAL		  					  
							__postModal:function (data, callback) {
						 
								  $scope.sendData = data;
								  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'db_post_Modal.html',
								      controller: formsModalCtrl.postModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.sendData;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){		
										custom.unlockBody();				
										modalOut(function(){												
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
								
		
							},
							// ---------------- 	
							
							// ----------------  POST MODAL		  					  
							__advSearchModal:function (data, callback) {
						 			
								  $scope.sendData = data;
								  custom.lockBody();	
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'advSearchModal.html',
								      controller: formsModalCtrl.advSearchModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.sendData;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){		
										custom.unlockBody();				
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();	
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
								
		
							},
							// ---------------- 
															
								
						};
					})();  				
					// -------------- end STANDARD CORE
							    				

					
					
					
						
					
					
					
					//-----------------		
					function modalIn(cb){
						var duration = 250;
						$timeout(function(){	
							
							$('html, body').css({
							    'overflow': 'hidden',
							    'height': '100%',
							});
												
							
							$(".reveal-modal").transition({ y: '-40px', opacity: 0 }, 0)
											  .transition({ y: '0px', opacity: 1   }, duration);

							
							setTimeout(function(){
								$timeout(function(){
									if (cb!=null){cb();};
								}, 0);
							}, duration);
						}, 0);						
					}; 
					
					
					function modalOut(cb){
						var duration = 250;
						$timeout(function(){
							

							$('html, body').css({
							    'overflow': 'visible',
							    'height': 'auto'
							});

							$(".modal-body").transition({ y: '0px' }, 0)
											.transition({ y: '-40px' }, 500);										
							$(".reveal-modal").transition({ opacity: 0 }, duration);
							setTimeout(function(){
								$timeout(function(){
									if (cb!=null){cb();};
								}, 0);
							}, duration);
						}, 0);						
					};
					//-----------------		
			
					
//-------------------------------------
					// COMMUNICATE BETWEEN CONTROLLERS	 				  
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {

						 	// RUN REQUEST
							runRequest:(function () {
					
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											packet: returnData,
											button: {obj: data.info.button, content: data.info.buttonContent}
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});		
	
							  };
					
					  var execute = function (data) {
					  		
					  	
					  		
							switch(data.execute.name) {
								 // required for callbacks to other controllers
							    case "calledBack":
							        $scope._watched.callback = data.returnData.data;
							    break;		
							    
							    //--------  PHP FUNCTIONS ----------//
							    case "phpModifyEntry":
							        $scope.phpCore.__phpModifyEntry(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;
							    
							    case "phpCreateBlankEntry":
							        $scope.phpCore.__phpCreateBlankEntry(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;		    
							    

							    case "phpCreateNewEntry":
							        $scope.phpCore.__phpCreateNewEntry(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    							    
							    case "phpGetTableDetails":								    						   
							        $scope.phpCore.__phpGetTableDetails(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;									    
							    
							    case "phpDeleteFolder":
							        $scope.phpCore.__phpDeleteFolder(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    
							    case "phpDeleteFiles":
							        $scope.phpCore.__phpDeleteFiles(data.info.data, function(returnData){							        	
							        	if (data.execute.useCallback){					        							        		
							        		callback(data, returnData); 
							        	};
							        });	
							    break;		
											    								    
							    case "phpCreateFolder":
							        $scope.phpCore.__phpCreateFolder(data.info.data, function(returnData){							        	
							        	if (data.execute.useCallback){					        							        		
							        		callback(data, returnData); 
							        	};
							        });	
							    break;								    
							    
							    case "phpBase64Upload":
							        $scope.phpCore.__phpbase64Upload(data.info.data, function(returnData){
							        	if (data.execute.useCallback){							        		
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    	    
							    case "phpFileUpload":
							        $scope.phpCore.__phpfileUpload(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;								    
							    
							    case "phpRunQuery":
							        $scope.phpCore.__phpRunQuery(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    
							    
							    case "phpListOfFiles":
							        $scope.phpCore.__phpListOfFiles(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;
							    
							    
							    case "phpRunQueryForTotalEntry":
							        $scope.phpCore.__queryNumberOfEntries(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	    
							    
							    
							    case "phpSendEmail":
							        $scope.phpCore.__phpSendEmail(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    //-------- end PHP FUNCTIONS --------//							    	
							    
							    
							    //--------  ANGULAR FUNCTIONS -------//			
							    case "ping":
							        $scope.standardCore.__pong(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;    
							    case "editorCollectionSettings":
							        $scope.standardCore.__editorCollectionSettings(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "editorPageSettings":
							        $scope.standardCore.__editorPageSettings(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "editorSystemSettings":
							        $scope.standardCore.__editorSystemSettings(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;							    
							    case "scriptCreator":
							        $scope.standardCore.__scriptCreator(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    
							    case "mediaManager":
							    	
							        $scope.standardCore.__mediaManager(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;			    
							    
							    						
							    case "login":
							        $scope.standardCore.__login(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "logout":
							        $scope.standardCore.__logout(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "resetPassword":
							        $scope.standardCore.__resetPassword(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;				    
							    case "editProfileImage":
							        $scope.standardCore.__editProfilePic(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "createNewAccount":
							        $scope.standardCore.__createNewAccount(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    case "editProfileData":
							        $scope.standardCore.__editProfileData(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "deleteMyAccount":
							        $scope.standardCore.__deleteMyAccount(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    
							     
							    case "changePassword":
							        $scope.standardCore.__changePassword(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    
							    
							    case "postModal":
							        $scope.standardCore.__postModal(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;		
							    case "advSearchModal":
							        $scope.standardCore.__advSearchModal(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;						     							    						    
							    case "confirmBox":
							        $scope.standardCore.__confirmBox(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "choiceBox":
							        $scope.standardCore.__choiceBox(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    case "browserInfo":
							        $scope.standardCore.__browserInfo(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "pictureBox":						    
							        $scope.standardCore.__pictureBox(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;							    							    
							    case "showSplash":
							        $scope.standardCore.__showSplash(function(returnData){							        	
							        	if (data.execute.useCallback){							        		
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "hideSplash":
							        $scope.standardCore.__hideSplash(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    	
							    case "showThinking":
							        $scope.standardCore.__showThinking(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "hideThinking":
							        $scope.standardCore.__hideThinking(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    							    							    
							    case "refreshPage":
							        $scope.standardCore.__refreshPage(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    						    
							    case "refreshData":
							        $scope.standardCore.__refreshData(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    
							    //-------- end ANGULAR FUNCTIONS ----//									    
							    
							    default:
							      // do something
							}					   	
					  };
					    
					  return {
					    execute: execute
					  };
					
					})(),


							// shorthand for calling 
							callController:function(e, returnPacket){
				
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
					

						// create callback system for talking to the master controller							
						masterCall:(function () {
						  var execute = function(packet, callback){
								$scope._watched = {execute: null, callback: null};								
								var unbindWatch = $scope.$watch('_watched.execute', function() { 
										if($scope._watched.execute != null){
							       			$scope.broadcast($scope._watched.execute);	
							       		};	
							    });
						  		packet["order"] = executeOrder.length;						  		
						  		executeOrder.push(packet);							    
								$scope.$watch('_watched.callback', function() {										
										if($scope._watched.callback != null && $scope._watched.execute != null){
											$scope._watched.execute.execute.callback($scope._watched.callback);
											$scope._watched = {execute: null, callback: null}; 									
										}    	
										unbindWatch(); // remove event binder so it does not duplicate
							    });	
							   	$scope._watched.execute = packet;						   	
						  };
					    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 			
					
				   

				}]);  
	




//end
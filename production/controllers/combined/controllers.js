define(['custom', 'sharedData', 'firebaseSettings', 'app'], function(custom, sharedData, firebaseSettings, app){//startWrapper

//start

				app.controller('configController', 
							['$rootScope', '$stateParams', '$scope', '$timeout', '$location', '$sessionStorage', '$localStorage', 'SmoothScroll', 'cfpLoadingBar', 'toaster', 'TransactionManager', 'angularLoad',
					function($rootScope, $stateParams, $scope, $timeout, $location, $sessionStorage, $localStorage, SmoothScroll, cfpLoadingBar, toaster, TransactionManager, angularLoad) {
			
	    		// reference self via url
	    		var fileRefName		= "shared",
	    			sharedName		= decodeURIComponent($location.url().replace('/','').toLowerCase());			
					if(sharedName.indexOf("?search=") > -1){
						sharedName	= sharedName.slice(0, sharedName.indexOf("?"));
					}
					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					
								// ---------------- VARIABLES 
							    resetVariables:function(){
							    	$scope.page = {
							    		loadComponents: custom.fillArray(2),  // number should equal # of load components below
							    		isLoaded: false,							    		
							    		settings: null,
							    		checkPass: false,
							    		access: null,
							    		hasErrors: false,
							    		sidebar: "none",
							    		noTable: false,
							    		noTableError: false,
							    		
							    		searchOpen: false,
							    		editOpen: false,
							    		layoutOpen: false
							    	};
							    	
							    },
							   	//-----------------		
					
								//----------------- REFRESH
								onRefresh:function(){	
									$timeout(function(){								
										sharedData.add("currentController", fileRefName);	// current controller
										$scope.masterData = sharedData.getAll();										
										$scope.callCore.callController({who: "master", action: "hideThinking"});										
										$scope.mainCore.checkPermissions(function(){});																				
									});
								},
								//-----------------						
					
								// ---------------- INIT
								init:function(){
									
									
									custom.offCanvasReset();
									$scope.initCore.resetVariables();
									cfpLoadingBar.start();
									SmoothScroll.$goTo(0);							
									$scope.initCore.loadComponents(function(){
										sharedData.add("currentController", fileRefName);	// current controller
										
										
										
										// if parallax 
										if(!$scope.page.hasErrors){
											if ($scope.page.settings.bgType == 'parallax'){
												custom.parallaxStart();	
										   		SmoothScroll.$goTo(25).then(function() {
						 						   return SmoothScroll.$goTo(0);
						 						});
					 						}
				 						}else{
				 							console.log("Fail to load parallax.");
				 						};
												
										
										// initiate edit 
										if(!$scope.page.hasErrors){
											$scope.mainCore.checkPermissions(function(){
												$scope.editCore.init(function(){
													if ($scope.page.access.canRead){										
														$scope.mainCore.start();	
													}
												});
											});
										}else{
											console.log("Could not load page due to incorrect data from _pagedata.  Check table to make sure names match up.");
										}
										
										
									});				
								},
								//-----------------        
					        
								// ---------------- LOAD COMPONENTS
								loadComponents:function(callback){
									$scope.initCore.checkMaster(function(state){
										if (state){
											$scope.masterData = sharedData.getAll();
											$scope.page.loadComponents[0] = true;
										
											phpJS.returnJsonAsObject("/../production/settings/pagedata/" + sharedName + ".json", function(state, data){   
												

												// no data load with errors
												if (data == null){
													$scope.page.hasErrors = true;
													console.log("Failed to load correct page data.  Entry is not found or name is invalid.");													
													$scope.page.loadComponents[1] = true;
													$scope.initCore.checkLoad(callback);																											
												}
												// page data loaded
												else{
													if (!$scope.masterData.browserDetails.mobile){
														$scope.page.browserType = "desktop";
													}
													else{
														$scope.page.browserType = "mobile";
													}												
													
													// SET PAGE SETTINGS
													$scope.pagedata = data;
													$scope.page.settings = data.pageSettings[$scope.page.browserType];
													$scope.page.settings.currentTable = data[$scope.page.browserType + "Table"];	
													
													// LOAD THEME SPECIFIC CSS				
													var themeRoot = "themes/templates/" + _global_setup.theme;	
				
														// LOAD COMPONENTS FOR THEMEING
														var packetData = {
															location: themeRoot + "/layout/components"
														};																															
														$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){	
															
															layoutString = themeRoot + "/layout/";
															$scope.page.theming = {
																main: themeRoot + "/layout/html/layout.html"
															};
															
															for (i = 0; i < d.packet.data.files.length; i++){
																fileName = d.packet.data.files[i];
																$scope.page.theming[fileName] = themeRoot + "/layout/components/" + fileName;
															}
	
															// setup layout info
															$scope.page.settings.layoutUrl = themeRoot + "/layout/layout.html";																											
															$scope.page.fullSettings = data.pageSettings;	
																
															$scope.page.loadComponents[1] = true;
															$scope.initCore.checkLoad(callback);
															
														});	
				
																						
												};												
																							
											});				
										}
									});
									
			
									$scope.initCore.doSomething(function(state){
										if (state){
											$scope.page.loadComponents[1] = true;
											$scope.initCore.checkLoad(callback);
										}
									});						
								},
								//-------------------		
								
								// ------------------ LOAD COMPONENTS
								checkMaster:function(callback){
									// wait for master.js to finish loading 	
									sharedData.request("masterReady", function(state, data){
										if(state){
											if(data.ready == true){
												callback(true);
											}
											else{
												alert(data.ready);
											}
										}
										else{
											alert(data);
										}	
									});							
			
								},
								animate:function(callback){
					
																		
									if(!$scope.page.hasErrors){
										var bgType = $scope.page.settings.bgType;
										
										// set default if nothing is selected
										if (bgType == null || bgType == undefined){ bgType = "solid"; }
										
										
										switch(bgType) {
											
										    case "solid":
										    	var color = $scope.page.settings.solidColor || '#ffffff';
										    	$('#nt-colorContainer').transition({ 'background-color': color }, 750);										    		
										    	break;
										    case "static":
										    	$('#nt-colorContainer').transition({ 'background-color': 'rgba(0, 0, 0, 0)' }, 500);
										        custom.backstretchBG("themes/templates/" + _global_setup.theme + "/background/" + $scope.page.settings.staticBG, 0, 1000);
										        break;
										    case "parallax":		
										    	$('#nt-colorContainer').transition({ 'background-color': 'rgba(0, 0, 0, 0)' }, 500);								    
										   		$('#parallax-container').addClass('parallax-scroll-main').css('background', 'url(../web/themes/templates/' + _global_setup.theme + '/parallax/' + $scope.page.settings.parallaxBG + ')');										   							    	
										        break;									        
										        
										}		
									}else{
										console.log("Failed to find background type data.");
									}						
												
									$timeout(function(){	
									
										
										$('#content-page')
											.transition({   x: -20,  opacity: 0, delay: 0}, 0)
											.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
											.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
											
										callback(true);
									}, 500); // slight delay needed
									
								},
								

															
								// -- blank component for example
								doSomething:function(callback){	
									callback(true);
								},
								//-----------------		
								
								//----------------- CHECK LOAD
								checkLoad:function(callback){
									var check = true,
										array = $scope.page.loadComponents; 					
									var i = array.length; while(i--){
										if ( array[i] == false){
											check = false; 
										};
									};
									// all loads completed
									if (check){
									// CONTENT PAGE IS READY - INSERT CODE HERE		
																		
										// check for existing modal
										var duration = 2000;
										var hasModalOpen = $('.reveal-modal').hasClass('in');
										if (!hasModalOpen){ duration = 0; };
										
										$timeout(function(){ 
											cfpLoadingBar.complete();
											$scope.page.isLoaded = true;																
											$scope.callCore.callController({who: "master", action: "hideSplash"});
																
											callback();											
										}, duration);									
										
									}						
								},					
								//-----------------		
       
					    };
					})();
					//-----------------        
				
					//----------------- 
					$scope.editCore = (function() {
						
							// variables ---
							$scope.layout = {
								selected: null,
								types: [],
								currentType: null
							};
							// -------------
							 
							 return {
							 		 	
							 	//-----------------								      
								init:function(callback){
									
	
									$scope.layout.currentType = $scope.page.browserType; 
									
									$scope.editCore.getLayoutFiles(function(d){
																				
										// folder doesn't exist; create it
										if (d.data == 'no folder'){											
											callback();																				
										}										
										// no files; create a blank template
										else if (d.data == 'no files'){
											callback();		
										}										
										// files present - create selection 
										else{
											for (var i = 0; i < d.data.length; i++){
												$scope.layout.types.push(d.data[i]);
											}
											$timeout(function(){
												$scope.editCore.layout = $scope.page.settings.abstractLayout;	
											});
											
											callback();
										};	
									
										
									});
										
										
										
									
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	changeTo:function(version){
						      		 $timeout(function(){
						      		 	$scope.editCore.layout = $scope.pagedata.pageSettings[version].abstractLayout;							      		 	      		 	
						      		 });
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	layoutSave:function(){
						      										    
								    $scope.pagedata.pageSettings[$scope.layout.currentType].abstractLayout = $scope.editCore.layout;
								    // SAVE AS JSON
									phpJS.convertObjectToJson($scope.pagedata, "../production/settings/pagedata/" + $scope.pagedata.name + ".json", function(state, data){
										
									});		
									
							
									// SAVE IN DATABASE (as backup)
									var jsonString = JSON.stringify($scope.pagedata);
									phpJS.encodeHtmlEntities(jsonString, function(state, encodedString){
											
											var packet = {	pageName: $scope.pagedata.name, settings: encodedString, 
															desktopTable: $scope.pagedata.pageSettings.desktop, 
															mobileTable:  $scope.pagedata.pageSettings.mobile};
												
											phpJS.updatePageSettings(packet, function(state, data){	
												$timeout(function(){					
													toaster.pop('success', "Updated!", "Layout has been updated.");
												});
											});	
													
									});									
																
										
						      	},
						      	//-----------------
						      	

						      	//-----------------
						      	editSystemSettings:function(){
									
						      		var packetData = {system: $scope.masterData.system};
						      		$scope.callCore.callController({who: "master", action: "editorSystemSettings", data: packetData}, function(d){
						      			//console.log(d);
						      		});	
						      						      			
						      	},
						      	//-----------------						      	
						      	
						      	//-----------------
						      	editPageSettings:function(){

									var packetData = {
										location: "themes/structure/" + $scope.page.settings.currentTable + "/"
									};
																					
						      		var packetData = {pagename: sharedName, url: $scope.masterData.url, settings: $scope.page.fullSettings, displayAs: $scope.page.browserType, database: $scope.masterData.system.usingdb};
						      		$scope.callCore.callController({who: "master", action: "editorPageSettings", data: packetData}, function(d){
						      			
						      		});							      			
	
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	editCollections:function(){
									
						      		var packetData = {system: $scope.masterData.system};
						      		$scope.callCore.callController({who: "master", action: "editorCollectionSettings", data: packetData}, function(d){
						      			
						      		});	
						      		
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	updatePageSettings:function(){						      		
						      		$timeout(function(){ 
						      			$scope.page.settings.abstractLayout = $scope.editCore.layout;						      			
						      		});
						      	},
						      	//-----------------
						      	 	
						      	//-----------------						      	
								getLayoutFiles:function(callback){
									
									var packetData = {
										location: "themes/structure/" + $scope.page.settings.currentTable + "/"
									};
																					
									$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){
										
										if (d.packet.data.status == true){
												// pull data from folder
												var files = d.packet.data.files; 
												// files exist
												_return = [];
												if (d.packet.data.files.length > 0){
													for (i = 0; i < files.length; i++){
														_return.push( files[i].replace(/\.[^/.]+$/, "") );	
													};																							
													callback({status: true, data: _return});
												}
												// no files exist
												else{
													callback({status: false, data: "no files"});
												}
										}
										if (d.packet.data.status == "error"){											
											callback({status: false, data: "no folder"});
										}
										
										
										
										
										
									});
						      	},
						      	//-----------------	
						      	
						      	//-----------------						      	
								openLayoutEditor:function(callback){
									var packetData = {
										fields: $scope.collection.searchFields
									};											
									$scope.callCore.callController({who: "master", action: "editorLayout", data: packetData}, function(d){
										//console.log(d)
									});
						      	},
						      	//-----------------				      	
 
						      };							
					})();
					//----------------- 
					
					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
							//----------------- START 
							start:function(){

								// remove hidden class from initial load
								$('#nt-editor-sidebar').removeClass('hidden');
								$('#nt-editor-bottom').removeClass('hidden');
								$('#nt-layout-button-group, #nt-overlay-btn').removeClass('hidden')
																
								
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.premadeCollection.init();
		
							},
							//-----------------
							
							//----------------- check permissions and redirect
							checkPermissions:function(callback){
								var path = $location.path().replace(/[^a-zA-Z1-9 ]/g, ""),								
									allowed = $scope.masterData.accessLevel.allowed;
							
								$scope.page.checkPass = false;
								for (var i = 0; i < allowed.length; i++){
									check = allowed[i];
									if (check == path){
										$scope.page.checkPass = true;
									}
								}
								// redirect
								if(!$scope.page.checkPass){
									alert("You dont' have the proper permission to access this page.");
									$location.path("/redirect");  
								}
								else{	
									var userPermission = $scope.masterData.accessLevel.permission,
										permissionTypes = $scope.masterData.system.systemdata.permissionTypes, 
										index = custom.findWithAttr(permissionTypes, 'label', userPermission);									
										access = permissionTypes[index].access;										
										currentTable = $scope.page.settings.currentTable;										
										t = custom.findWithAttr(access, 'tableName', currentTable);
										
										// IF TABLE DOES NOT EXIST, PASS GENERIC PERMISSIONS TO AVOID ERRORS
										if (t == undefined){										  	
										  	$scope.page.settings.currentTable = false;
										  	$scope.page.settings.noTable = true;
										  	$scope.page.settings.noTableError = true;
										  	$scope.page.access = {canComment: true, canCreate: true, canDelete: true, canRead: true, canWrite: true, tableName: "none"};
										}
										else{
										  	$scope.page.settings.noTable = false;
										  	$scope.page.settings.noTableError = false;											
											$scope.page.access = access[t];
										}											 
									callback();
								}
							},
							//-----------------
							
							//----------------- PICTURE BOX 
							pictureBox:function(_list, _index){								
								var packetData = {
									list: _list,
									index: _index
								};
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.callCore.callController({who: "master", action:"pictureBox", data: packetData}, function(d){
									//console.log(d);						
								});		
							},
							//-----------------							
							

						};
					})();
					//-----------------  
					
					
					
					//-----------------  COLLECT FROM DATABASE PREMADE FUNCTIONS
					$scope.premadeCollection = (function(){
					
						var listOfTemplates = {};
						
						return { 
							
							
							//-----------------
							resetVariables:function(){
									// ----------------------------------
									
									var settings = $scope.page.settings,
										 _nFx = {x: 0, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0},
										 _pFx = {x: 0, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0},
										 _speed = 500;

									if (settings.ani_enter != undefined){

											switch(settings.ani_enter.toLowerCase()) {
											    case "slide left":
											        _nFx = {x: -800, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "slide right":
											        _nFx = {x: 800, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade left":
											        _nFx = {x: -100, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade right":
											        _nFx = {x: 100, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;									    
											    case "fade up":
											        _nFx = {x: 0, y: -100, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade down":
											        _nFx = {x: 0, y: 100, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;		
											    case "zoom in":
											        _nFx = {x: 0, y: 0, scale: 1.5, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "shrink out":
											        _nFx = {x: 0, y: 0, scale: 0.5, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;		
											    case "skew left":
											        _nFx = {x: 0, y: 0, scale: 1, skewX: '-90deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "skew right":
											        _nFx = {x: 0, y: 0, scale: 1, skewX: '90deg',  skewY: '0deg',  opacity: 0};
											    break;	
											    case "none":
											        _nFx = {x: 0, y: 0, scale: 1, skewX: '90deg',  skewY: '0deg',  opacity: 1};
											    break;										    										    									        
											}
											
											
											switch(settings.ani_exit.toLowerCase()) {
											    case "slide left":
											        _pFx = {x: -800, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "slide right":
											        _pFx = {x: 800, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade left":
											        _pFx = {x: -100, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade right":
											        _pFx = {x: 100, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;									    
											    case "fade up":
											        _pFx = {x: 0, y: -100, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade down":
											        _pFx = {x: 0, y: 100, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;		
											    case "zoom in":
											        _pFx = {x: 0, y: 0, scale: 1.5, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "shrink out":
											        _pFx = {x: 0, y: 0, scale: 0.5, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;		
											    case "skew left":
											        _pFx = {x: 0, y: 0, scale: 1, skewX: '-90deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "skew right":
											        _pFx = {x: 0, y: 0, scale: 1, skewX: '90deg',  skewY: '0deg',  opacity: 0};
											    break;	
											    case "none":
											        _nFx = {x: 0, y: 0, scale: 1, skewX: '90deg',  skewY: '0deg',  opacity: 1};
											    break;										    
											}	
											
											switch(settings.ani_speed) {
											    case 1:
											        _speed = 250;
											    break;	
											    case 2:
											        _speed = 500;
											    break;	
											    case 3:
											        _speed = 1000;
											    break;										    									    
											}
									
									}	
									
									
	
									$scope.transitionFX = {
										using: "nextFX",  // default
										nextFX: _nFx,
										backFX: _pFx,
										duration: _speed,										
										enabled: settings.ani_enable,
										rush: settings.ani_rush
									};
									
									
									
								
									if ($scope.transitionFX.enabled){
										$scope.myStyle={"opacity":"0"};
									}
									else{
										$scope.myStyle={"opacity":"1"};
									}
									
									
									
							    	$scope.collection = {
							    		
							    		table: $scope.page.settings.currentTable, 
							    		imageStorage: "uploads/images/" + $scope.page.settings.currentTable + "/",		 // 
							    		searchWhere: "",
							    		
							    		pagination:{
							    			form: settings.layout,
							    			showPagination: settings.showPagination,
							    			showSort: settings.showSort,
							    			showFilter: settings.showFilter, 
							    			showAdvancedSearch: settings.showAdvancedSearch,
							    			showTotalEntries: settings.showTotalEntries,
							    			showNextBtn: settings.showNextBtn,
							    			showBackBtn: settings.showBackBtn,
							    			showRandomBtn: settings.showRandomBtn,
							    			morePerClick: settings.fetchPerClick, 
							    			multipBackBtn: settings.multipBackBtn,
							    			multipNextBtn: settings.multipNextBtn,
							    			defaultPagiLimit: settings.defaultPagiLimit,
							    			entriesOnPage: settings.entriesOnPage,
							    			equalizeEntries: settings.equalizeEntry, 
							    			sortBy: settings.sortBy,
							    			loadMsg: settings.loadMsg,
							    			
							    			count: 0,						 	// default 
							    			current: 1, 					 	// 
							    			limit: 5,							// entries per page (initial value)							    			
							    			filerBy: null,
							    			reverse: true,
							    			max: null,
							    			filter: null,
							    			hasNext: null,
							    			hasPrev: null,
							    			isLast: null,
							    			isFirst: null,
							    			range: [
							    				{id: 1, label: 5},
							    				{id: 2, label: 10},
							    				{id: 3, label: 25},
							    				{id: 4, label: 50}				    				
							    			],
							    			selectResults: null
							    		},
			
							    		remember:{
							    			useLocalStorage: true,
							    			rememberWith:  "id",
							    			hasStorage: false
							    		},
							    		
							    		
										allEntries: [],
							    		useEntries: null,	
							    		numberOfEntries: null,		    		
							    		
							    		filterSearch: null,
							    		searchResultsCount: null,
							    		searchType: null,
							    		searchTerm: null,
							    		searchFields: null,
										userEntries: [], 										
							    	};	
							    	
							    	
							    	
							    	// ----------------------------------
			
									// ----------------------------------	
									$scope.entryStatistics = {
										hasFirst: 		null,
										hasPrev: 		null,
										hasNext: 		null,
										hasLast: 		null,										
										hasMore:   		null,
										hasMorePage:    null,
										hasLessPage:    null,
										
										startAt:		0,
										available: 		null,
										total: 			null
									};
									// ----------------------------------	
									
									// ----------------------------------
									// when exiting this page	
									$scope.$on('$destroy', function() {
										//$scope.premadeCollection.saveSearch();
									});
									// ----------------------------------	
										
							    	// ----------------------------------
							    	$scope.firebasePermission = {
							    		fbID_required_toCreate: true,
							    		fbID_required_toEdit: true,
							    		fbID_required_toDelete: true,		
							    		
							    		fbIDmustMatchEntryId_edit: true,
							    		fbIDmustMatchEntryId_delete: true
							    	};
							    	// ----------------------------------
							    	
									// ----------------------------------
									$scope.$storage 		= $sessionStorage;	
									$scope.editBtn 			= {value: 0};
									$scope.deleteBtn 		= {value: 0};
									$scope.loadingEntries 	= true;
									$scope.isUsingAdvanced 	= false;
									// ----------------------------------	
																
							},
							//-----------------
							
							
							//-----------------
							init:function(){
								
								$scope.premadeCollection.resetVariables();
								$scope.initCore.animate(function(state){ });
								
								// table exists
								if ($scope.collection.table != false){
										var packetData = {	
											database: $scope.masterData.system.usingdb,
											table: $scope.collection.table
										};
										
										// get entry row totals
										$scope.callCore.callController({who: "master", action:"phpRunQueryForTotalEntry", data: packetData}, function(d){
											$scope.entryStatistics.total = d.packet.data.rows;
											
											
											// get table data
											$scope.callCore.callController({who: "master", action: "phpGetTableDetails", data: packetData}, function(d){ 
												
												$scope.collection.searchFields = custom.returnPropertyInObject(d.packet.data.formData, ["Field", "Type", "inputType", "Comment"]);
		
												// search comments for isSearchable attribute
												$scope.collection.searchFields.isSearchable = []; 
												for (i = 0; i < $scope.collection.searchFields.Comment.length; i++){
													var c = $scope.collection.searchFields.Comment[i];
													
													if (c.indexOf("isSearchable") > -1){
														$scope.collection.searchFields.isSearchable[i] = true;
													}
													else{
														$scope.collection.searchFields.isSearchable[i] = false;
													}
												}
												
												$scope.collection.pagination.filterBy = $scope.collection.searchFields.Field[0];
												$scope.premadeCollection.initialLoad();
											});									
										});
								}
								// table does not exists
								else{
									$scope.premadeCollection.initialLoad();
								}
							
							},
							//-----------------
							
							//-----------------
							
							layoutUrl:function(index){	
																	
								if(!listOfTemplates.hasOwnProperty(index)){
									listOfTemplates[index] = $scope.page.settings.abstractLayout;
								}				

								if (listOfTemplates[index] == "none"){												
									//  		themes/templates/default					/layout/templates/no_template.html 						
									return "themes/templates/" + _global_setup.theme + "/layout/html/empty.html";
								}
								else{
									return "themes/structure/" + $scope.page.settings.currentTable + "/" + listOfTemplates[index] + ".html";
								}								
							},	

							
							
							//-----------------
							
							//-----------------
							initialLoad:function(){
								
								
								function loadDefault(){
									  
									  // reset
									  $scope.collection.userEntries = [];
									  
									  // load initial for single page 
									  if ($scope.collection.pagination.form == "single"){
									  		
										  	$scope.collection.pagination.startFromEntry = $scope.entryStatistics.total - 1;
									 		$scope.collection.pagination.currentEntry = $scope.entryStatistics.total - 1;								  											  							  	
											$scope.premadeCollection.fetchMostRecent(1);
									  }	
									  
									  // growing 
									  if ($scope.collection.pagination.form == "multi"){
									  		if ($scope.entryStatistics.total > $scope.collection.pagination.morePerClick){
												$scope.collection.pagination.startFromEntry = $scope.entryStatistics.total - $scope.collection.pagination.morePerClick;
												$scope.collection.pagination.currentEntry = $scope.entryStatistics.total - $scope.collection.pagination.morePerClick;
											}	
											else{
									 			$scope.collection.pagination.startFromEntry = 0;
									  			$scope.collection.pagination.currentEntry = 0;
									  		}
											
																			  		  	
										$scope.premadeCollection.fetchMostRecent($scope.collection.pagination.morePerClick);
									  }									  			
									  // if multiple per page
									  if($scope.collection.pagination.form == "multip"){
									  		if ($scope.entryStatistics.total > $scope.collection.pagination.entriesOnPage){
									 			$scope.collection.pagination.startFromEntry = $scope.entryStatistics.total - $scope.collection.pagination.entriesOnPage;
									  			$scope.collection.pagination.currentEntry = $scope.entryStatistics.total - $scope.collection.pagination.entriesOnPage;
									  		}
									  		else{
									 			$scope.collection.pagination.startFromEntry = 0;
									  			$scope.collection.pagination.currentEntry = 0;
									  		}
									  		
									  		// start from lmost recent and go down
											$scope.premadeCollection.fetchNextPage(false, null);
									  }							
									  
								}
								
								
								if ($scope.collection.table != false){
									$timeout(function(){
										$scope.loadingEntries = true;
										
											// if search string is in the url
											var searchString = $stateParams.search; 
											if (searchString != null){
												
																							
												var key = searchString.substr(0, searchString.indexOf(':'));
												var value = searchString.split(':')[1];
													value = value.replace(/_/g, " ");
												var query = "SELECT * FROM " + $scope.collection.table +  " WHERE " + key + " = " + "\""+ value + "\"";
													
												
												 	$scope.premadeCollection.fetchEntries(query, null, function(){
													    SmoothScroll.$goTo(0);								// scroll back to top
													    $scope.loadingEntries = false;						// hide loading
													    //$scope.premadeCollection.isUsingAdvanced = true;   // reset advanced
													    //$scope.entryStatistics.hasMore = false;			   // hides more
												 	 });											
											}
											// no search string - continue with default loading
											else{																   
											  	loadDefault();										
											};
									});
								}
								else{
									 $scope.loadingEntries = false;
								} 
							},
							//-----------------
							
							//-----------------
							clearAdvancedSearch:function(){
								$location.url($location.path());
								$scope.collection.userEntries = [];
								$scope.premadeCollection.initialLoad();
							},
							//-----------------
							
							//-----------------						
							loadFullEntry:function(index){	
								listOfTemplates[index] = $scope.page.settings.fullLayout;
								
								// resize	
								$('.content-entry-container').css('height', 'auto');
																
								
								if($scope.collection.pagination.equalizeEntries){
									custom.equalize('.content-entry-container', 1000, 1);
								}
						
							},	
							//-----------------	
							
							
							//-----------------
							toggleSideButtons:function(item){
								
								$timeout(function(){
									
									
									if ($scope.page.sidebar == item){
										$scope.page.sidebar = "none";
									}
									else{
										$scope.page.sidebar = item;
									};
									
									
									
									if (item == "search"){
										$scope.premadeCollection.toggleSearch();
										$scope.premadeCollection.toggleSettings(false);
										$scope.premadeCollection.toggleLayout(false);
									}
									if (item == "advsearch"){
										$scope.premadeCollection.advancedSearch();
										
										$scope.premadeCollection.toggleSearch(false);
										$scope.premadeCollection.toggleSettings(false);
										$scope.premadeCollection.toggleLayout(false);										
									}
									if (item == "settings"){
										$scope.premadeCollection.toggleSearch(false);
										$scope.premadeCollection.toggleSettings();
										$scope.premadeCollection.toggleLayout(false);
									}
									if  (item == "layout"){
										$scope.premadeCollection.toggleSearch(false);
										$scope.premadeCollection.toggleSettings(false);
										$scope.premadeCollection.toggleLayout();
									}
									
																
								});
							},					
							//-----------------

							//-----------------
							toggleSearch:function(state){
								
								if (state == undefined){
									$scope.page.searchOpen = !$scope.page.searchOpen;
								}
								else{
									$scope.page.searchOpen = state;
								};
								
								theHeight = parseInt($('#nt-search-bar').height()) + parseInt($('#nt-search-bar').css('padding'))*2; 								
								if ($scope.page.searchOpen){															
									$('#nt-search-bar').transition( {y: theHeight  }, 500 );
								}
								else{
									$('#nt-search-bar').transition( {y: 0 },  500 );
								}
	
							},
							//-----------------		
							
							
							//-----------------		
							toggleSettings:function(state){
								
								if (state == undefined){
									$scope.page.editOpen = !$scope.page.editOpen;
								}
								else{
									$scope.page.editOpen = state;
								};


								if($scope.page.editOpen){
									$('#siteContent')
										.transition({ x: parseInt($('#nt-editor-sidebar').width()) }, 500, 'ease');;	
									$('#content-header')
										.transition({ x: parseInt($('#nt-editor-sidebar').width()) }, 500, 'ease');;						
									$('#content-footer')
										.transition({ x: parseInt($('#nt-editor-sidebar').width()) }, 500, 'ease');;	
									$('#nt-editor-sidebar')											
										.transition({ x: parseInt($('#nt-editor-sidebar').width()) }, 500, 'ease');;	
								}
								else{
									$('#siteContent')
										.transition({ x: 0 }, 500, 'ease');											
									$('#content-header')
										.transition({ x: 0 }, 500, 'ease');									
									$('#content-footer')
										.transition({ x: 0 }, 500, 'ease');	
									$('#nt-editor-sidebar')											
										.transition({   x: 0 }, 500, 'ease');																		
								}
								
							},	
							//-----------------		
							
							
							//-----------------		
							toggleLayout:function(state){		
								

									if (state == undefined){
										$scope.page.layoutOpen = !$scope.page.layoutOpen;
									}
									else{
										$scope.page.layoutOpen = state;
									};
								
									height = parseInt($('#nt-editor-bottom').height()) + parseInt($('#nt-editor-bottom').css('padding'))*2;									
									if($scope.page.layoutOpen){
										$('#nt-editor-bottom')
											.transition({   y: -(height) }, 500, 'ease');									
									}
									else{
										$('#nt-editor-bottom')
											.transition({   y: 0 }, 500, 'ease');
									};	
								
							},	
							//-----------------		
							
							
							//-----------------
							generalSearch:function(keyword){
								
								// disabled buttons
								$('#nt-keyword-search, #nt-keyword-search-btn').attr('disabled', true);
								
								var q = 'SELECT * FROM ' + $scope.collection.table + ' WHERE id LIKE "%' + keyword + '%"';
								// start on one; skip id
								for (i = 1; i < $scope.collection.searchFields.Field.length; i++){
									q += " OR " +  $scope.collection.searchFields.Field[i] + " LIKE '%" + keyword + "%' "; 
								};
								
								var packet = {
									database: $scope.masterData.system.usingdb, 
									query: q
								};																
								phpJS.queryDatabase(packet, function(state, data){
									
									if(data.status == "error"){										
										$timeout(function(){
											// enable buttons
											$('#nt-keyword-search, #nt-keyword-search-btn').attr('disabled', false);
											toaster.pop('error', "", "No search results found.");
										});			
									}else{
			  			  				var idString = "";
						  			  	for (i = 0; i < data.length; i++){
						  			  		if (i != 0){ idString += ", "; }
						  			  		idString += data[i].id;
						  			  	}			
						  			  	if (data.length > 0){				
						  			  		$scope.entryStatistics.total = data.length; 		
											$scope.collection.searchWhere = " WHERE id IN(" + idString + ") ";
											$scope.premadeCollection.initialLoad();
										}	
										$('#nt-keyword-search, #nt-keyword-search-btn').attr('disabled', false);
										toaster.pop('success', "", "Found " + data.length + " entries.  Pulling them up now.");
									}
									
								});		
														
							},
							
							clearSearch:function(){
								
								// disabled buttons
								$('#nt-keyword-search-clear').attr('disabled', true);								
								
								var packetData = {	
									database: $scope.masterData.system.usingdb,
									table: $scope.collection.table
								};								
								
								$scope.callCore.callController({who: "master", action:"phpRunQueryForTotalEntry", data: packetData}, function(d){
									$timeout(function(){
										$scope.entryStatistics.total = d.packet.data.rows;								
										$scope.collection.searchWhere = '';
										$scope.premadeCollection.initialLoad();
										// disabled buttons
										$('#nt-keyword-search-clear').attr('disabled', false);		
									});								
								});
							},
							//-----------------
							
							//-----------------
							setFilter:function(){
								var packet = {
									objToPaginate: $scope.collection.userEntries,
									filterBy: $scope.collection.pagination.filterBy,
									reverse: $scope.collection.pagination.reverse,
									filterSize: 100,
								};									
								custom.sortAndPage( packet );							
							},
							//-----------------
							
							//-----------------
							saveSearch:function(){
								if ($scope.collection.remember.useLocalStorage){
									toaster.pop('success', "", "Save search results.");
						    		// build remember string
						    		var rememberArray = custom.returnPropertyInObject($scope.collection.allEntries, [$scope.collection.remember.rememberWith])[$scope.collection.remember.rememberWith],
						    			rememberString = '';									
									for (i = 0; i < rememberArray.length; i++){
										if (i != 0){ rememberString += ", "; }
										rememberString += rememberArray[i]; 
									};								
									$scope.$storage.rememberMe = {										
										key: $scope.collection.remember.rememberWith,
										value: rememberString			    
									};
   								}
							},		
							//-----------------
							
							
							//-----------------
							loadPriorSearch:function(){
								  if ($scope.$storage.rememberMe != undefined){		
									  	$scope.collection.remember.hasStorage = true;										

										var packet = {
											header: "Load saved entries?",
											field1: "Yes", 
											field2: "Cancel"
										};	
										$scope.callCore.callController({who: "master", action: "confirmBox", data: packet}, function(d){ 
											
											if (d.packet.status != 'dismiss'){
												if (d.packet.data){	
											  	  var query = 'SELECT * FROM ' + $scope.collection.table + ' WHERE ' + $scope.$storage.rememberMe.key + ' IN(' + $scope.$storage.rememberMe.value + ')';
											  	  $scope.loadingEntries = true;									// hide loading
											  	  $scope.collection.userEntries = [];
												  $scope.premadeCollection.fetchEntries(query, null, function(){
														 SmoothScroll.$goTo(0);								// scroll back to top
														 $scope.loadingEntries = false;						// hide loading
														 $scope.premadeCollection.isUsingAdvanced = true;   // reset advanced
														 $scope.entryStatistics.hasMore = false;			   // hides more
												  });	
												}		
											}
										});		
									}							
								
							},
							//-----------------
							
							//-----------------
							fetchMostRecent:function(limitTo){
								$scope.loadingEntries = true;
								$timeout(function(){								  
									  var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id " + $scope.collection.pagination.sortBy + " LIMIT " + $scope.collection.pagination.currentEntry + ", " + limitTo;
									  $scope.collection.allEntries = [];  
									  $scope.premadeCollection.fetchEntries(query, null, function(){
									     //SmoothScroll.$goTo(0);
										 $scope.loadingEntries = false;						// hide loading
										 $scope.premadeCollection.isUsingAdvanced = false;   // reset advanced
										 $scope.entryStatistics.hasMore = true;			   // hides more									    
									  });
								});
								
							},
							//-----------------
							
							//-----------------		
							fetchFirst:function($event){
								if ($scope.collection.pagination.currentEntry > 0){									
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
										
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
										$scope.collection.pagination.currentEntry = 0;
										
	  		 						 	// transition fx
	  								 	$scope.transitionFX.using = "nextFX";										
										
										var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id " + $scope.collection.pagination.sortBy + " LIMIT 0, 1";  
										$scope.premadeCollection.fetchEntries(query, null, function(){
											$(btn).html(btnRaw).attr('disabled', false);  												
										});	
									
								};	
							},	
							//-----------------		
							
							//-----------------		
							fetchRecent:function($event){
								
								if ( ($scope.collection.pagination.currentEntry + 1) < $scope.entryStatistics.total){									
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
										
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
										$scope.collection.pagination.currentEntry = ($scope.entryStatistics.total - 1);
										
	  		 						 	// transition fx
	  								 	$scope.transitionFX.using = "nextFX";										
										
										var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT 0, 1";  
										$scope.premadeCollection.fetchEntries(query, null, function(){
											$(btn).html(btnRaw).attr('disabled', false);  												
										});	
									
								};	
							},	
							//-----------------																
							
							
							//-----------------		
							fetchPrevPage:function(asBtn, $event){
								
							  	if (asBtn){	
							  		// now correct if 
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();											
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);															  		 
						  			
						  		}								
								
						  		// first check if more entries than entries per page						  		
						  		if ($scope.collection.pagination.currentEntry - $scope.collection.pagination.entriesOnPage > 0){						  			
						  		 	startAt = $scope.collection.pagination.currentEntry - $scope.collection.pagination.entriesOnPage;
						  		 	numberOfEntries = $scope.collection.pagination.entriesOnPage;
						  		 	$scope.collection.pagination.currentEntry = startAt; 						  		   						  		 	
						  		}
						  		
						  		else{							  						
						  		 	startAt = 0;
						  		 	numberOfEntries = $scope.entryStatistics.total - ($scope.entryStatistics.total - $scope.collection.pagination.currentEntry);
						  		 	$scope.collection.pagination.currentEntry = numberOfEntries; 
																		  		 	
						  		}

						  		
  		 						// transition fx
  								$scope.transitionFX.using = "backFX";
								var query = "SELECT * FROM " + $scope.collection.table +  $scope.collection.searchWhere + " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + startAt + ", " + numberOfEntries;   						
								
								$scope.premadeCollection.fetchEntries(query, null, function(){
										if (asBtn){$(btn).html(btnRaw).attr('disabled', false);};
										//SmoothScroll.$goTo(0);								// scroll back to top
										$scope.loadingEntries = false;						// hide loading
										$scope.premadeCollection.isUsingAdvanced = false;   // reset advanced
										$scope.entryStatistics.hasMore = true;			   // hides more											 
								});	
								
								
								  
							},	
							//-----------------	
							
							//-----------------		
							fetchNextPage:function(asBtn, $event){
								
								if (asBtn){								
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);									
								}
								
								
								if ($scope.collection.pagination.currentEntry + $scope.collection.pagination.entriesOnPage < $scope.entryStatistics.total){																																	
									 	startAt = $scope.collection.pagination.currentEntry;									 	
										numberOfEntries = $scope.collection.pagination.entriesOnPage;
									 	$scope.collection.pagination.currentEntry = startAt + $scope.collection.pagination.entriesOnPage; 										 								 
								}
								else{																												
									 	startAt = $scope.collection.pagination.currentEntry;									 	
										numberOfEntries = $scope.collection.pagination.entriesOnPage;
									 	$scope.collection.pagination.currentEntry = startAt; 										 	
								}
								
								// unique for onload 									
								if (!asBtn){
									 	startAt = $scope.entryStatistics.total - $scope.collection.pagination.entriesOnPage;
									 	if ($scope.collection.pagination.entriesOnPage > $scope.entryStatistics.total){
									 		startAt = 0;
									 	}
										numberOfEntries = $scope.collection.pagination.entriesOnPage;										
									 	$scope.collection.pagination.currentEntry = startAt; 	
								}
								
							
  		 						// transition fx
  								$scope.transitionFX.using = "nextFX";  								 
								var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere + " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + startAt + ", " + numberOfEntries;   									
								
								
								$scope.premadeCollection.fetchEntries(query, null, function(){
											if (asBtn){$(btn).html(btnRaw).attr('disabled', false);};  
											$scope.loadingEntries = false;						// hide loading
											$scope.premadeCollection.isUsingAdvanced = false;   // reset advanced
											$scope.entryStatistics.hasMore = true;			   // hides more											 
								});	
								
							},	
							//-----------------								
														
							
							//-----------------
							fetchEntry:function(whereStatement, insertInto, callback){
								$scope.loadingEntries = true;
								$timeout(function(){
								  // pull the most recent entry, but only one
								  var query = "SELECT * FROM " + $scope.collection.table + " " + whereStatement;  
								  $scope.premadeCollection.fetchEntries(query, insertInto, function(){								   
								   	$scope.loadingEntries = false;
								   	callback();
								  });
								});
							},
							//-----------------		
							
							//-----------------		
							fetchNext:function($event){
																
								if ($scope.collection.pagination.currentEntry + 1 < $scope.entryStatistics.total){									
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
										
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
										$scope.collection.pagination.currentEntry ++;
										
	  		 						 	// transition fx
	  								 	$scope.transitionFX.using = "nextFX";										
										
										var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + $scope.collection.pagination.currentEntry + ", " + 1;  
										$scope.premadeCollection.fetchEntries(query, null, function(){
											$(btn).html(btnRaw).attr('disabled', false);  												
										});	
									
								};									
								
							},	
							//-----------------	
							

							//-----------------	
							fetchPrev:function($event){
								
								if ($scope.collection.pagination.currentEntry - 1 >= 0){
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
											
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
										$scope.collection.pagination.currentEntry --;	
										
	  		 						 	// transition fx
	  								 	$scope.transitionFX.using = "backFX";										
										
										var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + $scope.collection.pagination.currentEntry + ", " + 1;  
										$scope.premadeCollection.fetchEntries(query, null, function(){
											$(btn).html(btnRaw).attr('disabled', false);  
											//SmoothScroll.$goTo(0);										
										});	
								};
								
							},	
							//-----------------	
							
							//-----------------		
							fetchRandom:function($event){
								
								$scope.collection.pagination.currentEntry = custom.getRandomNumber(0, ($scope.entryStatistics.total -1) );		
																
								var btn = $event.target;
									btnText = $(btn).text(),
									btnRaw = $(btn).html();	
									
									$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
									
									
  		 						 	// transition fx
  								 	$scope.transitionFX.using = "nextFX";									
									
									var query = "SELECT * FROM " + $scope.collection.table +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + $scope.collection.pagination.currentEntry + ", " + 1;  
									$scope.premadeCollection.fetchEntries(query, null, function(){
										$(btn).html(btnRaw).attr('disabled', false);  											
									});	
									
													
								
							},	
							//-----------------											

							//-----------------
							fetchMore:function($event){
								
								// turn into thinking
								var btn = $event.target;
									btnText = $(btn).text(),
									btnRaw = $(btn).html();	
									
								$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);								
								
								
								var checkDiff = $scope.entryStatistics.total - $scope.collection.userEntries.length; 

				 
						  		if (checkDiff > $scope.collection.pagination.morePerClick){
						  		 	$scope.collection.pagination.currentEntry -= $scope.collection.pagination.morePerClick;
						  		 	if ($scope.collection.pagination.currentEntry <= 0){ 
						  		 		$scope.entryStatistics.hasMore = false;
						  		 	}
						  		 	var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " +  $scope.collection.pagination.currentEntry  + ", " + $scope.collection.pagination.morePerClick;
						  		}
						  		else{								  		 					  		 
						  		 	var diff = $scope.entryStatistics.total - $scope.collection.userEntries.length;						  		 						  		 							  		 
						  		 	var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " +  0  + ", " + diff;						  		 	
						  		}	

								

	 						 	// transition fx
							 	$scope.transitionFX.using = "nextFX";						  		 						
								$scope.premadeCollection.fetchEntries(query, null, function(){
									$(btn).html(btnRaw).attr('disabled', false);  
								});
								
								
							},
							//-----------------
	
							//-----------------
							fetchEntries:function(query, insertInto, callback){	
									
								
									var queryString = {	
										query: query, 
										database: $scope.masterData.system.usingdb
									};
								
									$scope.callCore.callController({who: "master", action: "phpRunQuery",  data: queryString}, function(d){ 																

											
											// sanitize html types that use html entities (adv)
											var htmlEntities = [];
											var allInputTypes = $scope.collection.searchFields.inputType;
											var allFields = $scope.collection.searchFields.Field;
											for (var i = 0; i < allInputTypes.length; i++){
												var inputType = allInputTypes[i]; 
												if (inputType == 'adv' || inputType == 'imageUpload'){
													htmlEntities.push({data: allFields[i], type: inputType});
												}
											}													

											processed = 0;
											var entryPacket = d.packet.data; 
											function loopCycle(){
												
												//  cycle												
												if (processed < entryPacket.length){
			
														var subProcess = 0;	
														function subloopCycle(){
																												
															if (subProcess < htmlEntities.length){																	
																filterField = [htmlEntities[subProcess].data];
																filterType = htmlEntities[subProcess].type;
																
																
																filterMe = entryPacket[processed][filterField];
																phpJS.dencodeHtmlEntities(filterMe , function(state, data){																	
												    				escapedStrings = data.replace(/["']/g, "");
												    				
												    				// parse json string into object
												    				if (filterType == 'imageUpload'){
												    					if (data != null && data != undefined && data != ''){
												    						entryPacket[processed][filterField] = JSON.parse(data);
												    					}
												    					else{
												    						entryPacket[processed][filterField] = data; 
												    					}
												    				}
												    				// html text
												    				else{
												    					entryPacket[processed][filterField] = data; 
												    				}
												    				
																	subProcess++;
																	subloopCycle();													    				
												    			});																															
															}
															else{
																subloopComplete();
															}
														};
														subloopCycle();
														
														function subloopComplete(){
															processed++;															
															loopCycle();
														}								
												}	
												//  loop complete
												else{	
													// if inserting into an existing array location
													if (insertInto != null){				
														$timeout(function(){																							
															$scope.collection.userEntries[insertInto] = entryPacket[0];
															$scope.premadeCollection.initiateFX(true);													
														});
														callback();	
													}
													// normal behavior
													else{																								
														$scope.premadeCollection.arrangeEntries(d.packet.data, callback);	
													}
												}	
												
											};
											loopCycle();	
										
									});	
							},	
							//-----------------		
							
							//-----------------
							filterCorrection:function(){
								$timeout(function(){
									$('.content-entry-container').css('opacity', 1);
								}, 0);
							},
							
							initiateFX:function(reset){
										if ($scope.collection.pagination.form == "multi"){		
											if (reset){startPoint = 0}									
											$timeout(function(){$scope.premadeCollection.returnFX(startPoint);}, 0);
										}
										if ($scope.collection.pagination.form == "single" || $scope.collection.pagination.form == "multip"){  // single entry or multi page
											$timeout(function(){$scope.premadeCollection.returnFX(0);}, 0);
										}								
							},
							
							backFX:function(){
								if ($scope.transitionFX.enabled){
									$scope.transitionFX.using = "backFX";
	  								$('.content-entry-container').each(function(index){
	  								 	$(this).transition( $scope.transitionFX.backFX ,{delay: index * ($scope.transitionFX.duration/2) }, $scope.transitionFX.duration);
	  								});								
								}
								if($scope.collection.pagination.equalizeEntries){
									custom.equalize('.content-entry-container', 500, 3);
								}
							},	
														
							nextFX:function(){								
								if ($scope.transitionFX.enabled){
									$scope.transitionFX.using = "nextFX";
	  								$('.content-entry-container').each(function(index){	  		  		  																					
	  								 	$(this).transition( $scope.transitionFX.nextFX ,{delay: index * ($scope.transitionFX.duration/2) }, $scope.transitionFX.duration);
	  								});								
								}
								if($scope.collection.pagination.equalizeEntries){
									custom.equalize('.content-entry-container', 500, 3);
								}
							},	
							
							returnFX:function(threshold){		
								
								
								if ($scope.transitionFX.enabled){
	  								$('.content-entry-container').each(function(index){
	  									 
	  									 delaySpd = (index - $scope.entryStatistics.startAt) * ($scope.transitionFX.duration/2); 
	  									
	  								 	 if (index >= threshold && delaySpd >= 0){
	  								 	 	
	  								 	 	
		  								 	 $(this).transition( $scope.transitionFX[$scope.transitionFX.using] ,{delay: delaySpd }, $scope.transitionFX.duration)
		  								 	 		.transition({ 	x: 0, 
		  								 							y: 0, 
		  								 							scale: 1, 
		  								 							opacity: 1,		  								 							
																	skewX: '0deg',
																	skewY: '0deg',
		  								 							delay: ($scope.transitionFX.duration/2) }, $scope.transitionFX.duration);
	  								 	}
	  								});	

  								};
								if($scope.collection.pagination.equalizeEntries){
									custom.equalize('.content-entry-container', 500, 3);
								}		
							},
							//-----------------		
							
							//-----------------
							arrangeEntries:function(entryPackages, callback){
								
								var transitionStartTime = null;
									
  		 						// transition fx
  		 						if ($scope.collection.pagination.form == "single" || $scope.collection.pagination.form == "multip"){
  										$scope.premadeCollection[$scope.transitionFX.using]();
  										transitionStartTime = entryPackages.length * $scope.transitionFX.duration; 
  								}

								// for multi, lets animation pickup where it started instead of having a large delay
								if ($scope.collection.pagination.form == "multi"){  // growing
										$scope.entryStatistics.startAt = $scope.collection.userEntries.length;
										transitionStartTime = 0; 
								};
								
								// rush transition exits  								
						  		if($scope.transitionFX.rush){
						  			transitionStartTime = 0; 
						  		};	 						
  		 						
  		 						
  		 						$timeout(function(){	
										


										// push data into entries;		
																									
										$scope.collection.allEntries = [];																	
										for (i = 0; i < entryPackages.length; i++){
											$scope.collection.allEntries.push(entryPackages[i]);
										};	
										$scope.collection.allEntries.reverse();
										
										
										// sort entries into userEntires object;
												
										startPoint = $scope.collection.userEntries.length; 
										for (i = 0; i < $scope.collection.allEntries.length; i++){
											if ($scope.collection.pagination.form == "multi"){  // growing
												$scope.collection.userEntries.push($scope.collection.allEntries[i]);												
											};
											if ($scope.collection.pagination.form == "single" || $scope.collection.pagination.form == "multip"){  // single entry or multi page
												$scope.collection.userEntries[i] = $scope.collection.allEntries[i];	
											};																									
										}
										
										
										// remove unused entries for single or multip
										
										if ($scope.collection.pagination.form == "single" || $scope.collection.pagination.form == "multip"){ 
											if ($scope.collection.userEntries.length > entryPackages.length){
												deleteSize = $scope.collection.userEntries.length; 								
												for (i = entryPackages.length; i < deleteSize; i++){											
													$scope.collection.userEntries.splice(entryPackages.length, 1);
												}
											}
										}
										
										
										
										// multi					      returns true/false
										$scope.entryStatistics.hasMore  = !($scope.collection.userEntries.length >= $scope.entryStatistics.total);
										
										// single page 
										$scope.entryStatistics.hasFirst = (!$scope.collection.pagination.currentEntry == $scope.collection.userEntries.length);											
										$scope.entryStatistics.hasPrev  = (!$scope.collection.pagination.currentEntry > 0);											
										$scope.entryStatistics.hasNext  = (!(($scope.collection.pagination.currentEntry + 1) < $scope.entryStatistics.total));
										$scope.entryStatistics.hasLast  = (($scope.collection.pagination.currentEntry + 1) == $scope.entryStatistics.total);
										
										// multip entries
										//$scope.entryStatistics.hasLessPage = (($scope.collection.pagination.currentEntry < $scope.collection.pagination.entriesOnPage));
										//$scope.entryStatistics.hasMorePage = (($scope.collection.pagination.currentEntry + $scope.collection.pagination.entriesOnPage) >= $scope.entryStatistics.total);
									
										
										// ONLY RUNS ONCE - SHOWS BUTTONS
										$scope.premadeCollection.initiateFX(false);	
											
										if (callback != null || callback != undefined){
											callback();
										}
										
											
								}, transitionStartTime);									
											
									
							},
							//-----------------
							
							//-----------------
							advancedSearch:function(){
								
								
								packet = {
									isSearchable: $scope.collection.searchFields.Comment,
									fields: $scope.collection.searchFields.Field,
									types: $scope.collection.searchFields.Type,
									objToQuery: $scope.collection.allEntries,
									database: $scope.masterData.system.usingdb,
									table: $scope.collection.table
								};
								
								$scope.callCore.callController({who: "master", action: "advSearchModal", data: packet}, function(d){ 									
									if (d.packet.status  != 'dimiss'){
										if (d.packet.data.length > 0){
											$scope.entryStatistics.total = d.packet.data.length; 
											$scope.collection.searchWhere = " WHERE id IN(" + d.packet.data.returnData + ") ";
											$scope.premadeCollection.initialLoad();
										}
									}
									$scope.page.sidebar = "none";
									
								});
							},
							//-----------------

							//-----------------
							// addImagePacket = {
							//   allImages: [{name: pageName, src: data64String, thumbnail: data64String}]	
							//   newImages: [{name: pageName, src: data64String, thumbnail: data64String}]
							//   uploadId: 10
							// } 
							addImagesTo:function(addImagePacket, callback){
											
											
											
											// declare variables
											var _all = addImagePacket.allImages,
												_added = addImagePacket.newImages,
												_id = addImagePacket.uploadId,
												_next = function(){ count++; loop(); }, 
												count = 0;
											
											
														
											// loop function								
											function loop(){

												//  loop conditions
												if (count < _added.length){
													
													delete _added[count].parse;   // remove property so it doesn't get uploaded again
													
													// upload full size image
													function uploadFullSize(){
														var imageSrc = _added[count].src;
														var packet = {
															file: imageSrc,
															name:  _added[count].name,
															location: $scope.collection.imageStorage + _id
														};	
														
														phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
															if (state){
																$timeout(function(){
																	_added[count].src = data.src;
																	uploadThumbnail();
																});			
															}																						
														});																								
		
													}
													// upload thumbnail size image
													function uploadThumbnail(){
														var imageSrc = _added[count].thumbnail;
														var packet = {
															file: imageSrc,
															name: _added[count].name + "_thumbnail",
															location: $scope.collection.imageStorage + _id
														};
														
														phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
															if (state){
																$timeout(function(){
																	_added[count].thumbnail = data.src;
																	_next()
																});			
															}																						
														});																																													
													}																
													
													// loop logic 
													if (_added.length > 0){
														uploadFullSize();
													}
													// loop complete
													else{
														complete()
													}
													
												}
												
												// end loop conditions
												else{																						
													complete();
												}
											}
											// end loop function
											
											// complete function
											function complete(){
												_all = JSON.stringify(_all);	
												callback(_all);	
											}
											
											// start loop
											loop();	

							},
							//-----------------

							//-----------------
							editEntry:function($event, id, index){
									
									$scope.premadeCollection.buttonThink( $($event.target) );
								
									// grab entry information
									var queryString = {	
										query: "SELECT * FROM " + $scope.collection.table + " WHERE id=" + id, 
										database: $scope.masterData.system.usingdb
									};
									$scope.callCore.callController({who: "master", action: "phpRunQuery",  data: queryString}, function(d){ 											
											var editData = d.packet.data[0];		
											var packetData = {	
												database: $scope.masterData.system.usingdb,
												table: $scope.collection.table
											};
											// call the create module
											$scope.callCore.callController({who: "master", action: "phpGetTableDetails",  button: $($event.target), data: packetData}, function(d){ 	
												if (d.packet.status == 'success'){	
																				
																															
																				
													var tableFields = {
														form: d.packet.data.formData,
														meta: d.packet.data.metaData,
														editData: editData
													};
													// post modal
													$scope.callCore.callController({who: "master", action: "postModal", data: tableFields}, function(d){ 
														if (d != undefined && d.packet.status == "closed"){
		     												
		     												
		     												
														    // build update string
														    var buildString = "",
														    	loopCount = 0;
														    	
														    function addToString(fieldLabel, entry){
														    	buildString += fieldLabel + "='" + entry + "', ";
																loopCount++;
																loopCycle();
														    };	
														    
														    function loopCycle(){
															   	
															   	if (loopCount < tableFields.form.length){
															   		
															   		
															    	var fieldLabel = tableFields.form[loopCount].Field;
															    	var newEntry = d.packet.data.returnData[loopCount];
															    	var inputType = tableFields.form[loopCount].inputType;
															    	
															    	if (newEntry != undefined && newEntry != null && newEntry != "Invalid Date"){			
															    																	    		
															    		// sanitize advanced texts
															    		if(inputType == 'adv'){															    		
															    			phpJS.encodeHtmlEntities(newEntry, function(state, data){
															    				addToString(fieldLabel, data);
															    			});
															    		}
															    		
															    		// image uploads
															    		else if(inputType == 'imageUpload'){		
																				
																				// delete unused function
																				function deleteUnusedImages(){
																					var packetData = {
																						location: $scope.collection.imageStorage + id
																					};
																					
																					$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){
																						$timeout(function(){

																							// parse information
																							if (d.packet.data.files != null && d.packet.data.files != null){
																								
																								
																								var filesInDirectory = [];
																								for (var i = 0; i < d.packet.data.files.length; i++){
																									var f = (d.packet.data.files[i]).replace(/\.[^/.]+$/, "");
																									if (!/thumbnail/i.test(f)){  // exclude thumbnails names
																										filesInDirectory.push( f );
																									}
																								};
				
																								// delete files no longer in use
																								var currentFiles = [];
																								for (var i = 0; i < newEntry.length; i++){
																									var f = newEntry[i].name;
																									currentFiles.push( f );
																								}																						
																								
																								// get all files to delete
																								var toBeDeleted = custom.compareArrayForDifferences(filesInDirectory, currentFiles);
		
																								// get image and thumbnail
																								var packet = [];
																								for (var i = 0; i < toBeDeleted.length; i++){
																									packet.push({folder: "../" +  $scope.collection.imageStorage + id, file: toBeDeleted[i] + ".png"});
																									packet.push({folder: "../" +  $scope.collection.imageStorage + id, file: toBeDeleted[i] + "_thumbnail.png"});
																								}
																							
																								// delete files from																						
																								if( packet.length > 0 ){
																									$scope.callCore.callController({who: "master", action: "phpDeleteFiles", data: packet}, function(_d){																																																																																					
																										addNewImages();																																														
																									});
																								}
																								else{
																									addNewImages();
																								}
																								
																								
																							}
																							// nothing to parse, skip to add
																							else{																							
																								addNewImages();
																							}
																					
																						});
																					
																					});			
																				};
																				// end delete image function
																				
																				// add image functions		
																				function addNewImages(){
																		
																					// get files to add
																					var addList = [];
																					for (var i = 0; i < newEntry.length; i++){
																						if (newEntry[i].parse){
																							addList.push( newEntry[i] );
																						}
																					}	
																					
																					var addImagePacket = {
																						allImages: newEntry,
																						newImages: addList,
																						uploadId: id
																					};
																					
																					$scope.premadeCollection.addImagesTo(addImagePacket, function(data){
																						phpJS.encodeHtmlEntities(data, function(state, _data){
																							addToString(fieldLabel, _data);
										    											});		
																						
																						
																					});

																				};	
																				// end add image function		
			
																				// start
																				deleteUnusedImages();
															    			
															    		}															    		
															    		else if(inputType == 'date'){
															    		  	newEntry = newEntry.replace(/\D/g,'');
															    		  	addToString(fieldLabel, newEntry);
															    		}
															    		else if(inputType == 'time'){
															    		  	newEntry = moment(newEntry).format( "H:mm:ss");
															    		  	addToString(fieldLabel, newEntry);
															    		}
															    		else{
																    		addToString(fieldLabel, newEntry);
																    	}	
																    	// end sanitized edits before saving
																    	
															    	}
															    	else{
																	    loopCount++;
																	    loopCycle();															    		
															    	}
															    	
															    }
															    else{
															    	loopComplete();
															    }
															   
														    }
														    
														    
														    
														   function loopComplete(){
														  		
														   		buildString = buildString.replace(/,\s*$/, "");
														   		
															    // edit update
															    var packetData = {
															   		query: "UPDATE " + $scope.collection.table + " SET " + buildString + " WHERE id=" + id,
															   		database: $scope.masterData.system.usingdb
															    };	
															    // update edit!																      		
																$scope.callCore.callController({who: "master", action: "phpModifyEntry", button: $($event.target),  data: packetData}, function(d){ 
																	var whereStatement = 'WHERE id=' + id;
																	
																	$scope.premadeCollection.fetchEntry(whereStatement, index, function(){
																		toaster.pop('success', "", "Your post has been edited successfully.");
																	});
																});
																
																
															
															}
															// end edit
															
															// start loop
															loopCycle();
														}
													});	
													// /end post modal													
												}	
											});	
											// end call create module															
									});	
									// end grab edit informatoin
									
							},
							//-----------------	

							//-----------------
							deleteEntry:function(id, index){
									var packet = {
										header: "Delete this entry?",
										field1: "Yes", 
										field2: "No"
									};
								
									// confirm box
									$scope.callCore.callController({who: "master", action: "confirmBox", data: packet}, function(d){ 
										if (d.packet.data == true){		
												toaster.pop('warning', "", "Deleting...");
												// delete from database
												var queryString = {	
													query: "DELETE FROM " + $scope.collection.table + " WHERE id=" + id, 
													database: $scope.masterData.system.usingdb
												};
												// delete all information
												$scope.callCore.callController({who: "master", action: "phpModifyEntry",  data: queryString}, function(d){ 
													
													// delete all images from folder
													$scope.premadeCollection.deleteFolder($scope.collection.imageStorage + id, function(){
														$timeout(function(){
															$scope.collection.userEntries.splice(index, 1);
															$scope.collection.allEntries.splice(index, 1);
															$scope.entryStatistics.total --;
															toaster.pop('success', "", "Your post has been deleted successfully.");
														});		
													});					
												});	
												// end delete
										}				
									});	
									// end confirm								
							},
							//-----------------
							
							//-----------------
							deleteFolder:function(folder, callback){				
							   	
							    	var packetData = {	
										folder: "../" + folder
									};

											
									$scope.callCore.callController({who: "master", action: "phpDeleteFolder", data: packetData}, function(d){
										 															
										callback(d);
									});								
								
							},
							//-----------------
							
							//-----------------
							backToTop:function(){
									// go to top
							    	//SmoothScroll.$goTo( $('#contentAnchor') );	
							},
							//-----------------
				
							
							//-----------------
							paginateEntries:function(type){					

									/*
									if (type == "first"){
										$scope.collection.pagination.current = 1; 
									}
									if (type == "last"){
										$scope.collection.pagination.current = $scope.collection.pagination.max;
									}
									if (type == "add"){
										if ($scope.collection.pagination.current + 1 <= $scope.collection.pagination.max){
											$scope.collection.pagination.current++; 
										}
									}
									if (type == "sub"){
										if ($scope.collection.pagination.current - 1 > 0){
											$scope.collection.pagination.current--; 
										}
									}										
									
									var packet = {
										objToPaginate: $scope.collection.allEntries,
										filterBy: $scope.collection.pagination.filterBy,
										reverse: $scope.collection.pagination.reverse,
										filterSize: $scope.collection.pagination.limit,
									};
								

									var _results =  custom.sortAndPage( packet );
										// apply to page
									$timeout(function(){
										// content for the main page
										$scope.collection.userEntries = _results.page( $scope.collection.pagination.current );;

										// get pagination data
							    		$scope.collection.pagination.max = _results.totalPages;						    		
							    		$scope.collection.pagination.hasNext = _results.hasNext();
							    		$scope.collection.pagination.hasPrev = _results.hasPrev();
							    		
							    		if ($scope.collection.pagination.max == $scope.collection.pagination.current ){
							    			$scope.collection.pagination.isLast = true;
							    		}else{
							    			$scope.collection.pagination.isLast = false;
							    		}
							    		if (_results.currentPage == 1 ){
							    			$scope.collection.pagination.isFirst = true;
							    		}else{
							    			$scope.collection.pagination.isFirst = false;
							    		}	
							    	});		
							    	*/	
									
								
							},							
							//-----------------				

							//-----------------
							openNewEntry:function($event, tableName){
								$scope.premadeCollection.buttonThink( $($event.target) );
												
								var packetData = {	
									database: $scope.masterData.system.usingdb, 
									table: $scope.collection.table
								};
								$scope.callCore.callController({who: "master", action: "phpGetTableDetails",  button: $($event.target), data: packetData}, function(d){ 	
									if (d.packet.status == 'success'){								
										var tableFields = {
											form: d.packet.data.formData,
											meta: d.packet.data.metaData,
											editData: null
										};
										$scope.callCore.callController({who: "master", action: "postModal", data: tableFields}, function(d){ 
												if(d.packet.status == "closed"){
													var packetData = {
											 			database: $scope.masterData.system.usingdb,
											 			table: $scope.collection.table,
											 			fields: tableFields.form, 
											 			inputData: d.packet.data.returnData 									 			
													};											
													$scope.premadeCollection.createNewEntry(packetData);
												}
										});														
									}	
								});	
							},
							//-----------------
							
							//-----------------
							createNewEntry:function(packetData){
								
								$timeout(function(){
									toaster.pop('info', "", "Submitting your entry.");
								});
								var cloneArray = $.extend(true, [], packetData.inputData);

								// GET NEXT IN ID
								var queryString = {	
									table: $scope.collection.table, 
									database: $scope.masterData.system.usingdb
								};
							
								$scope.callCore.callController({who: "master", action: "phpCreateBlankEntry",  data: queryString}, function(d){ 
										
										var nextId = d.packet.data.id;

										// SANITIZE ENTRIES OF NEW POSTS
										var counter = 0; 
										var _next = function(){
											counter++;
											setLoop();
										};								
										function setLoop(){
											
											
											if (counter < packetData.fields.length){
													var type = packetData.fields[counter].Type,
														inputType = packetData.fields[counter].inputType;
												
													if (type == "time"){
														cloneArray[counter] = moment(packetData.inputData[counter]).format( "H:mm:ss");
														_next();											
													}
													else if (inputType == "imageUpload"){
														var index = counter; 
														var subCounter = 0; 
													
														var addImagePacket = {
															allImages: packetData.inputData[index],
															newImages: packetData.inputData[index],
															uploadId: nextId
														};
														
														$scope.premadeCollection.addImagesTo(addImagePacket, function(data){															
															cloneArray[index]  = data; 
															_next();
														});
												
													}
													else{
														_next();
													}													
		
											}
											else{
													imageloopComplete();
											}
											counter++;
										}								
										setLoop();  // start loop
										
										
										function imageloopComplete(){
											
											// clone array has been formmated correctly
											packetData.inputData = cloneArray; 
											
											// sanitize and ready edit statement
 											custom.sanitizePacketsForDatabase(packetData, function(sanitizedData){	
											    // edit update
											    var queryPacket = {
											   		query: custom.returnEditString($scope.collection.table, sanitizedData, nextId),
											   		database: $scope.masterData.system.usingdb
											    };								     		
										    
											    // update newly created entry!													    		
												$scope.callCore.callController({who: "master", action: "phpModifyEntry",  data: queryPacket}, function(d){ 
													if (d.packet.status == "success"){			
																												
														
														/*
														if ($scope.collection.pagination.form == "multip" || $scope.collection.pagination.form == "multip"){ 
															var insertInto = null;															
														}
														if ($scope.collection.pagination.form == "single"){ 
															
														}														
														
														$scope.entryStatistics.total++;
														$scope.premadeCollection.initialLoad();
														var whereStatement = 'WHERE id IN(' + nextId + ')';			
														$scope.premadeCollection.fetchEntry(whereStatement, insertInto, function(){															
															toaster.pop('success', "", "Your post has been uploaded successfully.");
														});
														*/
														location.reload();
														
													};		
												});
											
											});		

										}
								});
								
							},
							//-----------------		
							
							//-----------------
							buttonThink:function(btn){
								var btnText = $(btn).text();								
								$(btn).html('<i class="fa fa-spinner fa-pulse"></i> ' + btnText).attr('disabled', true);
							},
							//-----------------							
								
						};		
					})();
					//-----------------  
					
					

					

					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: fileRefName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileRefName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileRefName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 										
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileRefName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 	


				}]);  
	



//end
//start

				
				app.controller('firstLoginController', ['$stateParams', '$state', '$rootScope', '$scope', '$timeout', 'SmoothScroll', 'cfpLoadingBar', 
						function($stateParams, $state, $rootScope, $scope, $timeout, SmoothScroll, cfpLoadingBar) {	   
				    
				    var fileName  = 'firstLogin';
				    
					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					
								
					
								// ---------------- VARIABLES 
							    resetVariables:function(){
							    	$scope.page = {
							    		loadComponents: custom.fillArray(3),  // number should equal # of load components below
							    		isLoaded: false,
							    	};
							    	$scope.content = "I am test content.";
							    },
							   	//-----------------		
					
								//----------------- REFRESH
								onRefresh:function(){
									sharedData.add("currentController", fileName);	// current controller
									$scope.masterData = sharedData.getAll();
									$scope.callCore.callController({who: "master", action: "hideThinking"});
										
								},
								//-----------------						
					
								// ---------------- INIT
								init:function(){
									custom.offCanvasReset();
									$scope.initCore.resetVariables();
									cfpLoadingBar.start();
									SmoothScroll.$goTo(0);							
									$scope.initCore.loadComponents(function(){
										sharedData.add("currentController", fileName);	// current controller
										$scope.mainCore.start();
									});				
								},
								//-----------------        
					        
								// ---------------- LOAD COMPONENTS
								loadComponents:function(callback){
									$scope.initCore.checkMaster(function(state){
										if (state){
											$scope.masterData = sharedData.getAll();
											$scope.page.loadComponents[0] = true;
											$scope.initCore.animate(function(state){
												if (state){
													$scope.page.loadComponents[1] = true;
													$scope.initCore.checkLoad(callback);
												}
											});									
											
										}
									});
			
									$scope.initCore.doSomething(function(state){
										if (state){
											$scope.page.loadComponents[2] = true;
											$scope.initCore.checkLoad(callback);
										}
									});						
								},
								//-------------------		
								
								// ------------------ LOAD COMPONENTS
								checkMaster:function(callback){
									// wait for master.js to finish loading 	
									sharedData.request("masterReady", function(state, data){
										if(state){
											if(data.ready == true){
												callback(true);
											}
											else{
												alert(data.ready);
											}
										}
										else{
											alert(data);
										}	
									});							
			
								},
								animate:function(callback){
									if ($scope.masterData.browserDetails.mobile){
										$('#' + fileName + '_id').addClass('gray-gradient');							
									}
									// for desktop
									else{
										custom.backstretchBG("media/background.jpg", 0, 1000);
									}		
									$timeout(function(){	
										$('#content-page')
											.transition({   x: -20,  opacity: 0, delay: 0}, 0)
											.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
											.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
											
										callback(true);
									}, 500); // slight delay needed
								},
								
								// -- blank component for example
								doSomething:function(callback){
									callback(true);
								},
								//-----------------		
								
								//----------------- CHECK LOAD
								checkLoad:function(callback){
									var check = true,
										array = $scope.page.loadComponents; 					
									var i = array.length; while(i--){
										if ( array[i] == false){
											check = false; 
										};
									};
									// all loads completed
									if (check){
									// CONTENT PAGE IS READY - INSERT CODE HERE		
																		
										// check for existing modal
										var duration = 2000;
										var hasModalOpen = $('.reveal-modal').hasClass('in');
										if (!hasModalOpen){ duration = 0; };
										
										$timeout(function(){ 
											cfpLoadingBar.complete();
											$scope.page.isLoaded = true;																
											$scope.callCore.callController({who: "master", action: "hideSplash"});
																
											callback();											
										}, duration);									
										
									}						
								},					
								//-----------------																	        
					        
					        
					    };
					})();
					//-----------------      


					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.mainCore.checkUser();	
							},
							//-----------------

							//-----------------
							checkUser:function(){
								
								if ($scope.masterData.logState == false){
					 				var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );						
									var check = 0; checkPass = true; 
									var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {
												
											if (check > 0 && checkPass){ 	
												// FAILURE TO LOGIN
												if (error != null){
													$scope.mainCore.setPageStatus("fail");	
													checkPass = false;
												}
												// LOGIN SUCCESSFUL
												else{
													$scope.page.user = user;
													$scope.mainCore.setPageStatus("success");											
													checkPass = false;
												}
											}
											else{
												check++;
											}
											$scope.page.isLoaded = true;
			
											
										});		
										auth.login('password', {  // PASSWORD IS THE METHOD ()
										  email:		$scope.page.email,
										  password: 	$scope.page.tempPassword
										});		
									}
									else{								
										$state.go('home');						
									}
					  				
							 },	
							//-----------------
							
							//-----------------
						    first_login:function(){
								callController({who: "master", action: "login"}, function(data){
									console.log(data)
								});
		                 	},
							//-----------------
							
							
							//-----------------
		                    first_createAccount:function(){
								callController({who: "master", action: "createNewAccount"}, function(data){
									if(data.data == "updated"){ 
										alert("new account created");	
									}
								});	
		                    },
							//-----------------
							
							//-----------------		
							setPageStatus:function(type){
								$timeout(function(){ 
									callController({who: "master", action: "hideSplash"});	
									$scope.page.status = type;
									$scope.$apply();										
								});							
							},	
							//-----------------					
					
							//-----------------
							update:function(){
								if ($scope.formData.password.length < 8){
									alert("Password must be at least 8 characters in length.");
								}
								else if ($scope.formData.password != $scope.formData.confirm){
									alert("Passwords do not match.  Please try again.");
								}
								else{
									callController({who: "master", action: "showThinking"});
									
									var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );
									var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {		});								
									auth.changePassword($scope.page.email, $scope.page.tempPassword, $scope.formData.password, function(error, success) {							  
									  if (!error) {	
									  	alert("Your password has been updated.  Please login.");			   	
									    $state.go('home');			  								    
									  }
									  else{							  								  	
									  	alert(error);
									  	callController({who: "master", action: "hideThinking"});
									  }
									});	
		
								}
							},
							//-----------------					
					
					
						};
					})();
					//-----------------  

					
					








					
					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		console.log(data);
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 	
				   

				}]);		

//end
//start

			
				app.controller('footerController', ['$rootScope', '$scope', '$timeout', '$location',
				function($rootScope, $scope, $timeout, $location) {	   
					
	    			// reference self via url
	    			var sharedName		= decodeURIComponent($location.url().replace('/','').toLowerCase());			
					if(sharedName.indexOf("?search=") > -1){
						sharedName	= sharedName.slice(0, sharedName.indexOf("?"));
					}  
					
					// ---------------- INIT CORE					
					$scope.initCore = (function () {
					    return {
					    	
							// ---------------- VARIABLES 
						    resetVariables:function(){
						    	$scope.page = {
						    		loadComponents: custom.fillArray(2),  // number should equal # of load components below
						    		isLoaded: false,
						    		browserType: null	
						    	};
						    },
						   	//-----------------							    	
					    	
							// ---------------- INIT
							init:function(){
								$scope.initCore.resetVariables();
							
								$scope.initCore.loadComponents(function(){					
									$scope.mainCore.start();
								});					
							},
							//-----------------
							
							//----------------- REFRESH
							onRefresh:function(callback){
								$scope.masterData = sharedData.getAll();
								$scope.$apply();		
							},
							//-----------------							
							
							
							// ---------------- LOAD COMPONENTS
							loadComponents:function(callback){
								$scope.initCore.checkMaster(function(state){
									if (state){
										$scope.page.loadComponents[0] = true;
										$scope.initCore.checkLoad(callback);
									}
								});
								$scope.initCore.doSomething(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										$scope.initCore.checkLoad(callback);
									}
								});							
							},
							//-------------------
							
							
							// ------------------ SAMPLE COMPONENTS
							checkMaster:function(callback){
								// wait for master.js to finish loading 		
								sharedData.request("masterReady", function(state, data){
									if(state){
										if(data.ready == true){
											callback(true);
										}
										else{
											alert(data.ready);
										}
									}
									else{
										alert(data);
									}	
								});	
							}, 
							
							doSomething:function(callback){
								// LOAD A COMPONENT						
								callback(true);
							},
							//-----------------
							
							
							//----------------- CHECK LOAD
							checkLoad:function(callback){
								var check = true,
									array = $scope.page.loadComponents; 					
								var i = array.length; while(i--){
									if ( array[i] == false){
										check = false; 
									};
								};
								// all loads completed
								if (check){		
									$timeout(function(){
										$scope.page.isLoaded = true;					
										$scope.masterData = sharedData.getAll();	
										callback();			
									});									
								}						
							},					
							//-----------------



					    };
					})(); 
				   	//-----------------
				      				    				      
				      

				
					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								
								// GET PAGE SETTINGS
								if (!$scope.masterData.browserDetails.mobile){
									$scope.page.browserType = "desktop";
								}
								else{
									$scope.page.browserType = "mobile";
								}			
								$scope.pagedata = [];
													
								sharedData.request("pagedata_" + sharedName, function(state, data){
									$scope.pagedata = data.pageSettings[$scope.page.browserType];
									 

									// GIVE SECOND FOR POTENTIAL IMAGES TO LOAD
									$timeout(function(){
										custom.initSlick();
									}, 1000);											
								});
										
							},
							//-----------------
							
							//-----------------
							browserInfo:function(){
									$scope.callCore.callController({who: "master", action: "browserInfo"}, function(data){
										
									});
							},
							//-----------------									
							
			
						};
					})();
					//-----------------  


					
					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: sharedName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(sharedName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: sharedName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: sharedName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 				
				    
				}]);
				


//end
//start

			
				app.controller('headerController', ['$scope', '$timeout', '$rootScope', 'browserInfo', 'psResponsive', '$localStorage', '$sessionStorage', '$detection', '$modal', 'uiModalCtrl', 'accountModalCtrl', '$location', 
				function($scope, $timeout, $rootScope, browserInfo, psResponsive, $localStorage, $sessionStorage, $detection, $modal, uiModalCtrl, accountModalCtrl, $location) {	   

	    			// reference self via url
	    			var sharedName		= decodeURIComponent($location.url().replace('/','').toLowerCase());			
					if(sharedName.indexOf("?search=") > -1){
						sharedName	= sharedName.slice(0, sharedName.indexOf("?"));
					}  

					// ---------------- INIT CORE					
					$scope.initCore = (function () {
					    return {
					    	
							// ---------------- VARIABLES 
						    resetVariables:function(){
							      $scope.page = {
							    	  loadComponents: custom.fillArray(2),
							    	  isLoaded: false,
							    	  gapLoaded: false,
							    	  browserType: null							    	  
							      };

						    },
						   	//-----------------							    	
					    	
							// ---------------- INIT
							init:function(){
								$scope.initCore.resetVariables();
								$scope.initCore.loadComponents(function(){									
									$scope.mainCore.start();
								});					
							},
							//-----------------
							
							//----------------- REFRESH
							onRefresh:function(callback){
								$scope.masterData = sharedData.getAll();
								$scope.$apply();		
							},
							//-----------------							
							
							
							// ---------------- LOAD COMPONENTS
							loadComponents:function(callback){
								$scope.initCore.checkMaster(function(state){
									if (state){
										$scope.page.loadComponents[0] = true;
										$scope.initCore.checkLoad(callback);
									}
								});
								$scope.initCore.doSomething(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										$scope.initCore.checkLoad(callback);
									}
								});							
							},
							//-------------------
							
							
							// ------------------ SAMPLE COMPONENTS
							checkMaster:function(callback){
								// wait for master.js to finish loading 		
								sharedData.request("masterReady", function(state, data){
									if(state){
										if(data.ready == true){
											callback(true);
										}
										else{
											alert(data.ready);
										}
									}
									else{
										alert(data);
									}	
								});	
							}, 
							
							doSomething:function(callback){
								// LOAD A COMPONENT						
								callback(true);
							},
							//-----------------
							
							
							//----------------- CHECK LOAD
							checkLoad:function(callback){
								var check = true,
									array = $scope.page.loadComponents; 					
								var i = array.length; while(i--){
									if ( array[i] == false){
										check = false; 
									};
								};
								// all loads completed
								if (check){		
									$timeout(function(){
										$scope.page.isLoaded = true;				
										$scope.masterData = sharedData.getAll();	
										callback();			
									});									
								}						
							},					
							//-----------------



					    };
					})(); 
				   	//-----------------


					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
									
									
								
								
									// GET PAGE SETTINGS
									if (!$scope.masterData.browserDetails.mobile){
										$scope.page.browserType = "desktop";
									}
									else{
										$scope.page.browserType = "mobile";
									}		
									$scope.pagedata = [];
																
									sharedData.request("pagedata_" + sharedName, function(state, data){
										
										$scope.pagedata = data.pageSettings[$scope.page.browserType]; 
										
										// GIVE SECOND FOR POTENTIAL IMAGES TO LOAD
										$timeout(function(){
											custom.initSlick(function(){										
												$scope.page.gapLoaded = true;
											});
										}, 1000);											
									});
								
	
									// pull pages from master data
									$scope.pages =  $scope.masterData.system.systemdata.pages;
									
							},
							//-----------------
							
							//-----------------
							logout:function(){
								$scope.callCore.callController({who: "master", action: "logout"}, function(data){});
		 					},
							//-----------------									
							
							//-----------------		
							login:function () {
								$scope.callCore.callController({who: "master", action: "login"}, function(data){
									if(data.data == "updated"){ 
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "showThinking"});
										}, 300);
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "refreshData"});
										}, 600);
									}
									
								});
							},
							//-----------------		
					
						};
					})();
					//----------------- 
					
								

					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: sharedName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(sharedName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: sharedName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: sharedName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ----------------   

					
				}]);			
				
				


//end
//start

	
				var fileName  = 'master';
			
				app.controller('masterController', [
								'$state', '$stateParams', '$rootScope', '$scope', '$timeout', '$modal',  
								'$detection', '$localStorage', '$sessionStorage', 
								'SmoothScroll', 'angularLoad',  
								'browserInfo', 'cfpLoadingBar', 'browserInfo', 
								'accountModalCtrl', 'uiModalCtrl', 'formsModalCtrl', 'editorModalCtrl',
								
					function(	$state, $stateParams, $rootScope, $scope, $timeout, $modal,  
								$detection, $localStorage, $sessionStorage, 
								SmoothScroll, angularLoad,  
								browserInfo, cfpLoadingBar, browserInfo, 
								accountModalCtrl, uiModalCtrl, formsModalCtrl, editorModalCtrl) {

			
					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					    	
						    // ---------------- VARIABLES 
						    resetVariables:function(){
							    $scope.page = {
							    	loadComponents: custom.fillArray(9),				    	
							    	browserSettings: browserInfo.giveMeAllYouGot()
							    };
						    },
						   	//-----------------

							// ---------------- INIT
							init:function(){

								console.groupCollapsed("Init Sequence");
								console.time("Loadtime");
								$scope.initCore.resetVariables();					
								$scope.initCore.loadComponents(function(){
									$scope.mainCore.start();
								});
																												
							},
							//-----------------
							
							//----------------- REFRESH
							masterRefresh:function(){	
								    
									console.groupCollapsed("Refresh Sequence");
									console.time("Loadtime");
										
									var currentPage = sharedData.getAll().currentController; 
									$scope.callCore.callController({who: "master", action: "showThinking"});
									
		    						$scope.initCore.resetVariables();
		   						    sharedData.clearAll();	
									$scope.initCore.loadComponents(function(){
										
									
																	
										$timeout(function(){
											sharedData.add("masterReady", {ready: true});	
											
											// REFRESH CURRENT PAGE
											var packet = {
													info:{to: currentPage,	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);
											
											// REFRESH HEADER
											var packet = {
													info:{to: "header",	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);	
											
											// REFRESH FOOTER
											var packet = {
													info:{to: "footer",	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);	
											
											// REFRESH OVERLAY
											var packet = {
													info:{to: "offcanvas",	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);	
											
											// REFRESH OFF CANVAS
											var packet = {
													info:{to: "overlay",	from: fileName},
													execute: { name: "refresh",	callback: false	}
												};					
											$scope.broadcast(packet);
																				
											console.timeEnd("Loadtime");
											console.groupEnd("Refresh Sequence");
																		
										}, 2000);
											
									});
							},
							//-----------------							
							
							//----------------- LOAD COMPONENTS 
							loadComponents:function(_callback){
													
								$scope.initCore.initalizeDatabase(function(state){								
									if (state){
										
										if ($scope.page.loadComponents[0] != true){
										  $scope.page.loadComponents[0] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});
								
								$scope.initCore.fetchSystemData(function(state){
									if (state){
										if ($scope.page.loadComponents[1] != true){
									  	  $scope.page.loadComponents[1] = true;
										  $scope.initCore.checkLoad(_callback);
										}																		
									}										
								});
								
								
								$scope.initCore.fetchUrl(function(state){														
									if (state){
										
										if ($scope.page.loadComponents[2] != true){
									  	  $scope.page.loadComponents[2] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});
								
								
								$scope.initCore.fetchIPAddress(function(state){							
									if (state){
										
										if ($scope.page.loadComponents[3] != true){
										  $scope.page.loadComponents[3] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});	
								
									
								$scope.initCore.detectType(function(state){												
									if (state){
										
										if ($scope.page.loadComponents[4] != true){
										  $scope.page.loadComponents[4] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});	
								
																		
								$scope.initCore.userData(function(state){
									if (state){
										
										if ($scope.page.loadComponents[5] != true){
										  $scope.page.loadComponents[5] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});
								
								
								$scope.initCore.userDetails(function(state){											
									if (state){
										if ($scope.page.loadComponents[6] != true){
										  $scope.page.loadComponents[6] = true;
										  $scope.initCore.checkLoad(_callback);
										}
									}
								});	
								
								
								$scope.initCore.getBrowserDetails(function(state){														
									if (state){
										
										if ($scope.page.loadComponents[7] != true){
										  $scope.page.loadComponents[7] = true;
										  $scope.initCore.checkLoad(_callback); 
										}
									}
								});	
								
								
								$scope.initCore.resolveOverflow(function(state){
									
									if (state){
										
										if ($scope.page.loadComponents[8] != true){
										  $scope.page.loadComponents[8] = true;
										  $scope.initCore.checkLoad(_callback); 	
										}
									}
								});	
									
							},
							//-------------------
							
							
							// LOAD COMPONENTS
							initalizeDatabase:function(callback){
								
								
								custom.databaseInit(function(status){
									if (status){								
										callback(true);
									}
									else{
										alert("Timeout: Database could not be reached.");
									}
								});
								
							},
							

							fetchSystemData:function(callback){
								
								
								phpJS.returnJsonAsObject("/../production/settings/systemdata/system.json", function(state, data){

									if (state){
										sharedData.add('system', data );
										callback(true);
									}else{
										alert("Timeout:  Could not fetch system data.");
									}									
									
										 
								});		
													
							},

							
							fetchUrl:function(callback){
								phpJS.getFullUrl(function(state, data){
									if (state){
										sharedData.add('url', JSON.parse(data) );
										callback(true);
									}else{
										alert("Timeout:  Could not fetch URL.");
									}
								});
							},
							
							fetchIPAddress:function(callback){					
								phpJS.getIPAddress(function(state, data){
									if (state){
										sharedData.add('ipAddress', JSON.parse(data) );
										callback(true);
									}else{
										alert("Timeout: IP Address could not be retrieved.");
									}	
								});						
							},
							
							detectType:function(callback){
							  	var type = '';
							  	if ($detection.isAndroid()){
							  		type = "android";
							  	}
							  	else if($detection.isiOS()){				  		
							  		type = "ios";
							  	}
							  	else if($detection.isWindowsPhone()){
							  		type = "windows-phone";
							  	}
							  	else{				  		
							  		type = "desktop";
							  	}
								sharedData.add('ios', type);					   											
								callback(true);						
							},
							
							userData:function(callback){
								
					  			firebaseSettings.getPermissionLevel(function(isUser, accessLevel){			  				
		
					  			
					  				sharedData.add("accessLevel", accessLevel);
					  				callback(true);					  				
					  			});	
					  			
							},
							
							userDetails:function(callback){
						  		var userData = [];	
		
							  		firebaseSettings.checkUserData(function(returnState, data){					  			
										if(returnState){
											// retrieved user data
											userData = data.user;	
											userData["avatar"] = data.image;
											userData["auth"] = JSON.parse(localStorage.getItem("user")).auth; 
											sharedData.add("logState", true);	
											localStorage.setItem("logState", true);
																											
										}
										else{
											// not logged in
											userData = {
												id: null,
												email: null,
												firstName: null,
												lastname: null,
												permission: 'guest',										
												userName: 'anonymous',
											};
											userData["avatar"] = {small: null, standard: null, thumbnail: null};
											userData["auth"] = null;
											sharedData.add("logState", false);
											localStorage.setItem("logState", false);
										};
										sharedData.add("userInfo", userData);
										callback(true);								
									});				
							},
							
							getBrowserDetails:function(callback){
								 var size = null,
								 	 width = browserInfo.giveMeAllYouGot().screenSize.width; 
								 
								 if (width < 550){
								 	size = "small";
								 }
								 if (width >= 550 && width < 1080){						
								 	size = "medium";
								 }
								 if (width >= 1080){						
								 	size = "large";
								 }		
								 sharedData.add('browserSize', size);				 
								 sharedData.add('browserDetails', browserInfo.giveMeAllYouGot() );
								 callback(true);
							},
							
							resolveOverflow:function(callback){
								
	
								sharedData.request('browserDetails', function(state, data){
	
											// LOAD THEME SPECIFIC CSS				
											var themeRoot = "themes/available/" + _global_setup.theme;
								
										  	angularLoad.loadCSS(themeRoot + "/css/theme-base.css")
										  	.then(function() {
											if (state){
												var isMobile = data.mobile;
												
												if(isMobile){
													
												   	angularLoad.loadCSS(themeRoot + "/css/theme-mobile.css")
									  				.then(function() {
									  					callback(true);	
									  				});												
											   	}
											   	else{
												   	angularLoad.loadCSS(themeRoot + "/css/theme-desktop.css")
									  				.then(function() {
									  					callback(true);	
									  				});	
											   	}
				
											}
											else{
												alert("Cannot load proper CSS.");
											}
								
									});								

								});
								
								
								
							},
							//-----------------		
							
							// CHECK LOAD -----
							checkLoad:function(callback){
								var check = true,
									array = $scope.page.loadComponents; 	
								
								var i = array.length; while(i--){
									if ( array[i] == false){
										check = false; 
									};
								};
								
								// all loads completed
								if (check){
									callback();
								}						
							},					
							// -----------------												
			        
					    };
					})();
					//-----------------   
					
					


					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								console.timeEnd("Loadtime");
								console.groupEnd("Init Sequence");
																			
								$timeout(function(){
									sharedData.add("masterReady", {ready: true});
									$scope.callCore.callController({who: "master", action: "showSplash"}, function(data){});
									$('body').transition({  opacity: 1}, 1000, 'ease').css('background-color', 'transparent');	
								}, 1000);	
							
							},
							//-----------------
							
							
						};
					})();
					//-----------------  
					
					//-----------------  
					$scope.tokenInits = (function () {
						return {
						
							gallery:function(folderName, id){
								
								phpJS.listOfFiles("../media/manager/images/" + folderName + "/", function(state, data){ 
									var img = custom.parseManagerImages(data, folderName),
										buildString = "";
									
									for (var i = 0; i < img.full.length; i++){
										buildString += "<img src='" + img.full[i].src + "'>";
									}
									
									$('#' + id).append(buildString);
									custom.initSlick();
								});
							}
						
						};
					})();
					//-----------------  

			    
				    // ---------------- PHP CALLS 
					$scope.phpCore = (function () {
					    return {

							// ----------------  RUN QUERY
							__phpRunQuery:function(data, callback){
									var queryString = data.query;
								   	var packet = {
								   		query: queryString,
								   		database: data.database
								   	};
								   	
								   	
								   	
									phpJS.queryDatabase(packet, function(state, data){
										if (state){
											$timeout(function(){
											  callback({status: "success", data: data});
											});
										}
									});					
							},
  							// ----------------
  							
							// ----------------  RUN QUERY
							__queryNumberOfEntries:function(packet, callback){
									
									phpJS.queryNumberOfEntries(packet, function(state, data){
										if (state){
											$timeout(function(){
											  callback({status: "success", data: data});
											});
										}
									});					
							},
  							// ----------------  							
  							
							// ----------------  SEND EMAIL
							__phpSendEmail:function(packet, callback){
									phpJS.sendEmail(packet, function(state, data){
										$timeout(function(){
											  callback({status: "success", data: data});
										});
									});
							}, 							
  							// ----------------
					    	
							// ----------------  SEND EMAIL
							__phpSendEmail:function(packet, callback){
									phpJS.sendEmail(packet, function(state, data){
										$timeout(function(){
											  callback({status: "success", data: data});
										});
									});
							},
							// ----------------			

							// ----------------  DELETE FOLDER
							__phpDeleteFolder:function(packet, callback){	
								if (packet.folder == undefined || packet.folder == null){
									callback({status: "fail", msg: "No file included."});
								}
								else{
								
									phpJS.deleteFolder(packet.folder, function(state, data){
										$timeout(function(){
											callback({status: "success", data: data});
										});
									});	
								}
							},		
							// ----------------	
							
							// ----------------  DELETE FOLDER
							__phpDeleteFiles:function(packet, callback){	
									phpJS.deleteFiles(packet, function(state, data){
										$timeout(function(){
											callback({status: "success", data: data});
										});
									});	
								
							},		
							// ----------------		
							
							// ----------------  DELETE FOLDER
							__phpCreateFolder:function(packet, callback){
									phpJS.createFolder(packet, function(state, data){
									  callback({status: "success", data: data});
									});										
							},		
							// ----------------															
							
							
															
							// ----------------  UPLOAD FILE
							__phpfileUpload:function(packet, callback){	
								if (packet.file == undefined || packet.file == null){
									callback({status: "fail", msg: "No file included."});
								}
								else{
								
									phpJS.basicUpload(packet.file, packet.name, packet.location, function(state, data){
										$timeout(function(){
											callback({status: "success", data: data});
										});
									});	
								}
							},		
							// ----------------	
							
							// ----------------  UPLOAD FILE
							__phpbase64Upload:function(packet, callback){
								
								if (packet.file == undefined || packet.file == null){
									callback({status: "fail", msg: "No file included."});
								}
								else{
									
									phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
										$timeout(function(){
											callback({status: "success", data: data});
										});
									});
									
								}
							},		
							// ----------------								
							
							// ----------------  BROWSER INFO	
							__phpGetTableDetails:function(packet, callback){												
								phpJS.getTableFields(packet, function(state, data){
									if (state){
										
										
		
										// strip numbers out of type
										var requiredCount = 0;
										
										for (var key in data) {
										   var obj = data[key];
										   for (var prop in obj) {
										      // important check that this is objects own property 
										      // not from prototype prop inherited
										      if(obj.hasOwnProperty(prop)){
										      	
										      	// parse the types
										      	if (prop == "Type"){
													var strippedType = $.trim(obj[prop].replace(/[, 0-9()]/g,'').toLowerCase());
													var strippedSize = $.trim(obj[prop].replace(/[A-Za-z()]/g,''));
													obj["Type"] = strippedType;
													obj["Size"] = strippedSize;
										        }
										        
										        // parse the types
										        if (prop == "Comment"){
										        	
													// ATTRIBUTES
										        	if (obj[prop].indexOf("noshow") > -1){ obj["noshow"] = true; }		else{	obj["noshow"] = false;		};
										        	if (obj[prop].indexOf("disabled") > -1){ obj["isDisabled"] = true;}	else{	obj["isDisabled"] = false;	};										        											        	
										        	if (obj[prop].indexOf("hidden") > -1){ obj["isHidden"] = true; }	else{	obj["isHidden"] = false;	};
										        	if (obj[prop].indexOf("required") > -1){ obj["isRequired"] = true;}	else{	obj["isRequired"] = false;	};
										        	// ----- END
										        	
										        	
										        	
										        	// FIELDS
										        	// general 
										        	if (obj[prop].indexOf("label") > -1){ 
										        		    var matches = obj[prop].match(/label=\"(.*?)\"/);    												
										        			obj["label"] = matches[1]; 								        	
										        	}else{	obj["label"] = '';	};
													if (obj[prop].indexOf("placeholder") > -1){ 
										        		    var matches = obj[prop].match(/placeholder=\"(.*?)\"/);    												
										        			obj["placeholder"] = matches[1]; 								        	
										        	}else{	obj["placeholder"] = '';	};
										        	
										        	// number specific
										        	if (obj[prop].indexOf("allowNeg") > -1){
										        		 var matches = obj[prop].match(/allowNeg=\"(.*?)\"/);    
										        		 obj["allowNeg"] = matches[1]; 											        		 
										        	}										        	
										        	if (obj[prop].indexOf("rangeMax") > -1){
										        		 var matches = obj[prop].match(/rangeMax=\"(.*?)\"/);    
										        		 obj["rangeMax"] = matches[1]; 	
										        	}	
										        	if (obj[prop].indexOf("rangeMin") > -1){
										        		 var matches = obj[prop].match(/rangeMin=\"(.*?)\"/);    
										        		 obj["rangeMin"] = matches[1]; 											        		 
										        	}	
										        	if (obj[prop].indexOf("stepValue") > -1){
										        		 var matches = obj[prop].match(/stepValue=\"(.*?)\"/);    
										        		 obj["stepValue"] = matches[1]; 											        		 
										        	}											        											        											        	
										        	// ----- END
										        											        	

													// GRAB FIELDS AUTOMATICALLY
													obj["autoFill"] = false;
													if (obj[prop].indexOf("autoCollect") > -1){
														obj["autoFill"] = true; 
														var matches = obj[prop].match(/autoCollect=\"(.*?)\"/);  
														obj["autoCollect"] = matches[1]																										
													}else{	
														obj["autoFill"] = false;	
													};
													// ----- END

										        	// DEFINE INPUT TYPE (input/textarea/advanced)
										        	if (obj[prop].indexOf("inputType") > -1){ 
										        		    var matches = obj[prop].match(/inputType=\"(.*?)\"/);    												
										        			obj["inputType"] = matches[1]; 								        	
										        	}
										        	// DEFAULT
										        	else{	
										        		obj["inputType"] = 'input';	
										        	};
										        	// ----- END									        	
										        	
										        	// DEFINE REGEXP
										        	if (obj[prop].indexOf("regexp") > -1){ 
										        		    var matches = obj[prop].match(/regexp=\"(.*?)\"/); // adds escape characters for strings if added   												
										        			obj["textRestrict"] = matches[1].replace(/~escape~/g, "\\");								        	
										        	}
										        	// DEFAULTS
										        	else{	
										        		var type = obj["Type"]; 
										        		
										        		// ALL TEXT INPUT
										        		if (type == "input" || type == "varchar" || type == "text" || type == "tinytext" || type == "mediumtext" || type == "longtext"){
										        			obj["textRestrict"] = '';	
										        		};
										        		
										        		// ALL DEFAULT NUMBERS
										        		if (type == "tinyint" || type == "smallint" || type == "mediumint" || type == "int" || type == "bigint" || type == "bit"){
										        			obj["textRestrict"] = '^[0-9]*$';	
										        		};										        		
										        		
										        		// ALL DECIMAL NUMBERS * needs to be redone - direct regexp work best
										        		if (type == "double" || type == "decimal" || type == "float"){
										        			obj["textRestrict"] = '^\\d+(\\.\\d{0,2})?$';	
										        		};	
										        		
										        		// TIME * need to be redone to match mysql database - direct regexp work best
										        		if (type == "date"){
										        			obj["textRestrict"] = "";
										        		};	
										        		if (type == "time"){	
										        			obj["textRestrict"] = new Date(moment().format());
										        		};		
										        												        												        											        		
										        	};
										        	// ----- END
										        	
										        	
										        	// ----- IMAGE UPLOADS										        			
										        	if (obj["inputType"] == "imageUpload"){	
										        			if (obj[prop].indexOf("limit") > -1){
											        		    var matches = obj[prop].match(/limit=\"(.*?)\"/);    																					        				 
										        				obj["limit"] = matches[1]; 	; }
										        			else{
										        				obj["limit"] = 10;	
										        			};
										        			
										        			if (obj[prop].indexOf("captions") > -1){  																					        				 
										        				obj["captions"] = true; }
										        			else{
										        				obj["captions"] = false;	
										        			};
										        	};	
										        	
										        }
										        
										      }
										   }
										}
										
										var meta = {
											required: 0
										};
										
										var _returnData = {
											formData: data,
											metaData: meta
										};
										
		
										$timeout(function(){
											callback({status: "success", data: _returnData});
										});
									}
									else{
										$timeout(function(){
											callback({status: "failure", msg: "Could not retrieve value keys."});
										});
									}
								});	
							},		
							// ----------------	
							
							
						    // ----------------  CREATE ENTRY
						    __phpCreateBlankEntry:function(packetData, callback){
						 		
					
									phpJS.createBlankEntry({database: packetData.database, table: packetData.table}, function(state, data){	
			 							if (state){
			 								$timeout(function(){
													callback({status: "success", data: data});
											});
			 							}
			 							else{
			 								// backend failure
											$timeout(function(){
												callback({status: "failure", msg: "Could not create blank entry."});
											});
			 							};						
									});
									
						
						    },
						    // ----------------								
							
						    // ----------------  CREATE ENTRY
						    __phpCreateNewEntry:function(packetData, callback){
						 		
						 		// SANITIZE PACKGES BEFORE SENDING
						 		custom.sanitizePacketsForDatabase(packetData, function(sanitizedData){	
						 			
							 		var valuePacket = JSON.stringify(sanitizedData);						
									phpJS.createNewEntry({database: packetData.database, table: packetData.table}, valuePacket, function(state, data){	
			 							if (state){
			 								// new entry created successfully
			 								if(data.status == "good"){
												$timeout(function(){
													callback({status: "success", data: data});
												});
											};
											// error in the input (typos, mismatched fields, etc)
											if(data.status == "error"){
												$timeout(function(){
													callback({status: "error", msg: "Could not create new entry.  Check for mismatched fields or required fields."});
												});
											};
			 							}
			 							else{
			 								// backend failure
											$timeout(function(){
												callback({status: "failure", msg: "Could not create new entry."});
											});
			 							};						
			
									});
									
									
								});
						    },
						    // ----------------	
						    	
						    
						    // ----------------  DELETE DENTRY
							__phpModifyEntry:function(packet, callback){
											
									phpJS.queryDatabase2(packet, function(state, data){
								
										
										if (state){
											if (data.status == "good"){
												$timeout(function(){
													callback({status: "success", msg: "Modification complete."});
												});	
											};
											if (data.status == "error"){
												$timeout(function(){
													callback({status: "error", msg: "Modification has failed."});
												});
											};
										}
										else{
											$timeout(function(){
												callback({status: "failure", msg: "Could not reach database."});
											});	
										};
									});
							},
							// ----------------		
							
							// ----------------	
							__phpListOfFiles:function(packet, callback){
								phpJS.listOfFiles("../" +  packet.location, function(state, data){
										
			 							if (state){
			 								$timeout(function(){
													callback({status: "success", data: data});
											});
			 							}
			 							else{
			 								// backend failure
											$timeout(function(){
												callback({status: "failure", msg: "Could not create blank entry."});
											});
			 							};	
								});
							}
							// ----------------			
									    	
					    	
						};
					})();    
					
	
					// -------------- STANDARD CORE
					$scope.standardCore = (function () {
					    return {
					    	
							// ----------------  PING/PONG	  		
							__pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},
							// ---------------- 
							
							
							// ---------------- 
							__editorCollectionSettings:function(data, callback){
								  
								  $scope.items = data;
								  custom.lockBody();
								  
								 

								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-collectionSettingsModal.html',
								      controller: editorModalCtrl.collectionSettingsModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 
							
							// ---------------- 
							__editorPageSettings:function(data, callback){
								  $scope.items = data;
								  
								  custom.lockBody();
		  
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-pageSettingsModal.html',
								      controller: editorModalCtrl.pageSettingsModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){
										custom.unlockBody();											
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){										
										custom.unlockBody();																			
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 
							
							
							// ---------------- 
							__editorSystemSettings:function(data, callback){
								
								
								  $scope.items = data;
								  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-pageSystemModal.html',
								      controller: editorModalCtrl.pageSystemModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 
											
							
							// ----------------  CREATE/EDIT/DELETE LAYOUTS	
							__scriptCreator:function(data, callback){
								  $scope.items = data;
		  							
		  						  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-scriptCreator.html',
								      controller: editorModalCtrl.scriptCreatorCntrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 
							
							// ---------------- 
							__mediaManager:function(data, callback){
								  $scope.items = data;
		  							
		  						  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'editor-mediaManager.html',
								      controller: editorModalCtrl.scriptMediaCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
							},
							// ---------------- 				
							
							// ----------------  BROWSER INFO		  		
							__browserInfo:function (callback) {
								
								    custom.lockBody();
									var control = uiModalCtrl.browserInfoCtrl(); 
									var modalInstance = $modal.open({
								      	templateUrl: 'browserInfoModal.html',
								  		controller: control,
									    resolve: {
									        data: function () {
									       
									        }
									    }
									});
									
									// modal animation in
									modalIn();			
								
									modalInstance.result.then(
										function (returnData) {  // CLOSE	
											custom.unlockBody();																								
											modalOut(function(){
												callback({status: "closed", msg: "closed"});
											});	
										}, 
										function () {			// DISMISS
											custom.unlockBody();
											modalOut(function(){											
								  				callback({status: "dismiss", data: "dismissed"});
								  			});	
								    	}
								    );						
							},
							// ---------------- 						
								
							// ----------------  CONFIRM		  					  
							__pictureBox:function (data, callback) {
								  
								  $scope.items = data;
		                        
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'pictureModal.html',
								      controller: uiModalCtrl.pictureModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
		
							},
							// ---------------- 								
								
								
							// ----------------  CONFIRM		  					  
							__choiceBox:function (data, callback) {
								  
								  $scope.items = data;
		  						  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'choiceModal.html',
								      controller: uiModalCtrl.choiceModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){	
										custom.unlockBody();
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
		
							},
							// ---------------- 								
								
								
							// ----------------  CONFIRM		  					  
							__confirmBox:function (data, callback) {
								
								 $scope.items = data;
								 custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'confirmModal.html',
								      controller: uiModalCtrl.confirmModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.items;
								        }
								      }
								    });
		
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData)
									{	
										custom.unlockBody();
										modalOut(function(){			
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){	
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
		
							},
							// ---------------- 
							
						 	// ---------------- LOGOUT
							__logout:function (callback) {						
				 				
				 				
				 				var checkOnce = false; 
				 				var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );
								var auth = new FirebaseSimpleLogin(fbLogin, function() {
										if (!checkOnce){
											checkOnce = true; 			
											firebaseSettings.setLogout();								
							 			} 	
								});	
				 				auth.logout();	
							},
							//-----------------		
							
						 	// ---------------- RESET PASSWORD
							__resetPassword:function (callback) {						
								
								var modalInstance = $modal.open({
							      	templateUrl: 'resetModal.html',
							  		controller: accountModalCtrl.resetEmailCtrl(),
								});
								
								// modal animation in
								modalIn();
								
								// VIA CLOSE
								modalInstance.result.then(function(returnData)
								{							 
									modalOut(function(){
										callback({status: "closed", data: "updated"});		
									});			  							 		
								}, 
								// VIA DISMISS
								function (){
									modalOut(function(){
							  			callback({status: "dimiss", data: "dismissed"});
							  		});
								});	
								
							},
							//-----------------							
												
															
						 	// ---------------- LOGIN
							__login:function (callback) {						
								custom.lockBody();
								var modalInstance = $modal.open({
							      	templateUrl: 'loginModal.html',
							  		controller: accountModalCtrl.loginModalCtrl(),
								});
								
								// modal animation in
								modalIn();
								
								// VIA CLOSE
								modalInstance.result.then(function(returnData)
								{		
									custom.unlockBody();					 
									modalOut(function(){
										callback({status: "closed", data: "closed"});
									});					  							 		
								}, 
								// VIA DISMISS
								function (){
									custom.unlockBody();
									modalOut(function(){
							  			callback({status: "dimiss", data: "dismissed"});
							  		});
								});	
								
							},
							//-----------------
							
							
							//----------------- EDIT PROFILE IMAGE					
							__editProfilePic:function(callback){						
									
									custom.lockBody();						
									var modalInstance = $modal.open({
								      	templateUrl: 'editProfileModal.html',
								  		controller:  accountModalCtrl.editProfileModalCntrl(),
									    resolve: {
									        data: function () {
									        // return $scope.deleteUserObj;
									        }
									    }
									});
									
									// modal animation in
									modalIn();
								
									// VIA CLOSE
									modalInstance.result.then(function(returnData)
									{	
										custom.unlockBody();	
										modalOut(function(){
											callback({status: "closed", data: "updated"});	
										});				  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
							
				
							},
							//-----------------									


							//-----------------	EDIT PROFILE IMAGE					
							__editProfileData:function(callback){						
										
									custom.lockBody();						
									var modalInstance = $modal.open({
								      	templateUrl: 'editProfileDataModal.html',
								  		controller: accountModalCtrl.editProfileDataModalCntrl(),
									    resolve: {
									       
									    }
									});
									
									// modal animation in
									modalIn();
								
									modalInstance.result.then(
										function (returnData) {  // CLOSE
											custom.unlockBody();	
											modalOut(function(){																																								
												callback({status: "closed", data: "updated"});
											});											
										}, 
										function () {			// DISMISS
											custom.unlockBody();
											modalOut(function(){								
								  				callback({status: "closed", data: "dismissed"});
								  			});						  			
								    	});
							
				
							},
							//-----------------		
							
							
							//----------------- CHANGE PASSWORD
							__changePassword:function(callback){
									
									custom.lockBody();
									var modalInstance = $modal.open({
								      	templateUrl: 'changePasswordModal.html',
								  		controller: accountModalCtrl.changePasswordCtrl(),
									    resolve: {
									        data: function () {
									        // return $scope.deleteUserObj;
									        }
									    }
									});
									
									// modal animation in
									modalIn();							
									
									modalInstance.result.then(
										function (returnData) {  // CLOSE
											custom.unlockBody();
											modalOut(function(){																															
												callback({status: "closed", data: "updated"});	
											});	
										}, 
										function () {			// DISMISS
											custom.unlockBody();
											modalOut(function(){	
								  				callback({status: "closed", data: "dismissed"});
								  			});	
								   		});
							},
							//-----------------		
							
							
							//-----------------	DELETE ACCOUNT
							__deleteMyAccount:function(callback){
												
								custom.lockBody();
								var modalInstance = $modal.open({
							      	templateUrl: 'deleteModal.html',
							  		controller: accountModalCtrl.deleteModalCtrl(),
								    resolve: {
								        data: function () {
								         // return $scope.deleteUserObj;
								        }
								    }
								});
							
								// modal animation in
								modalIn();						
							
								modalInstance.result.then(
									function (returnData) {  // CLOSE
										custom.unlockBody();
										modalOut(function(){																														
											callback({status: "closed", data: "deleted"});
										});	
									}, 
									function () {			// DISMISS
										custom.unlockBody();
										modalOut(function(){
							  				callback({status: "closed", data: "dismissed"});
							  			});	
							  		});
							},	
							//-----------------														
					
					
							//-----------------	 CREATE NEW ACCOUNT
							__createNewAccount:function(callback){
									
									custom.lockBody();		
									var modalInstance = $modal.open({
								      	templateUrl: 'createModal.html',
								  		controller: accountModalCtrl.createModalCtrl(),
									    resolve: {
									        data: function () {
									        // return $scope.deleteUserObj;
									        }
									    }
									});
									
									// modal animation in
									modalIn();	
									
									modalInstance.result.then(
										function (returnData) {  // CLOSE
											custom.unlockBody();
											modalOut(function(){	
												callback({status: "closed", data: "created"});	
											});
										}, 
										function () {			// DISMISS
											custom.unlockBody();
											modalOut(function(){									
								  				callback({status: "closed", data: "dismissed"});
								  			});
								    	});
				
							},
							//////////////////////		
							
							//----------------- RELOAD DOM
							__refreshPage:function(){						
								$state.transitionTo($state.current, $stateParams, {
									    reload: true,
									    inherit: false,
									    notify: true
								});								
							},
							//-----------------							
											
							//----------------- REFRESH DATA
							__refreshData:function(){
								$scope.initCore.masterRefresh();
							},
							//-----------------
							
							//----------------- SHOW/HIDE THINKING
							__showSplash:function(callback){
								var isActive = $(".reveal-modal").hasClass("in"); 
								if (!isActive){
															
									var modalInstance = $modal.open({
								      	templateUrl: 'splashModal.html',
								  		controller: uiModalCtrl.splashModalCtrl(),
									});
									
									modalIn();
									
									callback({active: isActive});
								};		
								callback({active: isActive});											
							},
							__hideSplash:function(callback){
							
								$(".reveal-modal").transition({ opacity: 0 }, 500);
								$(".reveal-modal-bg").transition({ opacity: 0 }, 500);
								var isActive = $(".reveal-modal").hasClass("in"); 
								
								modalOut();
								
								if (isActive){
									setTimeout(function(){
										$('.modal-body').addClass('hidden');
										$(".reveal-modal").removeClass();
										$(".reveal-modal-bg").removeClass();
										$('body').removeClass('modal-open');
										callback({active: isActive});
									}, 500);	
								}
								callback({active: isActive});
								
							},
							//-----------------								
							
							//----------------- SHOW/HIDE THINKING
							__showThinking:function(callback){
								var isActive = $(".reveal-modal").hasClass("in"); 
								if (!isActive){					
									var modalInstance = $modal.open({
								      	templateUrl: 'thinkingModal.html',
								  		controller: uiModalCtrl.thinkingModalCtrl(),
									});
									
									modalIn();
									
									callback({active: isActive});
								};	
								callback({active: isActive});												
							},
							
							__hideThinking:function(callback){
								$(".reveal-modal").transition({ opacity: 0 }, 500);
								$(".reveal-modal-bg").transition({ opacity: 0 }, 500);
								var isActive = $(".reveal-modal").hasClass("in"); 
								
								modalOut();
								
								if (isActive){
									setTimeout(function(){
										$('.modal-body').addClass('hidden');
										$(".reveal-modal").removeClass();
										$(".reveal-modal-bg").removeClass();
										$('body').removeClass('modal-open');
										callback({active: isActive});
									}, 500);	
								}
								callback({active: isActive});
							},
							//-----------------		
							
							// ----------------  POST MODAL		  					  
							__postModal:function (data, callback) {
						 
								  $scope.sendData = data;
								  custom.lockBody();
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'db_post_Modal.html',
								      controller: formsModalCtrl.postModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.sendData;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){		
										custom.unlockBody();				
										modalOut(function(){												
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
								
		
							},
							// ---------------- 	
							
							// ----------------  POST MODAL		  					  
							__advSearchModal:function (data, callback) {
						 			
								  $scope.sendData = data;
								  custom.lockBody();	
								  $scope.open = function () {
								    var modalInstance = $modal.open({
								      templateUrl: 'advSearchModal.html',
								      controller: formsModalCtrl.advSearchModalCtrl,
								      resolve: {
								        items: function () {
								          return $scope.sendData;
								        }
								      }
								    });
								    
									// modal animation in
									modalIn();							    
		
									// VIA CLOSE
									modalInstance.result.then(function(returnData){		
										custom.unlockBody();				
										modalOut(function(){		
											callback({status: "closed", data: returnData});
										});					  							 		
									}, 
									// VIA DISMISS
									function (){
										custom.unlockBody();	
										modalOut(function(){
								  			callback({status: "dimiss", data: "dismissed"});
								  		});
									});	
								  };
		
								$scope.open();
								
		
							},
							// ---------------- 
															
								
						};
					})();  				
					// -------------- end STANDARD CORE
							    				

					
					
					
						
					
					
					
					//-----------------		
					function modalIn(cb){
						var duration = 250;
						$timeout(function(){	
							
							$('html, body').css({
							    'overflow': 'hidden',
							    'height': '100%',
							});
												
							
							$(".reveal-modal").transition({ y: '-40px', opacity: 0 }, 0)
											  .transition({ y: '0px', opacity: 1   }, duration);

							
							setTimeout(function(){
								$timeout(function(){
									if (cb!=null){cb();};
								}, 0);
							}, duration);
						}, 0);						
					}; 
					
					
					function modalOut(cb){
						var duration = 250;
						$timeout(function(){
							

							$('html, body').css({
							    'overflow': 'visible',
							    'height': 'auto'
							});

							$(".modal-body").transition({ y: '0px' }, 0)
											.transition({ y: '-40px' }, 500);										
							$(".reveal-modal").transition({ opacity: 0 }, duration);
							setTimeout(function(){
								$timeout(function(){
									if (cb!=null){cb();};
								}, 0);
							}, duration);
						}, 0);						
					};
					//-----------------		
			
					
//-------------------------------------
					// COMMUNICATE BETWEEN CONTROLLERS	 				  
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {

						 	// RUN REQUEST
							runRequest:(function () {
					
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											packet: returnData,
											button: {obj: data.info.button, content: data.info.buttonContent}
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});		
	
							  };
					
					  var execute = function (data) {
					  		
					  	
					  		
							switch(data.execute.name) {
								 // required for callbacks to other controllers
							    case "calledBack":
							        $scope._watched.callback = data.returnData.data;
							    break;		
							    
							    //--------  PHP FUNCTIONS ----------//
							    case "phpModifyEntry":
							        $scope.phpCore.__phpModifyEntry(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;
							    
							    case "phpCreateBlankEntry":
							        $scope.phpCore.__phpCreateBlankEntry(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;		    
							    

							    case "phpCreateNewEntry":
							        $scope.phpCore.__phpCreateNewEntry(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    							    
							    case "phpGetTableDetails":								    						   
							        $scope.phpCore.__phpGetTableDetails(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;									    
							    
							    case "phpDeleteFolder":
							        $scope.phpCore.__phpDeleteFolder(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    
							    case "phpDeleteFiles":
							        $scope.phpCore.__phpDeleteFiles(data.info.data, function(returnData){							        	
							        	if (data.execute.useCallback){					        							        		
							        		callback(data, returnData); 
							        	};
							        });	
							    break;		
											    								    
							    case "phpCreateFolder":
							        $scope.phpCore.__phpCreateFolder(data.info.data, function(returnData){							        	
							        	if (data.execute.useCallback){					        							        		
							        		callback(data, returnData); 
							        	};
							        });	
							    break;								    
							    
							    case "phpBase64Upload":
							        $scope.phpCore.__phpbase64Upload(data.info.data, function(returnData){
							        	if (data.execute.useCallback){							        		
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    	    
							    case "phpFileUpload":
							        $scope.phpCore.__phpfileUpload(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;								    
							    
							    case "phpRunQuery":
							        $scope.phpCore.__phpRunQuery(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    
							    
							    case "phpListOfFiles":
							        $scope.phpCore.__phpListOfFiles(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;
							    
							    
							    case "phpRunQueryForTotalEntry":
							        $scope.phpCore.__queryNumberOfEntries(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	    
							    
							    
							    case "phpSendEmail":
							        $scope.phpCore.__phpSendEmail(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });	
							    break;	
							    //-------- end PHP FUNCTIONS --------//							    	
							    
							    
							    //--------  ANGULAR FUNCTIONS -------//			
							    case "ping":
							        $scope.standardCore.__pong(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;    
							    case "editorCollectionSettings":
							        $scope.standardCore.__editorCollectionSettings(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "editorPageSettings":
							        $scope.standardCore.__editorPageSettings(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "editorSystemSettings":
							        $scope.standardCore.__editorSystemSettings(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;							    
							    case "scriptCreator":
							        $scope.standardCore.__scriptCreator(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    
							    case "mediaManager":
							    	
							        $scope.standardCore.__mediaManager(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;			    
							    
							    						
							    case "login":
							        $scope.standardCore.__login(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "logout":
							        $scope.standardCore.__logout(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "resetPassword":
							        $scope.standardCore.__resetPassword(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;				    
							    case "editProfileImage":
							        $scope.standardCore.__editProfilePic(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "createNewAccount":
							        $scope.standardCore.__createNewAccount(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    case "editProfileData":
							        $scope.standardCore.__editProfileData(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "deleteMyAccount":
							        $scope.standardCore.__deleteMyAccount(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    
							     
							    case "changePassword":
							        $scope.standardCore.__changePassword(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    
							    
							    case "postModal":
							        $scope.standardCore.__postModal(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;		
							    case "advSearchModal":
							        $scope.standardCore.__advSearchModal(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;						     							    						    
							    case "confirmBox":
							        $scope.standardCore.__confirmBox(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "choiceBox":
							        $scope.standardCore.__choiceBox(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    case "browserInfo":
							        $scope.standardCore.__browserInfo(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "pictureBox":						    
							        $scope.standardCore.__pictureBox(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;							    							    
							    case "showSplash":
							        $scope.standardCore.__showSplash(function(returnData){							        	
							        	if (data.execute.useCallback){							        		
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "hideSplash":
							        $scope.standardCore.__hideSplash(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    	
							    case "showThinking":
							        $scope.standardCore.__showThinking(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "hideThinking":
							        $scope.standardCore.__hideThinking(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    							    							    
							    case "refreshPage":
							        $scope.standardCore.__refreshPage(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    						    
							    case "refreshData":
							        $scope.standardCore.__refreshData(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    
							    //-------- end ANGULAR FUNCTIONS ----//									    
							    
							    default:
							      // do something
							}					   	
					  };
					    
					  return {
					    execute: execute
					  };
					
					})(),


							// shorthand for calling 
							callController:function(e, returnPacket){
				
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
					

						// create callback system for talking to the master controller							
						masterCall:(function () {
						  var execute = function(packet, callback){
								$scope._watched = {execute: null, callback: null};								
								var unbindWatch = $scope.$watch('_watched.execute', function() { 
										if($scope._watched.execute != null){
							       			$scope.broadcast($scope._watched.execute);	
							       		};	
							    });
						  		packet["order"] = executeOrder.length;						  		
						  		executeOrder.push(packet);							    
								$scope.$watch('_watched.callback', function() {										
										if($scope._watched.callback != null && $scope._watched.execute != null){
											$scope._watched.execute.execute.callback($scope._watched.callback);
											$scope._watched = {execute: null, callback: null}; 									
										}    	
										unbindWatch(); // remove event binder so it does not duplicate
							    });	
							   	$scope._watched.execute = packet;						   	
						  };
					    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 			
					
				   

				}]);  
	




//end
//start

			app.controller('offcanvasController', ['$rootScope', '$scope', '$timeout', 'SmoothScroll', '$location',
											function($rootScope, $scope, $timeout, SmoothScroll, $location) {	   
				 
					var fileName  = 'offcanvas';
					
					// ---------------- INIT CORE					
					$scope.initCore = (function () {
					    return {
					    	
							// ---------------- VARIABLES 
						    resetVariables:function(){
							      $scope.page = {
							    	  loadComponents: custom.fillArray(2),
							    	  isLoaded: false,
							      };
						    },
						   	//-----------------							    	
					    	
							// ---------------- INIT
							init:function(){
								$scope.initCore.resetVariables();
								$scope.initCore.loadComponents(function(){									
									$scope.mainCore.start();
								});					
							},
							//-----------------
							
							//----------------- REFRESH
							onRefresh:function(callback){
								$scope.masterData = sharedData.getAll();
								$scope.$apply();		
							},
							//-----------------							
							
							
							// ---------------- LOAD COMPONENTS
							loadComponents:function(callback){
								$scope.initCore.checkMaster(function(state){
									if (state){
										$scope.page.loadComponents[0] = true;
										$scope.initCore.checkLoad(callback);
									}
								});
								$scope.initCore.doSomething(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										$scope.initCore.checkLoad(callback);
									}
								});							
							},
							//-------------------
							
							
							// ------------------ SAMPLE COMPONENTS
							checkMaster:function(callback){
								// wait for master.js to finish loading 		
								sharedData.request("masterReady", function(state, data){
									if(state){
										if(data.ready == true){
											callback(true);
										}
										else{
											alert(data.ready);
										}
									}
									else{
										alert(data);
									}	
								});	
							}, 
							
							doSomething:function(callback){
								// LOAD A COMPONENT						
								callback(true);
							},
							//-----------------
							
							
							//----------------- CHECK LOAD
							checkLoad:function(callback){
								var check = true,
									array = $scope.page.loadComponents; 					
								var i = array.length; while(i--){
									if ( array[i] == false){
										check = false; 
									};
								};
								// all loads completed
								if (check){		
									$timeout(function(){
										$scope.page.isLoaded = true;				
										$scope.masterData = sharedData.getAll();	
										callback();			
									});									
								}						
							},					
							//-----------------



					    };
					})(); 
				   	//-----------------



					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								
									// pull pages from master data
									$scope.pages =  $scope.masterData.system.systemdata.pages;
									
							},
							//-----------------
							
							//-----------------
							offcanvasToggle:function(location){					   
								   SmoothScroll.$goTo(0).then(function() {
							            custom.offcanvas('toggle');
							           		
							       });					           	
							       $timeout(function(){					             
		 							 custom.offCanvasReset();
							         $location.path("/" + location);  
							       },800);	
							},
							//-----------------									
							
							//-----------------		
							offcanvasLogin:function(){
						   		
								if ($scope.masterData.logState){
									$scope.callCore.callController({who: "master", action: "logout"}, function(data){});
								}
								else{
									custom.offcanvas('toggle');							        
									$scope.callCore.callController({who: "master", action: "login"}, function(data){});
								}
	
							},
							//-----------------		
							
							//-----------------		
							close:function(){
					           custom.offcanvas('toggle');        
							},	
							//-----------------									
					
						};
					})();
					//----------------- 
					


					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
									//	e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ----------------   

					
				}]);			
							   


//end
//start

				app.controller('overlayController', ['$rootScope', '$scope', '$firebase', '$timeout', 'SmoothScroll', '$location',
											function($rootScope, $scope, $firebase, $timeout, SmoothScroll, $location) {	   
				    
				    var fileName  = 'overlay';
				    
					// ---------------- INIT CORE					
					$scope.initCore = (function () {
					    return {
					    	
							// ---------------- VARIABLES 
						    resetVariables:function(){
							      $scope.page = {
							    	  loadComponents: custom.fillArray(2),
							    	  isLoaded: false,
							    	  isCanvasOpen: false
							      };
						    },
						   	//-----------------							    	
					    	
							// ---------------- INIT
							init:function(){
								$scope.initCore.resetVariables();
								$scope.initCore.loadComponents(function(){									
									$scope.mainCore.start();
								});					
							},
							//-----------------
							
							//----------------- REFRESH
							onRefresh:function(callback){
								$scope.masterData = sharedData.getAll();
								$scope.$apply();		
							},
							//-----------------							
							
							
							// ---------------- LOAD COMPONENTS
							loadComponents:function(callback){
								$scope.initCore.checkMaster(function(state){
									if (state){
										$scope.page.loadComponents[0] = true;
										$scope.initCore.checkLoad(callback);
									}
								});
								$scope.initCore.doSomething(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										$scope.initCore.checkLoad(callback);
									}
								});							
							},
							//-------------------
							
							
							// ------------------ SAMPLE COMPONENTS
							checkMaster:function(callback){
								// wait for master.js to finish loading 		
								sharedData.request("masterReady", function(state, data){
									if(state){
										if(data.ready == true){
											callback(true);
										}
										else{
											alert(data.ready);
										}
									}
									else{
										alert(data);
									}	
								});	
							}, 
							
							doSomething:function(callback){
								// LOAD A COMPONENT						
								callback(true);
							},
							//-----------------
							
							
							//----------------- CHECK LOAD
							checkLoad:function(callback){
								var check = true,
									array = $scope.page.loadComponents; 					
								var i = array.length; while(i--){
									if ( array[i] == false){
										check = false; 
									};
								};
								// all loads completed
								if (check){		
									$timeout(function(){
										$scope.page.isLoaded = true;				
										$scope.masterData = sharedData.getAll();	
										callback();			
									});									
								}						
							},					
							//-----------------



					    };
					})(); 
				   	//-----------------

					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								
							},
							//-----------------
							
							//-----------------
						   	offCanvasRevel:function(){
						   		$scope.page.isCanvasOpen = custom.offcanvas('toggle');
						   		if ( $scope.page.isCanvasOpen){
						   			; 	
						   		}else{
						   			; 
						   		}
						   	},
							//-----------------									
							
								
					
						};
					})();
					//----------------- 
				
					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ----------------   



				}]);				


//end
//start

	   	
				app.controller('redirectController', 
					['$rootScope', '$stateParams', '$scope', '$timeout', '$location', '$sessionStorage', '$localStorage', 'SmoothScroll', 'cfpLoadingBar', 'toaster', 
					function($rootScope, $stateParams, $scope, $timeout, $location, $sessionStorage, $localStorage, SmoothScroll, cfpLoadingBar, toaster) {	   
					
					var pageName  		= "redirect";  		      		
					 
					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					
								// ---------------- VARIABLES 
							    resetVariables:function(){
							    	$scope.page = {
							    		loadComponents: custom.fillArray(2),  // number should equal # of load components below
							    		isLoaded: false,
							    		editOpen: false,
							    		settings: null,
							    		checkPass: false
							    	};
							    },							    
							   	//-----------------		
					
								//----------------- REFRESH
								onRefresh:function(){
									sharedData.add("currentController", pageName);	// current controller
									$scope.masterData = sharedData.getAll();
									$scope.callCore.callController({who: "master", action: "hideThinking"});
									$scope.$apply();		
								},
								//-----------------						
					
								// ---------------- INIT
								init:function(){
									
									
									custom.offCanvasReset();
									$scope.initCore.resetVariables();
									cfpLoadingBar.start();
									SmoothScroll.$goTo(0);							
									$scope.initCore.loadComponents(function(){
										sharedData.add("currentController", pageName);	// current controller
										
										// initiate edit 
										$scope.editCore.init(function(){
											$scope.mainCore.start();	
										});
										
										
									});				
								},
								//-----------------        
					        
								// ---------------- LOAD COMPONENTS
								loadComponents:function(callback){
									$scope.initCore.checkMaster(function(state){
										if (state){
											$scope.masterData = sharedData.getAll();
											$scope.page.loadComponents[0] = true;
											$scope.initCore.animate(function(state){
													if (state){
														// get page setting data
														phpJS.getPageData(pageName, function(data){															
															$scope.page.settings = data.pageSettings;																										
															$scope.page.loadComponents[1] = true;
															$scope.initCore.checkLoad(callback);
														});		
													}
											});												
											
																	
											
										}
									});
									
			
									$scope.initCore.doSomething(function(state){
										if (state){
											$scope.page.loadComponents[1] = true;
											$scope.initCore.checkLoad(callback);
										}
									});						
								},
								//-------------------		
								
								// ------------------ LOAD COMPONENTS
								checkMaster:function(callback){
									// wait for master.js to finish loading 	
									sharedData.request("masterReady", function(state, data){
										if(state){
											if(data.ready == true){
												callback(true);
											}
											else{
												alert(data.ready);
											}
										}
										else{
											alert(data);
										}	
									});							
			
								},
								animate:function(callback){
									
									if ($scope.masterData.browserDetails.mobile){
										$('#' + pageName + '_id').addClass('gray-gradient');							
									}
									// for desktop
									else{
										custom.backstretchBG("media/background.jpg", 0, 1000);
									}		
									
									$timeout(function(){	
										$('#content-page')
											.transition({   x: -20,  opacity: 0, delay: 0}, 0)
											.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
											.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
											
										callback(true);
									}, 500); // slight delay needed
									
								},
								

															
								// -- blank component for example
								doSomething:function(callback){
									callback(true);
								},
								//-----------------		
								
								//----------------- CHECK LOAD
								checkLoad:function(callback){
									var check = true,
										array = $scope.page.loadComponents; 					
									var i = array.length; while(i--){
										if ( array[i] == false){
											check = false; 
										};
									};
									// all loads completed
									if (check){
									// CONTENT PAGE IS READY - INSERT CODE HERE		
																		
										// check for existing modal
										var duration = 2000;
										var hasModalOpen = $('.reveal-modal').hasClass('in');
										if (!hasModalOpen){ duration = 0; };
										
										$timeout(function(){ 
											cfpLoadingBar.complete();
											$scope.page.isLoaded = true;																
											$scope.callCore.callController({who: "master", action: "hideSplash"});
																
											callback();											
										}, duration);									
										
									}						
								},					
								//-----------------		
       
					    };
					})();
					//-----------------        
				
					//----------------- 
					$scope.editCore = (function() {
						
							// variables ---
							$scope.layout = {
								selected: null,
								types: [],
							};
							// -------------
							 
							
							
							 return {
							 		 	
							 	//-----------------								      
								init:function(callback){
									
										
									$scope.editCore.getLayoutFiles(function(d){	
										// folder doesn't exist; create it
										if (d.data == 'no folder'){
											
											
											var packetData = {
  												location: "themes/structure/" + $scope.page.settings.currentTable + "/",
 												name: "layout"
											};	
											
																					
											$scope.callCore.callController({who: "master", action: "phpCreateFolder", data: packetData}, function(_d){
												// folder created successful
												if (_d.packet.data.status == "created"){
													//console.log("folder created successfully")
												}						
																		
											});											
										};
										
										// no files; create a blank template
										if (d.data == 'no files'){
											// create files
										}
										
										// files present - create selection 
										else{
											for (var i = 0; i < d.data.length; i++){
												$scope.layout.types.push(d.data[i]);
											}
											$timeout(function(){
												$scope.editCore.layout = $scope.page.settings.currentLayout;	
											});
											
											callback();
										};	
									});
										
										
										
									
						      	},
						      	//-----------------
						      	

						      	//-----------------
						      	editSystemSettings:function(){

						      		var packetData = {system: $scope.masterData.system};
						      		$scope.callCore.callController({who: "master", action: "editorSystemSettings", data: packetData}, function(d){
						      			console.log(d)
						      		});							      			
		
						      	},
						      	//-----------------						      	
						      	
						      	
						      	//-----------------
						      	editPageSettings:function(){
									phpJS.returnListOfTables($scope.masterData.system.usingdb, function(state, data){
										if (state){
								      		var packetData = {pagename: pageName, settings: $scope.page.settings, database: $scope.masterData.system.usingdb, collection: data};
								      		$scope.callCore.callController({who: "master", action: "editorPageSettings", data: packetData}, function(d){
								      			console.log(d)
								      		});							      			
										}
									});							      		
						      		
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	updatePageSettings:function(){						      		
						      		$timeout(function(){ 
						      			$scope.page.settings.currentLayout = $scope.editCore.layout;						      			
						      		});
						      	},
						      	//-----------------
						      	 	
						      	//-----------------						      	
								getLayoutFiles:function(callback){
									var packetData = {
										location: "themes/structure/" + $scope.page.settings.currentTable + "/"
									};
																					
									$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){
										
										if (d.packet.data.status == true){
												// pull data from folder
												var files = d.packet.data.files; 
												// files exist
												_return = [];
												if (d.packet.data.files.length > 0){
													for (i = 0; i < files.length; i++){
														_return.push( files[i].replace(/\.[^/.]+$/, "") );	
													};																							
													callback({status: true, data: _return});
												}
												// no files exist
												else{
													callback({status: false, data: "no files"});
												}
										}
										if (d.packet.data.status == "error"){											
											callback({status: false, data: "no folder"});
										}
										
									});
						      	},
						      	//-----------------	
						      	
						      	//-----------------						      	
								openLayoutEditor:function(callback){
									var packetData = {
										fields: $scope.collection.searchFields
									};											
									$scope.callCore.callController({who: "master", action: "editorLayout", data: packetData}, function(d){
										//console.log(d)
									});
						      	},
						      	//-----------------	


							 	//-----------------								      
								toggleEditor:function(callback){
									if (!$scope.page.editOpen){
										$('#__editor-sidebar')											
											.transition({   x: -($('#__editor-sidebar').width()) }, 500);	
										$('#__editor-bottom')
											.transition({   y: -($('#__editor-bottom').height()), delay: 250 }, 500);	
									}
									else{
										$('#__editor-sidebar')											
											.transition({   x: 0 }, 500);	
										$('#__editor-bottom')	
											.transition({   y: ($('#__editor-bottom').height()), delay: 250 }, 500);	
									}
									$scope.page.editOpen = !$scope.page.editOpen;
						      	},
						      	//-----------------		
						      
						      
						      };							
					})();
					//----------------- 
					
					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								//$scope.mainCore.checkPermissions();
								$scope.page.isLoaded = true;
								console.log("redirect loaded");
								//$scope.premadeCollection.init();
		
							},
							//-----------------
							
							checkPermissions:function(){
								var path = $location.path().replace(/[^a-zA-Z1-9 ]/g, ""),								
									allowed = $scope.masterData.accessLevel.allowed;
							
								$scope.page.checkPass = false;
								for (var i = 0; i < allowed.length; i++){
									check = allowed[i];
									if (check == path){
										$scope.page.checkPass = true;
									}
								}
								if(!$scope.page.checkPass){
									alert("You dont' have the proper permission to access this page.");
								}
							},
							
							//----------------- PICTURE BOX 
							pictureBox:function(_list, _index){								
								var packetData = {
									list: _list,
									index: _index
								};
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.callCore.callController({who: "master", action:"pictureBox", data: packetData}, function(d){
									//console.log(d);						
								});		
							},
							//-----------------							
							

						};
					})();
					//-----------------  
					
					//-----------------  COLLECT FROM DATABASE PREMADE FUNCTIONS
					$scope.premadeCollection = (function(){
					
				
						
						return { 
							
							
							//-----------------
							resetVariables:function(){
									// ----------------------------------
									
									
							    	$scope.collection = {
							    		
							    		table: $scope.page.settings.currentTable, 
							    		imageStorage: "uploads/images/" + $scope.page.settings.currentTable + "/",		 // 
							    		
							    		pagination:{
							    			current: 1, 					 // current page
							    			limit: 50, 						 // entries per page (initial value)
							    			morePerClick: 5, 				 // each time you click for more, this is how many entries it retrieves
							    			filerBy: null,
							    			reverse: true,
							    			max: null,
							    			filter: null,
							    			hasNext: null,
							    			hasPrev: null,
							    			isLast: null,
							    			isFirst: null,
							    			range: [
							    				{id: 1, label: 5},
							    				{id: 2, label: 10},
							    				{id: 3, label: 25},
							    				{id: 4, label: 50}				    				
							    			],
							    			selectResults: null
							    		},
			
							    		remember:{
							    			useLocalStorage: true,
							    			rememberWith:  "id",
							    			hasStorage: false
							    		},
							    		
							    		
										allEntries: [],
							    		useEntries: null,	
							    		numberOfEntries: null,		    		
							    		
							    		filterSearch: null,
							    		searchResultsCount: null,
							    		searchType: null,
							    		searchTerm: null,
							    		searchFields: null,
			
							    	};	
							    	// ----------------------------------
			
									// ----------------------------------	
									$scope.entryStatistics = {
										hasMore:    null,
										available: 	null,
										total: 		null
									};
									// ----------------------------------	
									
									// ----------------------------------
									// when exiting this page	
									$scope.$on('$destroy', function() {
										$scope.premadeCollection.saveSearch();
									});
									// ----------------------------------	
										
							    	// ----------------------------------
							    	$scope.firebasePermission = {
							    		fbID_required_toCreate: true,
							    		fbID_required_toEdit: true,
							    		fbID_required_toDelete: true,		
							    		
							    		fbIDmustMatchEntryId_edit: true,
							    		fbIDmustMatchEntryId_delete: true
							    	};
							    	// ----------------------------------
							    	
									// ----------------------------------
									$scope.$storage 		= $sessionStorage;	
									$scope.editBtn 			= {value: 0};
									$scope.deleteBtn 		= {value: 0};
									$scope.loadingEntries 	= true;
									$scope.isUsingAdvanced 	= false;
									// ----------------------------------	
																
							},
							//-----------------
							
							
							//-----------------
							init:function(){
								$scope.premadeCollection.resetVariables();
								
								var packetData = {	
									database: $scope.masterData.system.usingdb,
									table: $scope.collection.table
								};
								
								// get entry row totals
								$scope.callCore.callController({who: "master", action:"phpRunQueryForTotalEntry", data: packetData}, function(d){
									$scope.entryStatistics.total = d.packet.data.rows;
									
									
									// get table data
									$scope.callCore.callController({who: "master", action: "phpGetTableDetails", data: packetData}, function(d){ 
										$scope.collection.searchFields = custom.returnPropertyInObject(d.packet.data.formData, ["Field", "Type", "inputType"]);
										$scope.collection.pagination.filterBy = $scope.collection.searchFields.Field[0];
										$scope.premadeCollection.initialLoad();
									});									
								});
							
							},
							//-----------------
							
							//-----------------
							layoutUrl:function(){
								return "themes/structure/" + $scope.page.settings.currentTable + "/" + $scope.page.settings.currentLayout + ".html";								
							},	
							//-----------------						
							
							//-----------------
							initialLoad:function(){
								function loadDefault(){
									  var query = "SELECT * FROM " + $scope.collection.table +  " ORDER BY id DESC LIMIT 0, 1";   
									  $scope.premadeCollection.fetchEntries(query, null, function(){
											 SmoothScroll.$goTo(0);								// scroll back to top
											 $scope.loadingEntries = false;						// hide loading
											 $scope.premadeCollection.isUsingAdvanced = false;   // reset advanced
											 $scope.entryStatistics.hasMore = true;			   // hides more											 
									  });										
									  
								}
								
								$timeout(function(){
									$scope.loadingEntries = true;
									
										// if search string is in the url
										var searchString = $stateParams.search; 
										if (searchString != null){
											var key = searchString.substr(0, searchString.indexOf(':'));
											var value = searchString.split(':')[1];
												value = value.replace(/_/g, " ");
											var query = "SELECT * FROM " + $scope.collection.table +  " WHERE " + key + " = " + "\""+ value + "\"";
											
											  $scope.premadeCollection.fetchEntries(query, null, function(){
											    SmoothScroll.$goTo(0);								// scroll back to top
											    $scope.loadingEntries = false;						// hide loading
											    $scope.premadeCollection.isUsingAdvanced = true;   // reset advanced
											    $scope.entryStatistics.hasMore = false;			   // hides more
											  });											
										}
										// no search string - continue with default loading
										else{																   
										  	loadDefault();										
										};
								});
							},
							//-----------------
							
							//-----------------
							clearAdvancedSearch:function(){
								$location.url($location.path());
								$scope.collection.allEntries = [];
								$scope.premadeCollection.initialLoad();
							},
							//-----------------
							
							//-----------------
							saveSearch:function(){
								if ($scope.collection.remember.useLocalStorage){
									toaster.pop('success', "", "Save search results.");
						    		// build remember string
						    		var rememberArray = custom.returnPropertyInObject($scope.collection.allEntries, [$scope.collection.remember.rememberWith])[$scope.collection.remember.rememberWith],
						    			rememberString = '';									
									for (i = 0; i < rememberArray.length; i++){
										if (i != 0){ rememberString += ", "; }
										rememberString += rememberArray[i]; 
									};								
									$scope.$storage.rememberMe = {										
										key: $scope.collection.remember.rememberWith,
										value: rememberString			    
									};
   								}
							},		
							//-----------------
							
							
							//-----------------
							loadPriorSearch:function(){
								  if ($scope.$storage.rememberMe != undefined){		
									  	$scope.collection.remember.hasStorage = true;										

										var packet = {
											header: "Load saved entries?",
											field1: "Yes", 
											field2: "Cancel"
										};	
										$scope.callCore.callController({who: "master", action: "confirmBox", data: packet}, function(d){ 
											
											if (d.packet.status != 'dismiss'){
												if (d.packet.data){	
											  	  var query = 'SELECT * FROM ' + $scope.collection.table + ' WHERE ' + $scope.$storage.rememberMe.key + ' IN(' + $scope.$storage.rememberMe.value + ')';
											  	  $scope.loadingEntries = true;									// hide loading
											  	  $scope.collection.allEntries = [];
												  $scope.premadeCollection.fetchEntries(query, null, function(){
														 SmoothScroll.$goTo(0);								// scroll back to top
														 $scope.loadingEntries = false;						// hide loading
														 $scope.premadeCollection.isUsingAdvanced = true;   // reset advanced
														 $scope.entryStatistics.hasMore = false;			   // hides more
												  });	
												}		
											}
										});		
									}							
								
							},
							//-----------------
							
							//-----------------
							fetchMostRecent:function(){
								$scope.loadingEntries = true;
								$timeout(function(){
								  // pull the most recent entry, but only one
								  var query = "SELECT * FROM " + $scope.collection.table +  " ORDER BY id DESC LIMIT 0, 1";  
								  $scope.premadeCollection.fetchEntries(query, null, function(){
								    SmoothScroll.$goTo(0);
								    $scope.loadingEntries = false;
								  });
								});
							},
							//-----------------
							
							//-----------------
							fetchEntry:function(whereStatement, insertInto, callback){
								$scope.loadingEntries = true;
								$timeout(function(){
								  // pull the most recent entry, but only one
								  var query = "SELECT * FROM " + $scope.collection.table + " " + whereStatement;  
								  $scope.premadeCollection.fetchEntries(query, insertInto, function(){								   
								   	$scope.loadingEntries = false;
								   	callback();
								  });
								});
							},
							//-----------------							

							//-----------------
							fetchMore:function($event){
								// turn into thinking
								var btn = $event.target;
									btnText = $(btn).text(),
									btnRaw = $(btn).html();	
														
								$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
								
								var query = "SELECT * FROM " + $scope.collection.table +  " ORDER BY id DESC LIMIT " + $scope.entryStatistics.available + ", " + $scope.collection.pagination.morePerClick;  
								$scope.premadeCollection.fetchEntries(query, null, function(){
									$(btn).html(btnRaw).attr('disabled', false);  
								});
							},
							//-----------------
	
							//-----------------
							fetchEntries:function(query, insertInto, callback){	
									var queryString = {	
										query: query, 
										database: $scope.masterData.system.usingdb
									};
								
									$scope.callCore.callController({who: "master", action: "phpRunQuery",  data: queryString}, function(d){ 																

											// sanitize html types that use html entities (adv)
											var htmlEntities = [];
											var allInputTypes = $scope.collection.searchFields.inputType;
											var allFields = $scope.collection.searchFields.Field;
											for (var i = 0; i < allInputTypes.length; i++){
												var inputType = allInputTypes[i]; 
												if (inputType == 'adv' || inputType == 'imageUpload'){
													htmlEntities.push({data: allFields[i], type: inputType});
												}
											}													

											processed = 0;
											var entryPacket = d.packet.data; 
											function loopCycle(){
												
												//  cycle												
												if (processed < entryPacket.length){
			
														var subProcess = 0;	
														function subloopCycle(){
																												
															if (subProcess < htmlEntities.length){																	
																filterField = [htmlEntities[subProcess].data];
																filterType = htmlEntities[subProcess].type;
																
																
																filterMe = entryPacket[processed][filterField];
																phpJS.dencodeHtmlEntities(filterMe , function(state, data){																	
												    				escapedStrings = data.replace(/["']/g, "");
												    				
												    				// parse json string into object
												    				if (filterType == 'imageUpload'){
												    					if (data != null && data != undefined && data != ''){
												    						entryPacket[processed][filterField] = JSON.parse(data);
												    					}
												    					else{
												    						entryPacket[processed][filterField] = data; 
												    					}
												    				}
												    				// html text
												    				else{
												    					entryPacket[processed][filterField] = data; 
												    				}
												    				
																	subProcess++;
																	subloopCycle();													    				
												    			});																															
															}
															else{
																subloopComplete();
															}
														};
														subloopCycle();
														
														function subloopComplete(){
															processed++;															
															loopCycle();
														}								
												}	
												//  loop complete
												else{	
													// if inserting into an existing array location
													if (insertInto != null){				
														$timeout(function(){									
															$scope.collection.userEntries[insertInto] = entryPacket[0];

														});
														callback();	
													}
													// normal behavior
													else{																									
														$scope.premadeCollection.arrangeEntries(d.packet.data, callback);	
													}
												}	
												
											};
											loopCycle();	
										
									});	
							},	
							//-----------------		
							
							//-----------------
							arrangeEntries:function(entryPackages, callback){
									// add results to allEntries
									// $scope.collection.allEntries.push(d.packet.data);
									for (i = 0; i < entryPackages.length; i++){
										$scope.collection.allEntries.push(entryPackages[i]);
									};
									
									// number of entries
									$scope.collection.numberOfEntries = custom.returnLengthOfObject( $scope.collection.allEntries ); 
									$scope.entryStatistics.available = custom.returnLengthOfObject( $scope.collection.allEntries ); 
									
									// more entires does not appear when using advanced search
									if (!$scope.premadeCollection.isUsingAdvanced){
										if ( $scope.entryStatistics.available == $scope.entryStatistics.total){
											$scope.entryStatistics.hasMore = false;
										}
										else{
											$scope.entryStatistics.hasMore = true;
										}
									}
									else{
										$scope.entryStatistics.hasMore = false;
									}
									
									// paginate to current page
									$scope.premadeCollection.paginateEntries();
									if (callback != null || callback != undefined){
										callback();
									}
							},
							//-----------------
							
							//-----------------
							advancedSearch:function(){
								
								packet = {
									fields: $scope.collection.searchFields.Field,
									types: $scope.collection.searchFields.Type,
									objToQuery: $scope.collection.allEntries,
									database: $scope.masterData.system.usingdb,
									table: $scope.collection.table
								};
								
								$scope.callCore.callController({who: "master", action: "advSearchModal", data: packet}, function(d){ 
									if(d.packet.data != "dismissed"){
										$scope.premadeCollection.isUsingAdvanced = true;		
										toaster.pop('success', "", "Loading search results.");
										$scope.collection.allEntries = [];
										var whereStatement = 'WHERE id IN(' + d.packet.data.returnData + ')';																	
										$scope.premadeCollection.fetchEntry(whereStatement, null, function(){
											
										});
									}
								});
							},
							//-----------------

							//-----------------
							// addImagePacket = {
							//   allImages: [{name: pageName, src: data64String, thumbnail: data64String}]	
							//   newImages: [{name: pageName, src: data64String, thumbnail: data64String}]
							//   uploadId: 10
							// } 
							addImagesTo:function(addImagePacket, callback){
											
											
											
											// declare variables
											var _all = addImagePacket.allImages,
												_added = addImagePacket.newImages,
												_id = addImagePacket.uploadId,
												_next = function(){ count++; loop(); }, 
												count = 0;
											
											
														
											// loop function								
											function loop(){

												//  loop conditions
												if (count < _added.length){
													
													delete _added[count].parse;   // remove property so it doesn't get uploaded again
													
													// upload full size image
													function uploadFullSize(){
														var imageSrc = _added[count].src;
														var packet = {
															file: imageSrc,
															name:  _added[count].name,
															location: $scope.collection.imageStorage + _id
														};	
														
														phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
															if (state){
																$timeout(function(){
																	_added[count].src = data.src;
																	uploadThumbnail();
																});			
															}																						
														});																								
		
													}
													// upload thumbnail size image
													function uploadThumbnail(){
														var imageSrc = _added[count].thumbnail;
														var packet = {
															file: imageSrc,
															name: _added[count].name + "_thumbnail",
															location: $scope.collection.imageStorage + _id
														};
														
														phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
															if (state){
																$timeout(function(){
																	_added[count].thumbnail = data.src;
																	_next()
																});			
															}																						
														});																																													
													}																
													
													// loop logic 
													if (_added.length > 0){
														uploadFullSize();
													}
													// loop complete
													else{
														complete()
													}
													
												}
												
												// end loop conditions
												else{																						
													complete();
												}
											}
											// end loop function
											
											// complete function
											function complete(){
												_all = JSON.stringify(_all);	
												callback(_all);	
											}
											
											// start loop
											loop();	

							},
							//-----------------

							//-----------------
							editEntry:function($event, id, index){
													
									$scope.premadeCollection.buttonThink( $($event.target) );
								
									// grab entry information
									var queryString = {	
										query: "SELECT * FROM " + $scope.collection.table + " WHERE id=" + id, 
										database: $scope.masterData.system.usingdb
									};
									$scope.callCore.callController({who: "master", action: "phpRunQuery",  data: queryString}, function(d){ 											
											var editData = d.packet.data[0];		
											var packetData = {	
												database: $scope.masterData.system.usingdb,
												table: $scope.collection.table
											};
											// call the create module
											$scope.callCore.callController({who: "master", action: "phpGetTableDetails",  button: $($event.target), data: packetData}, function(d){ 	
												if (d.packet.status == 'success'){	
																				
																															
																				
													var tableFields = {
														form: d.packet.data.formData,
														meta: d.packet.data.metaData,
														editData: editData
													};
													// post modal
													$scope.callCore.callController({who: "master", action: "postModal", data: tableFields}, function(d){ 
														if (d != undefined && d.packet.status == "closed"){
		     												
		     												
		     												
														    // build update string
														    var buildString = "",
														    	loopCount = 0;
														    	
														    function addToString(fieldLabel, entry){
														    	buildString += fieldLabel + "='" + entry + "', ";
																loopCount++;
																loopCycle();
														    };	
														    
														    function loopCycle(){
															   	
															   	if (loopCount < tableFields.form.length){
															   		
															   		
															    	var fieldLabel = tableFields.form[loopCount].Field;
															    	var newEntry = d.packet.data.returnData[loopCount];
															    	var inputType = tableFields.form[loopCount].inputType;
															    	
															    	if (newEntry != undefined && newEntry != null && newEntry != "Invalid Date"){			
															    																	    		
															    		// sanitize advanced texts
															    		if(inputType == 'adv'){															    		
															    			phpJS.encodeHtmlEntities(newEntry, function(state, data){
															    				addToString(fieldLabel, data);
															    			});
															    		}
															    		
															    		// image uploads
															    		else if(inputType == 'imageUpload'){		
																				
																				// delete unused function
																				function deleteUnusedImages(){
																					var packetData = {
																						location: $scope.collection.imageStorage + id
																					};
																					
																					$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){
																						$timeout(function(){

																							// parse information
																							if (d.packet.data.files != null && d.packet.data.files != null){
																								
																								
																								var filesInDirectory = [];
																								for (var i = 0; i < d.packet.data.files.length; i++){
																									var f = (d.packet.data.files[i]).replace(/\.[^/.]+$/, "");
																									if (!/thumbnail/i.test(f)){  // exclude thumbnails names
																										filesInDirectory.push( f );
																									}
																								};
				
																								// delete files no longer in use
																								var currentFiles = [];
																								for (var i = 0; i < newEntry.length; i++){
																									var f = newEntry[i].name;
																									currentFiles.push( f );
																								}																						
																								
																								// get all files to delete
																								var toBeDeleted = custom.compareArrayForDifferences(filesInDirectory, currentFiles);
		
																								// get image and thumbnail
																								var packet = [];
																								for (var i = 0; i < toBeDeleted.length; i++){
																									packet.push({folder: "../" +  $scope.collection.imageStorage + id, file: toBeDeleted[i] + ".png"});
																									packet.push({folder: "../" +  $scope.collection.imageStorage + id, file: toBeDeleted[i] + "_thumbnail.png"});
																								}
																							
																								// delete files from																						
																								if( packet.length > 0 ){
																									$scope.callCore.callController({who: "master", action: "phpDeleteFiles", data: packet}, function(_d){																																																																																					
																										addNewImages();																																														
																									});
																								}
																								else{
																									addNewImages();
																								}
																								
																								
																							}
																							// nothing to parse, skip to add
																							else{																							
																								addNewImages();
																							}
																					
																						});
																					
																					});			
																				};
																				// end delete image function
																				
																				// add image functions		
																				function addNewImages(){
																		
																					// get files to add
																					var addList = [];
																					for (var i = 0; i < newEntry.length; i++){
																						if (newEntry[i].parse){
																							addList.push( newEntry[i] );
																						}
																					}	
																					
																					var addImagePacket = {
																						allImages: newEntry,
																						newImages: addList,
																						uploadId: id
																					};
																					
																					$scope.premadeCollection.addImagesTo(addImagePacket, function(data){
																						phpJS.encodeHtmlEntities(data, function(state, _data){
																							addToString(fieldLabel, _data);
										    											});		
																						
																						
																					});

																				};	
																				// end add image function		
			
																				// start
																				deleteUnusedImages();
															    			
															    		}															    		
															    		else if(inputType == 'date'){
															    		  	newEntry = newEntry.replace(/\D/g,'');
															    		  	addToString(fieldLabel, newEntry);
															    		}
															    		else if(inputType == 'time'){
															    		  	newEntry = moment(newEntry).format( "H:mm:ss");
															    		  	addToString(fieldLabel, newEntry);
															    		}
															    		else{
																    		addToString(fieldLabel, newEntry);
																    	}	
																    	// end sanitized edits before saving
																    	
															    	}
															    	else{
																	    loopCount++;
																	    loopCycle();															    		
															    	}
															    	
															    }
															    else{
															    	loopComplete();
															    }
															   
														    }
														    
														    
														    
														   function loopComplete(){
														  		
														   		buildString = buildString.replace(/,\s*$/, "");
														   		
															    // edit update
															    var packetData = {
															   		query: "UPDATE " + $scope.collection.table + " SET " + buildString + " WHERE id=" + id,
															   		database: $scope.masterData.system.usingdb
															    };	
															    // update edit!																      		
																$scope.callCore.callController({who: "master", action: "phpModifyEntry", button: $($event.target),  data: packetData}, function(d){ 
																	var whereStatement = 'WHERE id=' + id;
																	
																															
																	$scope.premadeCollection.fetchEntry(whereStatement, index, function(){
																		toaster.pop('success', "", "Your post has been edited successfully.");
																	});
																});
																
																
															
															}
															// end edit
															
															// start loop
															loopCycle();
														}
													});	
													// /end post modal													
												}	
											});	
											// end call create module															
									});	
									// end grab edit informatoin
									
							},
							//-----------------	

							//-----------------
							deletePost:function(id, index){
									var packet = {
										header: "Delete this entry?",
										field1: "Yes", 
										field2: "NOOOO!!"
									};
								
									// confirm box
									$scope.callCore.callController({who: "master", action: "confirmBox", data: packet}, function(d){ 
										if (d.packet.data == true){		
												// delete from database
												var queryString = {	
													query: "DELETE FROM " + $scope.collection.table + " WHERE id=" + id, 
													database: $scope.masterData.system.usingdb
												};
												// delete all information
												$scope.callCore.callController({who: "master", action: "phpModifyEntry",  data: queryString}, function(d){ 
													
													// delete all images from folder
													$scope.premadeCollection.deleteFolder($scope.collection.imageStorage + id, function(){
														$timeout(function(){
															$scope.collection.userEntries.splice(index, 1);
															toaster.pop('success', "", "Your post has been delete successfully.");
														});		
													});					
												});	
												// end delete
										}				
									});	
									// end confirm								
							},
							//-----------------
							
							//-----------------
							deleteFolder:function(folder, callback){				
							   	
							    	var packetData = {	
										folder: "../" + folder
									};

											
									$scope.callCore.callController({who: "master", action: "phpDeleteFolder", data: packetData}, function(d){
										 															
										callback(d);
									});								
								
							},
							//-----------------
							
							//-----------------
							paginateEntries:function(){					
								
									var packet = {
										objToPaginate: $scope.collection.allEntries,
										filterBy: $scope.collection.pagination.filterBy,
										reverse: $scope.collection.pagination.reverse,
										filterSize: $scope.collection.pagination.limit,
									};
								

									var _results =  custom.sortAndPage( packet );
										// apply to page
									$timeout(function(){
										// content for the main page
										$scope.collection.userEntries = _results.page( $scope.collection.pagination.current );;

										// get pagination data
							    		$scope.collection.pagination.max = _results.totalPages;						    		
							    		$scope.collection.pagination.hasNext = _results.hasNext();
							    		$scope.collection.pagination.hasPrev = _results.hasPrev();
							    		
							    		if ($scope.collection.pagination.max == $scope.collection.pagination.current ){
							    			$scope.collection.pagination.isLast = true;
							    		}else{
							    			$scope.collection.pagination.isLast = false;
							    		}
							    		if (_results.currentPage == 1 ){
							    			$scope.collection.pagination.isFirst = true;
							    		}else{
							    			$scope.collection.pagination.isFirst = false;
							    		}		
									});
								
							},							
							//-----------------				

							//-----------------
							openNewEntry:function($event, tableName){
								$scope.premadeCollection.buttonThink( $($event.target) );
												
								var packetData = {	
									database: $scope.masterData.system.usingdb, 
									table: $scope.collection.table
								};
								$scope.callCore.callController({who: "master", action: "phpGetTableDetails",  button: $($event.target), data: packetData}, function(d){ 	
									if (d.packet.status == 'success'){								
										var tableFields = {
											form: d.packet.data.formData,
											meta: d.packet.data.metaData,
											editData: null
										};
										$scope.callCore.callController({who: "master", action: "postModal", data: tableFields}, function(d){ 
												if(d.packet.status == "closed"){
													var packetData = {
											 			database: $scope.masterData.system.usingdb,
											 			table: $scope.collection.table,
											 			fields: tableFields.form, 
											 			inputData: d.packet.data.returnData 									 			
													};											
													$scope.premadeCollection.createNewEntry(packetData);
												}
										});														
									}	
								});	
							},
							//-----------------
							
							//-----------------
							createNewEntry:function(packetData){
								
								$timeout(function(){
									toaster.pop('info', "", "Submitting your entry.");
								});
								var cloneArray = $.extend(true, [], packetData.inputData);

								// GET NEXT IN ID
								var queryString = {	
									table: $scope.collection.table, 
									database: $scope.masterData.system.usingdb
								};
							
								$scope.callCore.callController({who: "master", action: "phpCreateBlankEntry",  data: queryString}, function(d){ 
										
										var nextId = d.packet.data.id;

										// SANITIZE ENTRIES OF NEW POSTS
										var counter = 0; 
										var _next = function(){
											counter++;
											setLoop();
										};								
										function setLoop(){
											
											
											if (counter < packetData.fields.length){
													var type = packetData.fields[counter].Type,
														inputType = packetData.fields[counter].inputType;
												
													if (type == "time"){
														cloneArray[counter] = moment(packetData.inputData[counter]).format( "H:mm:ss");
														_next();											
													}
													else if (inputType == "imageUpload"){
														var index = counter; 
														var subCounter = 0; 
													
														var addImagePacket = {
															allImages: packetData.inputData[index],
															newImages: packetData.inputData[index],
															uploadId: nextId
														};
														
														$scope.premadeCollection.addImagesTo(addImagePacket, function(data){															
															cloneArray[index]  = data; 
															_next();
														});
												
													}
													else{
														_next();
													}													
		
											}
											else{
													imageloopComplete();
											}
											counter++;
										}								
										setLoop();  // start loop
										
										
										function imageloopComplete(){
											
											// clone array has been formmated correctly
											packetData.inputData = cloneArray; 
											
											// sanitize and ready edit statement
 											custom.sanitizePacketsForDatabase(packetData, function(sanitizedData){	
											    // edit update
											    var queryPacket = {
											   		query: custom.returnEditString($scope.collection.table, sanitizedData, nextId),
											   		database: $scope.masterData.system.usingdb
											    };								     		
										    
											    // update newly created entry!													    		
												$scope.callCore.callController({who: "master", action: "phpModifyEntry",  data: queryPacket}, function(d){ 
													if (d.packet.status == "success"){			
														var whereStatement = 'WHERE id IN(' + nextId + ')';																	
														$scope.premadeCollection.fetchEntry(whereStatement, null, function(){
															
														});
														toaster.pop('success', "", "Your post has been uploaded successfully.");
													};		
												});
											
											});		

										}
								});
								
							},
							//-----------------		
							
							//-----------------
							buttonThink:function(btn){
								var btnText = $(btn).text();								
								$(btn).html('<i class="fa fa-spinner fa-pulse"></i> ' + btnText).attr('disabled', true);
							},
							//-----------------							
								
						};		
					})();
					//-----------------  
					
					

					

					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: pageName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(pageName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: pageName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 										
									}
									
									var packet = {
											info:{
												to: e.who,
												from: pageName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 			
				   

				}]);		

//end
//start

				app.controller('sharedController', 
							['$rootScope', '$stateParams', '$scope', '$timeout', '$location', '$sessionStorage', '$localStorage', 'SmoothScroll', 'cfpLoadingBar', 'toaster', 'TransactionManager', 'angularLoad',
					function($rootScope, $stateParams, $scope, $timeout, $location, $sessionStorage, $localStorage, SmoothScroll, cfpLoadingBar, toaster, TransactionManager, angularLoad) {
			
	    			// reference self via url
	    			var sharedName		= decodeURIComponent($location.url().replace('/','').toLowerCase());			
					if(sharedName.indexOf("?search=") > -1){
						sharedName	= sharedName.slice(0, sharedName.indexOf("?"));
					}
					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					
								// ---------------- VARIABLES 
							    resetVariables:function(){
							    	$scope.page = {
							    		loadComponents: custom.fillArray(2),  // number should equal # of load components below
							    		isLoaded: false,							    		
							    		settings: null,
							    		checkPass: false,
							    		access: null,
							    		hasErrors: false,
							    		sidebar: "none",
							    		noTable: false,
							    		noTableError: false,
							    		
							    		searchOpen: false,
							    		editOpen: false,
							    		layoutOpen: false,
							    		theme:_global_setup.theme
							    	};
							    	
							    	
							    },
							   	//-----------------		
					
								//----------------- REFRESH
								onRefresh:function(){	
									$timeout(function(){								
										sharedData.add("currentController", sharedName);	// current controller
										$scope.masterData = sharedData.getAll();										
										$scope.callCore.callController({who: "master", action: "hideThinking"});										
										$scope.mainCore.checkPermissions(function(){});																				
									});
								},
								//-----------------						
					
								// ---------------- INIT
								init:function(){
									
									
									custom.offCanvasReset();
									$scope.initCore.resetVariables();
									cfpLoadingBar.start();
									SmoothScroll.$goTo(0);							
									$scope.initCore.loadComponents(function(){
										sharedData.add("currentController", sharedName);	// current controller
										
										
										
										
										// if parallax 
										if(!$scope.page.hasErrors){
											if ($scope.page.settings.bgType == 'parallax'){
												custom.parallaxStart();	
										   		SmoothScroll.$goTo(25).then(function() {
						 						   return SmoothScroll.$goTo(0);
						 						});
					 						}
				 						}else{
				 							console.log("Fail to load parallax.");
				 						};
												
										
										// initiate edit 
										if(!$scope.page.hasErrors){
											$scope.mainCore.checkPermissions(function(){
												$scope.editCore.init(function(){
													if ($scope.page.access.canRead){										
														$scope.mainCore.start();	
													}
												});
											});
										}else{
											console.log("Could not load page due to incorrect data from _pagedata.  Check table to make sure names match up.");
										}
										
										
									});				
								},
								//-----------------        
					        
								// ---------------- LOAD COMPONENTS
								loadComponents:function(callback){
									$scope.initCore.checkMaster(function(state){
										if (state){
											$scope.masterData = sharedData.getAll();
											$scope.page.loadComponents[0] = true;
										
											phpJS.returnJsonAsObject("/../production/settings/pagedata/" + sharedName + ".json", function(state, data){   
												
												sharedData.add("pagedata_" + sharedName, data);	// current controller
												
												function loadcomplete(){
													
												};

												// no data load with errors
												if (data == null){
													// create json if it does not exist and reload
													var packet = {source: '../production/settings/defaults/newpagedata.json', newName: '../production/settings/pagedata/' + sharedName + '.json'};													
													phpJS.copyFile(packet, function(state, data){
														$timeout(function(){
															toaster.pop('success', "Page data created...", "Refreshing the page in 3 seconds.");
														});
														setTimeout(function(){
															location.reload();
														}, 3000);
														
													});													
																										
												}
												// page data loaded
												else{
													if (!$scope.masterData.browserDetails.mobile){
														$scope.page.browserType = "desktop";
													}
													else{
														$scope.page.browserType = "mobile";
													}												
													
													// SET PAGE SETTINGS
													$scope.pagedata = data;
													$scope.page.settings = data.pageSettings[$scope.page.browserType];
													$scope.page.settings.currentTable = data[$scope.page.browserType + "Table"];	
													
													// LOAD THEME SPECIFIC CSS				
													var themeRoot = "themes/available/" + _global_setup.theme;	
														
				
														// LOAD COMPONENTS FOR THEMEING
														var packetData = {
															location: themeRoot + "/layout/components"
														};																															
														$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){	
															
															layoutString = themeRoot + "/layout/";
															$scope.page.theming = {
																main: themeRoot + "/layout/html/layout.html"
															};
															
															for (i = 0; i < d.packet.data.files.length; i++){
																fileName = d.packet.data.files[i];
																$scope.page.theming[fileName] = themeRoot + "/layout/components/" + fileName;
															}
	
															// setup layout info
															$scope.page.settings.layoutUrl = themeRoot + "/layout/layout.html";																											
															$scope.page.fullSettings = data.pageSettings;	
																
															$scope.page.loadComponents[1] = true;
															$scope.initCore.checkLoad(callback);
															
														});	
				
																						
												};												
																							
											});				
										}
									});
									
			
									$scope.initCore.doSomething(function(state){
										if (state){
											$scope.page.loadComponents[1] = true;
											$scope.initCore.checkLoad(callback);
										}
									});						
								},
								//-------------------		
								
								// ------------------ LOAD COMPONENTS
								checkMaster:function(callback){
									// wait for master.js to finish loading 	
									sharedData.request("masterReady", function(state, data){
										if(state){
											if(data.ready == true){
												callback(true);
											}
											else{
												alert(data.ready);
											}
										}
										else{
											alert(data);
										}	
									});							
			
								},
								animate:function(callback){
					
																		
									if(!$scope.page.hasErrors){
										var bgType = $scope.page.settings.bgType;
										
										// set default if nothing is selected
										if (bgType == null || bgType == undefined){ bgType = "solid"; }
										
										
										switch(bgType) {
											
										    case "solid":
										    	var color = $scope.page.settings.solidColor || '#ffffff';
										    	$('#nt-colorContainer').transition({ 'background-color': color }, 750);										    		
										    	break;
										    case "static":
										    	$('#nt-colorContainer').transition({ 'background-color': 'rgba(0, 0, 0, 0)' }, 500);
										        custom.backstretchBG("themes/available/" + _global_setup.theme + "/background/" + $scope.page.settings.staticBG, 0, 1000);
										        break;
										    case "parallax":		
										    	$('#nt-colorContainer').transition({ 'background-color': 'rgba(0, 0, 0, 0)' }, 500);								    
										   		$('#parallax-container').addClass('parallax-scroll-main').css('background', 'url(../web/themes/available/' + _global_setup.theme + '/parallax/' + $scope.page.settings.parallaxBG + ')');										   							    	
										        break;									        
										        
										}		
									}else{
										console.log("Failed to find background type data.");
									}						
												
									$timeout(function(){	
									
										
										$('#content-page')
											.transition({   x: -20,  opacity: 0, delay: 0}, 0)
											.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
											.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
											
										callback(true);
									}, 500); // slight delay needed
									
								},
								

															
								// -- blank component for example
								doSomething:function(callback){	
									callback(true);
								},
								//-----------------		
								
								//----------------- CHECK LOAD
								checkLoad:function(callback){
									var check = true,
										array = $scope.page.loadComponents; 					
									var i = array.length; while(i--){
										if ( array[i] == false){
											check = false; 
										};
									};
									// all loads completed
									if (check){
									// CONTENT PAGE IS READY - INSERT CODE HERE		
																		
										// check for existing modal
										var duration = 2000;
										var hasModalOpen = $('.reveal-modal').hasClass('in');
										if (!hasModalOpen){ duration = 0; };
										
										$timeout(function(){ 
											cfpLoadingBar.complete();
											$scope.page.isLoaded = true;																
											$scope.callCore.callController({who: "master", action: "hideSplash"});
																
											callback();											
										}, duration);									
										
									}						
								},					
								//-----------------		
       
					    };
					})();
					//-----------------        
				
					//----------------- 
					$scope.editCore = (function() {
						
							// variables ---
							$scope.layout = {
								selected: null,
								types: [],
								currentType: null
							};
							// -------------
							 
							 return {
							 		 	
							 	//-----------------								      
								init:function(callback){
									
	
									$scope.layout.currentType = $scope.page.browserType; 
									
									$scope.editCore.getLayoutFiles(function(d){
																				
										// folder doesn't exist; create it
										if (d.data == 'no folder'){											
											callback();																				
										}										
										// no files; create a blank template
										else if (d.data == 'no files'){
											callback();		
										}										
										// files present - create selection 
										else{
											for (var i = 0; i < d.data.length; i++){
												$scope.layout.types.push(d.data[i]);
											}
											$timeout(function(){
												$scope.editCore.layout = $scope.page.settings.abstractLayout;	
											});
											
											callback();
										};	
									
										
									});
										
										
										
									
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	changeTo:function(version){
						      		 $timeout(function(){
						      		 	$scope.editCore.layout = $scope.pagedata.pageSettings[version].abstractLayout;							      		 	      		 	
						      		 });
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	layoutSave:function(){
						      										    
								    $scope.pagedata.pageSettings[$scope.layout.currentType].abstractLayout = $scope.editCore.layout;
								    // SAVE AS JSON
									phpJS.convertObjectToJson($scope.pagedata, "../production/settings/pagedata/" + $scope.pagedata.name + ".json", function(state, data){
										
									});		
									
							
									// SAVE IN DATABASE (as backup)
									var jsonString = JSON.stringify($scope.pagedata);
									phpJS.encodeHtmlEntities(jsonString, function(state, encodedString){
											
											var packet = {	pageName: $scope.pagedata.name, settings: encodedString, 
															desktopTable: $scope.pagedata.pageSettings.desktop, 
															mobileTable:  $scope.pagedata.pageSettings.mobile};
												
											phpJS.updatePageSettings(packet, function(state, data){	
												$timeout(function(){					
													toaster.pop('success', "Updated!", "Layout has been updated.");
												});
											});	
													
									});									
																
										
						      	},
						      	//-----------------
						      	

						      	//-----------------
						      	editSystemSettings:function(){
									
						      		var packetData = {system: $scope.masterData.system};
						      		$scope.callCore.callController({who: "master", action: "editorSystemSettings", data: packetData}, function(d){
						      			//console.log(d);
						      		});	
						      						      			
						      	},
						      	//-----------------		
						      	
						      	//-----------------
						      	scriptCreator:function(){
					
						      		var packetData = {system: $scope.masterData.system};
						      		$scope.callCore.callController({who: "master", action: "scriptCreator", data: packetData}, function(d){
						      			//console.log(d);
						      		});	
						      						      			
						      	},
						      	//-----------------		
						      	
						      	//-----------------		
						      	mediaManager:function(){
						      		
						      		var packetData = {system: $scope.masterData.system};
						      		$scope.callCore.callController({who: "master", action: "mediaManager", data: packetData}, function(d){
						      			//console.log(d);
						      		});	
						      		
						      	},					      	
						      	//-----------------						      	
						      	
						      	//-----------------
						      	editPageSettings:function(){

									var packetData = {
										location: "themes/available/" + $scope.page.settings.currentTable + "/"
									};
																					
						      		var packetData = {pagename: sharedName, url: $scope.masterData.url, settings: $scope.page.fullSettings, displayAs: $scope.page.browserType, database: $scope.masterData.system.usingdb};
						      		$scope.callCore.callController({who: "master", action: "editorPageSettings", data: packetData}, function(d){
						      			
						      		});							      			
	
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	editCollections:function(){
									
						      		var packetData = {system: $scope.masterData.system};
						      		$scope.callCore.callController({who: "master", action: "editorCollectionSettings", data: packetData}, function(d){
						      			
						      		});	
						      		
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	updatePageSettings:function(){						      		
						      		$timeout(function(){ 
						      			$scope.page.settings.abstractLayout = $scope.editCore.layout;						      			
						      		});
						      	},
						      	//-----------------
						      	 	
						      	//-----------------						      	
								getLayoutFiles:function(callback){
									
									var packetData = {
										location: "themes/available/" + $scope.page.settings.currentTable + "/"
									};
																					
									$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){
										
										if (d.packet.data.status == true){
												// pull data from folder
												var files = d.packet.data.files; 
												// files exist
												_return = [];
												if (d.packet.data.files.length > 0){
													for (i = 0; i < files.length; i++){
														_return.push( files[i].replace(/\.[^/.]+$/, "") );	
													};																							
													callback({status: true, data: _return});
												}
												// no files exist
												else{
													callback({status: false, data: "no files"});
												}
										}
										if (d.packet.data.status == "error"){											
											callback({status: false, data: "no folder"});
										}
										
										
										
										
										
									});
						      	},
						      	//-----------------	
						      	
						      	//-----------------						      	
								openLayoutEditor:function(callback){
									var packetData = {
										fields: $scope.collection.searchFields
									};											
									$scope.callCore.callController({who: "master", action: "editorLayout", data: packetData}, function(d){
										//console.log(d)
									});
						      	},
						      	//-----------------				      	
 
						      };							
					})();
					//----------------- 
					
					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
							//----------------- START 
							start:function(){

								// remove hidden class from initial load
								$('#nt-editor-sidebar').removeClass('hidden');
								$('#nt-editor-bottom').removeClass('hidden');
								$('#nt-layout-button-group, #nt-overlay-btn').removeClass('hidden');
								
								
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.premadeCollection.init();
		
							},
							//-----------------
							
							//----------------- check permissions and redirect
							checkPermissions:function(callback){
								var path = $location.path().replace(/[^a-zA-Z1-9 ]/g, ""),								
									allowed = $scope.masterData.accessLevel.allowed;
							
								$scope.page.checkPass = false;
								for (var i = 0; i < allowed.length; i++){
									check = allowed[i];
									if (check == path){
										$scope.page.checkPass = true;
									}
								}
								// redirect
								if(!$scope.page.checkPass){
									alert("You dont' have the proper permission to access this page.");
									$location.path("/redirect");  
								}
								else{	
									var userPermission = $scope.masterData.accessLevel.permission,
										permissionTypes = $scope.masterData.system.systemdata.permissionTypes, 
										index = custom.findWithAttr(permissionTypes, 'label', userPermission);									
										access = permissionTypes[index].access;										
										currentTable = $scope.page.settings.currentTable;										
										t = custom.findWithAttr(access, 'tableName', currentTable);
										
										
										
										// IF TABLE DOES NOT EXIST, PASS GENERIC PERMISSIONS TO AVOID ERRORS
										if (t == undefined){										  	
										  	$scope.page.settings.currentTable = false;
										  	$scope.page.settings.noTable = true;
										  	$scope.page.settings.noTableError = true;
										  	$scope.page.access = {canComment: true, canCreate: true, canDelete: true, canRead: true, canWrite: true, tableName: "none"};
										}
										else{
										  	$scope.page.settings.noTable = false;
										  	$scope.page.settings.noTableError = false;											
											$scope.page.access = access[t];
										}											 
									callback();
								}
							},
							//-----------------
							
							//----------------- PICTURE BOX 
							pictureBox:function(_list, _index){								
								var packetData = {
									list: _list,
									index: _index
								};
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.callCore.callController({who: "master", action:"pictureBox", data: packetData}, function(d){
									//console.log(d);						
								});		
							},
							//-----------------							
							

						};
					})();
					//-----------------  
					
					
					
					//-----------------  COLLECT FROM DATABASE PREMADE FUNCTIONS
					$scope.premadeCollection = (function(){
					
						var listOfTemplates = {};
						
						return { 
							
							
							//-----------------
							resetVariables:function(){
									// ----------------------------------
									
									var settings = $scope.page.settings,
										 _nFx = {x: 0, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0},
										 _pFx = {x: 0, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0},
										 _speed = 500;

									if (settings.ani_enter != undefined){

											switch(settings.ani_enter.toLowerCase()) {
											    case "slide left":
											        _nFx = {x: -800, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "slide right":
											        _nFx = {x: 800, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade left":
											        _nFx = {x: -100, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade right":
											        _nFx = {x: 100, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;									    
											    case "fade up":
											        _nFx = {x: 0, y: -100, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade down":
											        _nFx = {x: 0, y: 100, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;		
											    case "zoom in":
											        _nFx = {x: 0, y: 0, scale: 1.5, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "shrink out":
											        _nFx = {x: 0, y: 0, scale: 0.5, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;		
											    case "skew left":
											        _nFx = {x: 0, y: 0, scale: 1, skewX: '-90deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "skew right":
											        _nFx = {x: 0, y: 0, scale: 1, skewX: '90deg',  skewY: '0deg',  opacity: 0};
											    break;	
											    case "none":
											        _nFx = {x: 0, y: 0, scale: 1, skewX: '90deg',  skewY: '0deg',  opacity: 1};
											    break;										    										    									        
											}
											
											
											switch(settings.ani_exit.toLowerCase()) {
											    case "slide left":
											        _pFx = {x: -800, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "slide right":
											        _pFx = {x: 800, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade left":
											        _pFx = {x: -100, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade right":
											        _pFx = {x: 100, y: 0, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;									    
											    case "fade up":
											        _pFx = {x: 0, y: -100, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "fade down":
											        _pFx = {x: 0, y: 100, scale: 1, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;		
											    case "zoom in":
											        _pFx = {x: 0, y: 0, scale: 1.5, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "shrink out":
											        _pFx = {x: 0, y: 0, scale: 0.5, skewX: '0deg',  skewY: '0deg',  opacity: 0};
											    break;		
											    case "skew left":
											        _pFx = {x: 0, y: 0, scale: 1, skewX: '-90deg',  skewY: '0deg',  opacity: 0};
											    break;
											    case "skew right":
											        _pFx = {x: 0, y: 0, scale: 1, skewX: '90deg',  skewY: '0deg',  opacity: 0};
											    break;	
											    case "none":
											        _nFx = {x: 0, y: 0, scale: 1, skewX: '90deg',  skewY: '0deg',  opacity: 1};
											    break;										    
											}	
											
											switch(settings.ani_speed) {
											    case 1:
											        _speed = 250;
											    break;	
											    case 2:
											        _speed = 500;
											    break;	
											    case 3:
											        _speed = 1000;
											    break;										    									    
											}
									
									}	
									
									
	
									$scope.transitionFX = {
										using: "nextFX",  // default
										nextFX: _nFx,
										backFX: _pFx,
										duration: _speed,										
										enabled: settings.ani_enable,
										rush: settings.ani_rush
									};
									

									
									
								
									if ($scope.transitionFX.enabled){
										$scope.myStyle={"opacity":"0"};
									}
									else{
										$scope.myStyle={"opacity":"1"};
									}
									
									
							    	$scope.collection = {
							    		
							    		table: $scope.page.settings.currentTable, 
							    		imageStorage: "uploads/images/" + $scope.page.settings.currentTable + "/",		 // 
							    		searchWhere: "",
							    		
							    		pagination:{
							    			form: settings.layout,
							    			showHeader: settings.showHeader,
							    			showLoading: settings.showLoading,
							    			showPagination: settings.showPagination,
							    			showSort: settings.showSort,
							    			showFilter: settings.showFilter, 
							    			showQuickSearch: settings.showQuickSearch,
							    			showAdvancedSearch: settings.showAdvancedSearch,
							    			showTotalEntries: settings.showTotalEntries,
							    			showNextBtn: settings.showNextBtn,
							    			showBackBtn: settings.showBackBtn,
							    			showRandomBtn: settings.showRandomBtn,
							    			morePerClick: settings.fetchPerClick, 
							    			fetchOnLoad: settings.fetchOnLoad,
							    			multipBackBtn: settings.multipBackBtn,
							    			multipNextBtn: settings.multipNextBtn,
							    			defaultPagiLimit: settings.defaultPagiLimit,
							    			entriesOnPage: settings.entriesOnPage,
							    			equalizeEntries: settings.equalizeEntry, 
							    			sortBy: settings.sortBy,
							    			loadMsg: settings.loadMsg,
							    			prescripts: settings.prescripts,
							    			postscripts: settings.postscripts,
							    			
							    			count: 0,						 	// default 
							    			current: 1, 					 	// 
							    			limit: 5,							// entries per page (initial value)							    			
							    			filerBy: null,
							    			reverse: true,
							    			max: null,
							    			filter: null,
							    			hasNext: null,
							    			hasPrev: null,
							    			isLast: null,
							    			isFirst: null,
							    			range: [
							    				{id: 1, label: 5},
							    				{id: 2, label: 10},
							    				{id: 3, label: 25},
							    				{id: 4, label: 50}				    				
							    			],
							    			selectResults: null
							    		},
			
							    		remember:{
							    			useLocalStorage: true,
							    			rememberWith:  "id",
							    			hasStorage: false
							    		},
							    		
							    		
										allEntries: [],
							    		useEntries: null,	
							    		numberOfEntries: null,		    		
							    		
							    		filterSearch: null,
							    		searchResultsCount: null,
							    		searchType: null,
							    		searchTerm: null,
							    		searchFields: null,
										userEntries: [], 	
										isAutoDisabled: false,									
							    	};	
							    	
							    	
							    	
							    	// ----------------------------------
			
									// ----------------------------------	
									$scope.entryStatistics = {
										hasFirst: 		null,
										hasPrev: 		null,
										hasNext: 		null,
										hasLast: 		null,										
										hasMore:   		null,
										hasMorePage:    null,
										hasLessPage:    null,
										
										startAt:		0,
										available: 		null,
										total: 			null
									};
									// ----------------------------------	
									
									// ----------------------------------
									// when exiting this page	
									$scope.$on('$destroy', function() {
										//$scope.premadeCollection.saveSearch();
									});
									// ----------------------------------	
										
							    	// ----------------------------------
							    	$scope.firebasePermission = {
							    		fbID_required_toCreate: true,
							    		fbID_required_toEdit: true,
							    		fbID_required_toDelete: true,		
							    		
							    		fbIDmustMatchEntryId_edit: true,
							    		fbIDmustMatchEntryId_delete: true
							    	};
							    	// ----------------------------------
							    	
									// ----------------------------------
									$scope.$storage 		= $sessionStorage;	
									$scope.editBtn 			= {value: 0};
									$scope.deleteBtn 		= {value: 0};
									$scope.loadingEntries 	= true;
									$scope.isUsingAdvanced 	= false;
									// ----------------------------------	
																
							},
							//-----------------
							
							
							//-----------------
							init:function(){
								
								$scope.premadeCollection.resetVariables();
								$scope.initCore.animate(function(state){ });
								
								// table exists
								if ($scope.collection.table != false){
										var packetData = {	
											database: $scope.masterData.system.usingdb,
											table: $scope.collection.table
										};
										
										// get entry row totals
										$scope.callCore.callController({who: "master", action:"phpRunQueryForTotalEntry", data: packetData}, function(d){
											$scope.entryStatistics.total = d.packet.data.rows;
											
											
											
											// get table data
											$scope.callCore.callController({who: "master", action: "phpGetTableDetails", data: packetData}, function(d){ 
												
												
												
												$scope.collection.searchFields = custom.returnPropertyInObject(d.packet.data.formData, ["Field", "Type", "inputType", "Comment"]);
		
												// search comments for isSearchable attribute
												$scope.collection.searchFields.isSearchable = []; 
												for (i = 0; i < $scope.collection.searchFields.Comment.length; i++){
													var c = $scope.collection.searchFields.Comment[i];
													
													if (c.indexOf("isSearchable") > -1){
														$scope.collection.searchFields.isSearchable[i] = true;
													}
													else{
														$scope.collection.searchFields.isSearchable[i] = false;
													}
												}
												
												$scope.collection.pagination.filterBy = $scope.collection.searchFields.Field[0];
												$scope.premadeCollection.initialLoad();
											});									
										});
								}
								// table does not exists
								else{
									$scope.premadeCollection.initialLoad();
								}
							
							},
							//-----------------
							
							//-----------------
							
							layoutUrl:function(index){	
																	
								if(!listOfTemplates.hasOwnProperty(index)){
									listOfTemplates[index] = $scope.page.settings.abstractLayout;
								}		
								

								if (listOfTemplates[index] == "none"){												
									//  		themes/available/default					/layout/templates/no_template.html 							
									return "themes/available/" + _global_setup.theme + "/layout/html/empty.html";
								}
								else{
									//console.log("template/available/" + $scope.page.settings.currentTable + "/" + listOfTemplates[index] + ".html")
									return "templates/collection/" + $scope.page.settings.currentTable + "/" + listOfTemplates[index] + ".html";
								}								
							},	


							//-----------------
							
							//-----------------
							initialLoad:function(){
								
								
								function loadDefault(){
									  
									  // reset
									  $scope.collection.userEntries = [];
									  
									  // load initial for single page 
									  if ($scope.collection.pagination.form == "single"){
									  		
										  	$scope.collection.pagination.startFromEntry = $scope.entryStatistics.total - 1;
									 		$scope.collection.pagination.currentEntry = $scope.entryStatistics.total - 1;								  											  							  	
											$scope.premadeCollection.fetchMostRecent(1);
									  }	
									  
									  // growing 
									  if ($scope.collection.pagination.form == "multi" || $scope.collection.pagination.form == "multi_auto"){
									  		if ($scope.entryStatistics.total > $scope.collection.pagination.fetchOnLoad){
												$scope.collection.pagination.startFromEntry = $scope.entryStatistics.total - $scope.collection.pagination.fetchOnLoad;
												$scope.collection.pagination.currentEntry = $scope.entryStatistics.total - $scope.collection.pagination.fetchOnLoad;
											}	
											else{
									 			$scope.collection.pagination.startFromEntry = 0;
									  			$scope.collection.pagination.currentEntry = 0;
									  		}
											
																			  		  	
										$scope.premadeCollection.fetchMostRecent($scope.collection.pagination.morePerClick);
									  }									  			
									  // if multiple per page
									  if($scope.collection.pagination.form == "multip"){
									  		if ($scope.entryStatistics.total > $scope.collection.pagination.entriesOnPage){
									 			$scope.collection.pagination.startFromEntry = $scope.entryStatistics.total - $scope.collection.pagination.entriesOnPage;
									  			$scope.collection.pagination.currentEntry = $scope.entryStatistics.total - $scope.collection.pagination.entriesOnPage;
									  		}
									  		else{
									 			$scope.collection.pagination.startFromEntry = 0;
									  			$scope.collection.pagination.currentEntry = 0;
									  		}
									  		
									  		// start from lmost recent and go down
											$scope.premadeCollection.fetchNextPage(false, null);
									  }							
									  
								}
								
								
								if ($scope.collection.table != false){
									$timeout(function(){
										$scope.loadingEntries = true;
										
											// if search string is in the url
											var searchString = $stateParams.search; 
											if (searchString != null){
												
																							
												var key = searchString.substr(0, searchString.indexOf(':'));
												var value = searchString.split(':')[1];
													value = value.replace(/_/g, " ");
												var query = "SELECT * FROM " + $scope.collection.table +  " WHERE " + key + " = " + "\""+ value + "\"";
													
												
												 	$scope.premadeCollection.fetchEntries(query, null, function(){
													    SmoothScroll.$goTo(0);								// scroll back to top
													    $scope.loadingEntries = false;						// hide loading
													    //$scope.premadeCollection.isUsingAdvanced = true;   // reset advanced
													    //$scope.entryStatistics.hasMore = false;			   // hides more
												 	 });											
											}
											// no search string - continue with default loading
											else{																   
											  	loadDefault();										
											};
									});
								}
								else{
									 $scope.loadingEntries = false;
								} 
							},
							//-----------------
							
							//-----------------
							clearAdvancedSearch:function(){
								$location.url($location.path());
								$scope.collection.userEntries = [];
								$scope.premadeCollection.initialLoad();
							},
							//-----------------
							
							//-----------------						
							loadFullEntry:function(index){	
								listOfTemplates[index] = $scope.page.settings.fullLayout;
								
								// resize	
								$('.content-entry-container').css('height', 'auto');
																
								// equalize
								if($scope.collection.pagination.equalizeEntries){
									custom.equalize('.content-entry-container', 1000, 1);
								}
								
						    	// initiate slick
						    	$scope.premadeCollection.checkForSlick();									
																	
						
							},	
							//-----------------	
							
							
							//-----------------
							toggleSideButtons:function(item){
								
								$timeout(function(){
									
									
									if ($scope.page.sidebar == item){
										$scope.page.sidebar = "none";
									}
									else{
										$scope.page.sidebar = item;
									};
									
									
									
									if (item == "search"){
										$scope.premadeCollection.toggleSearch();
										$scope.premadeCollection.toggleSettings(false);
										$scope.premadeCollection.toggleLayout(false);
									}
									if (item == "advsearch"){
										$scope.premadeCollection.advancedSearch();
										
										$scope.premadeCollection.toggleSearch(false);
										$scope.premadeCollection.toggleSettings(false);
										$scope.premadeCollection.toggleLayout(false);										
									}
									if (item == "settings"){
										$scope.premadeCollection.toggleSearch(false);
										$scope.premadeCollection.toggleSettings();
										$scope.premadeCollection.toggleLayout(false);
									}
									if  (item == "layout"){
										$scope.premadeCollection.toggleSearch(false);
										$scope.premadeCollection.toggleSettings(false);
										$scope.premadeCollection.toggleLayout();
									}
									
																
								});
							},					
							//-----------------

							//-----------------
							toggleSearch:function(state){
								
								if (state == undefined){
									$scope.page.searchOpen = !$scope.page.searchOpen;
								}
								else{
									$scope.page.searchOpen = state;
								};
								
								theHeight = parseInt($('#nt-search-bar').height()) + parseInt($('#nt-search-bar').css('padding'))*2; 								
								if ($scope.page.searchOpen){															
									$('#nt-search-bar').transition( {y: theHeight  }, 500 );
								}
								else{
									$('#nt-search-bar').transition( {y: 0 },  500 );
								}
	
							},
							//-----------------		
							
							
							//-----------------		
							toggleSettings:function(state){
								
								if (state == undefined){
									$scope.page.editOpen = !$scope.page.editOpen;
								}
								else{
									$scope.page.editOpen = state;
								};


								if($scope.page.editOpen){
									$('#siteContent')
										.transition({ x: parseInt($('#nt-editor-sidebar').width()) }, 500, 'ease');;	
									$('#content-header')
										.transition({ x: parseInt($('#nt-editor-sidebar').width()) }, 500, 'ease');;						
									$('#content-footer')
										.transition({ x: parseInt($('#nt-editor-sidebar').width()) }, 500, 'ease');;	
									$('#nt-editor-sidebar')											
										.transition({ x: parseInt($('#nt-editor-sidebar').width()) }, 500, 'ease');;	
								}
								else{
									$('#siteContent')
										.transition({ x: 0 }, 500, 'ease');											
									$('#content-header')
										.transition({ x: 0 }, 500, 'ease');									
									$('#content-footer')
										.transition({ x: 0 }, 500, 'ease');	
									$('#nt-editor-sidebar')											
										.transition({   x: 0 }, 500, 'ease');																		
								}
								
							},	
							//-----------------		
							
							
							//-----------------		
							toggleLayout:function(state){		
								

									if (state == undefined){
										$scope.page.layoutOpen = !$scope.page.layoutOpen;
									}
									else{
										$scope.page.layoutOpen = state;
									};
								
									height = parseInt($('#nt-editor-bottom').height()) + parseInt($('#nt-editor-bottom').css('padding'))*2;									
									if($scope.page.layoutOpen){
										$('#nt-editor-bottom')
											.transition({   y: -(height) }, 500, 'ease');									
									}
									else{
										$('#nt-editor-bottom')
											.transition({   y: 0 }, 500, 'ease');
									};	
									

								
							},	
							//-----------------		
							
							
							//-----------------
							generalSearch:function(keyword){
								
								// disabled buttons
								$('#nt-keyword-search, #nt-keyword-search-btn').attr('disabled', true);
								
								var q = 'SELECT * FROM ' + $scope.collection.table + ' WHERE id LIKE "%' + keyword + '%"';
								// start on one; skip id
								for (i = 1; i < $scope.collection.searchFields.Field.length; i++){
									q += " OR " +  $scope.collection.searchFields.Field[i] + " LIKE '%" + keyword + "%' "; 
								};
								
								var packet = {
									database: $scope.masterData.system.usingdb, 
									query: q
								};																
								phpJS.queryDatabase(packet, function(state, data){
									
									if(data.status == "error"){										
										$timeout(function(){
											// enable buttons
											$('#nt-keyword-search, #nt-keyword-search-btn').attr('disabled', false);
											toaster.pop('error', "", "No search results found.");
										});			
									}else{
			  			  				var idString = "";
						  			  	for (i = 0; i < data.length; i++){
						  			  		if (i != 0){ idString += ", "; }
						  			  		idString += data[i].id;
						  			  	}			
						  			  	if (data.length > 0){				
						  			  		$scope.entryStatistics.total = data.length; 		
											$scope.collection.searchWhere = " WHERE id IN(" + idString + ") ";
											$scope.premadeCollection.initialLoad();
										}	
										$('#nt-keyword-search, #nt-keyword-search-btn').attr('disabled', false);
										toaster.pop('success', "", "Found " + data.length + " entries.  Pulling them up now.");
									}
									
								});		
														
							},
							
							clearSearch:function(){
								
								// disabled buttons
								$('#nt-keyword-search-clear').attr('disabled', true);								
								
								var packetData = {	
									database: $scope.masterData.system.usingdb,
									table: $scope.collection.table
								};								
								
								$scope.callCore.callController({who: "master", action:"phpRunQueryForTotalEntry", data: packetData}, function(d){
									$timeout(function(){
										$scope.entryStatistics.total = d.packet.data.rows;								
										$scope.collection.searchWhere = '';
										$scope.premadeCollection.initialLoad();
										// disabled buttons
										$('#nt-keyword-search-clear').attr('disabled', false);		
									});								
								});
							},
							//-----------------
							
							//-----------------
							setFilter:function(){
								var packet = {
									objToPaginate: $scope.collection.userEntries,
									filterBy: $scope.collection.pagination.filterBy,
									reverse: $scope.collection.pagination.reverse,
									filterSize: 100,
								};									
								custom.sortAndPage( packet );							
							},
							//-----------------
							
							//-----------------
							saveSearch:function(){
								if ($scope.collection.remember.useLocalStorage){
									toaster.pop('success', "", "Save search results.");
						    		// build remember string
						    		var rememberArray = custom.returnPropertyInObject($scope.collection.allEntries, [$scope.collection.remember.rememberWith])[$scope.collection.remember.rememberWith],
						    			rememberString = '';									
									for (i = 0; i < rememberArray.length; i++){
										if (i != 0){ rememberString += ", "; }
										rememberString += rememberArray[i]; 
									};								
									$scope.$storage.rememberMe = {										
										key: $scope.collection.remember.rememberWith,
										value: rememberString			    
									};
   								}
							},		
							//-----------------
							
							
							//-----------------
							loadPriorSearch:function(){
								  if ($scope.$storage.rememberMe != undefined){		
									  	$scope.collection.remember.hasStorage = true;										

										var packet = {
											header: "Load saved entries?",
											field1: "Yes", 
											field2: "Cancel"
										};	
										$scope.callCore.callController({who: "master", action: "confirmBox", data: packet}, function(d){ 
											
											if (d.packet.status != 'dismiss'){
												if (d.packet.data){	
											  	  var query = 'SELECT * FROM ' + $scope.collection.table + ' WHERE ' + $scope.$storage.rememberMe.key + ' IN(' + $scope.$storage.rememberMe.value + ')';
											  	  $scope.loadingEntries = true;									// hide loading
											  	  $scope.collection.userEntries = [];
												  $scope.premadeCollection.fetchEntries(query, null, function(){
														 SmoothScroll.$goTo(0);								// scroll back to top
														 $scope.loadingEntries = false;						// hide loading
														 $scope.premadeCollection.isUsingAdvanced = true;   // reset advanced
														 $scope.entryStatistics.hasMore = false;			   // hides more
												  });	
												}		
											}
										});		
									}							
								
							},
							//-----------------
							
							//-----------------
							fetchMostRecent:function(limitTo){
								$scope.loadingEntries = true;
								$timeout(function(){								  
									  var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id " + $scope.collection.pagination.sortBy + " LIMIT " + $scope.collection.pagination.currentEntry + ", " + limitTo;
									  $scope.collection.allEntries = [];  
									  $scope.premadeCollection.fetchEntries(query, null, function(){
									     //SmoothScroll.$goTo(0);
										 $scope.loadingEntries = false;						// hide loading
										 $scope.premadeCollection.isUsingAdvanced = false;   // reset advanced
										 $scope.entryStatistics.hasMore = true;			   // hides more									    
									  });
								});
								
							},
							//-----------------
							
							//-----------------		
							fetchFirst:function($event){
								if ($scope.collection.pagination.currentEntry > 0){									
									var btn = $event.target;
										btnText = $(btn).text(),
								 		btnRaw = $(btn).html();	
										
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
										$scope.collection.pagination.currentEntry = 0;
										
	  		 						 	// transition fx
	  								 	$scope.transitionFX.using = "nextFX";										
										
										var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id " + $scope.collection.pagination.sortBy + " LIMIT 0, 1";  
										$scope.premadeCollection.fetchEntries(query, null, function(){
											$(btn).html(btnRaw).attr('disabled', false);  												
										});	
									
								};	
							},	
							//-----------------		
							
							//-----------------		
							fetchRecent:function($event){
								
								if ( ($scope.collection.pagination.currentEntry + 1) < $scope.entryStatistics.total){									
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
										
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
										$scope.collection.pagination.currentEntry = ($scope.entryStatistics.total - 1);
										
	  		 						 	// transition fx
	  								 	$scope.transitionFX.using = "nextFX";										
										
										var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT 0, 1";  
										$scope.premadeCollection.fetchEntries(query, null, function(){
											$(btn).html(btnRaw).attr('disabled', false);  												
										});	
									
								};	
							},	
							//-----------------																
							
							
							//-----------------		
							fetchPrevPage:function(asBtn, $event){
								
							  	if (asBtn){	
							  		// now correct if 
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();											
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);															  		 
						  			
						  		}								
								
						  		// first check if more entries than entries per page						  		
						  		if ($scope.collection.pagination.currentEntry - $scope.collection.pagination.entriesOnPage > 0){						  			
						  		 	startAt = $scope.collection.pagination.currentEntry - $scope.collection.pagination.entriesOnPage;
						  		 	numberOfEntries = $scope.collection.pagination.entriesOnPage;
						  		 	$scope.collection.pagination.currentEntry = startAt; 						  		   						  		 	
						  		}
						  		
						  		else{							  						
						  		 	startAt = 0;
						  		 	numberOfEntries = $scope.entryStatistics.total - ($scope.entryStatistics.total - $scope.collection.pagination.currentEntry);
						  		 	$scope.collection.pagination.currentEntry = numberOfEntries; 
																		  		 	
						  		}

						  		
  		 						// transition fx
  								$scope.transitionFX.using = "backFX";
								var query = "SELECT * FROM " + $scope.collection.table +  $scope.collection.searchWhere + " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + startAt + ", " + numberOfEntries;   						
								
								$scope.premadeCollection.fetchEntries(query, null, function(){
										if (asBtn){$(btn).html(btnRaw).attr('disabled', false);};
										//SmoothScroll.$goTo(0);								// scroll back to top
										$scope.loadingEntries = false;						// hide loading
										$scope.premadeCollection.isUsingAdvanced = false;   // reset advanced
										$scope.entryStatistics.hasMore = true;			   // hides more											 
								});	
								
								
								  
							},	
							//-----------------	
							
							//-----------------		
							fetchNextPage:function(asBtn, $event){
								
								if (asBtn){								
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);									
								}
								
								
								if ($scope.collection.pagination.currentEntry + $scope.collection.pagination.entriesOnPage < $scope.entryStatistics.total){																																	
									 	startAt = $scope.collection.pagination.currentEntry;									 	
										numberOfEntries = $scope.collection.pagination.entriesOnPage;
									 	$scope.collection.pagination.currentEntry = startAt + $scope.collection.pagination.entriesOnPage; 										 								 
								}
								else{																												
									 	startAt = $scope.collection.pagination.currentEntry;									 	
										numberOfEntries = $scope.collection.pagination.entriesOnPage;
									 	$scope.collection.pagination.currentEntry = startAt; 										 	
								}
								
								// unique for onload 									
								if (!asBtn){
									 	startAt = $scope.entryStatistics.total - $scope.collection.pagination.entriesOnPage;
									 	if ($scope.collection.pagination.entriesOnPage > $scope.entryStatistics.total){
									 		startAt = 0;
									 	}
										numberOfEntries = $scope.collection.pagination.entriesOnPage;										
									 	$scope.collection.pagination.currentEntry = startAt; 	
								}
								
							
  		 						// transition fx
  								$scope.transitionFX.using = "nextFX";  								 
								var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere + " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + startAt + ", " + numberOfEntries;   									
								
								
								$scope.premadeCollection.fetchEntries(query, null, function(){
											if (asBtn){$(btn).html(btnRaw).attr('disabled', false);};  
											$scope.loadingEntries = false;						// hide loading
											$scope.premadeCollection.isUsingAdvanced = false;   // reset advanced
											$scope.entryStatistics.hasMore = true;			   // hides more											 
								});	
								
							},	
							//-----------------								
														
							
							//-----------------
							fetchEntry:function(whereStatement, insertInto, callback){
								$scope.loadingEntries = true;
								$timeout(function(){
								  // pull the most recent entry, but only one
								  var query = "SELECT * FROM " + $scope.collection.table + " " + whereStatement;  
								  $scope.premadeCollection.fetchEntries(query, insertInto, function(){								   
								   	$scope.loadingEntries = false;
								   	callback();
								  });
								});
							},
							//-----------------		
							
							//-----------------		
							fetchNext:function($event){
																
								if ($scope.collection.pagination.currentEntry + 1 < $scope.entryStatistics.total){									
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
										
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
										$scope.collection.pagination.currentEntry ++;
										
	  		 						 	// transition fx
	  								 	$scope.transitionFX.using = "nextFX";										
										
										var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + $scope.collection.pagination.currentEntry + ", " + 1;  
										$scope.premadeCollection.fetchEntries(query, null, function(){
											$(btn).html(btnRaw).attr('disabled', false);  												
										});	
									
								};									
								
							},	
							//-----------------	
							

							//-----------------	
							fetchPrev:function($event){
								
								if ($scope.collection.pagination.currentEntry - 1 >= 0){
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
											
										$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
										$scope.collection.pagination.currentEntry --;	
										
	  		 						 	// transition fx
	  								 	$scope.transitionFX.using = "backFX";										
										
										var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + $scope.collection.pagination.currentEntry + ", " + 1;  
										$scope.premadeCollection.fetchEntries(query, null, function(){
											$(btn).html(btnRaw).attr('disabled', false);  
											//SmoothScroll.$goTo(0);										
										});	
								};
								
							},	
							//-----------------	
							
							//-----------------		
							fetchRandom:function($event){
								
								$scope.collection.pagination.currentEntry = custom.getRandomNumber(0, ($scope.entryStatistics.total -1) );		
																
								var btn = $event.target;
									btnText = $(btn).text(),
									btnRaw = $(btn).html();	
									
									$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
									
  		 						 	// transition fx
  								 	$scope.transitionFX.using = "nextFX";									
									
									var query = "SELECT * FROM " + $scope.collection.table +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " + $scope.collection.pagination.currentEntry + ", " + 1;  
									$scope.premadeCollection.fetchEntries(query, null, function(){
										$(btn).html(btnRaw).attr('disabled', false);  											
									});	
								
							},	
							//-----------------											


							//-----------------			
							fetchAuto:function(){
								
								if ($scope.collection.userEntries.length < $scope.entryStatistics.total){
									if (!$scope.collection.isAutoDisabled && $scope.collection.pagination.form == "multi_auto"){
										$scope.collection.isAutoDisabled = true;
										$scope.premadeCollection.fetchMore();
									};
								}
								
							},
							//-----------------			

							//-----------------
							fetchMore:function($event){
								
								// turn into thinking
								if ($event != undefined){
									var btn = $event.target;
										btnText = $(btn).text(),
										btnRaw = $(btn).html();	
										
									$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);	
								}							
								
								
								var checkDiff = $scope.entryStatistics.total - $scope.collection.userEntries.length; 

				 
						  		if (checkDiff > $scope.collection.pagination.morePerClick){
						  		 	$scope.collection.pagination.currentEntry -= $scope.collection.pagination.morePerClick;
						  		 	if ($scope.collection.pagination.currentEntry <= 0){ 
						  		 		$scope.entryStatistics.hasMore = false;
						  		 	}
						  		 	var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " +  $scope.collection.pagination.currentEntry  + ", " + $scope.collection.pagination.morePerClick;
						  		}
						  		else{								  		 					  		 
						  		 	var diff = $scope.entryStatistics.total - $scope.collection.userEntries.length;						  		 						  		 							  		 
						  		 	var query = "SELECT * FROM " + $scope.collection.table + $scope.collection.searchWhere +  " ORDER BY id "+ $scope.collection.pagination.sortBy +" LIMIT " +  0  + ", " + diff;						  		 	
						  		}	

								

	 						 	// transition fx
							 	$scope.transitionFX.using = "nextFX";						  		 						
								$scope.premadeCollection.fetchEntries(query, null, function(){
									if ($event != undefined){
										$(btn).html(btnRaw).attr('disabled', false);  
									};
								});
								
								
							},
							//-----------------
	
							//-----------------
							fetchEntries:function(query, insertInto, callback){	
									
								
									var queryString = {	
										query: query, 
										database: $scope.masterData.system.usingdb
									};
								
									$scope.callCore.callController({who: "master", action: "phpRunQuery",  data: queryString}, function(d){ 																

											
											// sanitize html types that use html entities (adv)
											var htmlEntities = [];
											var allInputTypes = $scope.collection.searchFields.inputType;
											var allFields = $scope.collection.searchFields.Field;
											for (var i = 0; i < allInputTypes.length; i++){
												var inputType = allInputTypes[i]; 
												if (inputType == 'adv' || inputType == 'imageUpload'){
													htmlEntities.push({data: allFields[i], type: inputType});
												}
											}													

											processed = 0;
											var entryPacket = d.packet.data; 
											function loopCycle(){
												
												//  cycle												
												if (processed < entryPacket.length){
			
														var subProcess = 0;	
														function subloopCycle(){
																												
															if (subProcess < htmlEntities.length){																	
																filterField = [htmlEntities[subProcess].data];
																filterType = htmlEntities[subProcess].type;
																
																
																filterMe = entryPacket[processed][filterField];
																phpJS.dencodeHtmlEntities(filterMe , function(state, data){																	
												    				escapedStrings = data.replace(/["']/g, "");
												    				
												    				// parse json string into object
												    				if (filterType == 'imageUpload'){
												    					if (data != null && data != undefined && data != ''){
												    						entryPacket[processed][filterField] = JSON.parse(data);
												    					}
												    					else{
												    						entryPacket[processed][filterField] = data; 
												    					}
												    				}
												    				// html text
												    				else{
												    					entryPacket[processed][filterField] = data; 
												    				}
												    				
																	subProcess++;
																	subloopCycle();													    				
												    			});																															
															}
															else{
																subloopComplete();
															}
														};
														subloopCycle();
														
														function subloopComplete(){
															processed++;															
															loopCycle();
														}								
												}	
												//  loop complete
												else{	
													// if inserting into an existing array location
													if (insertInto != null){				
														$timeout(function(){																							
															$scope.collection.userEntries[insertInto] = entryPacket[0];
															$scope.premadeCollection.initiateFX(true);													
														});
														callback();	
													}
													// normal behavior
													else{																								
														$scope.premadeCollection.arrangeEntries(d.packet.data, callback);	
													}
												}	
												
											};
											loopCycle();	
										
									});	
							},	
							//-----------------		
							
							//-----------------
							filterCorrection:function(){
								$timeout(function(){
									$('.content-entry-container').css('opacity', 1);
								}, 0);
							},
							
							initiateFX:function(reset){
										if ($scope.collection.pagination.form == "multi" || $scope.collection.pagination.form == "multi_auto"){		
											if (reset){startPoint = 0}									
											$timeout(function(){$scope.premadeCollection.returnFX(startPoint);}, 0);
										}
										if ($scope.collection.pagination.form == "single" || $scope.collection.pagination.form == "multip"){  // single entry or multi page
											$timeout(function(){$scope.premadeCollection.returnFX(0);}, 0);
										}								
							},
							
							backFX:function(){
								if ($scope.transitionFX.enabled){
									$scope.transitionFX.using = "backFX";
	  								$('.content-entry-container').each(function(index){
	  								 	$(this).transition( $scope.transitionFX.backFX ,{delay: index * ($scope.transitionFX.duration/2) }, $scope.transitionFX.duration);
	  								});								
								}
								if($scope.collection.pagination.equalizeEntries){
									custom.equalize('.content-entry-container', 500, 3);
								}
							},	
														
							nextFX:function(){								
								if ($scope.transitionFX.enabled){
									$scope.transitionFX.using = "nextFX";
	  								$('.content-entry-container').each(function(index){	  		  		  																					
	  								 	$(this).transition( $scope.transitionFX.nextFX ,{delay: index * ($scope.transitionFX.duration/2) }, $scope.transitionFX.duration);
	  								});								
								}
								if($scope.collection.pagination.equalizeEntries){
									custom.equalize('.content-entry-container', 500, 3);
								}
							},	
							
							returnFX:function(threshold){		
								
								
								if ($scope.transitionFX.enabled){
	  								$('.content-entry-container').each(function(index){
	  									 
	  									 delaySpd = (index - $scope.entryStatistics.startAt) * ($scope.transitionFX.duration/2); 
	  									
	  								 	 if (index >= threshold && delaySpd >= 0){
	  								 	 	
	  								 	 	
		  								 	 $(this).transition( $scope.transitionFX[$scope.transitionFX.using] ,{delay: delaySpd }, $scope.transitionFX.duration)
		  								 	 		.transition({ 	x: 0, 
		  								 							y: 0, 
		  								 							scale: 1, 
		  								 							opacity: 1,		  								 							
																	skewX: '0deg',
																	skewY: '0deg',
		  								 							delay: ($scope.transitionFX.duration/2) }, $scope.transitionFX.duration);
	  								 	}
	  								});	

  								};
								if($scope.collection.pagination.equalizeEntries){
									custom.equalize('.content-entry-container', 500, 3);
								}		
							},
							//-----------------		
							
							//-----------------
							arrangeEntries:function(entryPackages, callback){
								
								var transitionStartTime = null;
									
  		 						// transition fx
  		 						if ($scope.collection.pagination.form == "single" || $scope.collection.pagination.form == "multip"){
  										$scope.premadeCollection[$scope.transitionFX.using]();
  										transitionStartTime = entryPackages.length * $scope.transitionFX.duration; 
  								}

								// for multi, lets animation pickup where it started instead of having a large delay
								if ($scope.collection.pagination.form == "multi" || $scope.collection.pagination.form == "multi_auto" ){  // growing
										$scope.entryStatistics.startAt = $scope.collection.userEntries.length;
										transitionStartTime = 0; 
								};
								
								// rush transition exits  								
						  		if($scope.transitionFX.rush){
						  			transitionStartTime = 0; 
						  		};	 						
  		 						
  		 						
  		 						$timeout(function(){	

										// push data into entries;																
										$scope.collection.allEntries = [];																	
										for (i = 0; i < entryPackages.length; i++){
											$scope.collection.allEntries.push(entryPackages[i]);
										};	
										$scope.collection.allEntries.reverse();
										
										
										// sort entries into userEntires object;												
										startPoint = $scope.collection.userEntries.length; 
										for (i = 0; i < $scope.collection.allEntries.length; i++){
											if ($scope.collection.pagination.form == "multi" || $scope.collection.pagination.form == "multi_auto"){  // growing
												$scope.collection.userEntries.push($scope.collection.allEntries[i]);												
											};
											if ($scope.collection.pagination.form == "single" || $scope.collection.pagination.form == "multip"){  // single entry or multi page
												$scope.collection.userEntries[i] = $scope.collection.allEntries[i];	
											};																									
										}
										
										
										// remove unused entries for single or multip										
										if ($scope.collection.pagination.form == "single" || $scope.collection.pagination.form == "multip"){ 
											if ($scope.collection.userEntries.length > entryPackages.length){
												deleteSize = $scope.collection.userEntries.length; 								
												for (i = entryPackages.length; i < deleteSize; i++){											
													$scope.collection.userEntries.splice(entryPackages.length, 1);
												}
											}
										}
										
										
										
										// multi					      returns true/false
										$scope.entryStatistics.hasMore  = !($scope.collection.userEntries.length >= $scope.entryStatistics.total);
										
										// single page 
										$scope.entryStatistics.hasFirst = (!$scope.collection.pagination.currentEntry == $scope.collection.userEntries.length);											
										$scope.entryStatistics.hasPrev  = (!$scope.collection.pagination.currentEntry > 0);											
										$scope.entryStatistics.hasNext  = (!(($scope.collection.pagination.currentEntry + 1) < $scope.entryStatistics.total));
										$scope.entryStatistics.hasLast  = (($scope.collection.pagination.currentEntry + 1) == $scope.entryStatistics.total);
										
										// multip entries
										//$scope.entryStatistics.hasLessPage = (($scope.collection.pagination.currentEntry < $scope.collection.pagination.entriesOnPage));
										//$scope.entryStatistics.hasMorePage = (($scope.collection.pagination.currentEntry + $scope.collection.pagination.entriesOnPage) >= $scope.entryStatistics.total);
									
										
										// ONLY RUNS ONCE - SHOWS BUTTONS
										$scope.premadeCollection.initiateFX(false);	
												
								    	// initiate slick
								    	$scope.premadeCollection.checkForSlick();
										
										// REENALBE AUTO FETCH
										$scope.collection.isAutoDisabled = false;
											
										if (callback != null || callback != undefined){
											callback();
										}
										
											
								}, transitionStartTime);									
											
									
							},
							//-----------------
							
							//-----------------
							checkForSlick:function(){
						    	// slick behaviors
						    	$timeout(function(){
							    	custom.initSlick();						    	
								}, 500);								
							},
							//-----------------
							
							//-----------------
							advancedSearch:function(){
								
								
								packet = {
									isSearchable: $scope.collection.searchFields.Comment,
									fields: $scope.collection.searchFields.Field,
									types: $scope.collection.searchFields.Type,
									objToQuery: $scope.collection.allEntries,
									database: $scope.masterData.system.usingdb,
									table: $scope.collection.table
								};
								
								$scope.callCore.callController({who: "master", action: "advSearchModal", data: packet}, function(d){ 									
									if (d.packet.status  != 'dimiss'){
										if (d.packet.data.length > 0){
											$scope.entryStatistics.total = d.packet.data.length; 
											$scope.collection.searchWhere = " WHERE id IN(" + d.packet.data.returnData + ") ";
											$scope.premadeCollection.initialLoad();
										}
									}
									$scope.page.sidebar = "none";
									
								});
							},
							//-----------------

							//-----------------
							// addImagePacket = {
							//   allImages: [{name: pageName, src: data64String, thumbnail: data64String}]	
							//   newImages: [{name: pageName, src: data64String, thumbnail: data64String}]
							//   uploadId: 10
							// } 
							addImagesTo:function(addImagePacket, callback){
											
											
											
											// declare variables
											var _all = addImagePacket.allImages,
												_added = addImagePacket.newImages,
												_id = addImagePacket.uploadId,
												_next = function(){ count++; loop(); }, 
												count = 0;
											
											
														
											// loop function								
											function loop(){

												//  loop conditions
												if (count < _added.length){
													
													delete _added[count].parse;   // remove property so it doesn't get uploaded again
													
													// upload full size image
													function uploadFullSize(){
														var imageSrc = _added[count].src;
														var packet = {
															file: imageSrc,
															name:  _added[count].name,
															location: $scope.collection.imageStorage + _id
														};	
														
														phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
															if (state){
																$timeout(function(){
																	_added[count].src = data.src;
																	uploadThumbnail();
																});			
															}																						
														});																								
		
													}
													// upload thumbnail size image
													function uploadThumbnail(){
														var imageSrc = _added[count].thumbnail;
														var packet = {
															file: imageSrc,
															name: _added[count].name + "_thumbnail",
															location: $scope.collection.imageStorage + _id
														};
														
														phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
															if (state){
																$timeout(function(){
																	_added[count].thumbnail = data.src;
																	_next()
																});			
															}																						
														});																																													
													}																
													
													// loop logic 
													if (_added.length > 0){
														uploadFullSize();
													}
													// loop complete
													else{
														complete()
													}
													
												}
												
												// end loop conditions
												else{																						
													complete();
												}
											}
											// end loop function
											
											// complete function
											function complete(){
												_all = JSON.stringify(_all);	
												callback(_all);	
											}
											
											// start loop
											loop();	

							},
							//-----------------

							//-----------------
							editEntry:function($event, id, index){
									
									$scope.premadeCollection.buttonThink( $($event.target) );
								
									// grab entry information
									var queryString = {	
										query: "SELECT * FROM " + $scope.collection.table + " WHERE id=" + id, 
										database: $scope.masterData.system.usingdb
									};
									$scope.callCore.callController({who: "master", action: "phpRunQuery",  data: queryString}, function(d){ 											
											var editData = d.packet.data[0];		
											var packetData = {	
												database: $scope.masterData.system.usingdb,
												table: $scope.collection.table
											};
											// call the create module
											$scope.callCore.callController({who: "master", action: "phpGetTableDetails",  button: $($event.target), data: packetData}, function(d){ 	
												if (d.packet.status == 'success'){	
																				
																															
																				
													var tableFields = {
														form: d.packet.data.formData,
														meta: d.packet.data.metaData,
														editData: editData
													};
													// post modal
													$scope.callCore.callController({who: "master", action: "postModal", data: tableFields}, function(d){ 
														if (d != undefined && d.packet.status == "closed"){
		     												
		     												
		     												
														    // build update string
														    var buildString = "",
														    	loopCount = 0;
														    	
														    function addToString(fieldLabel, entry){
														    	buildString += fieldLabel + "='" + entry + "', ";
																loopCount++;
																loopCycle();
														    };	
														    
														    function loopCycle(){
															   	
															   	if (loopCount < tableFields.form.length){
															   		
															   		
															    	var fieldLabel = tableFields.form[loopCount].Field;
															    	var newEntry = d.packet.data.returnData[loopCount];
															    	var inputType = tableFields.form[loopCount].inputType;
															    	
															    	if (newEntry != undefined && newEntry != null && newEntry != "Invalid Date"){			
															    																	    		
															    		// sanitize advanced texts
															    		if(inputType == 'adv'){															    		
															    			phpJS.encodeHtmlEntities(newEntry, function(state, data){
															    				addToString(fieldLabel, data);
															    			});
															    		}
															    		
															    		// image uploads
															    		else if(inputType == 'imageUpload'){		
																				
																				// delete unused function
																				function deleteUnusedImages(){
																					var packetData = {
																						location: $scope.collection.imageStorage + id
																					};
																					
																					$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){
																						$timeout(function(){

																							// parse information
																							if (d.packet.data.files != null && d.packet.data.files != null){
																								
																								
																								var filesInDirectory = [];
																								for (var i = 0; i < d.packet.data.files.length; i++){
																									var f = (d.packet.data.files[i]).replace(/\.[^/.]+$/, "");
																									if (!/thumbnail/i.test(f)){  // exclude thumbnails names
																										filesInDirectory.push( f );
																									}
																								};
				
																								// delete files no longer in use
																								var currentFiles = [];
																								for (var i = 0; i < newEntry.length; i++){
																									var f = newEntry[i].name;
																									currentFiles.push( f );
																								}																						
																								
																								// get all files to delete
																								var toBeDeleted = custom.compareArrayForDifferences(filesInDirectory, currentFiles);
		
																								// get image and thumbnail
																								var packet = [];
																								for (var i = 0; i < toBeDeleted.length; i++){
																									packet.push({folder: "../" +  $scope.collection.imageStorage + id, file: toBeDeleted[i] + ".png"});
																									packet.push({folder: "../" +  $scope.collection.imageStorage + id, file: toBeDeleted[i] + "_thumbnail.png"});
																								}
																							
																								// delete files from																						
																								if( packet.length > 0 ){
																									$scope.callCore.callController({who: "master", action: "phpDeleteFiles", data: packet}, function(_d){																																																																																					
																										addNewImages();																																														
																									});
																								}
																								else{
																									addNewImages();
																								}
																								
																								
																							}
																							// nothing to parse, skip to add
																							else{																							
																								addNewImages();
																							}
																					
																						});
																					
																					});			
																				};
																				// end delete image function
																				
																				// add image functions		
																				function addNewImages(){
																		
																					// get files to add
																					var addList = [];
																					for (var i = 0; i < newEntry.length; i++){
																						if (newEntry[i].parse){
																							addList.push( newEntry[i] );
																						}
																					}	
																					
																					var addImagePacket = {
																						allImages: newEntry,
																						newImages: addList,
																						uploadId: id
																					};
																					
																					$scope.premadeCollection.addImagesTo(addImagePacket, function(data){
																						phpJS.encodeHtmlEntities(data, function(state, _data){
																							addToString(fieldLabel, _data);
										    											});		
																						
																						
																					});

																				};	
																				// end add image function		
			
																				// start
																				deleteUnusedImages();
															    			
															    		}															    		
															    		else if(inputType == 'date'){
															    		  	newEntry = newEntry.replace(/\D/g,'');
															    		  	addToString(fieldLabel, newEntry);
															    		}
															    		else if(inputType == 'time'){
															    		  	newEntry = moment(newEntry).format( "H:mm:ss");
															    		  	addToString(fieldLabel, newEntry);
															    		}
															    		else{
																    		addToString(fieldLabel, newEntry);
																    	}	
																    	// end sanitized edits before saving
																    	
															    	}
															    	else{
																	    loopCount++;
																	    loopCycle();															    		
															    	}
															    	
															    }
															    else{
															    	loopComplete();
															    }
															   
														    }
														    
														    
														    
														   function loopComplete(){
														  		
														   		buildString = buildString.replace(/,\s*$/, "");
														   		
															    // edit update
															    var packetData = {
															   		query: "UPDATE " + $scope.collection.table + " SET " + buildString + " WHERE id=" + id,
															   		database: $scope.masterData.system.usingdb
															    };	
															    // update edit!																      		
																$scope.callCore.callController({who: "master", action: "phpModifyEntry", button: $($event.target),  data: packetData}, function(d){ 
																	var whereStatement = 'WHERE id=' + id;
																	
																	$scope.premadeCollection.fetchEntry(whereStatement, index, function(){
									  										
									  																			//
																		$scope.premadeCollection.filterCorrection();																		
																		if ($scope.collection.pagination.form == "multi" || $scope.collection.pagination.form == "multi_auto"){
																			//location.reload();	
																		}
																		else{
																			toaster.pop('success', "Your post has been edited successfully.", "");	
																		}
																		
																	});
																});
																
																
															
															}
															// end edit
															
															// start loop
															loopCycle();
														}
													});	
													// /end post modal													
												}	
											});	
											// end call create module															
									});	
									// end grab edit informatoin
									
							},
							//-----------------	

							//-----------------
							deleteEntry:function(id, index){
									var packet = {
										header: "Delete this entry?",
										field1: "Yes", 
										field2: "No"
									};
								
									// confirm box
									$scope.callCore.callController({who: "master", action: "confirmBox", data: packet}, function(d){ 
										if (d.packet.data == true){		
												toaster.pop('warning', "", "Deleting...");
												// delete from database
												var queryString = {	
													query: "DELETE FROM " + $scope.collection.table + " WHERE id=" + id, 
													database: $scope.masterData.system.usingdb
												};
												// delete all information
												$scope.callCore.callController({who: "master", action: "phpModifyEntry",  data: queryString}, function(d){ 
													
													// delete all images from folder
													$scope.premadeCollection.deleteFolder($scope.collection.imageStorage + id, function(){
														$timeout(function(){
															$scope.collection.userEntries.splice(index, 1);
															$scope.collection.allEntries.splice(index, 1);
															$scope.entryStatistics.total --;
															toaster.pop('success', "", "Your post has been deleted successfully.");
														});		
													});					
												});	
												// end delete
										}				
									});	
									// end confirm								
							},
							//-----------------
							
							//-----------------
							deleteFolder:function(folder, callback){				
							   	
							    	var packetData = {	
										folder: "../" + folder
									};

											
									$scope.callCore.callController({who: "master", action: "phpDeleteFolder", data: packetData}, function(d){
										 															
										callback(d);
									});								
								
							},
							//-----------------
							
							//-----------------
							backToTop:function(){
									// go to top
							    	//SmoothScroll.$goTo( $('#contentAnchor') );	
							},
							//-----------------
				
							
							//-----------------
							paginateEntries:function(type){					

									/*
									if (type == "first"){
										$scope.collection.pagination.current = 1; 
									}
									if (type == "last"){
										$scope.collection.pagination.current = $scope.collection.pagination.max;
									}
									if (type == "add"){
										if ($scope.collection.pagination.current + 1 <= $scope.collection.pagination.max){
											$scope.collection.pagination.current++; 
										}
									}
									if (type == "sub"){
										if ($scope.collection.pagination.current - 1 > 0){
											$scope.collection.pagination.current--; 
										}
									}										
									
									var packet = {
										objToPaginate: $scope.collection.allEntries,
										filterBy: $scope.collection.pagination.filterBy,
										reverse: $scope.collection.pagination.reverse,
										filterSize: $scope.collection.pagination.limit,
									};
								

									var _results =  custom.sortAndPage( packet );
										// apply to page
									$timeout(function(){
										// content for the main page
										$scope.collection.userEntries = _results.page( $scope.collection.pagination.current );;

										// get pagination data
							    		$scope.collection.pagination.max = _results.totalPages;						    		
							    		$scope.collection.pagination.hasNext = _results.hasNext();
							    		$scope.collection.pagination.hasPrev = _results.hasPrev();
							    		
							    		if ($scope.collection.pagination.max == $scope.collection.pagination.current ){
							    			$scope.collection.pagination.isLast = true;
							    		}else{
							    			$scope.collection.pagination.isLast = false;
							    		}
							    		if (_results.currentPage == 1 ){
							    			$scope.collection.pagination.isFirst = true;
							    		}else{
							    			$scope.collection.pagination.isFirst = false;
							    		}	
							    	});		
							    	*/	
									
								
							},							
							//-----------------				

							//-----------------
							openNewEntry:function($event, tableName){
								$scope.premadeCollection.buttonThink( $($event.target) );
												
								var packetData = {	
									database: $scope.masterData.system.usingdb, 
									table: $scope.collection.table
								};
								$scope.callCore.callController({who: "master", action: "phpGetTableDetails",  button: $($event.target), data: packetData}, function(d){ 	
									if (d.packet.status == 'success'){								
										var tableFields = {
											form: d.packet.data.formData,
											meta: d.packet.data.metaData,
											editData: null
										};
										$scope.callCore.callController({who: "master", action: "postModal", data: tableFields}, function(d){ 
												if(d.packet.status == "closed"){
													var packetData = {
											 			database: $scope.masterData.system.usingdb,
											 			table: $scope.collection.table,
											 			fields: tableFields.form, 
											 			inputData: d.packet.data.returnData 									 			
													};											
													$scope.premadeCollection.createNewEntry(packetData);
												}
										});														
									}	
								});	
							},
							//-----------------
							
							//-----------------
							createNewEntry:function(packetData){
								
								$timeout(function(){
									toaster.pop('info', "", "Submitting your entry.");
								});
								var cloneArray = $.extend(true, [], packetData.inputData);

								// GET NEXT IN ID
								var queryString = {	
									table: $scope.collection.table, 
									database: $scope.masterData.system.usingdb
								};
							
								$scope.callCore.callController({who: "master", action: "phpCreateBlankEntry",  data: queryString}, function(d){ 
										
										var nextId = d.packet.data.id;

										// SANITIZE ENTRIES OF NEW POSTS
										var counter = 0; 
										var _next = function(){
											counter++;
											setLoop();
										};								
										function setLoop(){
											
											
											if (counter < packetData.fields.length){
													var type = packetData.fields[counter].Type,
														inputType = packetData.fields[counter].inputType;
												
													if (type == "time"){
														cloneArray[counter] = moment(packetData.inputData[counter]).format( "H:mm:ss");
														_next();											
													}
													else if (inputType == "imageUpload"){
														var index = counter; 
														var subCounter = 0; 
													
														var addImagePacket = {
															allImages: packetData.inputData[index],
															newImages: packetData.inputData[index],
															uploadId: nextId
														};
														
														$scope.premadeCollection.addImagesTo(addImagePacket, function(data){															
															cloneArray[index]  = data; 
															_next();
														});
												
													}
													else{
														_next();
													}													
		
											}
											else{
													imageloopComplete();
											}
											counter++;
										}								
										setLoop();  // start loop
										
										
										function imageloopComplete(){
											
											// clone array has been formmated correctly
											packetData.inputData = cloneArray; 
											
											// sanitize and ready edit statement
 											custom.sanitizePacketsForDatabase(packetData, function(sanitizedData){	
											    // edit update
											    var queryPacket = {
											   		query: custom.returnEditString($scope.collection.table, sanitizedData, nextId),
											   		database: $scope.masterData.system.usingdb
											    };								     		
										    
											    // update newly created entry!													    		
												$scope.callCore.callController({who: "master", action: "phpModifyEntry",  data: queryPacket}, function(d){ 
													if (d.packet.status == "success"){			
																												
														
														/*
														if ($scope.collection.pagination.form == "multip" || $scope.collection.pagination.form == "multip"){ 
															var insertInto = null;															
														}
														if ($scope.collection.pagination.form == "single"){ 
															
														}														
														
														$scope.entryStatistics.total++;
														$scope.premadeCollection.initialLoad();
														var whereStatement = 'WHERE id IN(' + nextId + ')';			
														$scope.premadeCollection.fetchEntry(whereStatement, insertInto, function(){															
															toaster.pop('success', "", "Your post has been uploaded successfully.");
														});
														*/
														location.reload();
														
													};		
												});
											
											});		

										}
								});
								
							},
							//-----------------		
							
							//-----------------
							buttonThink:function(btn){
								var btnText = $(btn).text();								
								$(btn).html('<i class="fa fa-spinner fa-pulse"></i> ' + btnText).attr('disabled', true);
							},
							//-----------------							
								
						};		
					})();
					//-----------------  
					
					

					

					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: sharedName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(sharedName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: sharedName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 										
									}
									
									var packet = {
											info:{
												to: e.who,
												from: sharedName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 	


				}]);  
	



//end
//start

				app.controller('sliderController', ['$rootScope', '$scope', '$firebase', '$timeout', 'SmoothScroll', '$location',
												function($rootScope, $scope, $firebase, $timeout, SmoothScroll, $location) {	   
			
					var fileName  = 'slider';
			
					// ---------------- INIT CORE					
					$scope.initCore = (function () {
					    return {
					    	
							// ---------------- VARIABLES 
						    resetVariables:function(){
							      $scope.page = {
							    	  loadComponents: custom.fillArray(2),
							    	  isLoaded: false,
							      };
							      
					          		$scope.slides = [	
					          			{ img: 'http://lorempixel.com/1000/400/city/1'},
					            		{ img: 'http://lorempixel.com/1000/400/city/2'},
					            		{ img: 'http://lorempixel.com/1000/400/city/3'},
					            		{ img: 'http://lorempixel.com/1000/400/city/4'},
					            		{ img: 'http://lorempixel.com/1000/400/city/5'}
					            	];								      
											      
						    },
						   	//-----------------							    	
					    	
							// ---------------- INIT
							init:function(){
								$scope.initCore.resetVariables();
								$scope.initCore.loadComponents(function(){									
									$scope.mainCore.start();
								});					
							},
							//-----------------
							
							//----------------- REFRESH
							onRefresh:function(callback){
								$scope.masterData = sharedData.getAll();
								$scope.$apply();		
							},
							//-----------------							
							
							
							// ---------------- LOAD COMPONENTS
							loadComponents:function(callback){
								$scope.initCore.checkMaster(function(state){
									if (state){
										$scope.page.loadComponents[0] = true;
										$scope.initCore.checkLoad(callback);
									}
								});
								$scope.initCore.doSomething(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										$scope.initCore.checkLoad(callback);
									}
								});							
							},
							//-------------------
							
							
							// ------------------ SAMPLE COMPONENTS
							checkMaster:function(callback){
								// wait for master.js to finish loading 		
								sharedData.request("masterReady", function(state, data){
									if(state){
										if(data.ready == true){
											callback(true);
										}
										else{
											alert(data.ready);
										}
									}
									else{
										alert(data);
									}	
								});	
							}, 
							
							doSomething:function(callback){
								// LOAD A COMPONENT						
								callback(true);
							},
							//-----------------
							
							
							//----------------- CHECK LOAD
							checkLoad:function(callback){
								var check = true,
									array = $scope.page.loadComponents; 					
								var i = array.length; while(i--){
									if ( array[i] == false){
										check = false; 
									};
								};
								// all loads completed
								if (check){		
									$timeout(function(){
										$scope.page.isLoaded = true;				
										$scope.masterData = sharedData.getAll();	
										callback();			
									});									
								}						
							},					
							//-----------------



					    };
					})(); 
				   	//-----------------
	

					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.mainCore.buildSlider();
							},
							//-----------------
													
													
							//----------------- START 
							buildSlider:function(){
								
								$timeout(function(){
									
							    	// LARGE-MEDIUM BROWSWERS
							    	if ($scope.masterData.browserSize == 'medium' || $scope.masterData.browserSize == 'large'){
								    	// must be appended in via jquery until an angular plugin works
								    	for (var i = 0; i < $scope.slides.length; i++){
								    		var image = $scope.slides[i].img; 
								    		$('.large-slider').append("<div><img src='" + image + "' style='width: 100%; height: auto'></div>");
								    	}
								    	
								    	// slick behaviors
								    	$('.large-slider').slick({
										  dots: true,
										  infinite: true,
										  speed: 500,
										  fade: true,
										  slide: 'div',
										  cssEase: 'linear',
						  					autoplay: true,
						  					autoplaySpeed: 2000,				  
									    });								    	
								    								    		
							    	}

									// SMALL BROWSWERS
							    	if ($scope.masterData.browserSize == 'small'){
										// must be appended in via jquery until an angular plugin works
								    	for (var i = 0; i < $scope.slides.length; i++){
								    		var image = $scope.slides[i].img; 
								    		$('.small-slider').append("<div><img src='" + image + "' style='width: 100%; height: auto'></div>");
								    	}
						
								    	// slick behaviors
								    	$('.small-slider').slick({
										  dots: true,
										  infinite: true,
										  speed: 200,
										  slide: 'div',
										  cssEase: 'linear',
						  					autoplay: true,
						  					autoplaySpeed: 2000,				  
									    });	 								    	
								    								    		
							    	}


	    
							    });		
							    
				
							},
							//-----------------													
														
					
						};
					})();
					//----------------- 
				

			
					
					
					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ----------------   
		
				    
   
  
				    
				}]);				

//end
//start

				app.controller('userController', [
						'$rootScope', '$scope', '$timeout', 'SmoothScroll', 'cfpLoadingBar', 'toaster',
				function($rootScope, $scope, $timeout, SmoothScroll, cfpLoadingBar, toaster) {	    

					var fileName  = 'user';

					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					
								// ---------------- VARIABLES 
							    resetVariables:function(){
							    	$scope.page = {
							    		loadComponents: custom.fillArray(3),  // number should equal # of load components below
							    		isLoaded: false,
							    	};
							    	$scope.content = "I am test content.";
							    },
							   	//-----------------		
					
								//----------------- REFRESH
								onRefresh:function(){
									sharedData.add("currentController", fileName);	// current controller
									$scope.masterData = sharedData.getAll();
									$scope.callCore.callController({who: "master", action: "hideThinking"});
									$scope.$apply();		
								},
								//-----------------						
					
								// ---------------- INIT
								init:function(){
									custom.offCanvasReset();
									$scope.initCore.resetVariables();
									cfpLoadingBar.start();
									SmoothScroll.$goTo(0);							
									$scope.initCore.loadComponents(function(){
										sharedData.add("currentController", fileName);	// current controller
										$scope.mainCore.start();
									});				
								},
								//-----------------        
					        
								// ---------------- LOAD COMPONENTS
								loadComponents:function(callback){
									$scope.initCore.checkMaster(function(state){
										if (state){
											$scope.masterData = sharedData.getAll();
											$scope.page.loadComponents[0] = true;
											$scope.initCore.animate(function(state){
												if (state){
													$scope.page.loadComponents[1] = true;
													$scope.initCore.checkLoad(callback);
												}
											});									
											
										}
									});
			
									$scope.initCore.doSomething(function(state){
										if (state){
											$scope.page.loadComponents[2] = true;
											$scope.initCore.checkLoad(callback);
										}
									});						
								},
								//-------------------		
								
								// ------------------ LOAD COMPONENTS
								checkMaster:function(callback){
									// wait for master.js to finish loading 	
									sharedData.request("masterReady", function(state, data){
										if(state){
											if(data.ready == true){
												callback(true);
											}
											else{
												alert(data.ready);
											}
										}
										else{
											alert(data);
										}	
									});							
			
								},
								animate:function(callback){
									if ($scope.masterData.browserDetails.mobile){
										$('#' + fileName + '_id').addClass('gray-gradient');							
									}
									// for desktop
									else{
										custom.backstretchBG("media/background.jpg", 0, 1000);
									}		
									$timeout(function(){	
										$('#content-page')
											.transition({   x: -20,  opacity: 0, delay: 0}, 0)
											.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
											.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
											
										callback(true);
									}, 500); // slight delay needed
								},
								
								// -- blank component for example
								doSomething:function(callback){
									callback(true);
								},
								//-----------------		
								
								//----------------- CHECK LOAD
								checkLoad:function(callback){
									var check = true,
										array = $scope.page.loadComponents; 					
									var i = array.length; while(i--){
										if ( array[i] == false){
											check = false; 
										};
									};
									// all loads completed
									if (check){
									// CONTENT PAGE IS READY - INSERT CODE HERE		
																		
										// check for existing modal
										var duration = 2000;
										var hasModalOpen = $('.reveal-modal').hasClass('in');
										if (!hasModalOpen){ duration = 0; };
										
										$timeout(function(){ 
											cfpLoadingBar.complete();
											$scope.page.isLoaded = true;																
											$scope.callCore.callController({who: "master", action: "hideSplash"});
																	
											callback();											
										}, duration);									
										
									}						
								},					
								//-----------------																	        
					        
					        
					    };
					})();
					//-----------------     


					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								console.log($scope.masterData);			
							},
							//-----------------


							//----------------- EDIT ACCOUNT
							editAccount:function(){
								$scope.callCore.callController({who: "master", action: "editProfileData"}, function(d){
									
									if(d.packet.data == "updated"){ 
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "showThinking"});
										}, 300);
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "refreshData"});
										}, 600);
									}
								
									
								});							
							},
							//-----------------	


							//----------------- EDIT ACCOUNT
							editPicture:function(){
								$scope.callCore.callController({who: "master", action: "editProfileImage"}, function(d){ 
									if(d.packet.data == "updated"){ 
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "showThinking"});
										}, 300);
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "refreshData"});
										}, 600);
									}
								});							
							},
							//-----------------		
							
							
							//----------------- CHANGE PASSWORD
							changePassword:function(){
								$scope.callCore.callController({who: "master", action: "changePassword"}, function(d){		
									if(d.packet.data == "updated"){ 
										toaster.pop('success', "Password has been updated!", '');
									}							
								});							
							},
							//-----------------	
							
							//----------------- RESET PASSWORD
							resetPassword:function(){
								$scope.callCore.callController({who: "master", action: "resetPassword"}, function(d){
									if(d.data == "updated"){ 
										toaster.pop('success', "Password has been reset!", 'Check your email for a new temporary password.');
									}							
								});							
							},
							//-----------------
							
							
							//----------------- new ACCOUNT
							createNewAccount:function(){
								$scope.callCore.callController({who: "master", action: "createNewAccount"}, function(d){
									if(d.packet.data == "updated"){ 
										toaster.pop('success', "Account has been created!", 'Check your email for a login link.');							
									}							
								});	
							},
							//-----------------	
							
							//----------------- new ACCOUNT
							deleteMyAccount:function(){
								$scope.callCore.callController({who: "master", action: "deleteMyAccount"}, function(d){
									if(d.packet.data == "deleted"){ 
										alert("Your account has been deleted.");
										localStorage.clear();
										location.reload();	
									}
								});	
							},
							//-----------------						
							
							//----------------- LOGIN
							adminLogin:function(){
								
								$scope.callCore.callController({who: "master", action: "login"}, function(d){
									if(d.packet.data == "updated"){ 
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "showThinking"});
										}, 300);
										$timeout(function(){
											$scope.callCore.callController({who: "master", action: "refreshData"});
										}, 600);
									}
									
								});
								
							},
							//-----------------								
								

						};
					})();
					//-----------------  




					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		console.log(data);
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 	    
				    
				   

				}]);  
	

//end
});//endwrapper
define(['custom', 'sharedData', 'firebaseSettings', 'app'], function(custom, sharedData, firebaseSettings, app){//startWrapper

				
				app.controller('firstLoginController', ['$stateParams', '$state', '$rootScope', '$scope', '$timeout', 'SmoothScroll', 'cfpLoadingBar', 
						function($stateParams, $state, $rootScope, $scope, $timeout, SmoothScroll, cfpLoadingBar) {	   
				    
				    var fileName  = 'firstLogin';
				    
					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					
								
					
								// ---------------- VARIABLES 
							    resetVariables:function(){
							    	$scope.page = {
							    		loadComponents: custom.fillArray(3),  // number should equal # of load components below
							    		isLoaded: false,
							    	};
							    	$scope.content = "I am test content.";
							    },
							   	//-----------------		
					
								//----------------- REFRESH
								onRefresh:function(){
									sharedData.add("currentController", fileName);	// current controller
									$scope.masterData = sharedData.getAll();
									$scope.callCore.callController({who: "master", action: "hideThinking"});
										
								},
								//-----------------						
					
								// ---------------- INIT
								init:function(){
									custom.offCanvasReset();
									$scope.initCore.resetVariables();
									cfpLoadingBar.start();
									SmoothScroll.$goTo(0);							
									$scope.initCore.loadComponents(function(){
										sharedData.add("currentController", fileName);	// current controller
										$scope.mainCore.start();
									});				
								},
								//-----------------        
					        
								// ---------------- LOAD COMPONENTS
								loadComponents:function(callback){
									$scope.initCore.checkMaster(function(state){
										if (state){
											$scope.masterData = sharedData.getAll();
											$scope.page.loadComponents[0] = true;
											$scope.initCore.animate(function(state){
												if (state){
													$scope.page.loadComponents[1] = true;
													$scope.initCore.checkLoad(callback);
												}
											});									
											
										}
									});
			
									$scope.initCore.doSomething(function(state){
										if (state){
											$scope.page.loadComponents[2] = true;
											$scope.initCore.checkLoad(callback);
										}
									});						
								},
								//-------------------		
								
								// ------------------ LOAD COMPONENTS
								checkMaster:function(callback){
									// wait for master.js to finish loading 	
									sharedData.request("masterReady", function(state, data){
										if(state){
											if(data.ready == true){
												callback(true);
											}
											else{
												alert(data.ready);
											}
										}
										else{
											alert(data);
										}	
									});							
			
								},
								animate:function(callback){
									if ($scope.masterData.browserDetails.mobile){
										$('#' + fileName + '_id').addClass('gray-gradient');							
									}
									// for desktop
									else{
										custom.backstretchBG("media/background.jpg", 0, 1000);
									}		
									$timeout(function(){	
										$('#content-page')
											.transition({   x: -20,  opacity: 0, delay: 0}, 0)
											.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
											.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
											
										callback(true);
									}, 500); // slight delay needed
								},
								
								// -- blank component for example
								doSomething:function(callback){
									callback(true);
								},
								//-----------------		
								
								//----------------- CHECK LOAD
								checkLoad:function(callback){
									var check = true,
										array = $scope.page.loadComponents; 					
									var i = array.length; while(i--){
										if ( array[i] == false){
											check = false; 
										};
									};
									// all loads completed
									if (check){
									// CONTENT PAGE IS READY - INSERT CODE HERE		
																		
										// check for existing modal
										var duration = 2000;
										var hasModalOpen = $('.reveal-modal').hasClass('in');
										if (!hasModalOpen){ duration = 0; };
										
										$timeout(function(){ 
											cfpLoadingBar.complete();
											$scope.page.isLoaded = true;																
											$scope.callCore.callController({who: "master", action: "hideSplash"});
																
											callback();											
										}, duration);									
										
									}						
								},					
								//-----------------																	        
					        
					        
					    };
					})();
					//-----------------      


					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.mainCore.checkUser();	
							},
							//-----------------

							//-----------------
							checkUser:function(){
								
								if ($scope.masterData.logState == false){
					 				var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );						
									var check = 0; checkPass = true; 
									var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {
												
											if (check > 0 && checkPass){ 	
												// FAILURE TO LOGIN
												if (error != null){
													$scope.mainCore.setPageStatus("fail");	
													checkPass = false;
												}
												// LOGIN SUCCESSFUL
												else{
													$scope.page.user = user;
													$scope.mainCore.setPageStatus("success");											
													checkPass = false;
												}
											}
											else{
												check++;
											}
											$scope.page.isLoaded = true;
			
											
										});		
										auth.login('password', {  // PASSWORD IS THE METHOD ()
										  email:		$scope.page.email,
										  password: 	$scope.page.tempPassword
										});		
									}
									else{								
										$state.go('home');						
									}
					  				
							 },	
							//-----------------
							
							//-----------------
						    first_login:function(){
								callController({who: "master", action: "login"}, function(data){
									console.log(data)
								});
		                 	},
							//-----------------
							
							
							//-----------------
		                    first_createAccount:function(){
								callController({who: "master", action: "createNewAccount"}, function(data){
									if(data.data == "updated"){ 
										alert("new account created");	
									}
								});	
		                    },
							//-----------------
							
							//-----------------		
							setPageStatus:function(type){
								$timeout(function(){ 
									callController({who: "master", action: "hideSplash"});	
									$scope.page.status = type;
									$scope.$apply();										
								});							
							},	
							//-----------------					
					
							//-----------------
							update:function(){
								if ($scope.formData.password.length < 8){
									alert("Password must be at least 8 characters in length.");
								}
								else if ($scope.formData.password != $scope.formData.confirm){
									alert("Passwords do not match.  Please try again.");
								}
								else{
									callController({who: "master", action: "showThinking"});
									
									var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );
									var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {		});								
									auth.changePassword($scope.page.email, $scope.page.tempPassword, $scope.formData.password, function(error, success) {							  
									  if (!error) {	
									  	alert("Your password has been updated.  Please login.");			   	
									    $state.go('home');			  								    
									  }
									  else{							  								  	
									  	alert(error);
									  	callController({who: "master", action: "hideThinking"});
									  }
									});	
		
								}
							},
							//-----------------					
					
					
						};
					})();
					//-----------------  

					
					








					
					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {
					    	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: fileName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: fileName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		console.log(data);
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 
										//e.button.html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', true);
									}
									
									var packet = {
											info:{
												to: e.who,
												from: fileName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 	
				   

				}]);		

});//endwrapper
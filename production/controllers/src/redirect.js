define(['custom', 'sharedData', 'firebaseSettings', 'app'], function(custom, sharedData, firebaseSettings, app){//startWrapper

	   	
				app.controller('redirectController', 
					['$rootScope', '$stateParams', '$scope', '$timeout', '$location', '$sessionStorage', '$localStorage', 'SmoothScroll', 'cfpLoadingBar', 'toaster', 
					function($rootScope, $stateParams, $scope, $timeout, $location, $sessionStorage, $localStorage, SmoothScroll, cfpLoadingBar, toaster) {	   
					
					var pageName  		= "redirect";  		      		
					 
					// ---------------- INIT CORE
					$scope.initCore = (function () {
					    return {
					
								// ---------------- VARIABLES 
							    resetVariables:function(){
							    	$scope.page = {
							    		loadComponents: custom.fillArray(2),  // number should equal # of load components below
							    		isLoaded: false,
							    		editOpen: false,
							    		settings: null,
							    		checkPass: false
							    	};
							    },							    
							   	//-----------------		
					
								//----------------- REFRESH
								onRefresh:function(){
									sharedData.add("currentController", pageName);	// current controller
									$scope.masterData = sharedData.getAll();
									$scope.callCore.callController({who: "master", action: "hideThinking"});
									$scope.$apply();		
								},
								//-----------------						
					
								// ---------------- INIT
								init:function(){
									
									
									custom.offCanvasReset();
									$scope.initCore.resetVariables();
									cfpLoadingBar.start();
									SmoothScroll.$goTo(0);							
									$scope.initCore.loadComponents(function(){
										sharedData.add("currentController", pageName);	// current controller
										
										// initiate edit 
										$scope.editCore.init(function(){
											$scope.mainCore.start();	
										});
										
										
									});				
								},
								//-----------------        
					        
								// ---------------- LOAD COMPONENTS
								loadComponents:function(callback){
									$scope.initCore.checkMaster(function(state){
										if (state){
											$scope.masterData = sharedData.getAll();
											$scope.page.loadComponents[0] = true;
											$scope.initCore.animate(function(state){
													if (state){
														// get page setting data
														phpJS.getPageData(pageName, function(data){															
															$scope.page.settings = data.pageSettings;																										
															$scope.page.loadComponents[1] = true;
															$scope.initCore.checkLoad(callback);
														});		
													}
											});												
											
																	
											
										}
									});
									
			
									$scope.initCore.doSomething(function(state){
										if (state){
											$scope.page.loadComponents[1] = true;
											$scope.initCore.checkLoad(callback);
										}
									});						
								},
								//-------------------		
								
								// ------------------ LOAD COMPONENTS
								checkMaster:function(callback){
									// wait for master.js to finish loading 	
									sharedData.request("masterReady", function(state, data){
										if(state){
											if(data.ready == true){
												callback(true);
											}
											else{
												alert(data.ready);
											}
										}
										else{
											alert(data);
										}	
									});							
			
								},
								animate:function(callback){
									
									if ($scope.masterData.browserDetails.mobile){
										$('#' + pageName + '_id').addClass('gray-gradient');							
									}
									// for desktop
									else{
										custom.backstretchBG("media/background.jpg", 0, 1000);
									}		
									
									$timeout(function(){	
										$('#content-page')
											.transition({   x: -20,  opacity: 0, delay: 0}, 0)
											.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
											.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
											
										callback(true);
									}, 500); // slight delay needed
									
								},
								

															
								// -- blank component for example
								doSomething:function(callback){
									callback(true);
								},
								//-----------------		
								
								//----------------- CHECK LOAD
								checkLoad:function(callback){
									var check = true,
										array = $scope.page.loadComponents; 					
									var i = array.length; while(i--){
										if ( array[i] == false){
											check = false; 
										};
									};
									// all loads completed
									if (check){
									// CONTENT PAGE IS READY - INSERT CODE HERE		
																		
										// check for existing modal
										var duration = 2000;
										var hasModalOpen = $('.reveal-modal').hasClass('in');
										if (!hasModalOpen){ duration = 0; };
										
										$timeout(function(){ 
											cfpLoadingBar.complete();
											$scope.page.isLoaded = true;																
											$scope.callCore.callController({who: "master", action: "hideSplash"});
																
											callback();											
										}, duration);									
										
									}						
								},					
								//-----------------		
       
					    };
					})();
					//-----------------        
				
					//----------------- 
					$scope.editCore = (function() {
						
							// variables ---
							$scope.layout = {
								selected: null,
								types: [],
							};
							// -------------
							 
							
							
							 return {
							 		 	
							 	//-----------------								      
								init:function(callback){
									
										
									$scope.editCore.getLayoutFiles(function(d){	
										// folder doesn't exist; create it
										if (d.data == 'no folder'){
											
											
											var packetData = {
  												location: "themes/structure/" + $scope.page.settings.currentTable + "/",
 												name: "layout"
											};	
											
																					
											$scope.callCore.callController({who: "master", action: "phpCreateFolder", data: packetData}, function(_d){
												// folder created successful
												if (_d.packet.data.status == "created"){
													//console.log("folder created successfully")
												}						
																		
											});											
										};
										
										// no files; create a blank template
										if (d.data == 'no files'){
											// create files
										}
										
										// files present - create selection 
										else{
											for (var i = 0; i < d.data.length; i++){
												$scope.layout.types.push(d.data[i]);
											}
											$timeout(function(){
												$scope.editCore.layout = $scope.page.settings.currentLayout;	
											});
											
											callback();
										};	
									});
										
										
										
									
						      	},
						      	//-----------------
						      	

						      	//-----------------
						      	editSystemSettings:function(){

						      		var packetData = {system: $scope.masterData.system};
						      		$scope.callCore.callController({who: "master", action: "editorSystemSettings", data: packetData}, function(d){
						      			console.log(d)
						      		});							      			
		
						      	},
						      	//-----------------						      	
						      	
						      	
						      	//-----------------
						      	editPageSettings:function(){
									phpJS.returnListOfTables($scope.masterData.system.usingdb, function(state, data){
										if (state){
								      		var packetData = {pagename: pageName, settings: $scope.page.settings, database: $scope.masterData.system.usingdb, collection: data};
								      		$scope.callCore.callController({who: "master", action: "editorPageSettings", data: packetData}, function(d){
								      			console.log(d)
								      		});							      			
										}
									});							      		
						      		
						      	},
						      	//-----------------
						      	
						      	//-----------------
						      	updatePageSettings:function(){						      		
						      		$timeout(function(){ 
						      			$scope.page.settings.currentLayout = $scope.editCore.layout;						      			
						      		});
						      	},
						      	//-----------------
						      	 	
						      	//-----------------						      	
								getLayoutFiles:function(callback){
									var packetData = {
										location: "themes/structure/" + $scope.page.settings.currentTable + "/"
									};
																					
									$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){
										
										if (d.packet.data.status == true){
												// pull data from folder
												var files = d.packet.data.files; 
												// files exist
												_return = [];
												if (d.packet.data.files.length > 0){
													for (i = 0; i < files.length; i++){
														_return.push( files[i].replace(/\.[^/.]+$/, "") );	
													};																							
													callback({status: true, data: _return});
												}
												// no files exist
												else{
													callback({status: false, data: "no files"});
												}
										}
										if (d.packet.data.status == "error"){											
											callback({status: false, data: "no folder"});
										}
										
									});
						      	},
						      	//-----------------	
						      	
						      	//-----------------						      	
								openLayoutEditor:function(callback){
									var packetData = {
										fields: $scope.collection.searchFields
									};											
									$scope.callCore.callController({who: "master", action: "editorLayout", data: packetData}, function(d){
										//console.log(d)
									});
						      	},
						      	//-----------------	


							 	//-----------------								      
								toggleEditor:function(callback){
									if (!$scope.page.editOpen){
										$('#__editor-sidebar')											
											.transition({   x: -($('#__editor-sidebar').width()) }, 500);	
										$('#__editor-bottom')
											.transition({   y: -($('#__editor-bottom').height()), delay: 250 }, 500);	
									}
									else{
										$('#__editor-sidebar')											
											.transition({   x: 0 }, 500);	
										$('#__editor-bottom')	
											.transition({   y: ($('#__editor-bottom').height()), delay: 250 }, 500);	
									}
									$scope.page.editOpen = !$scope.page.editOpen;
						      	},
						      	//-----------------		
						      
						      
						      };							
					})();
					//----------------- 
					
					//-----------------  MAIN CORE
					$scope.mainCore = (function () {
					    return {
			
							//----------------- START 
							start:function(){
								// CONTENT PAGE IS READY - INSERT CODE HERE
								//$scope.mainCore.checkPermissions();
								$scope.page.isLoaded = true;
								console.log("redirect loaded");
								//$scope.premadeCollection.init();
		
							},
							//-----------------
							
							checkPermissions:function(){
								var path = $location.path().replace(/[^a-zA-Z1-9 ]/g, ""),								
									allowed = $scope.masterData.accessLevel.allowed;
							
								$scope.page.checkPass = false;
								for (var i = 0; i < allowed.length; i++){
									check = allowed[i];
									if (check == path){
										$scope.page.checkPass = true;
									}
								}
								if(!$scope.page.checkPass){
									alert("You dont' have the proper permission to access this page.");
								}
							},
							
							//----------------- PICTURE BOX 
							pictureBox:function(_list, _index){								
								var packetData = {
									list: _list,
									index: _index
								};
								// CONTENT PAGE IS READY - INSERT CODE HERE
								$scope.callCore.callController({who: "master", action:"pictureBox", data: packetData}, function(d){
									//console.log(d);						
								});		
							},
							//-----------------							
							

						};
					})();
					//-----------------  
					
					//-----------------  COLLECT FROM DATABASE PREMADE FUNCTIONS
					$scope.premadeCollection = (function(){
					
				
						
						return { 
							
							
							//-----------------
							resetVariables:function(){
									// ----------------------------------
									
									
							    	$scope.collection = {
							    		
							    		table: $scope.page.settings.currentTable, 
							    		imageStorage: "uploads/images/" + $scope.page.settings.currentTable + "/",		 // 
							    		
							    		pagination:{
							    			current: 1, 					 // current page
							    			limit: 50, 						 // entries per page (initial value)
							    			morePerClick: 5, 				 // each time you click for more, this is how many entries it retrieves
							    			filerBy: null,
							    			reverse: true,
							    			max: null,
							    			filter: null,
							    			hasNext: null,
							    			hasPrev: null,
							    			isLast: null,
							    			isFirst: null,
							    			range: [
							    				{id: 1, label: 5},
							    				{id: 2, label: 10},
							    				{id: 3, label: 25},
							    				{id: 4, label: 50}				    				
							    			],
							    			selectResults: null
							    		},
			
							    		remember:{
							    			useLocalStorage: true,
							    			rememberWith:  "id",
							    			hasStorage: false
							    		},
							    		
							    		
										allEntries: [],
							    		useEntries: null,	
							    		numberOfEntries: null,		    		
							    		
							    		filterSearch: null,
							    		searchResultsCount: null,
							    		searchType: null,
							    		searchTerm: null,
							    		searchFields: null,
			
							    	};	
							    	// ----------------------------------
			
									// ----------------------------------	
									$scope.entryStatistics = {
										hasMore:    null,
										available: 	null,
										total: 		null
									};
									// ----------------------------------	
									
									// ----------------------------------
									// when exiting this page	
									$scope.$on('$destroy', function() {
										$scope.premadeCollection.saveSearch();
									});
									// ----------------------------------	
										
							    	// ----------------------------------
							    	$scope.firebasePermission = {
							    		fbID_required_toCreate: true,
							    		fbID_required_toEdit: true,
							    		fbID_required_toDelete: true,		
							    		
							    		fbIDmustMatchEntryId_edit: true,
							    		fbIDmustMatchEntryId_delete: true
							    	};
							    	// ----------------------------------
							    	
									// ----------------------------------
									$scope.$storage 		= $sessionStorage;	
									$scope.editBtn 			= {value: 0};
									$scope.deleteBtn 		= {value: 0};
									$scope.loadingEntries 	= true;
									$scope.isUsingAdvanced 	= false;
									// ----------------------------------	
																
							},
							//-----------------
							
							
							//-----------------
							init:function(){
								$scope.premadeCollection.resetVariables();
								
								var packetData = {	
									database: $scope.masterData.system.usingdb,
									table: $scope.collection.table
								};
								
								// get entry row totals
								$scope.callCore.callController({who: "master", action:"phpRunQueryForTotalEntry", data: packetData}, function(d){
									$scope.entryStatistics.total = d.packet.data.rows;
									
									
									// get table data
									$scope.callCore.callController({who: "master", action: "phpGetTableDetails", data: packetData}, function(d){ 
										$scope.collection.searchFields = custom.returnPropertyInObject(d.packet.data.formData, ["Field", "Type", "inputType"]);
										$scope.collection.pagination.filterBy = $scope.collection.searchFields.Field[0];
										$scope.premadeCollection.initialLoad();
									});									
								});
							
							},
							//-----------------
							
							//-----------------
							layoutUrl:function(){
								return "themes/structure/" + $scope.page.settings.currentTable + "/" + $scope.page.settings.currentLayout + ".html";								
							},	
							//-----------------						
							
							//-----------------
							initialLoad:function(){
								function loadDefault(){
									  var query = "SELECT * FROM " + $scope.collection.table +  " ORDER BY id DESC LIMIT 0, 1";   
									  $scope.premadeCollection.fetchEntries(query, null, function(){
											 SmoothScroll.$goTo(0);								// scroll back to top
											 $scope.loadingEntries = false;						// hide loading
											 $scope.premadeCollection.isUsingAdvanced = false;   // reset advanced
											 $scope.entryStatistics.hasMore = true;			   // hides more											 
									  });										
									  
								}
								
								$timeout(function(){
									$scope.loadingEntries = true;
									
										// if search string is in the url
										var searchString = $stateParams.search; 
										if (searchString != null){
											var key = searchString.substr(0, searchString.indexOf(':'));
											var value = searchString.split(':')[1];
												value = value.replace(/_/g, " ");
											var query = "SELECT * FROM " + $scope.collection.table +  " WHERE " + key + " = " + "\""+ value + "\"";
											
											  $scope.premadeCollection.fetchEntries(query, null, function(){
											    SmoothScroll.$goTo(0);								// scroll back to top
											    $scope.loadingEntries = false;						// hide loading
											    $scope.premadeCollection.isUsingAdvanced = true;   // reset advanced
											    $scope.entryStatistics.hasMore = false;			   // hides more
											  });											
										}
										// no search string - continue with default loading
										else{																   
										  	loadDefault();										
										};
								});
							},
							//-----------------
							
							//-----------------
							clearAdvancedSearch:function(){
								$location.url($location.path());
								$scope.collection.allEntries = [];
								$scope.premadeCollection.initialLoad();
							},
							//-----------------
							
							//-----------------
							saveSearch:function(){
								if ($scope.collection.remember.useLocalStorage){
									toaster.pop('success', "", "Save search results.");
						    		// build remember string
						    		var rememberArray = custom.returnPropertyInObject($scope.collection.allEntries, [$scope.collection.remember.rememberWith])[$scope.collection.remember.rememberWith],
						    			rememberString = '';									
									for (i = 0; i < rememberArray.length; i++){
										if (i != 0){ rememberString += ", "; }
										rememberString += rememberArray[i]; 
									};								
									$scope.$storage.rememberMe = {										
										key: $scope.collection.remember.rememberWith,
										value: rememberString			    
									};
   								}
							},		
							//-----------------
							
							
							//-----------------
							loadPriorSearch:function(){
								  if ($scope.$storage.rememberMe != undefined){		
									  	$scope.collection.remember.hasStorage = true;										

										var packet = {
											header: "Load saved entries?",
											field1: "Yes", 
											field2: "Cancel"
										};	
										$scope.callCore.callController({who: "master", action: "confirmBox", data: packet}, function(d){ 
											
											if (d.packet.status != 'dismiss'){
												if (d.packet.data){	
											  	  var query = 'SELECT * FROM ' + $scope.collection.table + ' WHERE ' + $scope.$storage.rememberMe.key + ' IN(' + $scope.$storage.rememberMe.value + ')';
											  	  $scope.loadingEntries = true;									// hide loading
											  	  $scope.collection.allEntries = [];
												  $scope.premadeCollection.fetchEntries(query, null, function(){
														 SmoothScroll.$goTo(0);								// scroll back to top
														 $scope.loadingEntries = false;						// hide loading
														 $scope.premadeCollection.isUsingAdvanced = true;   // reset advanced
														 $scope.entryStatistics.hasMore = false;			   // hides more
												  });	
												}		
											}
										});		
									}							
								
							},
							//-----------------
							
							//-----------------
							fetchMostRecent:function(){
								$scope.loadingEntries = true;
								$timeout(function(){
								  // pull the most recent entry, but only one
								  var query = "SELECT * FROM " + $scope.collection.table +  " ORDER BY id DESC LIMIT 0, 1";  
								  $scope.premadeCollection.fetchEntries(query, null, function(){
								    SmoothScroll.$goTo(0);
								    $scope.loadingEntries = false;
								  });
								});
							},
							//-----------------
							
							//-----------------
							fetchEntry:function(whereStatement, insertInto, callback){
								$scope.loadingEntries = true;
								$timeout(function(){
								  // pull the most recent entry, but only one
								  var query = "SELECT * FROM " + $scope.collection.table + " " + whereStatement;  
								  $scope.premadeCollection.fetchEntries(query, insertInto, function(){								   
								   	$scope.loadingEntries = false;
								   	callback();
								  });
								});
							},
							//-----------------							

							//-----------------
							fetchMore:function($event){
								// turn into thinking
								var btn = $event.target;
									btnText = $(btn).text(),
									btnRaw = $(btn).html();	
														
								$(btn).html('<i class="fa fa-spinner fa-pulse"></i> Fetching...').attr('disabled', true);
								
								var query = "SELECT * FROM " + $scope.collection.table +  " ORDER BY id DESC LIMIT " + $scope.entryStatistics.available + ", " + $scope.collection.pagination.morePerClick;  
								$scope.premadeCollection.fetchEntries(query, null, function(){
									$(btn).html(btnRaw).attr('disabled', false);  
								});
							},
							//-----------------
	
							//-----------------
							fetchEntries:function(query, insertInto, callback){	
									var queryString = {	
										query: query, 
										database: $scope.masterData.system.usingdb
									};
								
									$scope.callCore.callController({who: "master", action: "phpRunQuery",  data: queryString}, function(d){ 																

											// sanitize html types that use html entities (adv)
											var htmlEntities = [];
											var allInputTypes = $scope.collection.searchFields.inputType;
											var allFields = $scope.collection.searchFields.Field;
											for (var i = 0; i < allInputTypes.length; i++){
												var inputType = allInputTypes[i]; 
												if (inputType == 'adv' || inputType == 'imageUpload'){
													htmlEntities.push({data: allFields[i], type: inputType});
												}
											}													

											processed = 0;
											var entryPacket = d.packet.data; 
											function loopCycle(){
												
												//  cycle												
												if (processed < entryPacket.length){
			
														var subProcess = 0;	
														function subloopCycle(){
																												
															if (subProcess < htmlEntities.length){																	
																filterField = [htmlEntities[subProcess].data];
																filterType = htmlEntities[subProcess].type;
																
																
																filterMe = entryPacket[processed][filterField];
																phpJS.dencodeHtmlEntities(filterMe , function(state, data){																	
												    				escapedStrings = data.replace(/["']/g, "");
												    				
												    				// parse json string into object
												    				if (filterType == 'imageUpload'){
												    					if (data != null && data != undefined && data != ''){
												    						entryPacket[processed][filterField] = JSON.parse(data);
												    					}
												    					else{
												    						entryPacket[processed][filterField] = data; 
												    					}
												    				}
												    				// html text
												    				else{
												    					entryPacket[processed][filterField] = data; 
												    				}
												    				
																	subProcess++;
																	subloopCycle();													    				
												    			});																															
															}
															else{
																subloopComplete();
															}
														};
														subloopCycle();
														
														function subloopComplete(){
															processed++;															
															loopCycle();
														}								
												}	
												//  loop complete
												else{	
													// if inserting into an existing array location
													if (insertInto != null){				
														$timeout(function(){									
															$scope.collection.userEntries[insertInto] = entryPacket[0];

														});
														callback();	
													}
													// normal behavior
													else{																									
														$scope.premadeCollection.arrangeEntries(d.packet.data, callback);	
													}
												}	
												
											};
											loopCycle();	
										
									});	
							},	
							//-----------------		
							
							//-----------------
							arrangeEntries:function(entryPackages, callback){
									// add results to allEntries
									// $scope.collection.allEntries.push(d.packet.data);
									for (i = 0; i < entryPackages.length; i++){
										$scope.collection.allEntries.push(entryPackages[i]);
									};
									
									// number of entries
									$scope.collection.numberOfEntries = custom.returnLengthOfObject( $scope.collection.allEntries ); 
									$scope.entryStatistics.available = custom.returnLengthOfObject( $scope.collection.allEntries ); 
									
									// more entires does not appear when using advanced search
									if (!$scope.premadeCollection.isUsingAdvanced){
										if ( $scope.entryStatistics.available == $scope.entryStatistics.total){
											$scope.entryStatistics.hasMore = false;
										}
										else{
											$scope.entryStatistics.hasMore = true;
										}
									}
									else{
										$scope.entryStatistics.hasMore = false;
									}
									
									// paginate to current page
									$scope.premadeCollection.paginateEntries();
									if (callback != null || callback != undefined){
										callback();
									}
							},
							//-----------------
							
							//-----------------
							advancedSearch:function(){
								
								packet = {
									fields: $scope.collection.searchFields.Field,
									types: $scope.collection.searchFields.Type,
									objToQuery: $scope.collection.allEntries,
									database: $scope.masterData.system.usingdb,
									table: $scope.collection.table
								};
								
								$scope.callCore.callController({who: "master", action: "advSearchModal", data: packet}, function(d){ 
									if(d.packet.data != "dismissed"){
										$scope.premadeCollection.isUsingAdvanced = true;		
										toaster.pop('success', "", "Loading search results.");
										$scope.collection.allEntries = [];
										var whereStatement = 'WHERE id IN(' + d.packet.data.returnData + ')';																	
										$scope.premadeCollection.fetchEntry(whereStatement, null, function(){
											
										});
									}
								});
							},
							//-----------------

							//-----------------
							// addImagePacket = {
							//   allImages: [{name: pageName, src: data64String, thumbnail: data64String}]	
							//   newImages: [{name: pageName, src: data64String, thumbnail: data64String}]
							//   uploadId: 10
							// } 
							addImagesTo:function(addImagePacket, callback){
											
											
											
											// declare variables
											var _all = addImagePacket.allImages,
												_added = addImagePacket.newImages,
												_id = addImagePacket.uploadId,
												_next = function(){ count++; loop(); }, 
												count = 0;
											
											
														
											// loop function								
											function loop(){

												//  loop conditions
												if (count < _added.length){
													
													delete _added[count].parse;   // remove property so it doesn't get uploaded again
													
													// upload full size image
													function uploadFullSize(){
														var imageSrc = _added[count].src;
														var packet = {
															file: imageSrc,
															name:  _added[count].name,
															location: $scope.collection.imageStorage + _id
														};	
														
														phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
															if (state){
																$timeout(function(){
																	_added[count].src = data.src;
																	uploadThumbnail();
																});			
															}																						
														});																								
		
													}
													// upload thumbnail size image
													function uploadThumbnail(){
														var imageSrc = _added[count].thumbnail;
														var packet = {
															file: imageSrc,
															name: _added[count].name + "_thumbnail",
															location: $scope.collection.imageStorage + _id
														};
														
														phpJS.imageUploadBase64(packet.file, packet.name, packet.location, function(state, data){
															if (state){
																$timeout(function(){
																	_added[count].thumbnail = data.src;
																	_next()
																});			
															}																						
														});																																													
													}																
													
													// loop logic 
													if (_added.length > 0){
														uploadFullSize();
													}
													// loop complete
													else{
														complete()
													}
													
												}
												
												// end loop conditions
												else{																						
													complete();
												}
											}
											// end loop function
											
											// complete function
											function complete(){
												_all = JSON.stringify(_all);	
												callback(_all);	
											}
											
											// start loop
											loop();	

							},
							//-----------------

							//-----------------
							editEntry:function($event, id, index){
													
									$scope.premadeCollection.buttonThink( $($event.target) );
								
									// grab entry information
									var queryString = {	
										query: "SELECT * FROM " + $scope.collection.table + " WHERE id=" + id, 
										database: $scope.masterData.system.usingdb
									};
									$scope.callCore.callController({who: "master", action: "phpRunQuery",  data: queryString}, function(d){ 											
											var editData = d.packet.data[0];		
											var packetData = {	
												database: $scope.masterData.system.usingdb,
												table: $scope.collection.table
											};
											// call the create module
											$scope.callCore.callController({who: "master", action: "phpGetTableDetails",  button: $($event.target), data: packetData}, function(d){ 	
												if (d.packet.status == 'success'){	
																				
																															
																				
													var tableFields = {
														form: d.packet.data.formData,
														meta: d.packet.data.metaData,
														editData: editData
													};
													// post modal
													$scope.callCore.callController({who: "master", action: "postModal", data: tableFields}, function(d){ 
														if (d != undefined && d.packet.status == "closed"){
		     												
		     												
		     												
														    // build update string
														    var buildString = "",
														    	loopCount = 0;
														    	
														    function addToString(fieldLabel, entry){
														    	buildString += fieldLabel + "='" + entry + "', ";
																loopCount++;
																loopCycle();
														    };	
														    
														    function loopCycle(){
															   	
															   	if (loopCount < tableFields.form.length){
															   		
															   		
															    	var fieldLabel = tableFields.form[loopCount].Field;
															    	var newEntry = d.packet.data.returnData[loopCount];
															    	var inputType = tableFields.form[loopCount].inputType;
															    	
															    	if (newEntry != undefined && newEntry != null && newEntry != "Invalid Date"){			
															    																	    		
															    		// sanitize advanced texts
															    		if(inputType == 'adv'){															    		
															    			phpJS.encodeHtmlEntities(newEntry, function(state, data){
															    				addToString(fieldLabel, data);
															    			});
															    		}
															    		
															    		// image uploads
															    		else if(inputType == 'imageUpload'){		
																				
																				// delete unused function
																				function deleteUnusedImages(){
																					var packetData = {
																						location: $scope.collection.imageStorage + id
																					};
																					
																					$scope.callCore.callController({who: "master", action: "phpListOfFiles", data: packetData}, function(d){
																						$timeout(function(){

																							// parse information
																							if (d.packet.data.files != null && d.packet.data.files != null){
																								
																								
																								var filesInDirectory = [];
																								for (var i = 0; i < d.packet.data.files.length; i++){
																									var f = (d.packet.data.files[i]).replace(/\.[^/.]+$/, "");
																									if (!/thumbnail/i.test(f)){  // exclude thumbnails names
																										filesInDirectory.push( f );
																									}
																								};
				
																								// delete files no longer in use
																								var currentFiles = [];
																								for (var i = 0; i < newEntry.length; i++){
																									var f = newEntry[i].name;
																									currentFiles.push( f );
																								}																						
																								
																								// get all files to delete
																								var toBeDeleted = custom.compareArrayForDifferences(filesInDirectory, currentFiles);
		
																								// get image and thumbnail
																								var packet = [];
																								for (var i = 0; i < toBeDeleted.length; i++){
																									packet.push({folder: "../" +  $scope.collection.imageStorage + id, file: toBeDeleted[i] + ".png"});
																									packet.push({folder: "../" +  $scope.collection.imageStorage + id, file: toBeDeleted[i] + "_thumbnail.png"});
																								}
																							
																								// delete files from																						
																								if( packet.length > 0 ){
																									$scope.callCore.callController({who: "master", action: "phpDeleteFiles", data: packet}, function(_d){																																																																																					
																										addNewImages();																																														
																									});
																								}
																								else{
																									addNewImages();
																								}
																								
																								
																							}
																							// nothing to parse, skip to add
																							else{																							
																								addNewImages();
																							}
																					
																						});
																					
																					});			
																				};
																				// end delete image function
																				
																				// add image functions		
																				function addNewImages(){
																		
																					// get files to add
																					var addList = [];
																					for (var i = 0; i < newEntry.length; i++){
																						if (newEntry[i].parse){
																							addList.push( newEntry[i] );
																						}
																					}	
																					
																					var addImagePacket = {
																						allImages: newEntry,
																						newImages: addList,
																						uploadId: id
																					};
																					
																					$scope.premadeCollection.addImagesTo(addImagePacket, function(data){
																						phpJS.encodeHtmlEntities(data, function(state, _data){
																							addToString(fieldLabel, _data);
										    											});		
																						
																						
																					});

																				};	
																				// end add image function		
			
																				// start
																				deleteUnusedImages();
															    			
															    		}															    		
															    		else if(inputType == 'date'){
															    		  	newEntry = newEntry.replace(/\D/g,'');
															    		  	addToString(fieldLabel, newEntry);
															    		}
															    		else if(inputType == 'time'){
															    		  	newEntry = moment(newEntry).format( "H:mm:ss");
															    		  	addToString(fieldLabel, newEntry);
															    		}
															    		else{
																    		addToString(fieldLabel, newEntry);
																    	}	
																    	// end sanitized edits before saving
																    	
															    	}
															    	else{
																	    loopCount++;
																	    loopCycle();															    		
															    	}
															    	
															    }
															    else{
															    	loopComplete();
															    }
															   
														    }
														    
														    
														    
														   function loopComplete(){
														  		
														   		buildString = buildString.replace(/,\s*$/, "");
														   		
															    // edit update
															    var packetData = {
															   		query: "UPDATE " + $scope.collection.table + " SET " + buildString + " WHERE id=" + id,
															   		database: $scope.masterData.system.usingdb
															    };	
															    // update edit!																      		
																$scope.callCore.callController({who: "master", action: "phpModifyEntry", button: $($event.target),  data: packetData}, function(d){ 
																	var whereStatement = 'WHERE id=' + id;
																	
																															
																	$scope.premadeCollection.fetchEntry(whereStatement, index, function(){
																		toaster.pop('success', "", "Your post has been edited successfully.");
																	});
																});
																
																
															
															}
															// end edit
															
															// start loop
															loopCycle();
														}
													});	
													// /end post modal													
												}	
											});	
											// end call create module															
									});	
									// end grab edit informatoin
									
							},
							//-----------------	

							//-----------------
							deletePost:function(id, index){
									var packet = {
										header: "Delete this entry?",
										field1: "Yes", 
										field2: "NOOOO!!"
									};
								
									// confirm box
									$scope.callCore.callController({who: "master", action: "confirmBox", data: packet}, function(d){ 
										if (d.packet.data == true){		
												// delete from database
												var queryString = {	
													query: "DELETE FROM " + $scope.collection.table + " WHERE id=" + id, 
													database: $scope.masterData.system.usingdb
												};
												// delete all information
												$scope.callCore.callController({who: "master", action: "phpModifyEntry",  data: queryString}, function(d){ 
													
													// delete all images from folder
													$scope.premadeCollection.deleteFolder($scope.collection.imageStorage + id, function(){
														$timeout(function(){
															$scope.collection.userEntries.splice(index, 1);
															toaster.pop('success', "", "Your post has been delete successfully.");
														});		
													});					
												});	
												// end delete
										}				
									});	
									// end confirm								
							},
							//-----------------
							
							//-----------------
							deleteFolder:function(folder, callback){				
							   	
							    	var packetData = {	
										folder: "../" + folder
									};

											
									$scope.callCore.callController({who: "master", action: "phpDeleteFolder", data: packetData}, function(d){
										 															
										callback(d);
									});								
								
							},
							//-----------------
							
							//-----------------
							paginateEntries:function(){					
								
									var packet = {
										objToPaginate: $scope.collection.allEntries,
										filterBy: $scope.collection.pagination.filterBy,
										reverse: $scope.collection.pagination.reverse,
										filterSize: $scope.collection.pagination.limit,
									};
								

									var _results =  custom.sortAndPage( packet );
										// apply to page
									$timeout(function(){
										// content for the main page
										$scope.collection.userEntries = _results.page( $scope.collection.pagination.current );;

										// get pagination data
							    		$scope.collection.pagination.max = _results.totalPages;						    		
							    		$scope.collection.pagination.hasNext = _results.hasNext();
							    		$scope.collection.pagination.hasPrev = _results.hasPrev();
							    		
							    		if ($scope.collection.pagination.max == $scope.collection.pagination.current ){
							    			$scope.collection.pagination.isLast = true;
							    		}else{
							    			$scope.collection.pagination.isLast = false;
							    		}
							    		if (_results.currentPage == 1 ){
							    			$scope.collection.pagination.isFirst = true;
							    		}else{
							    			$scope.collection.pagination.isFirst = false;
							    		}		
									});
								
							},							
							//-----------------				

							//-----------------
							openNewEntry:function($event, tableName){
								$scope.premadeCollection.buttonThink( $($event.target) );
												
								var packetData = {	
									database: $scope.masterData.system.usingdb, 
									table: $scope.collection.table
								};
								$scope.callCore.callController({who: "master", action: "phpGetTableDetails",  button: $($event.target), data: packetData}, function(d){ 	
									if (d.packet.status == 'success'){								
										var tableFields = {
											form: d.packet.data.formData,
											meta: d.packet.data.metaData,
											editData: null
										};
										$scope.callCore.callController({who: "master", action: "postModal", data: tableFields}, function(d){ 
												if(d.packet.status == "closed"){
													var packetData = {
											 			database: $scope.masterData.system.usingdb,
											 			table: $scope.collection.table,
											 			fields: tableFields.form, 
											 			inputData: d.packet.data.returnData 									 			
													};											
													$scope.premadeCollection.createNewEntry(packetData);
												}
										});														
									}	
								});	
							},
							//-----------------
							
							//-----------------
							createNewEntry:function(packetData){
								
								$timeout(function(){
									toaster.pop('info', "", "Submitting your entry.");
								});
								var cloneArray = $.extend(true, [], packetData.inputData);

								// GET NEXT IN ID
								var queryString = {	
									table: $scope.collection.table, 
									database: $scope.masterData.system.usingdb
								};
							
								$scope.callCore.callController({who: "master", action: "phpCreateBlankEntry",  data: queryString}, function(d){ 
										
										var nextId = d.packet.data.id;

										// SANITIZE ENTRIES OF NEW POSTS
										var counter = 0; 
										var _next = function(){
											counter++;
											setLoop();
										};								
										function setLoop(){
											
											
											if (counter < packetData.fields.length){
													var type = packetData.fields[counter].Type,
														inputType = packetData.fields[counter].inputType;
												
													if (type == "time"){
														cloneArray[counter] = moment(packetData.inputData[counter]).format( "H:mm:ss");
														_next();											
													}
													else if (inputType == "imageUpload"){
														var index = counter; 
														var subCounter = 0; 
													
														var addImagePacket = {
															allImages: packetData.inputData[index],
															newImages: packetData.inputData[index],
															uploadId: nextId
														};
														
														$scope.premadeCollection.addImagesTo(addImagePacket, function(data){															
															cloneArray[index]  = data; 
															_next();
														});
												
													}
													else{
														_next();
													}													
		
											}
											else{
													imageloopComplete();
											}
											counter++;
										}								
										setLoop();  // start loop
										
										
										function imageloopComplete(){
											
											// clone array has been formmated correctly
											packetData.inputData = cloneArray; 
											
											// sanitize and ready edit statement
 											custom.sanitizePacketsForDatabase(packetData, function(sanitizedData){	
											    // edit update
											    var queryPacket = {
											   		query: custom.returnEditString($scope.collection.table, sanitizedData, nextId),
											   		database: $scope.masterData.system.usingdb
											    };								     		
										    
											    // update newly created entry!													    		
												$scope.callCore.callController({who: "master", action: "phpModifyEntry",  data: queryPacket}, function(d){ 
													if (d.packet.status == "success"){			
														var whereStatement = 'WHERE id IN(' + nextId + ')';																	
														$scope.premadeCollection.fetchEntry(whereStatement, null, function(){
															
														});
														toaster.pop('success', "", "Your post has been uploaded successfully.");
													};		
												});
											
											});		

										}
								});
								
							},
							//-----------------		
							
							//-----------------
							buttonThink:function(btn){
								var btnText = $(btn).text();								
								$(btn).html('<i class="fa fa-spinner fa-pulse"></i> ' + btnText).attr('disabled', true);
							},
							//-----------------							
								
						};		
					})();
					//-----------------  
					
					

					

					//-----------------  MAIN CORE
					$scope.standardCore = (function () {
					    return {	
						 	//  CALL METHODS
							pong:function(callback){
								callback({status: pageName, msg: "pong"});
							},	
						};
					})();
					//-----------------  

					//-----------------  CALL CORE 	
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(pageName + '_recieve', function(e, data) { $scope.callCore.runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
					var executeOrder = [];											
					$scope.callCore = (function () {
						 return {
						 	
	
						 	// RUN REQUEST
							runRequest:(function () {
								
							  var callback = function(data, returnData){
								var packet = {
										info:{
											to: data.info.from,
											from: pageName
										},
										execute: {
											name: "calledBack",
											callback: false
										},
										returnData: {
											data: returnData
										}
									};	
								$timeout(function(){				
									$scope.broadcast(packet);
								});
							  };						
								
							  var execute = function (data) {
									switch(data.execute.name) {
										
										 // required for callbacks to master controller
									    case "calledBack":
									    	// change button back if sent in parameters
									    	$timeout(function(){
									    		
									    		
										    	if (data.returnData.button.obj != false){
										    		$(data.returnData.button.obj).html(data.returnData.button.content).attr('disabled', false);
									    		}
									        	$scope._watched.callback = data.returnData;
									       	});
									    break;
									     // add to this list if you need to call this specific controller
									    case "refresh":
									        $scope.initCore.onRefresh();
									    break;								     
									    case "ping":
									        $scope.standardCore.pong(function(returnData){
									        	if (data.execute.useCallback){
									        		callback(data, returnData); 
									        	};
									        });
									    break;	
									   
									}					   	
							  };
							    
							  return {
							    execute: execute
							  };		
							})(),
							
							
							
							
							// shorthand for calling 
							callController:function(e, returnPacket){
									var useCallback = true;
									if (returnPacket == null || returnPacket == undefined){
										useCallback = false;
									} 
									if (e.data == undefined || e.data == null){
										e.data = {};
									}
									
									// CHANGE BUTTON TO THINKING IF IT EXISTS
									var buttonContent = false;
									if (e.button == undefined || e.data == null){								
										e.button == false;
									}
									else{
										buttonContent = e.button.text(); 										
									}
									
									var packet = {
											info:{
												to: e.who,
												from: pageName,
												data: e.data,
												button: e.button,
												buttonContent: buttonContent
											},
											execute: {
												name: e.action,
												useCallback: useCallback,
												callback: returnPacket
											}
									};	
										
									$timeout(function(){	
											$scope.callCore.masterCall.execute(packet, function(data){
												if (returnPacket != null && returnPacket != undefined){
													returnPacket(data);	
												};			
											});					
									});
							},
							
							// create callback system for talking to the master controller							
							masterCall:(function () {
								  var execute = function(packet, callback){
								  		packet["order"] = executeOrder.length;
								  		
								  		executeOrder.push(packet);
										$scope._watched = {execute: null, callback: null};
										var unbindWatch = $scope.$watch('_watched.execute', function() {	 
												if($scope._watched.execute != null){
									       			$scope.broadcast($scope._watched.execute);	
									       		};
									      	
									    });
										$scope.$watch('_watched.callback', function() {										
												if($scope._watched.callback != null && $scope._watched.execute != null){
													$scope._watched.execute.execute.callback($scope._watched.callback);
													$scope._watched = {execute: null, callback: null}; 									
												}    	
												unbindWatch(); // remove event binder so it does not duplicate
									    });	
									   	$scope._watched.execute = packet;						   	
								  };
							    
								  return {
								    execute: execute
								  };		
							})(),								
							
						 	
						 };
					})();
					// ---------------- 			
				   

				}]);		

});//endwrapper
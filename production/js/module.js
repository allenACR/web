/* global moment */

angular.module('sbDateSelect', [])

  .directive('sbDateSelect', [function () {

    var template = [
      '<div class="sb-date-select small-12">',
        '<select ng-change="checkFormValidation()" ng-required="field.isRequired == true" class="sb-date-select-day small-4 pull-left" ng-model="val.year" ng-options="y for y in years">',
          '<option value disabled selected>Year</option>',
        '</select>',      
        '<select ng-change="checkFormValidation()" ng-required="field.isRequired == true" class="sb-date-select-day small-4" ng-model="val.date", ng-options="d for d in dates">',
          '<option value disabled selected>Day</option>',
        '</select>',
        '<select ng-change="checkFormValidation()" ng-required="field.isRequired == true" class="sb-date-select-month small-4 pull-left" ng-model="val.month", ng-options="m.value as m.name for m in months">',
          '<option value disabled>Month</option>',
        '</select>',
      '</div>'
    ];

    return {
      restrict: 'A',
      replace: true,
      template: template.join(''),
      require: 'ngModel',
      scope: true,

      link: function(scope, elem, attrs, model) {
        scope.val = {};

        var min = scope.min = moment(attrs.min || '1900-01-01');
        var max = scope.max = moment(attrs.max); // Defaults to now

        scope.years = [];

        for (var i=max.years(); i>=min.years(); i--) {
          scope.years.push(i);
        }

        scope.$watch('val.year', function () {
          updateMonthOptions();
        });

        scope.$watchCollection('[val.month, val.year]', function () {
          updateDateOptions();
        });

        scope.$watchCollection('[val.date, val.month, val.year]', function () {
          if (scope.val.year && scope.val.month && scope.val.date) {
            var m = moment([scope.val.year, scope.val.month-1, scope.val.date]);
            model.$setViewValue(m.format('YYYY-MM-DD'));
          }
          else {
            model.$setViewValue();
          }
        });

        function updateMonthOptions () {
          // Values begin at 1 to permit easier boolean testing
          scope.months = [];

          var minMonth = scope.val.year && min.isSame([scope.val.year], 'year') ? min.month() : 0;
          var maxMonth = scope.val.year && max.isSame([scope.val.year], 'year') ? max.month() : 11;

          var monthNames = moment.months();

          for (var j=minMonth; j<=maxMonth; j++) {
            scope.months.push({
              name: monthNames[j],
              value: j+1
            });
          }

          if (scope.val.month-1 > maxMonth || scope.val.month-1 < minMonth) delete scope.val.month;
        }

        function updateDateOptions (year, month) {
          var minDate, maxDate;

          if (scope.val.year && scope.val.month && min.isSame([scope.val.year, scope.val.month-1], 'month')) {
            minDate = min.date();
          } else {
            minDate = 1;
          }

          if (scope.val.year && scope.val.month && max.isSame([scope.val.year, scope.val.month-1], 'month')) {
            maxDate = max.date();
          } else if (scope.val.year && scope.val.month) { 
            maxDate = moment([scope.val.year, scope.val.month-1]).daysInMonth();
          } else {
            maxDate = 31;
          }

          scope.dates = [];

          for (var i=minDate; i<=maxDate; i++) {
            scope.dates.push(i);
          }
          if (scope.val.date > scope.dates.length) delete scope.val.date;
        }

        // model -> view
        model.$render = function() {
          if (!model.$viewValue) return;

          var m = moment(model.$viewValue);

          // Always use a dot in ng-model attrs...
          scope.val = {
            year: m.year(),
            month: m.month()+1,
            date: m.date()
          };
        };
      }
    };
  }]);

angular.module('ps.inputTime', [])
.value('psInputTimeConfig', {
        minuteStep : 5,
        minDate : null,
        maxDate : null,
        fixedDay: true,
        format: 'hh:mma'
    })
.directive("psInputTime", ['$filter', 'psInputTimeConfig', '$parse', function($filter, psInputTimeConfig, $parse) {
    var temp12hr = '((0?[0-9])|(1[0-2]))(:|\s)([0-5][0-9])[apAP][mM]',
        temp24hr = '([01]?[0-9]|2[0-3])[:;][0-5][0-9]',
        temp24noColon = '(2[0-3]|[01]?[0-9])([0-5][0-9])';
    var customFloor = function(value, roundTo) {
        return Math.floor(value / psInputConfig.minuteStep) * psInputConfig.minuteStep;
    };
    var timeTest12hr = new RegExp('^' + temp12hr + '$', ['i']),
        timeTest24hr = new RegExp('^' + temp24hr + '$', ['i']),
        timeTest24noColon = new RegExp('^' + temp24noColon + '$', ['i']);
    return {
        restrict: "A",
        require: '?^ngModel',
        scope: {},
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model
            
            ngModel.$render = function () {
               
            };

            var minuteStep = getValue(attrs.minuteStep, psInputTimeConfig.minuteStep),
                fixedDay = getValue(attrs.fixedDay, psInputTimeConfig.fixedDay),
                timeFormat = attrs.format || psInputTimeConfig.format,
                maxDate = null,
                minDate = null;

            function getValue(value, defaultValue) {
                return angular.isDefined(value) ? scope.$parent.$eval(value) : defaultValue;
            }

            if(attrs.min || attrs.max){
                fixedDay = false;
            }

            function checkMinMaxValid(){
              if(minDate !== null && ngModel.$modelValue < minDate){
                  ngModel.$setValidity('time-min', false);
              }else if (minDate !== null) ngModel.$setValidity('time-min', true);
              
              if(maxDate !== null && ngModel.$modelValue > maxDate){
                  ngModel.$setValidity('time-max', false);
              } else if (maxDate !== null) ngModel.$setValidity('time-max', true);
                  
            }

            if (attrs.max) {
                scope.$parent.$watch($parse(attrs.max), function(value) {
                  maxDate = value ? new Date(value) : null;
                  checkMinMaxValid();
                });
            }

            if (attrs.min) {
                scope.$parent.$watch($parse(attrs.min), function(value) {
                  minDate = value ? new Date(value) : null;
                  checkMinMaxValid();
                });
            }
            
            var reservedKey = false;

            element.on('keydown', function(e) {
                reservedKey = false;
                switch (e.keyCode) {
                    case 37:
                        // left button hit
                        if(verifyFormat()){
                            tabBackward(e);
                            reservedKey = true;
                        }
                        break;
                    case 38:
                        // up button hit
                        if(verifyFormat()) {
                            addTime();
                            reservedKey = true;
                        }
                        break;
                    case 39:
                        // right button hit
                        if(verifyFormat()){
                            tabForward(e);
                            reservedKey = true;

                        }
                        break;
                    case 40:
                        // down button hit
                        if(verifyFormat()) {
                            subtractTime();
                            reservedKey = true;
                        }
                        break;
                    case 9:
                        // TAB

                        if(verifyFormat()){
                            if(e.shiftKey){
                                if(getSelectionPoint() != 'hour') {
                                    reservedKey = true;
                                    tabBackward(e);
                                }
                            } else{
                                if(getSelectionPoint() != 'meridian') {
                                    reservedKey = true;
                                    tabForward(e);
                                }
                            }
                        }
                        
                       
                        break;
                    default:
                        // e.preventDefault();
                        break;
                }
                if(reservedKey){
                    e.preventDefault();
                }
            }).on('keyup blur', function(){
                if(checkTimeFormat(element.val()) != 'invalid' && !reservedKey){
                    scope.$apply(function (){
                        ngModel.$setViewValue(createDateFromTime(element.val(), ngModel.$modelValue));
                    });
                }
                
            }).on('click', function() {

                selectTime(getSelectionPoint());

            });

            function verifyFormat(){
                if(checkTimeFormat( element.val() ) == '12hr') return true;
                else if (element.val() === ''){
                    element.val(formatter(getDefaultDate()));
                    ngModel.$setViewValue(getDefaultDate());
                    setTimeout(function() {
                        selectTime('hour');
                    }, 0);
                    return true;
                }
                else if (checkTimeFormat( element.val() ) != 'invalid') {
                    element.val(formatter(ngModel.$modelValue));
                    ngModel.$setViewValue(getDefaultDate());
                    setTimeout(function() {
                        selectTime('hour');
                    }, 0);
                    return true;
                } else return false;
            }

            function selectTime(part) {
                if (part == 'hour') {
                    setTimeout(function() {
                        element[0].setSelectionRange(0, 2);
                    }, 0);
                } else if (part == 'minute') {
                    setTimeout(function() {
                        element[0].setSelectionRange(3, 5);
                    }, 0);
                } else {
                    setTimeout(function() {
                        element[0].setSelectionRange(5, 7);
                    }, 0);
                }
            }

            function getSelectionPoint() {
                var pos = element.prop("selectionStart");
                if(element.val().length < 1){
                    return 'hour';
                }
                if (pos < 3) {
                    return 'hour';
                } else if (pos < 5) {
                    return 'minute';
                } else if (pos < 8) {
                    return 'meridian';
                } else return 'unknown';
            }

            function tabForward() {
                var cspot = getSelectionPoint();
                if (cspot == 'hour') {
                    selectTime('minute');
                } else if (cspot == 'minute') {
                    selectTime('meridian');
                } else {
                    selectTime('hour');
                }
            }

            function tabBackward(e) {
                var cspot = getSelectionPoint();
                if (cspot == 'meridian') {
                    selectTime('minute');
                    e.preventDefault();
                } else if (cspot == 'minute') {
                    selectTime('hour');
                    e.preventDefault();
                } else {
                    selectTime('meridian');
                }
            }
            
            function getDefaultDate(){
                if(minDate !== null) return new Date(minDate);
                else if (maxDate !== null) return new Date(maxDate);
                else return new Date();
            }

            function parser(value) {
               
                if(value){
                    if(angular.isDate(value)){
                        checkMinMaxValid();
                        ngModel.$setValidity('time', true);
                        
                        if(minDate !== null && value < minDate) value = minDate;
                        if(maxDate !== null && value > maxDate) value = maxDate;
                        
                        return value;
                        
                    } else{
                        
                        ngModel.$setValidity('time', false);
                        return ngModel.$modelValue;
                    }
                    
                }
                
            }
            
            ngModel.$parsers.push(parser);
            
            function formatter(value) {
                
                if (value) {
                    return $filter('date')(value, timeFormat);
                }
            }
            
            ngModel.$formatters.push(formatter);
            
            function createDateFromTime(time,cdate){
                if(isNaN(cdate)){
                    cdate = getDefaultDate();
                }
                var ct = checkTimeFormat(time),
                        minutes, hours, ampm, sHours, sMinutes;
                if(ct == '12hr'){
                    hours = Number(time.match(/^(\d+)/)[1]);
                    minutes = Number(time.match(/:(\d+)/)[1]);
                    AMPM = time.match(/[apAP][mM]/)[0];
                    if(AMPM.toUpperCase() == "PM" && hours<12) hours = hours+12;
                    if(AMPM.toUpperCase() == "AM" && hours==12) hours = hours-12;
                } else if (ct == '24hr'){
                    hours = time.split(/[;:]/)[0];
                    minutes = time.split(/[;:]/)[1];
                } else if (ct == '24nc') {
                    hours = time.length == 4 ? time.substr(0,2) : time.substr(0,1);
                    minutes = time.substr(-2);
                } else {
                    return 'invalid';
                }
                sHours = hours.toString();
                sMinutes = minutes.toString();
                if(hours<10) sHours = "0" + sHours;
                if(minutes<10) sMinutes = "0" + sMinutes;
                cdate.setHours(sHours,sMinutes);
                return new Date(cdate);
            }
            
            function checkTimeFormat(value){
                if(timeTest12hr.test(value)) return '12hr';
                else if (timeTest24hr.test(value)) return '24hr';
                else if (timeTest24noColon.test(value)) return '24nc';
                else return 'invalid';
            }
            
            
            function addTime() {
                var cPoint = getSelectionPoint();
                if (cPoint == 'hour') {
                    addMinutes(60);
                } else if (cPoint == 'minute') {
                     addMinutes(minuteStep);
                } else if (cPoint == 'meridian') {
                    if ((ngModel.$modelValue ? ngModel.$modelValue : getDefaultDate()).getHours > 12) {
                        addMinutes(-720);
                    } else {
                        addMinutes(720);
                    }
                }
                selectTime(cPoint);
            }

            function subtractTime() {
                var cPoint = getSelectionPoint();
                if (cPoint == 'hour') {
                    addMinutes(-60);
                } else if (cPoint == 'minute') {
                    addMinutes(-1);
                } else if (cPoint == 'meridian') {
                    if ((ngModel.$modelValue ? ngModel.$modelValue : getDefaultDate()).getHours > 12) {
                        addMinutes(720);
                    } else {
                        addMinutes(-720);
                    }
                }
                selectTime(cPoint);
            }
            
            function addMinutes(minutes){
                selected = ngModel.$modelValue ? new Date(ngModel.$modelValue) : getDefaultDate();
                dt = new Date(selected.getTime() + minutes * 60000);
                if(fixedDay === true || fixedDay == 'true'){
                    dt = selected.setHours(dt.getHours(), dt.getMinutes());
                    dt = new Date(dt);
                }
                scope.$apply(function (){
                    ngModel.$setViewValue(dt);
                });
                element.val(formatter(ngModel.$modelValue));
            }
        
        }
    };
}]);
/*!
 * angular-adaptive-detection v0.3.0
 * The MIT License
 * Copyright (c) 2014 Jan Antala <hello@janantala.com>
 */

!function(){"use strict";var t=angular.module("adaptive.detection",[]);t.provider("$detection",[function(){this.userAgent=navigator.userAgent,this.setUserAgent=function(t){this.userAgent=t},this.$get=function(){var t=this.userAgent;return{getUserAgent:function(){return t},isiOS:function(){return/(iPad|iPhone|iPod)/gi.test(t)},isAndroid:function(){return/(Android)/gi.test(t)},isWindowsPhone:function(){return/(IEMobile)/gi.test(t)}}}}])}();
/**
 * Created by Mariandi on 16/04/2014.
 *
 * This directive truncates the given text according to the specified length (n)
 * If words = true then the directive will display n number of words
 * If words = false then the directive will display n number of characters
 * If words flag is omitted then default behaviour is that words == true
 *
 */

/*global angular*/
var readMore = angular.module('readMore', []);

readMore.directive('readMore', function() {
    return {
        restrict: 'AE',
        replace: true,
        scope: {
            text: '=ngModel'
        },
        template:  "<p> {{text | readMoreFilter:[text, countingWords, textLength] }}" +
            "<a ng-show='showLinks' ng-click='changeLength()' >" +
            "<strong class='read-more' ng-show='isExpanded'>  Read less <i class='fa fa-level-up'></i></strong>" +
            "<strong class='read-more' ng-show='!isExpanded'>... Read more <i class='fa fa-level-down'></i></strong>" +
            "</a>" +
            "</p>",
        controller: ['$scope', '$attrs', '$element',
            function($scope, $attrs) {
                $scope.textLength = $attrs.length;
                $scope.isExpanded = false; // initialise extended status
                $scope.countingWords = $attrs.words !== undefined ? ($attrs.words === 'true') : true; //if this attr is not defined the we are counting words not characters

                if (!$scope.countingWords && $scope.text.length > $attrs.length) {
                    $scope.showLinks = true;
                } else if ($scope.countingWords && $scope.text.split(" ").length > $attrs.length) {
                    $scope.showLinks = true;
                } else {
                    $scope.showLinks = false;
                }

                $scope.changeLength = function (card) {
                    $scope.isExpanded = !$scope.isExpanded;
                    $scope.textLength = $scope.textLength !== $attrs.length ?  $attrs.length : $scope.text.length;
                };
            }]
    };
});
readMore.filter('readMoreFilter', function() {
    return function(str, args) {
        var strToReturn = str,
            length = str.length,
            foundWords = [],
            countingWords = (!!args[1]);

        if (!str || str === null) {
            // If no string is defined return the entire string and warn user of error
            console.log("Warning: Truncating text was not performed as no text was specified");
        }

        // Check length attribute
        if (!args[2] || args[2] === null) {
            // If no length is defined return the entire string and warn user of error
            console.log("Warning: Truncating text was not performed as no length was specified");
        } else if (typeof args[2] !== "number") { // if parameter is a string then cast it to a number
            length = Number(args[2]);
        }

        if (length <= 0) {
            return "";
        }


        if (str) {
            if (countingWords) { // Count words

                foundWords = str.split(/\s+/);

                if (foundWords.length > length) {
                    strToReturn = foundWords.slice(0, length).join(' ');
                }

            } else {  // Count characters

                if (str.length > length) {
                    strToReturn = str.slice(0, length);
                }

            }
        }

        return strToReturn;
    };
});
angular.module('simplifield.dragndrop', [
]).factory('sfDragNDropService', function DragNDropService() {
  var SESSION_KEYS = [
    'item', 'target', 'previous',
    'itemIndex', 'targetIndex', 'previousIndex'
  ];
  sfDragNDropService = {
    session: {},
    reset: function reset() {
      Object.keys(sfDragNDropService.session).forEach(function(key) {
        if('type' == key) {
          sfDragNDropService.session.type = '';
        } else if (-1 !== SESSION_KEYS.indexOf(key)) {
          sfDragNDropService.session[key] = null;
        } else {
          delete sfDragNDropService.session[key];
        }
      });
    }
  };
  sfDragNDropService.reset();

  return sfDragNDropService;
}).directive("sfDrop", ['$parse', 'sfDragNDropService',
  function DropDirective($parse, sfDragNDropService) {

  return {
    restrict: 'A',
    link: function($scope, element, attrs) {
      // Keep a ref to the dragged element
     	var item = $parse(attrs.sfDrop);
      // Setting callbacks
      var onDropCallback = $parse(attrs.sfOnDrop);
      var onDragEnterCallback = $parse(attrs.sfOnDragEnter);
      var onDragLeaveCallback = $parse(attrs.sfOnDragLeave);
      var onDragOverCallback = $parse(attrs.sfOnDragOver);
      // Bind drag events
      element.bind('dragenter', function(evt) {
        if(sfDragNDropService.session.type !== (attrs.sfDragType || 'all')) {
          return;
        }
        evt.preventDefault();
        sfDragNDropService.session.target = item($scope);
        sfDragNDropService.session.targetIndex = onDragEnterCallback($scope, {
          $type: sfDragNDropService.session.type,
          $item: sfDragNDropService.session.item,
          $itemIndex: sfDragNDropService.session.itemIndex,
          $target: sfDragNDropService.session.target,
          $targetIndex: sfDragNDropService.session.targetIndex,
          $session: sfDragNDropService.session
        });
        $scope.$apply();
      });
      element.bind('dragleave', function(evt) {
        if(sfDragNDropService.session.type !== (attrs.sfDragType || 'all')) {
          return;
        }
        evt.preventDefault();
        sfDragNDropService.session.previous = item($scope);
        sfDragNDropService.session.previousIndex = sfDragNDropService.session.targetIndex;
        onDragEnterCallback($scope, {
          $type: sfDragNDropService.session.type,
          $item: sfDragNDropService.session.item,
          $itemIndex: sfDragNDropService.session.itemIndex,
          $previous: sfDragNDropService.session.previous,
          $previousIndex: sfDragNDropService.session.previousIndex,
          $session: sfDragNDropService.session
        });
        
        $scope.$apply();
      });
      element.bind('dragover', function(evt) {
        if(sfDragNDropService.session.type !== (attrs.sfDragType || 'all')) {
          return;
        }
        evt.preventDefault();
        sfDragNDropService.session.target = item($scope);
        onDragOverCallback($scope, {
          $type: sfDragNDropService.session.type,
          $item: sfDragNDropService.session.item,
          $itemIndex: sfDragNDropService.session.itemIndex,
          $target: sfDragNDropService.session.target,
          $targetIndex: sfDragNDropService.session.targetIndex,
          $previous: sfDragNDropService.session.target,
          $previousIndex: sfDragNDropService.session.targetIndex,
          $session: sfDragNDropService.session
        });
        $scope.$apply();
      });
      element.bind('drop', function(evt) {
        if(sfDragNDropService.session.type !== (attrs.sfDragType || 'all')) {
          return;
        }
        evt.preventDefault();
        sfDragNDropService.session.target = item($scope);
        onDropCallback($scope, {
          $type: sfDragNDropService.session.type,
          $item: sfDragNDropService.session.item,
          $itemIndex: sfDragNDropService.session.itemIndex,
          $target: sfDragNDropService.session.target,
          $targetIndex: sfDragNDropService.session.targetIndex,
          $previous: sfDragNDropService.session.previous,
          $previousIndex: sfDragNDropService.session.previousIndex,
          $session: sfDragNDropService.session
        });
        $scope.$apply();
      });
    }
  };

}]).directive('sfDrag', ['$parse', 'sfDragNDropService',
  function DragDirective($parse, sfDragNDropService) {

  return {
    restrict: 'A',
    link: function($scope, element, attrs)  {
      // Keep a ref to the dragged model value
     	var item = $parse(attrs.sfDrag);

      // Try to get dragged datas
     	var getDragData = $parse(attrs.sfDragData);

      // Setting callbacks
      var dragGenerator = attrs.sfDragGenerator ?
        $parse(attrs.sfDragGenerator) :
        null;

      // Setting callbacks
      var onDragCallback = $parse(attrs.sfOnDrag);
      var onDragEndCallback = $parse(attrs.sfOnDragEnd);

      // Make the element draggable
      attrs.$set('draggable', 'true');

      // Bind drag events
      element.bind('dragstart', function(evt) {
        var draggedItem = item($scope);
        function computeDragItem() {
           var itemIndex = onDragCallback($scope, {
            $item: draggedItem,
            $session: sfDragNDropService.session
          });
          if(-1 !== itemIndex) {
            sfDragNDropService.session.itemIndex = itemIndex;
            sfDragNDropService.session.item = draggedItem;
            sfDragNDropService.session.type = attrs.sfDragType || 'all';
            sfDragNDropService.session.mime = attrs.sfDragMime || 'text/plain';
            sfDragNDropService.session.data = getDragData($scope) || '' + itemIndex;
            evt.stopPropagation();
            (evt.dataTransfer || evt.originalEvent.dataTransfer)
              .setData(sfDragNDropService.session.mime, sfDragNDropService.session.data);
            (evt.dataTransfer || evt.originalEvent.dataTransfer)
              .effectAllowed = attrs.sfDragEffect || 'move';
          }
        }
        if(dragGenerator) {
          draggedItem = dragGenerator($scope, {
            $type: attrs.sfDragType || 'all',
            $item: draggedItem
          });
        }
        if(draggedItem.then) {
          draggedItem.then(function(value) {
            draggedItem = value;
            computeDragItem();
          });
        } else {
          computeDragItem();
        }
        $scope.$apply();
      });

      element.bind('dragend', function(evt) {
        onDragEndCallback($scope, {
          $type: sfDragNDropService.session.type,
          $item: sfDragNDropService.session.item,
          $itemIndex: sfDragNDropService.session.itemIndex,
          $previous: sfDragNDropService.session.previous,
          $previousIndex: sfDragNDropService.session.previousIndex,
          $session: sfDragNDropService.session
        });
        sfDragNDropService.reset();
        $scope.$apply();
      });
    }
  };
}]);

angular.module('sticky', [])

.directive('sticky', ['$timeout', function($timeout){
	return {
		restrict: 'A',
		scope: {
			offset: '@',
		},
		link: function($scope, $elem, $attrs){
			$timeout(function(){
				var offsetTop = $scope.offset || 0,
					$window = angular.element(window),
					doc = document.documentElement,
					initialPositionStyle = $elem.css('position'),
					stickyLine,
					scrollTop;


				// Set the top offset
				//
				$elem.css('top', offsetTop+'px');
				$elem.css('z-index', '9900');



				// Get the sticky line
				//
				function setInitial(){
					stickyLine = $elem[0].offsetTop - offsetTop;
					checkSticky();
				}

				// Check if the window has passed the sticky line
				//
				function checkSticky(){
					scrollTop = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

					if ( scrollTop >= stickyLine ){						
						$elem.css('position', 'fixed');
						$('#headerFiller').css('height', '20px' ) ;

					} else {
						$elem.css('position', initialPositionStyle);
						$('#headerFiller').css('height', 'auto');
					}
				}


				// Handle the resize event
				//
				function resize(){
					$elem.css('position', initialPositionStyle);
					$timeout(setInitial);
				}


				// Attach our listeners
				//
				$window.on('scroll', checkSticky);
				$window.on('resize', resize);
				
				setInitial();
			});
		},
	};
}]);

/*
 * angular-elastic v2.4.2
 * (c) 2014 Monospaced http://monospaced.com
 * License: MIT
 */

angular.module('monospaced.elastic', [])

  .constant('msdElasticConfig', {
    append: ''
  })

  .directive('msdElastic', [
    '$timeout', '$window', 'msdElasticConfig',
    function($timeout, $window, config) {
      'use strict';

      return {
        require: 'ngModel',
        restrict: 'A, C',
        link: function(scope, element, attrs, ngModel) {

          // cache a reference to the DOM element
          var ta = element[0],
              $ta = element;

          // ensure the element is a textarea, and browser is capable
          if (ta.nodeName !== 'TEXTAREA' || !$window.getComputedStyle) {
            return;
          }

          // set these properties before measuring dimensions
          $ta.css({
            'overflow': 'hidden',
            'overflow-y': 'hidden',
            'word-wrap': 'break-word'
          });

          // force text reflow
          var text = ta.value;
          ta.value = '';
          ta.value = text;

          var append = attrs.msdElastic ? attrs.msdElastic.replace(/\\n/g, '\n') : config.append,
              $win = angular.element($window),
              mirrorInitStyle = 'position: absolute; top: -999px; right: auto; bottom: auto;' +
                                'left: 0; overflow: hidden; -webkit-box-sizing: content-box;' +
                                '-moz-box-sizing: content-box; box-sizing: content-box;' +
                                'min-height: 0 !important; height: 0 !important; padding: 0;' +
                                'word-wrap: break-word; border: 0;',
              $mirror = angular.element('<textarea aria-hidden="true" tabindex="-1" ' +
                                        'style="' + mirrorInitStyle + '"/>').data('elastic', true),
              mirror = $mirror[0],
              taStyle = getComputedStyle(ta),
              resize = taStyle.getPropertyValue('resize'),
              borderBox = taStyle.getPropertyValue('box-sizing') === 'border-box' ||
                          taStyle.getPropertyValue('-moz-box-sizing') === 'border-box' ||
                          taStyle.getPropertyValue('-webkit-box-sizing') === 'border-box',
              boxOuter = !borderBox ? {width: 0, height: 0} : {
                            width:  parseInt(taStyle.getPropertyValue('border-right-width'), 10) +
                                    parseInt(taStyle.getPropertyValue('padding-right'), 10) +
                                    parseInt(taStyle.getPropertyValue('padding-left'), 10) +
                                    parseInt(taStyle.getPropertyValue('border-left-width'), 10),
                            height: parseInt(taStyle.getPropertyValue('border-top-width'), 10) +
                                    parseInt(taStyle.getPropertyValue('padding-top'), 10) +
                                    parseInt(taStyle.getPropertyValue('padding-bottom'), 10) +
                                    parseInt(taStyle.getPropertyValue('border-bottom-width'), 10)
                          },
              minHeightValue = parseInt(taStyle.getPropertyValue('min-height'), 10),
              heightValue = parseInt(taStyle.getPropertyValue('height'), 10),
              minHeight = Math.max(minHeightValue, heightValue) - boxOuter.height,
              maxHeight = parseInt(taStyle.getPropertyValue('max-height'), 10),
              mirrored,
              active,
              copyStyle = ['font-family',
                           'font-size',
                           'font-weight',
                           'font-style',
                           'letter-spacing',
                           'line-height',
                           'text-transform',
                           'word-spacing',
                           'text-indent'];

          // exit if elastic already applied (or is the mirror element)
          if ($ta.data('elastic')) {
            return;
          }

          // Opera returns max-height of -1 if not set
          maxHeight = maxHeight && maxHeight > 0 ? maxHeight : 9e4;

          // append mirror to the DOM
          if (mirror.parentNode !== document.body) {
            angular.element(document.body).append(mirror);
          }

          // set resize and apply elastic
          $ta.css({
            'resize': (resize === 'none' || resize === 'vertical') ? 'none' : 'horizontal'
          }).data('elastic', true);

          /*
           * methods
           */

          function initMirror() {
            var mirrorStyle = mirrorInitStyle;

            mirrored = ta;
            // copy the essential styles from the textarea to the mirror
            taStyle = getComputedStyle(ta);
            angular.forEach(copyStyle, function(val) {
              mirrorStyle += val + ':' + taStyle.getPropertyValue(val) + ';';
            });
            mirror.setAttribute('style', mirrorStyle);
          }

          function adjust() {
            var taHeight,
                taComputedStyleWidth,
                mirrorHeight,
                width,
                overflow;

            if (mirrored !== ta) {
              initMirror();
            }

            // active flag prevents actions in function from calling adjust again
            if (!active) {
              active = true;

              mirror.value = ta.value + append; // optional whitespace to improve animation
              mirror.style.overflowY = ta.style.overflowY;

              taHeight = ta.style.height === '' ? 'auto' : parseInt(ta.style.height, 10);

              taComputedStyleWidth = getComputedStyle(ta).getPropertyValue('width');

              // ensure getComputedStyle has returned a readable 'used value' pixel width
              if (taComputedStyleWidth.substr(taComputedStyleWidth.length - 2, 2) === 'px') {
                // update mirror width in case the textarea width has changed
                width = parseInt(taComputedStyleWidth, 10) - boxOuter.width;
                mirror.style.width = width + 'px';
              }

              mirrorHeight = mirror.scrollHeight;

              if (mirrorHeight > maxHeight) {
                mirrorHeight = maxHeight;
                overflow = 'scroll';
              } else if (mirrorHeight < minHeight) {
                mirrorHeight = minHeight;
              }
              mirrorHeight += boxOuter.height;
              ta.style.overflowY = overflow || 'hidden';

              if (taHeight !== mirrorHeight) {
                ta.style.height = mirrorHeight + 'px';
                scope.$emit('elastic:resize', $ta);
              }

              // small delay to prevent an infinite loop
              $timeout(function() {
                active = false;
              }, 1);

            }
          }

          function forceAdjust() {
            active = false;
            adjust();
          }

          /*
           * initialise
           */

          // listen
          if ('onpropertychange' in ta && 'oninput' in ta) {
            // IE9
            ta['oninput'] = ta.onkeyup = adjust;
          } else {
            ta['oninput'] = adjust;
          }

          $win.bind('resize', forceAdjust);

          scope.$watch(function() {
            return ngModel.$modelValue;
          }, function(newValue) {
            forceAdjust();
          });

          scope.$on('elastic:adjust', function() {
            initMirror();
            forceAdjust();
          });

          $timeout(adjust);

          /*
           * destroy
           */

          scope.$on('$destroy', function() {
            $mirror.remove();
            $win.unbind('resize', forceAdjust);
          });
        }
      };
    }
  ]);
/*! ngStorage 0.3.0 | Copyright (c) 2013 Gias Kay Lee | MIT License */"use strict";!function(){function a(a){return["$rootScope","$window",function(b,c){for(var d,e,f,g=c[a]||(console.warn("This browser does not support Web Storage!"),{}),h={$default:function(a){for(var b in a)angular.isDefined(h[b])||(h[b]=a[b]);return h},$reset:function(a){for(var b in h)"$"===b[0]||delete h[b];return h.$default(a)}},i=0;i<g.length;i++)(f=g.key(i))&&"ngStorage-"===f.slice(0,10)&&(h[f.slice(10)]=angular.fromJson(g.getItem(f)));return d=angular.copy(h),b.$watch(function(){e||(e=setTimeout(function(){if(e=null,!angular.equals(h,d)){angular.forEach(h,function(a,b){angular.isDefined(a)&&"$"!==b[0]&&g.setItem("ngStorage-"+b,angular.toJson(a)),delete d[b]});for(var a in d)g.removeItem("ngStorage-"+a);d=angular.copy(h)}},100))}),"localStorage"===a&&c.addEventListener&&c.addEventListener("storage",function(a){"ngStorage-"===a.key.slice(0,10)&&(a.newValue?h[a.key.slice(10)]=angular.fromJson(a.newValue):delete h[a.key.slice(10)],d=angular.copy(h),b.$apply())}),h}]}angular.module("ngStorage",[]).factory("$localStorage",a("localStorage")).factory("$sessionStorage",a("sessionStorage"))}();
!function(e,t){"use strict";function r(e){var t,r={},n=e.split(",");for(t=0;t<n.length;t++)r[n[t]]=!0;return r}function n(e,r){function n(e,n,i,l){if(n=t.lowercase(n),z[n])for(;w.last()&&$[w.last()];)s("",w.last());y[n]&&w.last()==n&&s("",n),l=v[n]||!!l,l||w.push(n);var c={};i.replace(f,function(e,t,r,n,s){var i=r||n||s||"";c[t]=a(i)}),r.start&&r.start(n,c,l)}function s(e,n){var a,s=0;if(n=t.lowercase(n))for(s=w.length-1;s>=0&&w[s]!=n;s--);if(s>=0){for(a=w.length-1;a>=s;a--)r.end&&r.end(w[a]);w.length=s}}var i,c,b,w=[],x=e;for(w.last=function(){return w[w.length-1]};e;){if(c=!0,w.last()&&E[w.last()])e=e.replace(new RegExp("(.*)<\\s*\\/\\s*"+w.last()+"[^>]*>","i"),function(e,t){return t=t.replace(d,"$1").replace(m,"$1"),r.chars&&r.chars(a(t)),""}),s("",w.last());else if(0===e.indexOf("<!--")?(i=e.indexOf("--",4),i>=0&&e.lastIndexOf("-->",i)===i&&(r.comment&&r.comment(e.substring(4,i)),e=e.substring(i+3),c=!1)):g.test(e)?(b=e.match(g),b&&(e=e.replace(b[0],""),c=!1)):h.test(e)?(b=e.match(u),b&&(e=e.substring(b[0].length),b[0].replace(u,s),c=!1)):p.test(e)&&(b=e.match(o),b&&(e=e.substring(b[0].length),b[0].replace(o,n),c=!1)),c){i=e.indexOf("<");var k=0>i?e:e.substring(0,i);e=0>i?"":e.substring(i),r.chars&&r.chars(a(k))}if(e==x)throw l("badparse","The sanitizer was unable to parse the following block of html: {0}",e);x=e}s()}function a(e){return C.innerHTML=e.replace(/</g,"&lt;"),C.innerText||C.textContent||""}function s(e){return e.replace(/&/g,"&amp;").replace(w,function(e){return"&#"+e.charCodeAt(0)+";"}).replace(/</g,"&lt;").replace(/>/g,"&gt;")}function i(e){var r=!1,n=t.bind(e,e.push);return{start:function(e,a,i){e=t.lowercase(e),!r&&E[e]&&(r=e),r||O[e]!==!0||(n("<"),n(e),t.forEach(a,function(e,r){var a=t.lowercase(r);A[a]!==!0||T[a]===!0&&!e.match(b)||(n(" "),n(r),n('="'),n(s(e)),n('"'))}),n(i?"/>":">"))},end:function(e){e=t.lowercase(e),r||O[e]!==!0||(n("</"),n(e),n(">")),e==r&&(r=!1)},chars:function(e){r||n(s(e))}}}var l=t.$$minErr("$sanitize"),c=function(e){var t=[];return n(e,i(t)),t.join("")},o=/^<\s*([\w:-]+)((?:\s+[\w:-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)\s*>/,u=/^<\s*\/\s*([\w:-]+)[^>]*>/,f=/([\w:-]+)(?:\s*=\s*(?:(?:"((?:[^"])*)")|(?:'((?:[^'])*)')|([^>\s]+)))?/g,p=/^</,h=/^<\s*\//,d=/<!--(.*?)-->/g,g=/<!DOCTYPE([^>]*?)>/i,m=/<!\[CDATA\[(.*?)]]>/g,b=/^((ftp|https?):\/\/|mailto:|tel:|#)/i,w=/([^\#-~| |!])/g,v=r("area,br,col,hr,img,wbr"),x=r("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr"),k=r("rp,rt"),y=t.extend({},k,x),z=t.extend({},x,r("address,article,aside,blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6,header,hgroup,hr,ins,map,menu,nav,ol,pre,script,section,table,ul")),$=t.extend({},k,r("a,abbr,acronym,b,bdi,bdo,big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small,span,strike,strong,sub,sup,time,tt,u,var")),E=r("script,style"),O=t.extend({},v,z,$,y),T=r("background,cite,href,longdesc,src,usemap"),A=t.extend({},T,r("abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,scope,scrolling,shape,span,start,summary,target,title,type,valign,value,vspace,width")),C=document.createElement("pre");t.module("ngSanitize",[]).value("$sanitize",c),t.module("ngSanitize").filter("linky",function(){var e=/((ftp|https?):\/\/|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>]/,r=/^mailto:/;return function(n,a){if(!n)return n;var s,l,c,o=n,u=[],f=i(u),p={};for(t.isDefined(a)&&(p.target=a);s=o.match(e);)l=s[0],s[2]==s[3]&&(l="mailto:"+l),c=s.index,f.chars(o.substr(0,c)),p.href=l,f.start("a",p),f.chars(s[0].replace(r,"")),f.end("a"),o=o.substring(c+s[0].length);return f.chars(o),u.join("")}})}(window,window.angular);
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){var S,app,createFilterFor,name,_i,_j,_len,_len1,_ref,_ref1,__slice=[].slice;S=require("string");app=angular.module("string",[]);app.factory("string",function(){return S});createFilterFor=function(name,returnsStringObject){var filterName;filterName=S("string-"+name).camelize().toString();return app.filter(filterName,["string",function(S){return function(){var args,input,out,_ref;input=arguments[0],args=2<=arguments.length?__slice.call(arguments,1):[];out=(_ref=S(input))[name].apply(_ref,args);if(returnsStringObject){out=out.toString()}return out}}])};_ref=["between","camelize","capitalize","chompLeft","chompRight","collapseWhitespace","dasherize","decodeHTMLEntities","escapeHTML","ensureLeft","ensureRight","humanize","left","pad","padLeft","padRight","repeat","replaceAll","right","slugify","stripPunctuation","stripTags","times","toCSV","trim","trimLeft","trimRight","truncate","underscore","unescapeHTML"];for(_i=0,_len=_ref.length;_i<_len;_i++){name=_ref[_i];createFilterFor(name,true)}_ref1=["contains","count","endsWith","include","isAlpha","isAlphaNumeric","isEmpty","isLower","isNumeric","isUpper","lines","parseCSV","startsWith","toBoolean","toBool","toFloat","toInt","toInteger"];for(_j=0,_len1=_ref1.length;_j<_len1;_j++){name=_ref1[_j];createFilterFor(name,false)}},{string:2}],2:[function(require,module,exports){!function(){"use strict";var VERSION="1.7.0";var ENTITIES={};function initialize(object,s){if(s!==null&&s!==undefined){if(typeof s==="string")object.s=s;else object.s=s.toString()}else{object.s=s}object.orig=s;if(s!==null&&s!==undefined){if(object.__defineGetter__){object.__defineGetter__("length",function(){return object.s.length})}else{object.length=s.length}}else{object.length=-1}}function S(s){initialize(this,s)}var __nsp=String.prototype;var __sp=S.prototype={between:function(left,right){var s=this.s;var startPos=s.indexOf(left);var endPos=s.indexOf(right);var start=startPos+left.length;return new this.constructor(endPos>startPos?s.slice(start,endPos):"")},camelize:function(){var s=this.trim().s.replace(/(\-|_|\s)+(.)?/g,function(mathc,sep,c){return c?c.toUpperCase():""});return new this.constructor(s)},capitalize:function(){return new this.constructor(this.s.substr(0,1).toUpperCase()+this.s.substring(1).toLowerCase())},charAt:function(index){return this.s.charAt(index)},chompLeft:function(prefix){var s=this.s;if(s.indexOf(prefix)===0){s=s.slice(prefix.length);return new this.constructor(s)}else{return this}},chompRight:function(suffix){if(this.endsWith(suffix)){var s=this.s;s=s.slice(0,s.length-suffix.length);return new this.constructor(s)}else{return this}},collapseWhitespace:function(){var s=this.s.replace(/[\s\xa0]+/g," ").replace(/^\s+|\s+$/g,"");return new this.constructor(s)},contains:function(ss){return this.s.indexOf(ss)>=0},count:function(ss){var count=0,pos=this.s.indexOf(ss);while(pos>=0){count+=1;pos=this.s.indexOf(ss,pos+1)}return count},dasherize:function(){var s=this.trim().s.replace(/[_\s]+/g,"-").replace(/([A-Z])/g,"-$1").replace(/-+/g,"-").toLowerCase();return new this.constructor(s)},decodeHtmlEntities:function(){var s=this.s;s=s.replace(/&#(\d+);?/g,function(_,code){return String.fromCharCode(code)}).replace(/&#[xX]([A-Fa-f0-9]+);?/g,function(_,hex){return String.fromCharCode(parseInt(hex,16))}).replace(/&([^;\W]+;?)/g,function(m,e){var ee=e.replace(/;$/,"");var target=ENTITIES[e]||e.match(/;$/)&&ENTITIES[ee];if(typeof target==="number"){return String.fromCharCode(target)}else if(typeof target==="string"){return target}else{return m}});return new this.constructor(s)},endsWith:function(suffix){var l=this.s.length-suffix.length;return l>=0&&this.s.indexOf(suffix,l)===l},escapeHTML:function(){return new this.constructor(this.s.replace(/[&<>"']/g,function(m){return"&"+reversedEscapeChars[m]+";"}))},ensureLeft:function(prefix){var s=this.s;if(s.indexOf(prefix)===0){return this}else{return new this.constructor(prefix+s)}},ensureRight:function(suffix){var s=this.s;if(this.endsWith(suffix)){return this}else{return new this.constructor(s+suffix)}},humanize:function(){if(this.s===null||this.s===undefined)return new this.constructor("");var s=this.underscore().replace(/_id$/,"").replace(/_/g," ").trim().capitalize();return new this.constructor(s)},isAlpha:function(){return!/[^a-z\xC0-\xFF]/.test(this.s.toLowerCase())},isAlphaNumeric:function(){return!/[^0-9a-z\xC0-\xFF]/.test(this.s.toLowerCase())},isEmpty:function(){return this.s===null||this.s===undefined?true:/^[\s\xa0]*$/.test(this.s)},isLower:function(){return this.isAlpha()&&this.s.toLowerCase()===this.s},isNumeric:function(){return!/[^0-9]/.test(this.s)},isUpper:function(){return this.isAlpha()&&this.s.toUpperCase()===this.s},left:function(N){if(N>=0){var s=this.s.substr(0,N);return new this.constructor(s)}else{return this.right(-N)}},lines:function(){return this.replaceAll("\r\n","\n").s.split("\n")},pad:function(len,ch){if(ch==null)ch=" ";if(this.s.length>=len)return new this.constructor(this.s);len=len-this.s.length;var left=Array(Math.ceil(len/2)+1).join(ch);var right=Array(Math.floor(len/2)+1).join(ch);return new this.constructor(left+this.s+right)},padLeft:function(len,ch){if(ch==null)ch=" ";if(this.s.length>=len)return new this.constructor(this.s);return new this.constructor(Array(len-this.s.length+1).join(ch)+this.s)},padRight:function(len,ch){if(ch==null)ch=" ";if(this.s.length>=len)return new this.constructor(this.s);return new this.constructor(this.s+Array(len-this.s.length+1).join(ch))},parseCSV:function(delimiter,qualifier,escape,lineDelimiter){delimiter=delimiter||",";escape=escape||"\\";if(typeof qualifier=="undefined")qualifier='"';var i=0,fieldBuffer=[],fields=[],len=this.s.length,inField=false,self=this;var ca=function(i){return self.s.charAt(i)};if(typeof lineDelimiter!=="undefined")var rows=[];if(!qualifier)inField=true;while(i<len){var current=ca(i);switch(current){case escape:if(inField&&(escape!==qualifier||ca(i+1)===qualifier)){i+=1;fieldBuffer.push(ca(i));break}if(escape!==qualifier)break;case qualifier:inField=!inField;break;case delimiter:if(inField&&qualifier)fieldBuffer.push(current);else{fields.push(fieldBuffer.join(""));fieldBuffer.length=0}break;case lineDelimiter:if(inField){fieldBuffer.push(current)}else{if(rows){fields.push(fieldBuffer.join(""));rows.push(fields);fields=[];fieldBuffer.length=0}}break;default:if(inField)fieldBuffer.push(current);break}i+=1}fields.push(fieldBuffer.join(""));if(rows){rows.push(fields);return rows}return fields},replaceAll:function(ss,r){var s=this.s.split(ss).join(r);return new this.constructor(s)},right:function(N){if(N>=0){var s=this.s.substr(this.s.length-N,N);return new this.constructor(s)}else{return this.left(-N)}},setValue:function(s){initialize(this,s);return this},slugify:function(){var sl=new S(this.s.replace(/[^\w\s-]/g,"").toLowerCase()).dasherize().s;if(sl.charAt(0)==="-")sl=sl.substr(1);return new this.constructor(sl)},startsWith:function(prefix){return this.s.lastIndexOf(prefix,0)===0},stripPunctuation:function(){return new this.constructor(this.s.replace(/[^\w\s]|_/g,"").replace(/\s+/g," "))},stripTags:function(){var s=this.s,args=arguments.length>0?arguments:[""];multiArgs(args,function(tag){s=s.replace(RegExp("</?"+tag+"[^<>]*>","gi"),"")});return new this.constructor(s)},template:function(values,opening,closing){var s=this.s;var opening=opening||Export.TMPL_OPEN;var closing=closing||Export.TMPL_CLOSE;var open=opening.replace(/[-[\]()*\s]/g,"\\$&").replace(/\$/g,"\\$");var close=closing.replace(/[-[\]()*\s]/g,"\\$&").replace(/\$/g,"\\$");var r=new RegExp(open+"(.+?)"+close,"g");var matches=s.match(r)||[];matches.forEach(function(match){var key=match.substring(opening.length,match.length-closing.length);if(typeof values[key]!="undefined")s=s.replace(match,values[key])});return new this.constructor(s)},times:function(n){return new this.constructor(new Array(n+1).join(this.s))},toBoolean:function(){if(typeof this.orig==="string"){var s=this.s.toLowerCase();return s==="true"||s==="yes"||s==="on"}else return this.orig===true||this.orig===1},toFloat:function(precision){var num=parseFloat(this.s);if(precision)return parseFloat(num.toFixed(precision));else return num},toInt:function(){return/^\s*-?0x/i.test(this.s)?parseInt(this.s,16):parseInt(this.s,10)},trim:function(){var s;if(typeof __nsp.trim==="undefined")s=this.s.replace(/(^\s*|\s*$)/g,"");else s=this.s.trim();return new this.constructor(s)},trimLeft:function(){var s;if(__nsp.trimLeft)s=this.s.trimLeft();else s=this.s.replace(/(^\s*)/g,"");return new this.constructor(s)},trimRight:function(){var s;if(__nsp.trimRight)s=this.s.trimRight();else s=this.s.replace(/\s+$/,"");return new this.constructor(s)},truncate:function(length,pruneStr){var str=this.s;length=~~length;pruneStr=pruneStr||"...";if(str.length<=length)return new this.constructor(str);var tmpl=function(c){return c.toUpperCase()!==c.toLowerCase()?"A":" "},template=str.slice(0,length+1).replace(/.(?=\W*\w*$)/g,tmpl);if(template.slice(template.length-2).match(/\w\w/))template=template.replace(/\s*\S+$/,"");else template=new S(template.slice(0,template.length-1)).trimRight().s;return(template+pruneStr).length>str.length?new S(str):new S(str.slice(0,template.length)+pruneStr)},toCSV:function(){var delim=",",qualifier='"',escape="\\",encloseNumbers=true,keys=false;var dataArray=[];function hasVal(it){return it!==null&&it!==""}if(typeof arguments[0]==="object"){delim=arguments[0].delimiter||delim;delim=arguments[0].separator||delim;qualifier=arguments[0].qualifier||qualifier;encloseNumbers=!!arguments[0].encloseNumbers;escape=arguments[0].escape||escape;keys=!!arguments[0].keys}else if(typeof arguments[0]==="string"){delim=arguments[0]}if(typeof arguments[1]==="string")qualifier=arguments[1];if(arguments[1]===null)qualifier=null;if(this.orig instanceof Array)dataArray=this.orig;else{for(var key in this.orig)if(this.orig.hasOwnProperty(key))if(keys)dataArray.push(key);else dataArray.push(this.orig[key])}var rep=escape+qualifier;var buildString=[];for(var i=0;i<dataArray.length;++i){var shouldQualify=hasVal(qualifier);if(typeof dataArray[i]=="number")shouldQualify&=encloseNumbers;if(shouldQualify)buildString.push(qualifier);if(dataArray[i]!==null&&dataArray[i]!==undefined){var d=new S(dataArray[i]).replaceAll(qualifier,rep).s;buildString.push(d)}else buildString.push("");if(shouldQualify)buildString.push(qualifier);if(delim)buildString.push(delim)}buildString.length=buildString.length-1;return new this.constructor(buildString.join(""))},toString:function(){return this.s},underscore:function(){var s=this.trim().s.replace(/([a-z\d])([A-Z]+)/g,"$1_$2").replace(/[-\s]+/g,"_").toLowerCase();if(new S(this.s.charAt(0)).isUpper()){s="_"+s}return new this.constructor(s)},unescapeHTML:function(){return new this.constructor(this.s.replace(/\&([^;]+);/g,function(entity,entityCode){var match;if(entityCode in escapeChars){return escapeChars[entityCode]}else if(match=entityCode.match(/^#x([\da-fA-F]+)$/)){return String.fromCharCode(parseInt(match[1],16))}else if(match=entityCode.match(/^#(\d+)$/)){return String.fromCharCode(~~match[1])}else{return entity}}))},valueOf:function(){return this.s.valueOf()}};var methodsAdded=[];function extendPrototype(){for(var name in __sp){(function(name){var func=__sp[name];if(!__nsp.hasOwnProperty(name)){methodsAdded.push(name);__nsp[name]=function(){String.prototype.s=this;return func.apply(this,arguments)}}})(name)}}function restorePrototype(){for(var i=0;i<methodsAdded.length;++i)delete String.prototype[methodsAdded[i]];methodsAdded.length=0}var nativeProperties=getNativeStringProperties();for(var name in nativeProperties){(function(name){var stringProp=__nsp[name];if(typeof stringProp=="function"){if(!__sp[name]){if(nativeProperties[name]==="string"){__sp[name]=function(){return new this.constructor(stringProp.apply(this,arguments))}}else{__sp[name]=stringProp}}}})(name)}__sp.repeat=__sp.times;__sp.include=__sp.contains;__sp.toInteger=__sp.toInt;__sp.toBool=__sp.toBoolean;__sp.decodeHTMLEntities=__sp.decodeHtmlEntities;__sp.constructor=S;function getNativeStringProperties(){var names=getNativeStringPropertyNames();var retObj={};for(var i=0;i<names.length;++i){var name=names[i];var func=__nsp[name];try{var type=typeof func.apply("teststring",[]);retObj[name]=type}catch(e){}}return retObj}function getNativeStringPropertyNames(){var results=[];if(Object.getOwnPropertyNames){results=Object.getOwnPropertyNames(__nsp);results.splice(results.indexOf("valueOf"),1);results.splice(results.indexOf("toString"),1);return results}else{var stringNames={};var objectNames=[];for(var name in String.prototype)stringNames[name]=name;for(var name in Object.prototype)delete stringNames[name];for(var name in stringNames){results.push(name)}return results}}function Export(str){return new S(str)}Export.extendPrototype=extendPrototype;Export.restorePrototype=restorePrototype;Export.VERSION=VERSION;Export.TMPL_OPEN="{{";Export.TMPL_CLOSE="}}";Export.ENTITIES=ENTITIES;if(typeof module!=="undefined"&&typeof module.exports!=="undefined"){module.exports=Export}else{if(typeof define==="function"&&define.amd){define([],function(){return Export})}else{window.S=Export}}function multiArgs(args,fn){var result=[],i;for(i=0;i<args.length;i++){result.push(args[i]);if(fn)fn.call(args,args[i],i)}return result}var escapeChars={lt:"<",gt:">",quot:'"',apos:"'",amp:"&"};var reversedEscapeChars={};for(var key in escapeChars){reversedEscapeChars[escapeChars[key]]=key}ENTITIES={amp:"&",gt:">",lt:"<",quot:'"',apos:"'",AElig:198,Aacute:193,Acirc:194,Agrave:192,Aring:197,Atilde:195,Auml:196,Ccedil:199,ETH:208,Eacute:201,Ecirc:202,Egrave:200,Euml:203,Iacute:205,Icirc:206,Igrave:204,Iuml:207,Ntilde:209,Oacute:211,Ocirc:212,Ograve:210,Oslash:216,Otilde:213,Ouml:214,THORN:222,Uacute:218,Ucirc:219,Ugrave:217,Uuml:220,Yacute:221,aacute:225,acirc:226,aelig:230,agrave:224,aring:229,atilde:227,auml:228,ccedil:231,eacute:233,ecirc:234,egrave:232,eth:240,euml:235,iacute:237,icirc:238,igrave:236,iuml:239,ntilde:241,oacute:243,ocirc:244,ograve:242,oslash:248,otilde:245,ouml:246,szlig:223,thorn:254,uacute:250,ucirc:251,ugrave:249,uuml:252,yacute:253,yuml:255,copy:169,reg:174,nbsp:160,iexcl:161,cent:162,pound:163,curren:164,yen:165,brvbar:166,sect:167,uml:168,ordf:170,laquo:171,not:172,shy:173,macr:175,deg:176,plusmn:177,sup1:185,sup2:178,sup3:179,acute:180,micro:181,para:182,middot:183,cedil:184,ordm:186,raquo:187,frac14:188,frac12:189,frac34:190,iquest:191,times:215,divide:247,"OElig;":338,"oelig;":339,"Scaron;":352,"scaron;":353,"Yuml;":376,"fnof;":402,"circ;":710,"tilde;":732,"Alpha;":913,"Beta;":914,"Gamma;":915,"Delta;":916,"Epsilon;":917,"Zeta;":918,"Eta;":919,"Theta;":920,"Iota;":921,"Kappa;":922,"Lambda;":923,"Mu;":924,"Nu;":925,"Xi;":926,"Omicron;":927,"Pi;":928,"Rho;":929,"Sigma;":931,"Tau;":932,"Upsilon;":933,"Phi;":934,"Chi;":935,"Psi;":936,"Omega;":937,"alpha;":945,"beta;":946,"gamma;":947,"delta;":948,"epsilon;":949,"zeta;":950,"eta;":951,"theta;":952,"iota;":953,"kappa;":954,"lambda;":955,"mu;":956,"nu;":957,"xi;":958,"omicron;":959,"pi;":960,"rho;":961,"sigmaf;":962,"sigma;":963,"tau;":964,"upsilon;":965,"phi;":966,"chi;":967,"psi;":968,"omega;":969,"thetasym;":977,"upsih;":978,"piv;":982,"ensp;":8194,"emsp;":8195,"thinsp;":8201,"zwnj;":8204,"zwj;":8205,"lrm;":8206,"rlm;":8207,"ndash;":8211,"mdash;":8212,"lsquo;":8216,"rsquo;":8217,"sbquo;":8218,"ldquo;":8220,"rdquo;":8221,"bdquo;":8222,"dagger;":8224,"Dagger;":8225,"bull;":8226,"hellip;":8230,"permil;":8240,"prime;":8242,"Prime;":8243,"lsaquo;":8249,"rsaquo;":8250,"oline;":8254,"frasl;":8260,"euro;":8364,"image;":8465,"weierp;":8472,"real;":8476,"trade;":8482,"alefsym;":8501,"larr;":8592,"uarr;":8593,"rarr;":8594,"darr;":8595,"harr;":8596,"crarr;":8629,"lArr;":8656,"uArr;":8657,"rArr;":8658,"dArr;":8659,"hArr;":8660,"forall;":8704,"part;":8706,"exist;":8707,"empty;":8709,"nabla;":8711,"isin;":8712,"notin;":8713,"ni;":8715,"prod;":8719,"sum;":8721,"minus;":8722,"lowast;":8727,"radic;":8730,"prop;":8733,"infin;":8734,"ang;":8736,"and;":8743,"or;":8744,"cap;":8745,"cup;":8746,"int;":8747,"there4;":8756,"sim;":8764,"cong;":8773,"asymp;":8776,"ne;":8800,"equiv;":8801,"le;":8804,"ge;":8805,"sub;":8834,"sup;":8835,"nsub;":8836,"sube;":8838,"supe;":8839,"oplus;":8853,"otimes;":8855,"perp;":8869,"sdot;":8901,"lceil;":8968,"rceil;":8969,"lfloor;":8970,"rfloor;":8971,"lang;":9001,"rang;":9002,"loz;":9674,"spades;":9824,"clubs;":9827,"hearts;":9829,"diams;":9830}}.call(this)},{}]},{},[1]);
(function (ng) {
    'use strict';
    ng.module('lrFileReader', [])
        .factory('lrFileReader', ['$rootScope', '$q', function lrFileReader($rootScope, $q) {

            var eventsList = ['abort', 'loadend', 'loadstart', 'progress', 'error', 'load'];

            function LrFileReader(file, rejectOnError) {
                this.file = file;
                this.rejectOnError = rejectOnError !== false;
                this.fr = new FileReader();
            }

            function readAs(name) {
                return function () {
                    var deferred = $q.defer();

                    this.on('load', function load(event) {
                        deferred.resolve(this.fr.result);
                    }.bind(this));

                    if (this.rejectOnError === true) {
                        this.on('error', function error(event) {
                            deferred.reject(this.fr.error);
                        }.bind(this));
                    }

                    this.on('abort', function abort(event) {
                        deferred.reject(event);
                    });

                    this.promise = deferred.promise;


                    this.fr[name](this.file);
                    return this.promise;
                }
            }

            LrFileReader.prototype.readAsArrayBuffer = readAs('readAsArrayBuffer');
            LrFileReader.prototype.readAsBinaryString = readAs('readAsBinaryString');
            LrFileReader.prototype.readAsDataURL = readAs('readAsDataURL');
            LrFileReader.prototype.readAsText = readAs('readAsText');
            LrFileReader.prototype.abort = function () {
                this.fr.abort();
            };
            LrFileReader.prototype.on = function registerEventHandler(eventName, cb) {

                if (eventsList.indexOf(eventName) !== -1) {
                    this.fr.addEventListener(eventName, function (event) {
                        $rootScope.$apply(function () {
                            cb(event);
                        });
                    });
                } else {
                    console.log(eventName + ' is not supported by the FileReader specifications');
                }

                return this;
            };

            return function getFileReader(file) {
                return new LrFileReader(file);
            };
        }]);
})(angular);
angular.module("truncate",[]).filter("characters",function(){return function(r,t,n){if(isNaN(t))return r;if(0>=t)return"";if(r&&r.length>t){if(r=r.substring(0,t),n)for(;" "===r.charAt(r.length-1);)r=r.substr(0,r.length-1);else{var e=r.lastIndexOf(" ");-1!==e&&(r=r.substr(0,e))}return r+"..."}return r}}).filter("words",function(){return function(r,t){if(isNaN(t))return r;if(0>=t)return"";if(r){var n=r.split(/\s+/);n.length>t&&(r=n.slice(0,t).join(" ")+"...")}return r}});
angular.module("angular-centered",[]).directive("centered",function(){return{restrict:"ECA",transclude:!0,template:'<div class="angular-center-container">						<div class="angular-centered" ng-transclude>						</div>					</div>'}});
"use strict";angular.module("toaster",["ngAnimate"]).service("toaster",["$rootScope",function(t){this.pop=function(e,o,s,i,a){this.toast={type:e,title:o,body:s,timeout:i,bodyOutputType:a},t.$broadcast("toaster-newToast")},this.clear=function(){t.$broadcast("toaster-clearToasts")}}]).constant("toasterConfig",{limit:0,"tap-to-dismiss":!0,"newest-on-top":!0,"time-out":5e3,"icon-classes":{error:"toast-error",info:"toast-info",success:"toast-success",warning:"toast-warning"},"body-output-type":"","body-template":"toasterBodyTmpl.html","icon-class":"toast-info","position-class":"toast-top-right","title-class":"toast-title","message-class":"toast-message"}).directive("toasterContainer",["$compile","$timeout","$sce","toasterConfig","toaster",function(t,e,o,s,i){return{replace:!0,restrict:"EA",scope:!0,link:function(t,a,n){function r(e){switch(e.type=l["icon-classes"][e.type],e.type||(e.type=l["icon-class"]),u++,angular.extend(e,{id:u}),e.bodyOutputType=e.bodyOutputType||l["body-output-type"],e.bodyOutputType){case"trustedHtml":e.html=o.trustAsHtml(e.body);break;case"template":e.bodyTemplate=e.body||l["body-template"]}t.configureTimer(e),l["newest-on-top"]===!0?(t.toasters.unshift(e),l.limit>0&&t.toasters.length>l.limit&&t.toasters.pop()):(t.toasters.push(e),l.limit>0&&t.toasters.length>l.limit&&t.toasters.shift())}function c(o,s){o.timeout=e(function(){t.removeToast(o.id)},s)}var l,u=0;n.toasterOptions&&(l=angular.extend({},s,t.$eval(n.toasterOptions))),t.config={position:l["position-class"],title:l["title-class"],message:l["message-class"],tap:l["tap-to-dismiss"]},t.configureTimer=function(t){var e="number"==typeof t.timeout?t.timeout:l["time-out"];e>0&&c(t,e)},t.toasters=[],t.$on("toaster-newToast",function(){r(i.toast)}),t.$on("toaster-clearToasts",function(){t.toasters.splice(0,t.toasters.length)})},controller:["$scope","$element","$attrs",function(t){t.stopTimer=function(t){t.timeout&&(e.cancel(t.timeout),t.timeout=null)},t.restartTimer=function(e){e.timeout||t.configureTimer(e)},t.removeToast=function(e){var o=0;for(o;o<t.toasters.length&&t.toasters[o].id!==e;o++);t.toasters.splice(o,1)},t.remove=function(e){t.config.tap===!0&&t.removeToast(e)}}],template:'<div  id="toast-container" ng-class="config.position"><div ng-repeat="toaster in toasters" class="toast" ng-class="toaster.type" ng-click="remove(toaster.id)" ng-mouseover="stopTimer(toaster)"  ng-mouseout="restartTimer(toaster)"><div ng-class="config.title">{{toaster.title}}</div><div ng-class="config.message" ng-switch on="toaster.bodyOutputType"><div ng-switch-when="trustedHtml" ng-bind-html="toaster.html"></div><div ng-switch-when="template"><div ng-include="toaster.bodyTemplate"></div></div><div ng-switch-default >{{toaster.body}}</div></div></div></div>'}}]);
(function(){"use strict";angular.module("ngBrowserInfo",[]);angular.module("ngBrowserInfo").service("browserInfo",["$window","$document",function(e,t){this.giveMeAllYouGot=function(){return{screenSize:this.getScreenSize(),windowSize:this.getWindowSize(),mobile:this.isMobile(),cookiesEnabled:this.areCookiesEnabled(),os:this.getOSInfo(),browser:this.getBrowserInfo()}};this.getScreenSize=function(){return{width:screen.width,height:screen.height}};this.getWindowSize=function(){return{width:e.innerWidth,height:e.innerHeight}};this.isMobile=function(){return/Mobile|Android|iP(ad|od|hone)|Fennec|mini/.test(e.navigator.userAgent)};this.areCookiesEnabled=function(){var n=e.navigator.cookieEnabled;if(typeof n==="undefined"){t.cookie="test-cookie";return t.cookie.indexOf("test-cookie")!==-1}return n};this.getOSInfo=function(){var e=n();if(/Windows/.test(e)){return{name:"Windows",version:/Windows (.*)/.exec(e)[1]}}return{name:e,version:r(e)}};this.getBrowserInfo=function(){var t=e.navigator.userAgent;var n;if(/Opera/.test(t)){n=s()}else if(/MSIE/.test(t)){n=o()}else if(/Chrome/.test(t)){n=u()}else if(/Safari/.test(t)){n=a()}else if(/Firefox/.test(t)){n=f()}else if(/Trident\//.test(t)){n=l()}else{n=c()}n.version=p(n.version);return n};var n=function(){for(var t in i){if(i.hasOwnProperty(t)){var n=i[t];if(n.regex.test(e.navigator.userAgent)){return n.name}}}return"N/A"};var r=function(t){var n=e.navigator.userAgent;var r;switch(t){case"Mac OS X":r=/Mac OS X (10[\.\_\d]+)/.exec(n);if(r){return r[1].replace(/_/g,".")}break;case"Android":r=/Android ([\.\_\d]+)/.exec(n);if(r){return r[1]}break;case"iOS":r=/OS (\d+)_(\d+)_?(\d+)?/.exec(e.navigator.appVersion);if(r){return r[1]+"."+r[2]}break}return"N/A"};var i=[{name:"Windows 95",regex:/(Windows 95|Win95|Windows_95)/},{name:"Windows ME",regex:/(Win 9x 4.90|Windows ME)/},{name:"Windows 98",regex:/(Windows 98|Win98)/},{name:"Windows 2000",regex:/(Windows NT 5.0|Windows 2000)/},{name:"Windows XP",regex:/(Windows NT 5.1|Windows XP)/},{name:"Windows Server 2003",regex:/Windows NT 5.2/},{name:"Windows Vista",regex:/Windows NT 6.0/},{name:"Windows 7",regex:/(Windows 7|Windows NT 6.1)/},{name:"Windows 8.1",regex:/(Windows 8.1|Windows NT 6.3)/},{name:"Windows 8",regex:/(Windows 8|Windows NT 6.2)/},{name:"Windows NT 4.0",regex:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},{name:"Windows ME",regex:/Windows ME/},{name:"Android",regex:/Android/},{name:"Open BSD",regex:/OpenBSD/},{name:"Sun OS",regex:/SunOS/},{name:"Linux",regex:/(Linux|X11)/},{name:"iOS",regex:/(iPhone|iPad|iPod)/},{name:"Mac OS X",regex:/Mac OS X/},{name:"Mac OS",regex:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},{name:"QNX",regex:/QNX/},{name:"UNIX",regex:/UNIX/},{name:"BeOS",regex:/BeOS/},{name:"OS/2",regex:/OS\/2/},{name:"Search Bot",regex:/(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}];var s=function(){return{name:"Opera",version:e.navigator.userAgent.indexOf("Version")!==-1?h("Version",8):h("Opera",6)}};var o=function(){return{name:"Microsoft Internet Explorer",version:h("MSIE",5)}};var u=function(){return{name:"Chrome",version:h("Chrome",7)}};var a=function(){return{name:"Safari",version:e.navigator.userAgent.indexOf("Version")!==-1?h("Version",8):h("Safari",7)}};var f=function(){return{name:"Firefox",version:h("Firefox",8)}};var l=function(){return{name:"Microsoft Internet Explorer",version:e.navigator.userAgent.substring(e.navigator.userAgent.indexOf("rv:")+3)}};var c=function(){return{name:"N/A",version:"N/A"}};var h=function(t,n){return e.navigator.userAgent.substring(e.navigator.userAgent.indexOf(t)+n)};var p=function(e){var t;if((t=e.indexOf(")"))!==-1){e=e.substring(0,t)}if((t=e.indexOf(";"))!==-1){e=e.substring(0,t)}if((t=e.indexOf(" "))!==-1){e=e.substring(0,t)}return e}}])})()
angular.module('psResponsive', [])
    .value('psResponsiveConfig', {
        sizes: [{
            name: 'mobile',
            minWidth: 0
        }, 
        {
            name: 'small',
            minWidth: 480
        }, 
        {
            name: 'medium',
            minWidth: 640
        }, 
        {
            name: 'large',
            minWidth: 1028
        },
        {
            name: 'desktop',
            minWidth: 1200
        }
        
        ]
    })
    .factory('psResponsive', ['$window', '$filter', '$rootScope', 'psResponsiveConfig',
        function($window, $filter, $rootScope, psResponsiveConfig) {


            var opRegEx = /[<>]=?\s\w+/,
                forEach = angular.forEach,
                filter = angular.filter,
                sizes = psResponsiveConfig.sizes;
            
            sizes = $filter('orderBy')(sizes, '-minWidth');

                var getHeight = function() {
                    return $window.innerHeight;
            },

                getWidth = function() {
                    return $window.innerWidth;
                },

                getLabel = function() {
                    var cWidth = getWidth(),
                        returnVal = false;
                    for (var i = 0; i < sizes.length; i++) {
                        if (parseInt(cWidth) >= parseInt(sizes[i].minWidth)) {
                            return sizes[i].name;
                        }
                    }
                },
                getWidthFromLabel = function(label) {
                    return $filter('filter')(sizes, {
                        name: label
                    })[0]["minWidth"];
                },

                getTest = function(test) {
                    var thingy = test.split(' ')[0],
                        against = test.split(' ')[1];

                    if (isNaN(against)) {
                        return eval('(' + getWidth() + ' ' + thingy +  ' ' + getWidthFromLabel(against) + ')');
                    } else {
                        return eval('(' + getWidth() + thingy + parseInt(against) + ')');
                    }
                },

                getOrientation = function(){
                    if(getHeight() > getWidth()) return 'portrait';
                    else return 'landscape';
                };

            angular.element($window).on('resize', function() {
                $rootScope.$digest();
            });

            return function(onwha) {
                if (!onwha) {
                    return getLabel();
                } else if (onwha == 'width') {
                    return getWidth();
                } else if (onwha == 'height') {
                    return getHeight();
                } else if (onwha == 'orientation') {
                    return getOrientation();
                } else if (opRegEx.test(onwha)) {
                    return getTest(onwha);
                } else {
                    return (getLabel() == onwha);
                }
                return false;
            };
        }
    ]);

"use strict";angular.module("angularSpinkit",["ngRotatingPlaneSpinner","ngDoubleBounceSpinner","ngWaveSpinner","ngWanderingCubesSpinner","ngPulseSpinner","ngChasingDotsSpinner","ngCircleSpinner"]),angular.module("ngRotatingPlaneSpinner",[]).directive("rotatingPlaneSpinner",function(){return{restrict:"E",templateUrl:"vendor/spinkit/rotatingPlaneSpinner.html"}}),angular.module("ngDoubleBounceSpinner",[]).directive("doubleBounceSpinner",function(){return{restrict:"E",templateUrl:"vendor/spinkit/doubleBounceSpinner.html"}}),angular.module("ngWaveSpinner",[]).directive("waveSpinner",function(){return{restrict:"E",templateUrl:"vendor/spinkit/waveSpinner.html"}}),angular.module("ngWanderingCubesSpinner",[]).directive("wanderingCubesSpinner",function(){return{restrict:"E",templateUrl:"vendor/spinkit/wanderingCubesSpinner.html"}}),angular.module("ngPulseSpinner",[]).directive("pulseSpinner",function(){return{restrict:"E",templateUrl:"vendor/spinkit/pulseSpinner.html"}}),angular.module("ngChasingDotsSpinner",[]).directive("chasingDotsSpinner",function(){return{restrict:"E",templateUrl:"vendor/spinkit/chasingDotsSpinner.html"}}),angular.module("ngCircleSpinner",[]).directive("circleSpinner",function(){return{restrict:"E",templateUrl:"vendor/spinkit/circleSpinner.html"}});


"undefined"!=typeof module&&"undefined"!=typeof exports&&module.exports===exports&&(module.exports="ui.router"),function(r,t,e){"use strict";function n(r,t){return R(new(R(function(){},{prototype:r})),t)}function o(r){return A(arguments,function(t){t!==r&&A(t,function(t,e){r.hasOwnProperty(e)||(r[e]=t)})}),r}function i(r,t){var e=[];for(var n in r.path){if(r.path[n]!==t.path[n])break;e.push(r.path[n])}return e}function a(r,t){if(Array.prototype.indexOf)return r.indexOf(t,Number(arguments[2])||0);var e=r.length>>>0,n=Number(arguments[2])||0;for(n=0>n?Math.ceil(n):Math.floor(n),0>n&&(n+=e);e>n;n++)if(n in r&&r[n]===t)return n;return-1}function u(r,t,e,n){var o,u=i(e,n),s={},l=[];for(var c in u)if(u[c].params&&u[c].params.length){o=u[c].params;for(var f in o)a(l,o[f])>=0||(l.push(o[f]),s[o[f]]=r[o[f]])}return R({},s,t)}function s(r,t){var e={};return A(r,function(r){var n=t[r];e[r]=null!=n?String(n):null}),e}function l(r,t,e){if(!e){e=[];for(var n in r)e.push(n)}for(var o=0;o<e.length;o++){var i=e[o];if(r[i]!=t[i])return!1}return!0}function c(r,t){var e={};return A(r,function(r){e[r]=t[r]}),e}function f(r,t){var n=1,i=2,a={},u=[],s=a,l=R(r.when(a),{$$promises:a,$$values:a});this.study=function(a){function c(r,e){if(v[e]!==i){if(p.push(e),v[e]===n)throw p.splice(0,p.indexOf(e)),new Error("Cyclic dependency: "+p.join(" -> "));if(v[e]=n,I(r))h.push(e,[function(){return t.get(r)}],u);else{var o=t.annotate(r);A(o,function(r){r!==e&&a.hasOwnProperty(r)&&c(a[r],r)}),h.push(e,r,o)}p.pop(),v[e]=i}}function f(r){return k(r)&&r.then&&r.$$promises}if(!k(a))throw new Error("'invocables' must be an object");var h=[],p=[],v={};return A(a,c),a=p=v=null,function(n,i,a){function u(){--w||(g||o(d,i.$$values),$.$$values=d,$.$$promises=!0,v.resolve(d))}function c(r){$.$$failure=r,v.reject(r)}function p(e,o,i){function s(r){f.reject(r),c(r)}function l(){if(!C($.$$failure))try{f.resolve(t.invoke(o,a,d)),f.promise.then(function(r){d[e]=r,u()},s)}catch(r){s(r)}}var f=r.defer(),h=0;A(i,function(r){m.hasOwnProperty(r)&&!n.hasOwnProperty(r)&&(h++,m[r].then(function(t){d[r]=t,--h||l()},s))}),h||l(),m[e]=f.promise}if(f(n)&&a===e&&(a=i,i=n,n=null),n){if(!k(n))throw new Error("'locals' must be an object")}else n=s;if(i){if(!f(i))throw new Error("'parent' must be a promise returned by $resolve.resolve()")}else i=l;var v=r.defer(),$=v.promise,m=$.$$promises={},d=R({},n),w=1+h.length/3,g=!1;if(C(i.$$failure))return c(i.$$failure),$;i.$$values?(g=o(d,i.$$values),u()):(R(m,i.$$promises),i.then(u,c));for(var b=0,y=h.length;y>b;b+=3)n.hasOwnProperty(h[b])?u():p(h[b],h[b+1],h[b+2]);return $}},this.resolve=function(r,t,e,n){return this.study(r)(t,e,n)}}function h(r,t,e){this.fromConfig=function(r,t,e){return C(r.template)?this.fromString(r.template,t):C(r.templateUrl)?this.fromUrl(r.templateUrl,t):C(r.templateProvider)?this.fromProvider(r.templateProvider,t,e):null},this.fromString=function(r,t){return O(r)?r(t):r},this.fromUrl=function(e,n){return O(e)&&(e=e(n)),null==e?null:r.get(e,{cache:t}).then(function(r){return r.data})},this.fromProvider=function(r,t,n){return e.invoke(r,null,n||{params:t})}}function p(r){function t(t){if(!/^\w+(-+\w+)*$/.test(t))throw new Error("Invalid parameter name '"+t+"' in pattern '"+r+"'");if(i[t])throw new Error("Duplicate parameter name '"+t+"' in pattern '"+r+"'");i[t]=!0,l.push(t)}function e(r){return r.replace(/[\\\[\]\^$*+?.()|{}]/g,"\\$&")}var n,o=/([:*])(\w+)|\{(\w+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,i={},a="^",u=0,s=this.segments=[],l=this.params=[];this.source=r;for(var c,f,h;(n=o.exec(r))&&(c=n[2]||n[3],f=n[4]||("*"==n[1]?".*":"[^/]*"),h=r.substring(u,n.index),!(h.indexOf("?")>=0));)a+=e(h)+"("+f+")",t(c),s.push(h),u=o.lastIndex;h=r.substring(u);var p=h.indexOf("?");if(p>=0){var v=this.sourceSearch=h.substring(p);h=h.substring(0,p),this.sourcePath=r.substring(0,u+p),A(v.substring(1).split(/[&?]/),t)}else this.sourcePath=r,this.sourceSearch="";a+=e(h)+"$",s.push(h),this.regexp=new RegExp(a),this.prefix=s[0]}function v(){this.compile=function(r){return new p(r)},this.isMatcher=function(r){return k(r)&&O(r.exec)&&O(r.format)&&O(r.concat)},this.$get=function(){return this}}function $(r){function t(r){var t=/^\^((?:\\[^a-zA-Z0-9]|[^\\\[\]\^$*+?.()|{}]+)*)/.exec(r.source);return null!=t?t[1].replace(/\\(.)/g,"$1"):""}function e(r,t){return r.replace(/\$(\$|\d{1,2})/,function(r,e){return t["$"===e?0:Number(e)]})}function n(r,t,e){if(!e)return!1;var n=r.invoke(t,t,{$match:e});return C(n)?n:!0}var o=[],i=null;this.rule=function(r){if(!O(r))throw new Error("'rule' must be a function");return o.push(r),this},this.otherwise=function(r){if(I(r)){var t=r;r=function(){return t}}else if(!O(r))throw new Error("'rule' must be a function");return i=r,this},this.when=function(o,i){var a,u=I(i);if(I(o)&&(o=r.compile(o)),!u&&!O(i)&&!M(i))throw new Error("invalid 'handler' in when()");var s={matcher:function(t,e){return u&&(a=r.compile(e),e=["$match",function(r){return a.format(r)}]),R(function(r,o){return n(r,e,t.exec(o.path(),o.search()))},{prefix:I(t.prefix)?t.prefix:""})},regex:function(r,o){if(r.global||r.sticky)throw new Error("when() RegExp must not be global or sticky");return u&&(a=o,o=["$match",function(r){return e(a,r)}]),R(function(t,e){return n(t,o,r.exec(e.path()))},{prefix:t(r)})}},l={matcher:r.isMatcher(o),regex:o instanceof RegExp};for(var c in l)if(l[c])return this.rule(s[c](o,i));throw new Error("invalid 'what' in when()")},this.$get=["$location","$rootScope","$injector",function(r,t,e){function n(t){function n(t){var n=t(e,r);return n?(I(n)&&r.replace().url(n),!0):!1}if(!t||!t.defaultPrevented){var a,u=o.length;for(a=0;u>a;a++)if(n(o[a]))return;i&&n(i)}}return t.$on("$locationChangeSuccess",n),{sync:function(){n()}}}]}function m(r,o,i){function a(r){return 0===r.indexOf(".")||0===r.indexOf("^")}function f(r,t){var n=I(r),o=n?r:r.name,i=a(o);if(i){if(!t)throw new Error("No reference point given for path '"+o+"'");for(var u=o.split("."),s=0,l=u.length,c=t;l>s;s++)if(""!==u[s]||0!==s){if("^"!==u[s])break;if(!c.parent)throw new Error("Path '"+o+"' not valid for state '"+t.name+"'");c=c.parent}else c=t;u=u.slice(s).join("."),o=c.name+(c.name&&u?".":"")+u}var f=b[o];return!f||!n&&(n||f!==r&&f.self!==r)?e:f}function h(r,t){y[r]||(y[r]=[]),y[r].push(t)}function p(t){t=n(t,{self:t,resolve:t.resolve||{},toString:function(){return this.name}});var e=t.name;if(!I(e)||e.indexOf("@")>=0)throw new Error("State must have a valid name");if(b.hasOwnProperty(e))throw new Error("State '"+e+"'' is already defined");var o=-1!==e.indexOf(".")?e.substring(0,e.lastIndexOf(".")):I(t.parent)?t.parent:"";if(o&&!b[o])return h(o,t.self);for(var i in E)O(E[i])&&(t[i]=E[i](t,E.$delegates[i]));if(b[e]=t,!t[x]&&t.url&&r.when(t.url,["$match","$stateParams",function(r,e){g.$current.navigable==t&&l(r,e)||g.transitionTo(t,r,{location:!1})}]),y[e])for(var a=0;a<y[e].length;a++)p(y[e][a]);return t}function v(r,t){return I(r)&&!C(t)?E[r]:O(t)&&I(r)?(E[r]&&!E.$delegates[r]&&(E.$delegates[r]=E[r]),E[r]=t,this):this}function $(r,t){return k(r)?t=r:t.name=r,p(t),this}function m(r,o,a,h,p,v,$){function m(){$.url()!==I&&($.url(I),$.replace())}function y(r,e,n,i,u){var s=n?e:c(r.params,e),l={$stateParams:s};u.resolve=p.resolve(r.resolve,l,u.resolve,r);var f=[u.resolve.then(function(r){u.globals=r})];return i&&f.push(i),A(r.views,function(e,n){var o=e.resolve&&e.resolve!==r.resolve?e.resolve:{};o.$template=[function(){return a.load(n,{view:e,locals:l,params:s,notify:!1})||""}],f.push(p.resolve(o,l,u.resolve,r).then(function(i){if(O(e.controllerProvider)||M(e.controllerProvider)){var a=t.extend({},o,l);i.$$controller=h.invoke(e.controllerProvider,null,a)}else i.$$controller=e.controller;i.$$state=r,u[n]=i}))}),o.all(f).then(function(){return u})}var E=o.reject(new Error("transition superseded")),P=o.reject(new Error("transition prevented")),S=o.reject(new Error("transition aborted")),j=o.reject(new Error("transition failed")),I=$.url();return w.locals={resolve:null,globals:{$stateParams:{}}},g={params:{},current:w.self,$current:w,transition:null},g.reload=function(){g.transitionTo(g.current,v,{reload:!0,inherit:!1,notify:!1})},g.go=function(r,t,e){return this.transitionTo(r,t,R({inherit:!0,relative:g.$current},e))},g.transitionTo=function(t,e,i){e=e||{},i=R({location:!0,inherit:!1,relative:null,notify:!0,reload:!1,$retry:!1},i||{});var a,c=g.$current,p=g.params,b=c.path,O=f(t,i.relative);if(!C(O)){var k={to:t,toParams:e,options:i};if(a=r.$broadcast("$stateNotFound",k,c.self,p),a.defaultPrevented)return m(),S;if(a.retry){if(i.$retry)return m(),j;var M=g.transition=o.when(a.retry);return M.then(function(){return M!==g.transition?E:(k.options.$retry=!0,g.transitionTo(k.to,k.toParams,k.options))},function(){return S}),m(),M}if(t=k.to,e=k.toParams,i=k.options,O=f(t,i.relative),!C(O)){if(i.relative)throw new Error("Could not resolve '"+t+"' from state '"+i.relative+"'");throw new Error("No such state '"+t+"'")}}if(O[x])throw new Error("Cannot transition to abstract state '"+t+"'");i.inherit&&(e=u(v,e||{},g.$current,O)),t=O;var A,T,U=t.path,V=w.locals,N=[];for(A=0,T=U[A];T&&T===b[A]&&l(e,p,T.ownParams)&&!i.reload;A++,T=U[A])V=N[A]=T.locals;if(d(t,c,V,i))return t.self.reloadOnSearch!==!1&&m(),g.transition=null,o.when(g.current);if(e=s(t.params,e||{}),i.notify&&(a=r.$broadcast("$stateChangeStart",t.self,e,c.self,p),a.defaultPrevented))return m(),P;for(var D=o.when(V),q=A;q<U.length;q++,T=U[q])V=N[q]=n(V),D=y(T,e,T===t,D,V);var K=g.transition=D.then(function(){var n,o,a;if(g.transition!==K)return E;for(n=b.length-1;n>=A;n--)a=b[n],a.self.onExit&&h.invoke(a.self.onExit,a.self,a.locals.globals),a.locals=null;for(n=A;n<U.length;n++)o=U[n],o.locals=N[n],o.self.onEnter&&h.invoke(o.self.onEnter,o.self,o.locals.globals);if(g.transition!==K)return E;g.$current=t,g.current=t.self,g.params=e,F(g.params,v),g.transition=null;var u=t.navigable;return i.location&&u&&($.url(u.url.format(u.locals.globals.$stateParams)),"replace"===i.location&&$.replace()),i.notify&&r.$broadcast("$stateChangeSuccess",t.self,e,c.self,p),I=$.url(),g.current},function(n){return g.transition!==K?E:(g.transition=null,r.$broadcast("$stateChangeError",t.self,e,c.self,p,n),m(),o.reject(n))});return K},g.is=function(r,n){var o=f(r);return C(o)?g.$current!==o?!1:C(n)&&null!==n?t.equals(v,n):!0:e},g.includes=function(r,n){var o=f(r);if(!C(o))return e;if(!C(g.$current.includes[o.name]))return!1;var i=!0;return t.forEach(n,function(r,t){C(v[t])&&v[t]===r||(i=!1)}),i},g.href=function(r,t,e){e=R({lossy:!0,inherit:!1,absolute:!1,relative:g.$current},e||{});var n=f(r,e.relative);if(!C(n))return null;t=u(v,t||{},g.$current,n);var o=n&&e.lossy?n.navigable:n,a=o&&o.url?o.url.format(s(n.params,t||{})):null;return!i.html5Mode()&&a&&(a="#"+i.hashPrefix()+a),e.absolute&&a&&(a=$.protocol()+"://"+$.host()+(80==$.port()||443==$.port()?"":":"+$.port())+(!i.html5Mode()&&a?"/":"")+a),a},g.get=function(r,t){if(!C(r)){var e=[];return A(b,function(r){e.push(r.self)}),e}var n=f(r,t);return n&&n.self?n.self:null},g}function d(r,t,e,n){return r!==t||(e!==t.locals||n.reload)&&r.self.reloadOnSearch!==!1?void 0:!0}var w,g,b={},y={},x="abstract",E={parent:function(r){if(C(r.parent)&&r.parent)return f(r.parent);var t=/^(.+)\.[^.]+$/.exec(r.name);return t?f(t[1]):w},data:function(r){return r.parent&&r.parent.data&&(r.data=r.self.data=R({},r.parent.data,r.data)),r.data},url:function(r){var t=r.url;if(I(t))return"^"==t.charAt(0)?o.compile(t.substring(1)):(r.parent.navigable||w).url.concat(t);if(o.isMatcher(t)||null==t)return t;throw new Error("Invalid url '"+t+"' in state '"+r+"'")},navigable:function(r){return r.url?r:r.parent?r.parent.navigable:null},params:function(r){if(!r.params)return r.url?r.url.parameters():r.parent.params;if(!M(r.params))throw new Error("Invalid params in state '"+r+"'");if(r.url)throw new Error("Both params and url specicified in state '"+r+"'");return r.params},views:function(r){var t={};return A(C(r.views)?r.views:{"":r},function(e,n){n.indexOf("@")<0&&(n+="@"+r.parent.name),t[n]=e}),t},ownParams:function(r){if(!r.parent)return r.params;var t={};A(r.params,function(r){t[r]=!0}),A(r.parent.params,function(e){if(!t[e])throw new Error("Missing required parameter '"+e+"' in state '"+r.name+"'");t[e]=!1});var e=[];return A(t,function(r,t){r&&e.push(t)}),e},path:function(r){return r.parent?r.parent.path.concat(r):[]},includes:function(r){var t=r.parent?R({},r.parent.includes):{};return t[r.name]=!0,t},$delegates:{}};w=p({name:"",url:"^",views:null,"abstract":!0}),w.navigable=null,this.decorator=v,this.state=$,this.$get=m,m.$inject=["$rootScope","$q","$view","$injector","$resolve","$stateParams","$location","$urlRouter"]}function d(){function r(r,t){return{load:function(e,n){var o,i={template:null,controller:null,view:null,locals:null,notify:!0,async:!0,params:{}};return n=R(i,n),n.view&&(o=t.fromConfig(n.view,n.params,n.locals)),o&&n.notify&&r.$broadcast("$viewContentLoading",n),o}}}this.$get=r,r.$inject=["$rootScope","$templateFactory"]}function w(){var r=!1;this.useAnchorScroll=function(){r=!0},this.$get=["$anchorScroll","$timeout",function(t,e){return r?t:function(r){e(function(){r[0].scrollIntoView()},0,!1)}}]}function g(r,e,n,o,i,a){function u(){return o.has?function(r){return o.has(r)?o.get(r):null}:function(r){try{return o.get(r)}catch(t){return null}}}function s(r,t,e){var n=function(){return{leave:function(r){r.remove()},enter:function(r,t,e){e.after(r)}}};if(h)return function(r){return r?{enter:function(r,t,e){h.enter(r,null,e)},leave:function(r){h.leave(r,function(){r.remove()})}}:n()};if(f){var o=f&&f(e,t);return function(r){return r?{enter:function(r,t){o.enter(r,t)},leave:function(r){o.leave(r.contents(),r)}}:n()}}return n}var l=!1,c=u(),f=c("$animator"),h=c("$animate"),p={restrict:"ECA",compile:function(o,u){var c=o.html(),f=!0,h=t.element(a[0].createComment(" ui-view-anchor ")),v=o.parent();return o.prepend(h),function(a){function $(){w&&(P(!0).leave(w),w=null),d&&(d.$destroy(),d=null)}function m(u){var s=r.$current&&r.$current.locals[y];if(f&&(f=!1,o.replaceWith(h)),!s)return $(),w=o.clone(),w.html(c),P(u).enter(w,v,h),d=a.$new(),void e(w.contents())(d);if(s!==g){$(),w=o.clone(),w.html(s.$template?s.$template:c),P(!0).enter(w,v,h),w.data("$uiView",S),g=s,S.state=s.$$state;var l=e(w.contents());if(d=a.$new(),s.$$controller){s.$scope=d;var p=n(s.$$controller,s);w.children().data("$ngControllerController",p)}l(d),d.$emit("$viewContentLoaded"),x&&d.$eval(x),t.isDefined(E)&&E&&!a.$eval(E)||i(w)}}var d,w,g,b=v.inheritedData("$uiView"),y=u[p.name]||u.name||"",x=u.onload||"",E=u.autoscroll,P=s(o,u,a);y.indexOf("@")<0&&(y=y+"@"+(b?b.state.name:""));var S={name:y,state:null},j=function(){if(!l){l=!0;try{m(!0)}catch(r){throw l=!1,r}l=!1}};a.$on("$stateChangeSuccess",j),a.$on("$viewContentLoading",j),m(!1)}}};return p}function b(r){var t=r.replace(/\n/g," ").match(/^([^(]+?)\s*(\((.*)\))?$/);if(!t||4!==t.length)throw new Error("Invalid state ref '"+r+"'");return{state:t[1],paramExpr:t[3]||null}}function y(r){var t=r.parent().inheritedData("$uiView");return t&&t.state&&t.state.name?t.state:void 0}function x(r,t){return{restrict:"A",require:"?^uiSrefActive",link:function(e,n,o,i){var a=b(o.uiSref),u=null,s=y(n)||r.$current,l="FORM"===n[0].nodeName,c=l?"action":"href",f=!0,h=function(t){if(t&&(u=t),f){var e=r.href(a.state,u,{relative:s});return i&&i.$$setStateInfo(a.state,u),e?void(n[0][c]=e):(f=!1,!1)}};a.paramExpr&&(e.$watch(a.paramExpr,function(r){r!==u&&h(r)},!0),u=e.$eval(a.paramExpr)),h(),l||n.bind("click",function(e){var o=e.which||e.button;0!==o&&1!=o||e.ctrlKey||e.metaKey||e.shiftKey||n.attr("target")||(t(function(){r.go(a.state,u,{relative:s})}),e.preventDefault())})}}}function E(r,t,e){return{restrict:"A",controller:["$scope","$element","$attrs",function(n,o,i){function a(){r.$current.self===s&&u()?o.addClass(f):o.removeClass(f)}function u(){return!c||l(c,t)}var s,c,f;f=e(i.uiSrefActive||"",!1)(n),this.$$setStateInfo=function(t,e){s=r.get(t,y(o)),c=e,a()},n.$on("$stateChangeSuccess",a)}]}}function P(r){return function(t){return r.is(t)}}function S(r){return function(t){return r.includes(t)}}function j(r,t){function o(r){this.locals=r.locals.globals,this.params=this.locals.$stateParams}function i(){this.locals=null,this.params=null}function a(e,a){if(null!=a.redirectTo){var u,l=a.redirectTo;if(I(l))u=l;else{if(!O(l))throw new Error("Invalid 'redirectTo' in when()");u=function(r,t){return l(r,t.path(),t.search())}}t.when(e,u)}else r.state(n(a,{parent:null,name:"route:"+encodeURIComponent(e),url:e,onEnter:o,onExit:i}));return s.push(a),this}function u(r,t,n){function o(r){return""!==r.name?r:e}var i={routes:s,params:n,current:e};return t.$on("$stateChangeStart",function(r,e,n,i){t.$broadcast("$routeChangeStart",o(e),o(i))}),t.$on("$stateChangeSuccess",function(r,e,n,a){i.current=o(e),t.$broadcast("$routeChangeSuccess",o(e),o(a)),F(n,i.params)}),t.$on("$stateChangeError",function(r,e,n,i,a,u){t.$broadcast("$routeChangeError",o(e),o(i),u)}),i}var s=[];o.$inject=["$$state"],this.when=a,this.$get=u,u.$inject=["$state","$rootScope","$routeParams"]}var C=t.isDefined,O=t.isFunction,I=t.isString,k=t.isObject,M=t.isArray,A=t.forEach,R=t.extend,F=t.copy;t.module("ui.router.util",["ng"]),t.module("ui.router.router",["ui.router.util"]),t.module("ui.router.state",["ui.router.router","ui.router.util"]),t.module("ui.router",["ui.router.state"]),t.module("ui.router.compat",["ui.router"]),f.$inject=["$q","$injector"],t.module("ui.router.util").service("$resolve",f),h.$inject=["$http","$templateCache","$injector"],t.module("ui.router.util").service("$templateFactory",h),p.prototype.concat=function(r){return new p(this.sourcePath+r+this.sourceSearch)},p.prototype.toString=function(){return this.source},p.prototype.exec=function(r,t){var e=this.regexp.exec(r);if(!e)return null;var n,o=this.params,i=o.length,a=this.segments.length-1,u={};if(a!==e.length-1)throw new Error("Unbalanced capture group in route '"+this.source+"'");for(n=0;a>n;n++)u[o[n]]=e[n+1];for(;i>n;n++)u[o[n]]=t[o[n]];return u},p.prototype.parameters=function(){return this.params},p.prototype.format=function(r){var t=this.segments,e=this.params;if(!r)return t.join("");var n,o,i,a=t.length-1,u=e.length,s=t[0];for(n=0;a>n;n++)i=r[e[n]],null!=i&&(s+=encodeURIComponent(i)),s+=t[n+1];for(;u>n;n++)i=r[e[n]],null!=i&&(s+=(o?"&":"?")+e[n]+"="+encodeURIComponent(i),o=!0);return s},t.module("ui.router.util").provider("$urlMatcherFactory",v),$.$inject=["$urlMatcherFactoryProvider"],t.module("ui.router.router").provider("$urlRouter",$),m.$inject=["$urlRouterProvider","$urlMatcherFactoryProvider","$locationProvider"],t.module("ui.router.state").value("$stateParams",{}).provider("$state",m),d.$inject=[],t.module("ui.router.state").provider("$view",d),t.module("ui.router.state").provider("$uiViewScroll",w),g.$inject=["$state","$compile","$controller","$injector","$uiViewScroll","$document"],t.module("ui.router.state").directive("uiView",g),x.$inject=["$state","$timeout"],E.$inject=["$state","$stateParams","$interpolate"],t.module("ui.router.state").directive("uiSref",x).directive("uiSrefActive",E),P.$inject=["$state"],S.$inject=["$state"],t.module("ui.router.state").filter("isState",P).filter("includedByState",S),j.$inject=["$stateProvider","$urlRouterProvider"],t.module("ui.router.compat").provider("$route",j).directive("ngView",g)}(window,window.angular);


// Render module for formly to display forms
angular.module('formly.render', []);
// Main Formly Module
angular.module('formly', ['formly.render']);
'use strict';
angular.module('formly.render').directive('formlyField', [
  '$http',
  '$compile',
  '$templateCache',
  function formlyField($http, $compile, $templateCache) {
    var getTemplateUrl = function (type) {
      var templateUrl = '';
      switch (type) {
      case 'textarea':
        templateUrl = 'directives/formly-field-textarea.html';
        break;
      case 'radio':
        templateUrl = 'directives/formly-field-radio.html';
        break;
      case 'select':
        templateUrl = 'directives/formly-field-select.html';
        break;
      case 'number':
        templateUrl = 'directives/formly-field-number.html';
        break;
      case 'checkbox':
        templateUrl = 'directives/formly-field-checkbox.html';
        break;
      case 'password':
        templateUrl = 'directives/formly-field-password.html';
        break;
      case 'hidden':
        templateUrl = 'directives/formly-field-hidden.html';
        break;
      case 'email':
        templateUrl = 'directives/formly-field-email.html';
        break;
      case 'text':
        templateUrl = 'directives/formly-field-text.html';
        break;
      default:
        templateUrl = null;
        break;
      }
      return templateUrl;
    };
    return {
      restrict: 'AE',
      transclude: true,
      scope: {
        optionsData: '&options',
        formId: '@formId',
        index: '@index',
        value: '=formValue'
      },
      link: function fieldLink($scope, $element, $attr) {
        var templateUrl = getTemplateUrl($scope.options.type);
        if (templateUrl) {
          $http.get(templateUrl, { cache: $templateCache }).success(function (data) {
            //template data returned
            $element.html(data);
            $compile($element.contents())($scope);
          });
        } else {
          console.log('Formly Error: template type \'' + $scope.options.type + '\' not supported.');
        }
      },
      controller: [
        '$scope',
        function fieldController($scope) {
          $scope.options = $scope.optionsData();
          if ($scope.options.default) {
            $scope.value = $scope.options.default;
          }
          // set field id to link labels and fields
          $scope.id = $scope.formId + $scope.options.type + $scope.index;
        }
      ]
    };
  }
]);
'use strict';
angular.module('formly.render').directive('formlyForm', function formlyForm() {
  return {
    restrict: 'AE',
    templateUrl: 'directives/formly-form.html',
    replace: true,
    scope: {
      formId: '@formId',
      fields: '=fields',
      options: '=options',
      result: '=result'
    },
    controller: [
      '$scope',
      '$element',
      function formController($scope, $element) {
        $scope.populateResult = function () {
          var formChildren = $element.children();
          var fieldScope;
          angular.forEach(formChildren, function (fieldElement, key) {
            // grab fields isolate scope
            fieldScope = angular.element(fieldElement).scope();
            // check if its a form field, otherwise ignore, ie its the button
            if (fieldScope.field) {
              // if a key is set, then save the data with that key in the result object
              // otherwise use the field's index from the fields array
              var dataKey;
              if ('key' in fieldScope.field) {
                dataKey = fieldScope.field.key;
              } else {

                dataKey = 'field_' + fieldScope.$index;
              }
              // set value in result
              $scope.result[dataKey] = fieldScope.value;
            }
          });
        };
      }
    ]
  };
});
angular.module('formly.render').run([
  '$templateCache',
  function ($templateCache) {
    'use strict';
    $templateCache.put('directives/formly-field-checkbox.html',		'<div class=checkbox><label><input type=checkbox ng-required=options.required ng-disabled=options.disabled ng-model=value>{{options.label || \'Checkbox\'}} {{options.required ? \'*\' : \'\'}}</label></div>');
    
    
    $templateCache.put('directives/formly-field-email.html', 		'<div class=form-group><label for={{id}}>{{options.label || \'Email\'}} {{options.required ? \'*\' : \'\'}}</label><input type=email 			class=form-control id={{id}} placeholder={{options.placeholder}} ng-required=options.required ng-disabled=options.disabled ng-model=value></div>');
    $templateCache.put('directives/formly-field-text.html', 		'<div class=form-group><label for={{id}}>{{options.label || \'Text\'}}  {{options.required ? \'*\' : \'\'}}</label><input style="width: 100%" 	class=form-control id={{id}} placeholder={{options.placeholder}} ng-required=options.required ng-disabled=options.disabled ng-model=value></div>');
    
    
    $templateCache.put('directives/formly-field-hidden.html',		'<input type=hidden ng-model=value>');
    $templateCache.put('directives/formly-field-number.html', 		'<div class=form-group><label for={{id}}>{{options.label || \'Number\'}} {{options.required ? \'*\' : \'\'}}</label><input type=number class=form-control id={{id}} placeholder={{options.placeholder}} ng-required=options.required ng-disabled=options.disabled min={{options.min}} max={{options.max}} ng-minlength={{options.minlength}} ng-maxlength={{options.maxlength}} ng-model=value></div>');
    $templateCache.put('directives/formly-field-password.html', 	'<div class=form-group><label for={{id}}>{{options.label || \'Password\'}} {{options.required ? \'*\' : \'\'}}</label><input type=password class=form-control id={{id}} ng-required=options.required ng-disabled=options.disabled ng-model=value></div>');
    $templateCache.put('directives/formly-field-radio.html', 		'<div class=radio-group><label class=control-label>{{options.label}} {{options.required ? \'*\' : \'\'}}</label><div class=radio ng-repeat="(key, option) in options.options"><label><input type=radio name={{id}} id="{{id + \'_\'+ $index}}" ng-value=option.value ng-required=options.required ng-model=$parent.value>{{option.name}}</label></div></div>');
    $templateCache.put('directives/formly-field-select.html', 		'<div class=form-group><label for={{id}}>{{options.label || \'Select\'}} {{options.required ? \'*\' : \'\'}}</label><select class=form-control id={{id}} ng-model=value ng-required=options.required ng-disabled=options.disabled ng-init="value = options.options[options.default]" ng-options="option.name group by option.group for option in options.options"></select></div>');    
    $templateCache.put('directives/formly-field-textarea.html', 	'<div class=form-group><label for={{id}}>{{options.label || \'Text\'}} {{options.required ? \'*\' : \'\'}}</label><textarea style="resize: none;" type=text class=form-control id={{id}} rows={{options.lines}} placeholder={{options.placeholder}} ng-required=options.required ng-disabled=options.disabled ng-model=value>\n' + '\t</textarea></div>');
    $templateCache.put('directives/formly-field.html', '');
    $templateCache.put('directives/formly-form.html', 				'<form class=formly role=form name={{options.uniqueFormId}}><formly-field ng-repeat="field in fields" options=field form-value=value class=formly-field form-id={{options.uniqueFormId}} index={{$index}}></formly-field><button type=submit ng-hide=options.hideSubmit ng-click=populateResult() style="width: 100%; margin-top: 15px">{{options.submitCopy || "Submit"}}</button></form>');
  }
]);
!function(){"use strict";angular.module("angularLoad",[]).service("angularLoad",["$document","$q","$timeout",function(a,b,c){function d(a){var d={};return function(e){if("undefined"==typeof d[e]){var f=b.defer(),g=a(e);g.onload=g.onreadystatechange=function(a){c(function(){f.resolve(a)})},g.onerror=function(a){c(function(){f.reject(a)})},d[e]=f.promise}return d[e]}}this.loadScript=d(function(b){var c=a[0].createElement("script");return c.src=b,a[0].body.appendChild(c),c}),this.loadCSS=d(function(b){var c=a[0].createElement("link");return c.rel="stylesheet",c.type="text/css",c.href=b,a[0].head.appendChild(c),c})}])}();
//# sourceMappingURL=angular-load.min.js.map
/*! 
 * angular-smooth-scroll v0.0.0
 * 
 * Copyright (c) 2014 
 * License: BSD-2-Clause
 */
(function(){angular.module("SmoothScroll",[]).factory("SmoothScroll",["$q",function(a){var b,c;return c=function(a){var b,c;for(c=a.offsetTop,b=a.offsetParent;null!=b;)c+=b.offsetTop,b=b.offsetParent;return c},b=function(a,b,c){var d,e,f,g,h,i,j,k,l;if(d=b>a?b-a:a-b,100>d)return scrollTo(0,b),void c.resolve(g);if(h=Math.round(d/100),h>20&&(h=20),i=Math.round(d/25),g=b>a?a+i:a-i,j=0,e=function(a,b,d){return setTimeout(function(){return scrollTo(0,a),a>=d?c.resolve(a):void 0},b*h)},k=function(a,b,d){return setTimeout(function(){return scrollTo(0,a),d>=a?c.resolve(a):void 0},b*h)},b>a)for(f=a;b>f;)e(g,j,b),g+=i,g>b&&(g=b),j++,f+=i;for(f=a,l=[];f>b;)k(g,j,b),g-=i,b>g&&(g=b),j++,l.push(f-=i);return l},{$goTo:function(d){var e,f;return null!=(null!=(f=d[0])?f.tagName:void 0)&&(d=c(d[0])),null!=d.tagName&&(d=c(d)),"string"==typeof d&&(d=c(document.querySelector(d))),e=a.defer(),b(window.pageYOffset,d,e),e.promise.then(void 0,function(a){return null==a&&(a=window.pageYOffset),window.onload=function(){return scrollTo(0,a)}})}}}])}).call(this);
/**!
 * NgCoverVid
 * Make your HTML5 video behave like a background cover image with this lightweight Angular directive
 * @author  James Feigel <james.feigel@gmail.com>
 * @version 0.1.0
 */
angular.module("ngCovervid",[]).directive("covervid",["$window","$timeout",function(a,b){return{replace:!0,restrict:"EA",scope:{height:"@",width:"@"},template:"<video ng-transclude></video>",transclude:!0,link:function(c,d){function e(a,c){var d=null;return function(){var e=this,f=arguments;b.cancel(d),d=b(function(){a.apply(e,f)},c)}}function f(){var a=d.parent()[0].offsetHeight,b=d.parent()[0].offsetWidth,c=g,e=h,f=a/c,i=b/e;i>f?(d.css("height","auto"),d.css("width",b+"px")):(d.css("height",a+"px"),d.css("width","auto"))}var g=parseInt(c.height),h=parseInt(c.width);void 0!==c.height&&isNaN(g)&&console.error("Error: [covervid] 'height' provided is not a number. Found '"+c.height+"'. Using native height of video."),void 0!==c.width&&isNaN(h)&&console.error("Error: [covervid] 'width' provided is not a number. Found '"+c.width+"'. Using native width of video."),angular.element(a).bind("resize",function(){e(f(),50)}),d.css("position","absolute"),d.css("top","50%"),d.css("left","50%"),d.css("-webkit-transform","translate(-50%, -50%)"),d.css("-ms-transform","translate(-50%, -50%)"),d.css("transform","translate(-50%, -50%)"),d.parent().css("overflow","hidden"),isNaN(h)||isNaN(g)?d.bind("loadedmetadata",function(){h=d[0].videoWidth,g=d[0].videoHeight,f()}):f()}}}]);
'use strict';

angular.module("transactionManager", []).service('TransactionManager', function() {

	var doRollback = function(variable, prevVersion) {
		var tmp = angular.copy(variable);
		var snapshot_pop = variable.snapshot.pop();
		if(prevVersion) {
				tmp.snapshot.pop();
		}
		angular.forEach(variable, function(val, key) {
			if(key != "snapshot") {
				variable[key] = snapshot_pop[key];
			}
		});
		variable.snapshot = tmp.snapshot;
	}

	this.snapshot = function(variable) {
		if(!variable.snapshot) {
			variable.snapshot = [angular.copy(variable)];
		}else {
			var tmp = angular.copy(variable);
			delete tmp.snapshot;
			variable.snapshot.push(tmp);
		}
	}

	this.rollback = function(variable) {
		if(this.isTransaction(variable)) {
			doRollback(variable);
		}
	}

	this.prevVersion = function(variable) {
		if(this.isTransaction(variable)) {
			doRollback(variable, true);
			doRollback(variable);
		}
	}

	this.clear = function(variable) {
		if(this.isTransaction(variable)) {
			variable.snapshot = [];
		}
	}

	this.isTransaction = function(variable) {
		return variable.snapshot != undefined && variable.snapshot.length != 0 ? true : false
	}

	this.hasPrevVersion = function(variable) {
		if(this.isTransaction(variable)) {
			return variable.snapshot.length > 1 ? true : false;
		}
	}

	this.canRollback = function(variable) {
		if(this.isTransaction(variable)) {
			var notEquals = false;
			angular.forEach(variable, function(val, key) {
				if(key != "snapshot") {
					if(variable[key] != variable.snapshot[variable.snapshot.length -1][key]) {
						notEquals = true;
					}
				}
			});
			return notEquals;
		}
		return false;
	}

	return this;
});
/*jslint browser: true, plusplus: true, indent: 2 */
// Logic and fallbacks based on the following SO answers:
// - Getting caret position cross browser: http://stackoverflow.com/a/9370239/147507
// - Selection API on non input-text fields: http://stackoverflow.com/a/24247942/147507
// - Set cursor position on input text: http://stackoverflow.com/q/5755826/147507
angular.module('ngPatternRestrict', [])
  .value('ngPatternRestrictConfig', {
    showDebugInfo: false,
  })
  .directive('ngPatternRestrict', ['ngPatternRestrictConfig', '$log', function (patternRestrictConfig, $log) {
    'use strict';

    function showDebugInfo() {
      if (patternRestrictConfig.showDebugInfo) {
        $log.debug("[ngPatternRestrict] " + Array.prototype.join.call(arguments, ' '));
      }
    }

    return {
      restrict: 'A',
      require: "?ngModel",
      compile: function uiPatternRestrictCompile() {
        showDebugInfo("Loaded");

        return function ngPatternRestrictLinking(scope, iElement, iAttrs, ngModelController) {
          var regex, // validation regex object
            oldValue, // keeping track of the previous value of the element
            caretPosition, // keeping track of where the caret is at to avoid jumpiness
            // housekeeping
            initialized = false, // have we initialized our directive yet?
            eventsBound = false, // have we bound our events yet?
            // functions
            getCaretPosition, // function to get the caret position, set in detectGetCaretPositionMethods
            setCaretPosition; // function to set the caret position, set in detectSetCaretPositionMethods

          //-------------------------------------------------------------------
          // caret position
          function getCaretPositionWithInputSelectionStart() {
            return iElement[0].selectionStart; // we need to go under jqlite
          }

          function getCaretPositionWithDocumentSelection() {
            // create a selection range from where we are to the beggining
            // and measure how much we moved
            var range = document.selection.createRange();
            range.moveStart('character', -iElement.val().length);
            return range.text.length;
          }

          function getCaretPositionWithWindowSelection() {
            var s = window.getSelection(),
              originalSelectionLength = String(s).length,
              selectionLength,
              didReachZero = false,
              detectedCaretPosition,
              restorePositionCounter;

            do {
              selectionLength = String(s).length;
              s.modify('extend', 'backward', 'character');
              // we're undoing a selection, and starting a new one towards the beggining of the string
              if (String(s).length === 0) {
                didReachZero = true;
              }
            } while (selectionLength !== String(s).length);

            detectedCaretPosition = didReachZero ? selectionLength : selectionLength - originalSelectionLength;
            s.collapseToStart();

            restorePositionCounter = detectedCaretPosition;
            while (restorePositionCounter-- > 0) {
              s.modify('move', 'forward', 'character');
            }
            while (originalSelectionLength-- > 0) {
              s.modify('extend', 'forward', 'character');
            }

            return detectedCaretPosition;
          }

          function setCaretPositionWithSetSelectionRange(position) {
            iElement[0].setSelectionRange(position, position);
          }

          function setCaretPositoinWithCreateTextRange(position) {
            var textRange = iElement[0].createTextRange();
            textRange.collapse(true);
            textRange.moveEnd('character', position);
            textRange.moveStart('character', position);
            textRange.select();
          }

          function setCaretPositionWithWindowSelection(position) {
            var s = window.getSelection(),
              selectionLength;

            do {
              selectionLength = String(s).length;
              s.modify('extend', 'backward', 'line');
            } while (selectionLength !== String(s).length);
            s.collapseToStart();

            while (position--) {
              s.modify('move', 'forward', 'character');
            }
          }

          // HACK: Opera 12 won't give us a wrong validity status although the input is invalid
          // we can select the whole text and check the selection size
          // Congratulations to IE 11 for doing the same but not returning the selection.
          function getValueLengthThroughSelection(input) {
            // only do this on opera, since it'll mess up the caret position
            // and break Firefox functionality
            if (!/Opera/i.test(navigator.userAgent)) {
              return 0;
            }

            input.focus();
            document.execCommand("selectAll");
            var focusNode = window.getSelection().focusNode;
            return (focusNode || {}).selectionStart || 0;
          }

          //-------------------------------------------------------------------
          // event handlers
          function revertToPreviousValue() {
            if (ngModelController) {
              scope.$apply(function () {
                ngModelController.$setViewValue(oldValue);
              });
            }
            iElement.val(oldValue);

            if (!angular.isUndefined(caretPosition)) {
              setCaretPosition(caretPosition);
            }
          }

          function updateCurrentValue(newValue) {
            oldValue = newValue;
            caretPosition = getCaretPosition();
          }

          function genericEventHandler(evt) {
            showDebugInfo("Reacting to event:", evt.type);

            //HACK Chrome returns an empty string as value if user inputs a non-numeric string into a number type input
            // and this may happen with other non-text inputs soon enough. As such, if getting the string only gives us an
            // empty string, we don't have the chance of validating it against a regex. All we can do is assume it's wrong,
            // since the browser is rejecting it either way.
            var newValue = iElement.val(),
              inputValidity = iElement.prop("validity");
            if (newValue === "" && iElement.attr("type") !== "text" && inputValidity && inputValidity.badInput) {
              showDebugInfo("Value cannot be verified. Should be invalid. Reverting back to:", oldValue);
              evt.preventDefault();
              revertToPreviousValue();
            } else if (newValue === "" && getValueLengthThroughSelection(iElement[0]) !== 0) {
              showDebugInfo("Invalid input. Reverting back to:", oldValue);
              evt.preventDefault();
              revertToPreviousValue();
            } else if (regex.test(newValue)) {
              showDebugInfo("New value passed validation against", regex, newValue);
              updateCurrentValue(newValue);
            } else {
              showDebugInfo("New value did NOT pass validation against", regex, newValue, "Reverting back to:", oldValue);
              evt.preventDefault();
              revertToPreviousValue();
            }
          }

          //-------------------------------------------------------------------
          // setup based on attributes
          function tryParseRegex(regexString) {
            try {
              regex = new RegExp(regexString);
            } catch (e) {
              throw "Invalid RegEx string parsed for ngPatternRestrict: " + regexString;
            }
          }

          //-------------------------------------------------------------------
          // setup events
          function bindListeners() {
            if (eventsBound) {
              return;
            }

            iElement.bind('input keyup click', genericEventHandler);

            showDebugInfo("Bound events: input, keyup, click");
          }

          function unbindListeners() {
            if (!eventsBound) {
              return;
            }

            iElement.unbind('input', genericEventHandler);
            //input: HTML5 spec, changes in content

            iElement.unbind('keyup', genericEventHandler);
            //keyup: DOM L3 spec, key released (possibly changing content)

            iElement.unbind('click', genericEventHandler);
            //click: DOM L3 spec, mouse clicked and released (possibly changing content)

            showDebugInfo("Unbound events: input, keyup, click");

            eventsBound = false;
          }

          //-------------------------------------------------------------------
          // initialization
          function readPattern() {
            var entryRegex = !!iAttrs.ngPatternRestrict ? iAttrs.ngPatternRestrict : iAttrs.pattern;
            showDebugInfo("RegEx to use:", entryRegex);
            tryParseRegex(entryRegex);
          }

          function notThrows(testFn, shouldReturnTruthy) {
          	try {
          		return testFn() || !shouldReturnTruthy;
          	} catch (e) {
          		return false;
          	}
          }

          function detectGetCaretPositionMethods() {
            var input = iElement[0];
            if (notThrows(function () { return input.selectionStart; })) {
              getCaretPosition = getCaretPositionWithInputSelectionStart;
            } else if (notThrows(function () { return document.selection; }, true)) {
              getCaretPosition = getCaretPositionWithDocumentSelection;
            } else {
              getCaretPosition = getCaretPositionWithWindowSelection;
            }
          }

          function detectSetCaretPositionMethods() {
            var input = iElement[0];
            if (notThrows(function () { input.setSelectionRange(0, 0); })) {
              setCaretPosition = setCaretPositionWithSetSelectionRange;
            } else if (notThrows(function () { return input.createTextRange(); })) {
              setCaretPosition = setCaretPositoinWithCreateTextRange;
            } else {
              setCaretPosition = setCaretPositionWithWindowSelection;
            }
          }

          function initialize() {
            if (initialized) {
              return;
            }
            showDebugInfo("Initializing");

            readPattern();

            oldValue = iElement.val();
            if (!oldValue) {
              oldValue = "";
            }
            showDebugInfo("Original value:", oldValue);

            bindListeners();

            detectGetCaretPositionMethods();
            detectSetCaretPositionMethods();

            initialized = true;
          }

          function uninitialize() {
            showDebugInfo("Uninitializing");
            unbindListeners();
          }

          iAttrs.$observe("ngPatternRestrict", readPattern);
          iAttrs.$observe("pattern", readPattern);

          scope.$on("$destroy", uninitialize);

          initialize();
        };
      }
    };
  }]);
!function(){var e=angular.module("angularFileUpload",[]);e.service("$upload",["$http","$timeout",function(e,r){function n(n){n.method=n.method||"POST",n.headers=n.headers||{},n.transformRequest=n.transformRequest||function(r,n){return window.ArrayBuffer&&r instanceof window.ArrayBuffer?r:e.defaults.transformRequest[0](r,n)},window.XMLHttpRequest.__isShim&&(n.headers.__setXHR_=function(){return function(e){n.__XHR=e,n.xhrFn&&n.xhrFn(e),e.upload.addEventListener("progress",function(e){n.progress&&r(function(){n.progress&&n.progress(e)})},!1),e.upload.addEventListener("load",function(e){e.lengthComputable&&r(function(){n.progress&&n.progress(e)})},!1)}});var t=e(n);return t.progress=function(e){return n.progress=e,t},t.abort=function(){return n.__XHR&&r(function(){n.__XHR.abort()}),t},t.xhr=function(e){return n.xhrFn=e,t},t.then=function(e,r){return function(t,a,o){n.progress=o||n.progress;var i=r.apply(e,[t,a,o]);return i.abort=e.abort,i.progress=e.progress,i.xhr=e.xhr,i}}(t,t.then),t}this.upload=function(r){r.headers=r.headers||{},r.headers["Content-Type"]=void 0,r.transformRequest=r.transformRequest||e.defaults.transformRequest;var t=new FormData,a=r.transformRequest,o=r.data;return r.transformRequest=function(e,n){if(o)if(r.formDataAppender)for(var t in o){var i=o[t];r.formDataAppender(e,t,i)}else for(var t in o){var i=o[t];if("function"==typeof a)i=a(i,n);else for(var s=0;s<a.length;s++){var f=a[s];"function"==typeof f&&(i=f(i,n))}e.append(t,i)}if(null!=r.file){var u=r.fileFormDataName||"file";if("[object Array]"===Object.prototype.toString.call(r.file))for(var l="[object String]"===Object.prototype.toString.call(u),s=0;s<r.file.length;s++)e.append(l?u+s:u[s],r.file[s],r.file[s].name);else e.append(u,r.file,r.file.name)}return e},r.data=t,n(r)},this.http=function(e){return n(e)}}]),e.directive("ngFileSelect",["$parse","$timeout",function(e,r){return function(n,t,a){var o=e(a.ngFileSelect);t.bind("change",function(e){var t,a,i=[];if(t=e.target.files,null!=t)for(a=0;a<t.length;a++)i.push(t.item(a));r(function(){o(n,{$files:i,$event:e})})}),t.bind("click",function(){this.value=null})}}]),e.directive("ngFileDropAvailable",["$parse","$timeout",function(e,r){return function(n,t,a){if("draggable"in document.createElement("span")){var o=e(a.ngFileDropAvailable);r(function(){o(n)})}}}]),e.directive("ngFileDrop",["$parse","$timeout",function(e,r){return function(n,t,a){function o(e,r){if(r.isDirectory){var n=r.createReader();f++,n.readEntries(function(r){for(var n=0;n<r.length;n++)o(e,r[n]);f--})}else f++,r.file(function(r){f--,e.push(r)})}if("draggable"in document.createElement("span")){var i=null,s=e(a.ngFileDrop);t[0].addEventListener("dragover",function(e){r.cancel(i),e.stopPropagation(),e.preventDefault(),t.addClass(a.ngFileDragOverClass||"dragover")},!1),t[0].addEventListener("dragleave",function(){i=r(function(){t.removeClass(a.ngFileDragOverClass||"dragover")})},!1);var f=0;t[0].addEventListener("drop",function(e){e.stopPropagation(),e.preventDefault(),t.removeClass(a.ngFileDragOverClass||"dragover");var i=[],u=e.dataTransfer.items;if(u&&u.length>0&&u[0].webkitGetAsEntry)for(var l=0;l<u.length;l++)o(i,u[l].webkitGetAsEntry());else{var c=e.dataTransfer.files;if(null!=c)for(var l=0;l<c.length;l++)i.push(c.item(l))}!function d(t){r(function(){f?d(10):s(n,{$files:i,$event:e})},t||0)}()},!1)}}}])}();
/**
 * angular-ui-utils - Swiss-Army-Knife of AngularJS tools (with no external dependencies!)
 * @version v0.1.1 - 2014-02-05
 * @link http://angular-ui.github.com
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
"use strict";angular.module("ui.alias",[]).config(["$compileProvider","uiAliasConfig",function(a,b){b=b||{},angular.forEach(b,function(b,c){angular.isString(b)&&(b={replace:!0,template:b}),a.directive(c,function(){return b})})}]),angular.module("ui.event",[]).directive("uiEvent",["$parse",function(a){return function(b,c,d){var e=b.$eval(d.uiEvent);angular.forEach(e,function(d,e){var f=a(d);c.bind(e,function(a){var c=Array.prototype.slice.call(arguments);c=c.splice(1),f(b,{$event:a,$params:c}),b.$$phase||b.$apply()})})}}]),angular.module("ui.format",[]).filter("format",function(){return function(a,b){var c=a;if(angular.isString(c)&&void 0!==b)if(angular.isArray(b)||angular.isObject(b)||(b=[b]),angular.isArray(b)){var d=b.length,e=function(a,c){return c=parseInt(c,10),c>=0&&d>c?b[c]:a};c=c.replace(/\$([0-9]+)/g,e)}else angular.forEach(b,function(a,b){c=c.split(":"+b).join(a)});return c}}),angular.module("ui.highlight",[]).filter("highlight",function(){return function(a,b,c){return b||angular.isNumber(b)?(a=a.toString(),b=b.toString(),c?a.split(b).join('<span class="ui-match">'+b+"</span>"):a.replace(new RegExp(b,"gi"),'<span class="ui-match">$&</span>')):a}}),angular.module("ui.include",[]).directive("uiInclude",["$http","$templateCache","$anchorScroll","$compile",function(a,b,c,d){return{restrict:"ECA",terminal:!0,compile:function(e,f){var g=f.uiInclude||f.src,h=f.fragment||"",i=f.onload||"",j=f.autoscroll;return function(e,f){function k(){var k=++m,o=e.$eval(g),p=e.$eval(h);o?a.get(o,{cache:b}).success(function(a){if(k===m){l&&l.$destroy(),l=e.$new();var b;b=p?angular.element("<div/>").html(a).find(p):angular.element("<div/>").html(a).contents(),f.html(b),d(b)(l),!angular.isDefined(j)||j&&!e.$eval(j)||c(),l.$emit("$includeContentLoaded"),e.$eval(i)}}).error(function(){k===m&&n()}):n()}var l,m=0,n=function(){l&&(l.$destroy(),l=null),f.html("")};e.$watch(h,k),e.$watch(g,k)}}}}]),angular.module("ui.indeterminate",[]).directive("uiIndeterminate",[function(){return{compile:function(a,b){return b.type&&"checkbox"===b.type.toLowerCase()?function(a,b,c){a.$watch(c.uiIndeterminate,function(a){b[0].indeterminate=!!a})}:angular.noop}}}]),angular.module("ui.inflector",[]).filter("inflector",function(){function a(a){return a.replace(/^([a-z])|\s+([a-z])/g,function(a){return a.toUpperCase()})}function b(a,b){return a.replace(/[A-Z]/g,function(a){return b+a})}var c={humanize:function(c){return a(b(c," ").split("_").join(" "))},underscore:function(a){return a.substr(0,1).toLowerCase()+b(a.substr(1),"_").toLowerCase().split(" ").join("_")},variable:function(b){return b=b.substr(0,1).toLowerCase()+a(b.split("_").join(" ")).substr(1).split(" ").join("")}};return function(a,b){return b!==!1&&angular.isString(a)?(b=b||"humanize",c[b](a)):a}}),angular.module("ui.jq",[]).value("uiJqConfig",{}).directive("uiJq",["uiJqConfig","$timeout",function(a,b){return{restrict:"A",compile:function(c,d){if(!angular.isFunction(c[d.uiJq]))throw new Error('ui-jq: The "'+d.uiJq+'" function does not exist');var e=a&&a[d.uiJq];return function(a,c,d){function f(){b(function(){c[d.uiJq].apply(c,g)},0,!1)}var g=[];d.uiOptions?(g=a.$eval("["+d.uiOptions+"]"),angular.isObject(e)&&angular.isObject(g[0])&&(g[0]=angular.extend({},e,g[0]))):e&&(g=[e]),d.ngModel&&c.is("select,input,textarea")&&c.bind("change",function(){c.trigger("input")}),d.uiRefresh&&a.$watch(d.uiRefresh,function(){f()}),f()}}}}]),angular.module("ui.keypress",[]).factory("keypressHelper",["$parse",function(a){var b={8:"backspace",9:"tab",13:"enter",27:"esc",32:"space",33:"pageup",34:"pagedown",35:"end",36:"home",37:"left",38:"up",39:"right",40:"down",45:"insert",46:"delete"},c=function(a){return a.charAt(0).toUpperCase()+a.slice(1)};return function(d,e,f,g){var h,i=[];h=e.$eval(g["ui"+c(d)]),angular.forEach(h,function(b,c){var d,e;e=a(b),angular.forEach(c.split(" "),function(a){d={expression:e,keys:{}},angular.forEach(a.split("-"),function(a){d.keys[a]=!0}),i.push(d)})}),f.bind(d,function(a){var c=!(!a.metaKey||a.ctrlKey),f=!!a.altKey,g=!!a.ctrlKey,h=!!a.shiftKey,j=a.keyCode;"keypress"===d&&!h&&j>=97&&122>=j&&(j-=32),angular.forEach(i,function(d){var i=d.keys[b[j]]||d.keys[j.toString()],k=!!d.keys.meta,l=!!d.keys.alt,m=!!d.keys.ctrl,n=!!d.keys.shift;i&&k===c&&l===f&&m===g&&n===h&&e.$apply(function(){d.expression(e,{$event:a})})})})}}]),angular.module("ui.keypress").directive("uiKeydown",["keypressHelper",function(a){return{link:function(b,c,d){a("keydown",b,c,d)}}}]),angular.module("ui.keypress").directive("uiKeypress",["keypressHelper",function(a){return{link:function(b,c,d){a("keypress",b,c,d)}}}]),angular.module("ui.keypress").directive("uiKeyup",["keypressHelper",function(a){return{link:function(b,c,d){a("keyup",b,c,d)}}}]),angular.module("ui.mask",[]).value("uiMaskConfig",{maskDefinitions:{9:/\d/,A:/[a-zA-Z]/,"*":/[a-zA-Z0-9]/}}).directive("uiMask",["uiMaskConfig",function(a){return{priority:100,require:"ngModel",restrict:"A",compile:function(){var b=a;return function(a,c,d,e){function f(a){return angular.isDefined(a)?(s(a),N?(k(),l(),!0):j()):j()}function g(a){angular.isDefined(a)&&(D=a,N&&w())}function h(a){return N?(G=o(a||""),I=n(G),e.$setValidity("mask",I),I&&G.length?p(G):void 0):a}function i(a){return N?(G=o(a||""),I=n(G),e.$viewValue=G.length?p(G):"",e.$setValidity("mask",I),""===G&&void 0!==e.$error.required&&e.$setValidity("required",!1),I?G:void 0):a}function j(){return N=!1,m(),angular.isDefined(P)?c.attr("placeholder",P):c.removeAttr("placeholder"),angular.isDefined(Q)?c.attr("maxlength",Q):c.removeAttr("maxlength"),c.val(e.$modelValue),e.$viewValue=e.$modelValue,!1}function k(){G=K=o(e.$modelValue||""),H=J=p(G),I=n(G);var a=I&&G.length?H:"";d.maxlength&&c.attr("maxlength",2*B[B.length-1]),c.attr("placeholder",D),c.val(a),e.$viewValue=a}function l(){O||(c.bind("blur",t),c.bind("mousedown mouseup",u),c.bind("input keyup click focus",w),O=!0)}function m(){O&&(c.unbind("blur",t),c.unbind("mousedown",u),c.unbind("mouseup",u),c.unbind("input",w),c.unbind("keyup",w),c.unbind("click",w),c.unbind("focus",w),O=!1)}function n(a){return a.length?a.length>=F:!0}function o(a){var b="",c=C.slice();return a=a.toString(),angular.forEach(E,function(b){a=a.replace(b,"")}),angular.forEach(a.split(""),function(a){c.length&&c[0].test(a)&&(b+=a,c.shift())}),b}function p(a){var b="",c=B.slice();return angular.forEach(D.split(""),function(d,e){a.length&&e===c[0]?(b+=a.charAt(0)||"_",a=a.substr(1),c.shift()):b+=d}),b}function q(a){var b=d.placeholder;return"undefined"!=typeof b&&b[a]?b[a]:"_"}function r(){return D.replace(/[_]+/g,"_").replace(/([^_]+)([a-zA-Z0-9])([^_])/g,"$1$2_$3").split("_")}function s(a){var b=0;if(B=[],C=[],D="","string"==typeof a){F=0;var c=!1,d=a.split("");angular.forEach(d,function(a,d){R.maskDefinitions[a]?(B.push(b),D+=q(d),C.push(R.maskDefinitions[a]),b++,c||F++):"?"===a?c=!0:(D+=a,b++)})}B.push(B.slice().pop()+1),E=r(),N=B.length>1?!0:!1}function t(){L=0,M=0,I&&0!==G.length||(H="",c.val(""),a.$apply(function(){e.$setViewValue("")}))}function u(a){"mousedown"===a.type?c.bind("mouseout",v):c.unbind("mouseout",v)}function v(){M=A(this),c.unbind("mouseout",v)}function w(b){b=b||{};var d=b.which,f=b.type;if(16!==d&&91!==d){var g,h=c.val(),i=J,j=o(h),k=K,l=!1,m=y(this)||0,n=L||0,q=m-n,r=B[0],s=B[j.length]||B.slice().shift(),t=M||0,u=A(this)>0,v=t>0,w=h.length>i.length||t&&h.length>i.length-t,C=h.length<i.length||t&&h.length===i.length-t,D=d>=37&&40>=d&&b.shiftKey,E=37===d,F=8===d||"keyup"!==f&&C&&-1===q,G=46===d||"keyup"!==f&&C&&0===q&&!v,H=(E||F||"click"===f)&&m>r;if(M=A(this),!D&&(!u||"click"!==f&&"keyup"!==f)){if("input"===f&&C&&!v&&j===k){for(;F&&m>r&&!x(m);)m--;for(;G&&s>m&&-1===B.indexOf(m);)m++;var I=B.indexOf(m);j=j.substring(0,I)+j.substring(I+1),l=!0}for(g=p(j),J=g,K=j,c.val(g),l&&a.$apply(function(){e.$setViewValue(j)}),w&&r>=m&&(m=r+1),H&&m--,m=m>s?s:r>m?r:m;!x(m)&&m>r&&s>m;)m+=H?-1:1;(H&&s>m||w&&!x(n))&&m++,L=m,z(this,m)}}}function x(a){return B.indexOf(a)>-1}function y(a){if(!a)return 0;if(void 0!==a.selectionStart)return a.selectionStart;if(document.selection){a.focus();var b=document.selection.createRange();return b.moveStart("character",-a.value.length),b.text.length}return 0}function z(a,b){if(!a)return 0;if(0!==a.offsetWidth&&0!==a.offsetHeight)if(a.setSelectionRange)a.focus(),a.setSelectionRange(b,b);else if(a.createTextRange){var c=a.createTextRange();c.collapse(!0),c.moveEnd("character",b),c.moveStart("character",b),c.select()}}function A(a){return a?void 0!==a.selectionStart?a.selectionEnd-a.selectionStart:document.selection?document.selection.createRange().text.length:0:0}var B,C,D,E,F,G,H,I,J,K,L,M,N=!1,O=!1,P=d.placeholder,Q=d.maxlength,R={};d.uiOptions?(R=a.$eval("["+d.uiOptions+"]"),angular.isObject(R[0])&&(R=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&(b[c]?angular.extend(b[c],a[c]):b[c]=angular.copy(a[c]));return b}(b,R[0]))):R=b,d.$observe("uiMask",f),d.$observe("placeholder",g),e.$formatters.push(h),e.$parsers.push(i),c.bind("mousedown mouseup",u),Array.prototype.indexOf||(Array.prototype.indexOf=function(a){if(null===this)throw new TypeError;var b=Object(this),c=b.length>>>0;if(0===c)return-1;var d=0;if(arguments.length>1&&(d=Number(arguments[1]),d!==d?d=0:0!==d&&1/0!==d&&d!==-1/0&&(d=(d>0||-1)*Math.floor(Math.abs(d)))),d>=c)return-1;for(var e=d>=0?d:Math.max(c-Math.abs(d),0);c>e;e++)if(e in b&&b[e]===a)return e;return-1})}}}}]),angular.module("ui.reset",[]).value("uiResetConfig",null).directive("uiReset",["uiResetConfig",function(a){var b=null;return void 0!==a&&(b=a),{require:"ngModel",link:function(a,c,d,e){var f;f=angular.element('<a class="ui-reset" />'),c.wrap('<span class="ui-resetwrap" />').after(f),f.bind("click",function(c){c.preventDefault(),a.$apply(function(){e.$setViewValue(d.uiReset?a.$eval(d.uiReset):b),e.$render()})})}}}]),angular.module("ui.route",[]).directive("uiRoute",["$location","$parse",function(a,b){return{restrict:"AC",scope:!0,compile:function(c,d){var e;if(d.uiRoute)e="uiRoute";else if(d.ngHref)e="ngHref";else{if(!d.href)throw new Error("uiRoute missing a route or href property on "+c[0]);e="href"}return function(c,d,f){function g(b){var d=b.indexOf("#");d>-1&&(b=b.substr(d+1)),(j=function(){i(c,a.path().indexOf(b)>-1)})()}function h(b){var d=b.indexOf("#");d>-1&&(b=b.substr(d+1)),(j=function(){var d=new RegExp("^"+b+"$",["i"]);i(c,d.test(a.path()))})()}var i=b(f.ngModel||f.routeModel||"$uiRoute").assign,j=angular.noop;switch(e){case"uiRoute":f.uiRoute?h(f.uiRoute):f.$observe("uiRoute",h);break;case"ngHref":f.ngHref?g(f.ngHref):f.$observe("ngHref",g);break;case"href":g(f.href)}c.$on("$routeChangeSuccess",function(){j()}),c.$on("$stateChangeSuccess",function(){j()})}}}}]),angular.module("ui.scroll.jqlite",["ui.scroll"]).service("jqLiteExtras",["$log","$window",function(a,b){return{registerFor:function(a){var c,d,e,f,g,h,i;return d=angular.element.prototype.css,a.prototype.css=function(a,b){var c,e;return e=this,c=e[0],c&&3!==c.nodeType&&8!==c.nodeType&&c.style?d.call(e,a,b):void 0},h=function(a){return a&&a.document&&a.location&&a.alert&&a.setInterval},i=function(a,b,c){var d,e,f,g,i;return d=a[0],i={top:["scrollTop","pageYOffset","scrollLeft"],left:["scrollLeft","pageXOffset","scrollTop"]}[b],e=i[0],g=i[1],f=i[2],h(d)?angular.isDefined(c)?d.scrollTo(a[f].call(a),c):g in d?d[g]:d.document.documentElement[e]:angular.isDefined(c)?d[e]=c:d[e]},b.getComputedStyle?(f=function(a){return b.getComputedStyle(a,null)},c=function(a,b){return parseFloat(b)}):(f=function(a){return a.currentStyle},c=function(a,b){var c,d,e,f,g,h,i;return c=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,f=new RegExp("^("+c+")(?!px)[a-z%]+$","i"),f.test(b)?(i=a.style,d=i.left,g=a.runtimeStyle,h=g&&g.left,g&&(g.left=i.left),i.left=b,e=i.pixelLeft,i.left=d,h&&(g.left=h),e):parseFloat(b)}),e=function(a,b){var d,e,g,i,j,k,l,m,n,o,p,q,r;return h(a)?(d=document.documentElement[{height:"clientHeight",width:"clientWidth"}[b]],{base:d,padding:0,border:0,margin:0}):(r={width:[a.offsetWidth,"Left","Right"],height:[a.offsetHeight,"Top","Bottom"]}[b],d=r[0],l=r[1],m=r[2],k=f(a),p=c(a,k["padding"+l])||0,q=c(a,k["padding"+m])||0,e=c(a,k["border"+l+"Width"])||0,g=c(a,k["border"+m+"Width"])||0,i=k["margin"+l],j=k["margin"+m],n=c(a,i)||0,o=c(a,j)||0,{base:d,padding:p+q,border:e+g,margin:n+o})},g=function(a,b,c){var d,g,h;return g=e(a,b),g.base>0?{base:g.base-g.padding-g.border,outer:g.base,outerfull:g.base+g.margin}[c]:(d=f(a),h=d[b],(0>h||null===h)&&(h=a.style[b]||0),h=parseFloat(h)||0,{base:h-g.padding-g.border,outer:h,outerfull:h+g.padding+g.border+g.margin}[c])},angular.forEach({before:function(a){var b,c,d,e,f,g,h;if(f=this,c=f[0],e=f.parent(),b=e.contents(),b[0]===c)return e.prepend(a);for(d=g=1,h=b.length-1;h>=1?h>=g:g>=h;d=h>=1?++g:--g)if(b[d]===c)return void angular.element(b[d-1]).after(a);throw new Error("invalid DOM structure "+c.outerHTML)},height:function(a){var b;return b=this,angular.isDefined(a)?(angular.isNumber(a)&&(a+="px"),d.call(b,"height",a)):g(this[0],"height","base")},outerHeight:function(a){return g(this[0],"height",a?"outerfull":"outer")},offset:function(a){var b,c,d,e,f,g;return f=this,arguments.length?void 0===a?f:a:(b={top:0,left:0},e=f[0],(c=e&&e.ownerDocument)?(d=c.documentElement,e.getBoundingClientRect&&(b=e.getBoundingClientRect()),g=c.defaultView||c.parentWindow,{top:b.top+(g.pageYOffset||d.scrollTop)-(d.clientTop||0),left:b.left+(g.pageXOffset||d.scrollLeft)-(d.clientLeft||0)}):void 0)},scrollTop:function(a){return i(this,"top",a)},scrollLeft:function(a){return i(this,"left",a)}},function(b,c){return a.prototype[c]?void 0:a.prototype[c]=b})}}}]).run(["$log","$window","jqLiteExtras",function(a,b,c){return b.jQuery?void 0:c.registerFor(angular.element)}]),angular.module("ui.scroll",[]).directive("ngScrollViewport",["$log",function(){return{controller:["$scope","$element",function(a,b){return b}]}}]).directive("ngScroll",["$log","$injector","$rootScope","$timeout",function(a,b,c,d){return{require:["?^ngScrollViewport"],transclude:"element",priority:1e3,terminal:!0,compile:function(e,f,g){return function(f,h,i,j){var k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T;if(H=i.ngScroll.match(/^\s*(\w+)\s+in\s+(\w+)\s*$/),!H)throw new Error('Expected ngScroll in form of "item_ in _datasource_" but got "'+i.ngScroll+'"');if(F=H[1],v=H[2],D=function(a){return angular.isObject(a)&&a.get&&angular.isFunction(a.get)},u=f[v],!D(u)&&(u=b.get(v),!D(u)))throw new Error(v+" is not a valid datasource");return r=Math.max(3,+i.bufferSize||10),q=function(){return T.height()*Math.max(.1,+i.padding||.1)},O=function(a){return a[0].scrollHeight||a[0].document.documentElement.scrollHeight},k=null,g(R=f.$new(),function(a){var b,c,d,f,g,h;if(f=a[0].localName,"dl"===f)throw new Error("ng-scroll directive does not support <"+a[0].localName+"> as a repeating tag: "+a[0].outerHTML);return"li"!==f&&"tr"!==f&&(f="div"),h=j[0]||angular.element(window),h.css({"overflow-y":"auto",display:"block"}),d=function(a){var b,c,d;switch(a){case"tr":return d=angular.element("<table><tr><td><div></div></td></tr></table>"),b=d.find("div"),c=d.find("tr"),c.paddingHeight=function(){return b.height.apply(b,arguments)},c;default:return c=angular.element("<"+a+"></"+a+">"),c.paddingHeight=c.height,c}},c=function(a,b,c){return b[{top:"before",bottom:"after"}[c]](a),{paddingHeight:function(){return a.paddingHeight.apply(a,arguments)},insert:function(b){return a[{top:"after",bottom:"before"}[c]](b)}}},g=c(d(f),e,"top"),b=c(d(f),e,"bottom"),R.$destroy(),k={viewport:h,topPadding:g.paddingHeight,bottomPadding:b.paddingHeight,append:b.insert,prepend:g.insert,bottomDataPos:function(){return O(h)-b.paddingHeight()},topDataPos:function(){return g.paddingHeight()}}}),T=k.viewport,B=1,I=1,p=[],J=[],x=!1,n=!1,G=u.loading||function(){},E=!1,L=function(a,b){var c,d;for(c=d=a;b>=a?b>d:d>b;c=b>=a?++d:--d)p[c].scope.$destroy(),p[c].element.remove();return p.splice(a,b-a)},K=function(){return B=1,I=1,L(0,p.length),k.topPadding(0),k.bottomPadding(0),J=[],x=!1,n=!1,l(!1)},o=function(){return T.scrollTop()+T.height()},S=function(){return T.scrollTop()},P=function(){return!x&&k.bottomDataPos()<o()+q()},s=function(){var b,c,d,e,f,g;for(b=0,e=0,c=f=g=p.length-1;(0>=g?0>=f:f>=0)&&(d=p[c].element.outerHeight(!0),k.bottomDataPos()-b-d>o()+q());c=0>=g?++f:--f)b+=d,e++,x=!1;return e>0?(k.bottomPadding(k.bottomPadding()+b),L(p.length-e,p.length),I-=e,a.log("clipped off bottom "+e+" bottom padding "+k.bottomPadding())):void 0},Q=function(){return!n&&k.topDataPos()>S()-q()},t=function(){var b,c,d,e,f,g;for(e=0,d=0,f=0,g=p.length;g>f&&(b=p[f],c=b.element.outerHeight(!0),k.topDataPos()+e+c<S()-q());f++)e+=c,d++,n=!1;return d>0?(k.topPadding(k.topPadding()+e),L(0,d),B+=d,a.log("clipped off top "+d+" top padding "+k.topPadding())):void 0},w=function(a,b){return E||(E=!0,G(!0)),1===J.push(a)?z(b):void 0},C=function(a,b){var c,d,e;return c=f.$new(),c[F]=b,d=a>B,c.$index=a,d&&c.$index--,e={scope:c},g(c,function(b){return e.element=b,d?a===I?(k.append(b),p.push(e)):(p[a-B].element.after(b),p.splice(a-B+1,0,e)):(k.prepend(b),p.unshift(e))}),{appended:d,wrapper:e}},m=function(a,b){var c;return a?k.bottomPadding(Math.max(0,k.bottomPadding()-b.element.outerHeight(!0))):(c=k.topPadding()-b.element.outerHeight(!0),c>=0?k.topPadding(c):T.scrollTop(T.scrollTop()+b.element.outerHeight(!0)))},l=function(b,c,e){var f;return f=function(){return a.log("top {actual="+k.topDataPos()+" visible from="+S()+" bottom {visible through="+o()+" actual="+k.bottomDataPos()+"}"),P()?w(!0,b):Q()&&w(!1,b),e?e():void 0},c?d(function(){var a,b,d;for(b=0,d=c.length;d>b;b++)a=c[b],m(a.appended,a.wrapper);return f()}):f()},A=function(a,b){return l(a,b,function(){return J.shift(),0===J.length?(E=!1,G(!1)):z(a)})},z=function(b){var c;return c=J[0],c?p.length&&!P()?A(b):u.get(I,r,function(c){var d,e,f,g;if(e=[],0===c.length)x=!0,k.bottomPadding(0),a.log("appended: requested "+r+" records starting from "+I+" recieved: eof");else{for(t(),f=0,g=c.length;g>f;f++)d=c[f],e.push(C(++I,d));a.log("appended: requested "+r+" received "+c.length+" buffer size "+p.length+" first "+B+" next "+I)}return A(b,e)}):p.length&&!Q()?A(b):u.get(B-r,r,function(c){var d,e,f,g;if(e=[],0===c.length)n=!0,k.topPadding(0),a.log("prepended: requested "+r+" records starting from "+(B-r)+" recieved: bof");else{for(s(),d=f=g=c.length-1;0>=g?0>=f:f>=0;d=0>=g?++f:--f)e.unshift(C(--B,c[d]));a.log("prepended: requested "+r+" received "+c.length+" buffer size "+p.length+" first "+B+" next "+I)}return A(b,e)})},M=function(){return c.$$phase||E?void 0:(l(!1),f.$apply())},T.bind("resize",M),N=function(){return c.$$phase||E?void 0:(l(!0),f.$apply())},T.bind("scroll",N),f.$watch(u.revision,function(){return K()}),y=u.scope?u.scope.$new():f.$new(),f.$on("$destroy",function(){return y.$destroy(),T.unbind("resize",M),T.unbind("scroll",N)}),y.$on("update.items",function(a,b,c){var d,e,f,g,h;if(angular.isFunction(b))for(e=function(a){return b(a.scope)},f=0,g=p.length;g>f;f++)d=p[f],e(d);else 0<=(h=b-B-1)&&h<p.length&&(p[b-B-1].scope[F]=c);return null}),y.$on("delete.items",function(a,b){var c,d,e,f,g,h,i,j,k,m,n,o;if(angular.isFunction(b)){for(e=[],h=0,k=p.length;k>h;h++)d=p[h],e.unshift(d);for(g=function(a){return b(a.scope)?(L(e.length-1-c,e.length-c),I--):void 0},c=i=0,m=e.length;m>i;c=++i)f=e[c],g(f)}else 0<=(o=b-B-1)&&o<p.length&&(L(b-B-1,b-B),I--);for(c=j=0,n=p.length;n>j;c=++j)d=p[c],d.scope.$index=B+c;return l(!1)}),y.$on("insert.item",function(a,b,c){var d,e,f,g,h,i,j,k,m,n,o,q;if(e=[],angular.isFunction(b)){for(f=[],i=0,m=p.length;m>i;i++)c=p[i],f.unshift(c);for(h=function(a){var f,g,h,i,j;if(g=b(a.scope)){if(C=function(a,b){return C(a,b),I++},angular.isArray(g)){for(j=[],f=h=0,i=g.length;i>h;f=++h)c=g[f],j.push(e.push(C(d+f,c)));return j}return e.push(C(d,g))}},d=j=0,n=f.length;n>j;d=++j)g=f[d],h(g)}else 0<=(q=b-B-1)&&q<p.length&&(e.push(C(b,c)),I++);for(d=k=0,o=p.length;o>k;d=++k)c=p[d],c.scope.$index=B+d;return l(!1,e)})}}}}]),angular.module("ui.scrollfix",[]).directive("uiScrollfix",["$window",function(a){return{require:"^?uiScrollfixTarget",link:function(b,c,d,e){function f(){var b;if(angular.isDefined(a.pageYOffset))b=a.pageYOffset;else{var e=document.compatMode&&"BackCompat"!==document.compatMode?document.documentElement:document.body;b=e.scrollTop}!c.hasClass("ui-scrollfix")&&b>d.uiScrollfix?c.addClass("ui-scrollfix"):c.hasClass("ui-scrollfix")&&b<d.uiScrollfix&&c.removeClass("ui-scrollfix")}var g=c[0].offsetTop,h=e&&e.$element||angular.element(a);d.uiScrollfix?"string"==typeof d.uiScrollfix&&("-"===d.uiScrollfix.charAt(0)?d.uiScrollfix=g-parseFloat(d.uiScrollfix.substr(1)):"+"===d.uiScrollfix.charAt(0)&&(d.uiScrollfix=g+parseFloat(d.uiScrollfix.substr(1)))):d.uiScrollfix=g,h.on("scroll",f),b.$on("$destroy",function(){h.off("scroll",f)})}}}]).directive("uiScrollfixTarget",[function(){return{controller:["$element",function(a){this.$element=a}]}}]),angular.module("ui.showhide",[]).directive("uiShow",[function(){return function(a,b,c){a.$watch(c.uiShow,function(a){a?b.addClass("ui-show"):b.removeClass("ui-show")})}}]).directive("uiHide",[function(){return function(a,b,c){a.$watch(c.uiHide,function(a){a?b.addClass("ui-hide"):b.removeClass("ui-hide")})}}]).directive("uiToggle",[function(){return function(a,b,c){a.$watch(c.uiToggle,function(a){a?b.removeClass("ui-hide").addClass("ui-show"):b.removeClass("ui-show").addClass("ui-hide")})}}]),angular.module("ui.unique",[]).filter("unique",["$parse",function(a){return function(b,c){if(c===!1)return b;if((c||angular.isUndefined(c))&&angular.isArray(b)){var d=[],e=angular.isString(c)?a(c):function(a){return a},f=function(a){return angular.isObject(a)?e(a):a};angular.forEach(b,function(a){for(var b=!1,c=0;c<d.length;c++)if(angular.equals(f(d[c]),f(a))){b=!0;break}b||d.push(a)}),b=d}return b}}]),angular.module("ui.validate",[]).directive("uiValidate",function(){return{restrict:"A",require:"ngModel",link:function(a,b,c,d){function e(b){return angular.isString(b)?void a.$watch(b,function(){angular.forEach(g,function(a){a(d.$modelValue)})}):angular.isArray(b)?void angular.forEach(b,function(b){a.$watch(b,function(){angular.forEach(g,function(a){a(d.$modelValue)})})}):void(angular.isObject(b)&&angular.forEach(b,function(b,c){angular.isString(b)&&a.$watch(b,function(){g[c](d.$modelValue)}),angular.isArray(b)&&angular.forEach(b,function(b){a.$watch(b,function(){g[c](d.$modelValue)})})}))}var f,g={},h=a.$eval(c.uiValidate);h&&(angular.isString(h)&&(h={validator:h}),angular.forEach(h,function(b,c){f=function(e){var f=a.$eval(b,{$value:e});return angular.isObject(f)&&angular.isFunction(f.then)?(f.then(function(){d.$setValidity(c,!0)},function(){d.$setValidity(c,!1)}),e):f?(d.$setValidity(c,!0),e):(d.$setValidity(c,!1),e)},g[c]=f,d.$formatters.push(f),d.$parsers.push(f)}),c.uiValidateWatch&&e(a.$eval(c.uiValidateWatch)))}}}),angular.module("ui.utils",["ui.event","ui.format","ui.highlight","ui.include","ui.indeterminate","ui.inflector","ui.jq","ui.keypress","ui.mask","ui.reset","ui.route","ui.scrollfix","ui.scroll","ui.scroll.jqlite","ui.showhide","ui.unique","ui.validate"]);
var infiScroll=angular.module("infinite-scroll",[]);infiScroll.directive("infiniteScroll",["$rootScope","$window","$timeout",function(i,n,e){return{link:function(l,t,o){var r,c,f,a;return n=angular.element(n),f=0,null!=o.infiniteScrollDistance&&l.$watch(o.infiniteScrollDistance,function(i){return f=parseInt(i,10)}),a=!0,r=!1,null!=o.infiniteScrollDisabled&&l.$watch(o.infiniteScrollDisabled,function(i){return a=!i,a&&r?(r=!1,c()):void 0}),c=function(){var e,c,u,s;return s=n.height()+n.scrollTop(),e=t.offset().top+t.height(),c=e-s,u=c<=n.height()*f,u&&a?i.$$phase?l.$eval(o.infiniteScroll):l.$apply(o.infiniteScroll):u?r=!0:void 0},n.on("scroll",c),l.$on("$destroy",function(){return n.off("scroll",c)}),e(function(){return o.infiniteScrollImmediateCheck?l.$eval(o.infiniteScrollImmediateCheck)?c():void 0:c()},0)}}}]);
!function(n,e){return"function"==typeof define&&define.amd?void define(["angular"],function(n){return e(n)}):e(n)}(window.angular||null,function(n){var e=n.module("ngClickSelect",[]);return e.directive("ngClickSelect",function(){return{restrict:"AC",link:function(n,e){e.bind("click",function(){e.select()})}}}),e});

/*!
 * AngularFire is the officially supported AngularJS binding for Firebase. Firebase
 * is a full backend so you don't need servers to build your Angular app. AngularFire
 * provides you with the $firebase service which allows you to easily keep your $scope
 * variables in sync with your Firebase backend.
 *
 * AngularFire 1.0.0
 * https://github.com/firebase/angularfire/
 * Date: 03/04/2015
 * License: MIT
 */
!function(a){"use strict";angular.module("firebase",[]).value("Firebase",a.Firebase).value("firebaseBatchDelay",50)}(window),function(){"use strict";angular.module("firebase").factory("$firebaseArray",["$log","$firebaseUtils",function(a,b){function c(a){if(!(this instanceof c))return new c(a);var e=this;return this._observers=[],this.$list=[],this._ref=a,this._sync=new d(this),b.assertValidRef(a,"Must pass a valid Firebase reference to $firebaseArray (not a string or URL)"),this._indexCache={},b.getPublicMethods(e,function(a,b){e.$list[b]=a.bind(e)}),this._sync.init(this.$list),this.$list}function d(c){function d(a){if(!p.isDestroyed){p.isDestroyed=!0;var b=c.$ref();b.off("child_added",i),b.off("child_moved",k),b.off("child_changed",j),b.off("child_removed",l),c=null,o(a||"destroyed")}}function e(b){var d=c.$ref();d.on("child_added",i,n),d.on("child_moved",k,n),d.on("child_changed",j,n),d.on("child_removed",l,n),d.once("value",function(c){angular.isArray(c.val())&&a.warn("Storing data using array indices in Firebase can result in unexpected behavior. See https://www.firebase.com/docs/web/guide/understanding-data.html#section-arrays-in-firebase for more information."),o(null,b)},o)}function f(a,b){m||(m=!0,a?g.reject(a):g.resolve(b))}var g=b.defer(),h=b.batch(),i=h(function(a,b){var d=c.$$added(a,b);d&&c.$$process("child_added",d,b)}),j=h(function(a){var d=c.$getRecord(b.getKey(a));if(d){var e=c.$$updated(a);e&&c.$$process("child_changed",d)}}),k=h(function(a,d){var e=c.$getRecord(b.getKey(a));if(e){var f=c.$$moved(a,d);f&&c.$$process("child_moved",e,d)}}),l=h(function(a){var d=c.$getRecord(b.getKey(a));if(d){var e=c.$$removed(a);e&&c.$$process("child_removed",d)}}),m=!1,n=h(function(a){f(a),c.$$error(a)}),o=h(f),p={destroy:d,isDestroyed:!1,init:e,ready:function(){return g.promise}};return p}return c.prototype={$add:function(a){this._assertNotDestroyed("$add");var c=b.defer(),d=this.$ref().ref().push();return d.set(b.toJSON(a),b.makeNodeResolver(c)),c.promise.then(function(){return d})},$save:function(a){this._assertNotDestroyed("$save");var c=this,d=c._resolveItem(a),e=c.$keyAt(d);if(null!==e){var f=c.$ref().ref().child(e),g=b.toJSON(d);return b.doSet(f,g).then(function(){return c.$$notify("child_changed",e),f})}return b.reject("Invalid record; could determine key for "+a)},$remove:function(a){this._assertNotDestroyed("$remove");var c=this.$keyAt(a);if(null!==c){var d=this.$ref().ref().child(c);return b.doRemove(d).then(function(){return d})}return b.reject("Invalid record; could not determine key for "+a)},$keyAt:function(a){var b=this._resolveItem(a);return this.$$getKey(b)},$indexFor:function(a){var b=this,c=b._indexCache;if(!c.hasOwnProperty(a)||b.$keyAt(c[a])!==a){var d=b.$list.findIndex(function(c){return b.$$getKey(c)===a});-1!==d&&(c[a]=d)}return c.hasOwnProperty(a)?c[a]:-1},$loaded:function(a,b){var c=this._sync.ready();return arguments.length&&(c=c.then.call(c,a,b)),c},$ref:function(){return this._ref},$watch:function(a,b){var c=this._observers;return c.push([a,b]),function(){var d=c.findIndex(function(c){return c[0]===a&&c[1]===b});d>-1&&c.splice(d,1)}},$destroy:function(b){this._isDestroyed||(this._isDestroyed=!0,this._sync.destroy(b),this.$list.length=0,a.debug("destroy called for FirebaseArray: "+this.$ref().ref().toString()))},$getRecord:function(a){var b=this.$indexFor(a);return b>-1?this.$list[b]:null},$$added:function(a){var c=this.$indexFor(b.getKey(a));if(-1===c){var d=a.val();return angular.isObject(d)||(d={$value:d}),d.$id=b.getKey(a),d.$priority=a.getPriority(),b.applyDefaults(d,this.$$defaults),d}return!1},$$removed:function(a){return this.$indexFor(b.getKey(a))>-1},$$updated:function(a){var c=!1,d=this.$getRecord(b.getKey(a));return angular.isObject(d)&&(c=b.updateRec(d,a),b.applyDefaults(d,this.$$defaults)),c},$$moved:function(a){var c=this.$getRecord(b.getKey(a));return angular.isObject(c)?(c.$priority=a.getPriority(),!0):!1},$$error:function(b){a.error(b),this.$destroy(b)},$$getKey:function(a){return angular.isObject(a)?a.$id:null},$$process:function(a,b,c){var d,e=this.$$getKey(b),f=!1;switch(a){case"child_added":d=this.$indexFor(e);break;case"child_moved":d=this.$indexFor(e),this._spliceOut(e);break;case"child_removed":f=null!==this._spliceOut(e);break;case"child_changed":f=!0;break;default:throw new Error("Invalid event type: "+a)}return angular.isDefined(d)&&(f=this._addAfter(b,c)!==d),f&&this.$$notify(a,e,c),f},$$notify:function(a,b,c){var d={event:a,key:b};angular.isDefined(c)&&(d.prevChild=c),angular.forEach(this._observers,function(a){a[0].call(a[1],d)})},_addAfter:function(a,b){var c;return null===b?c=0:(c=this.$indexFor(b)+1,0===c&&(c=this.$list.length)),this.$list.splice(c,0,a),this._indexCache[this.$$getKey(a)]=c,c},_spliceOut:function(a){var b=this.$indexFor(a);return b>-1?(delete this._indexCache[a],this.$list.splice(b,1)[0]):null},_resolveItem:function(a){var b=this.$list;if(angular.isNumber(a)&&a>=0&&b.length>=a)return b[a];if(angular.isObject(a)){var c=this.$$getKey(a),d=this.$getRecord(c);return d===a?d:null}return null},_assertNotDestroyed:function(a){if(this._isDestroyed)throw new Error("Cannot call "+a+" method on a destroyed $firebaseArray object")}},c.$extend=function(a,d){return 1===arguments.length&&angular.isObject(a)&&(d=a,a=function(){return c.apply(this,arguments)}),b.inherit(a,c,d)},c}]),angular.module("firebase").factory("$FirebaseArray",["$log","$firebaseArray",function(a,b){return function(){return a.warn("$FirebaseArray has been renamed. Use $firebaseArray instead."),b.apply(null,arguments)}}])}(),function(){"use strict";var a;angular.module("firebase").factory("$firebaseAuth",["$q","$firebaseUtils","$log",function(b,c,d){return function(e){var f=new a(b,c,d,e);return f.construct()}}]),a=function(a,b,c,d){if(this._q=a,this._utils=b,this._log=c,"string"==typeof d)throw new Error("Please provide a Firebase reference instead of a URL when creating a `$firebaseAuth` object.");this._ref=d},a.prototype={construct:function(){return this._object={$authWithCustomToken:this.authWithCustomToken.bind(this),$authAnonymously:this.authAnonymously.bind(this),$authWithPassword:this.authWithPassword.bind(this),$authWithOAuthPopup:this.authWithOAuthPopup.bind(this),$authWithOAuthRedirect:this.authWithOAuthRedirect.bind(this),$authWithOAuthToken:this.authWithOAuthToken.bind(this),$unauth:this.unauth.bind(this),$onAuth:this.onAuth.bind(this),$getAuth:this.getAuth.bind(this),$requireAuth:this.requireAuth.bind(this),$waitForAuth:this.waitForAuth.bind(this),$createUser:this.createUser.bind(this),$changePassword:this.changePassword.bind(this),$changeEmail:this.changeEmail.bind(this),$removeUser:this.removeUser.bind(this),$resetPassword:this.resetPassword.bind(this)},this._object},authWithCustomToken:function(a,b){var c=this._q.defer();try{this._ref.authWithCustomToken(a,this._utils.makeNodeResolver(c),b)}catch(d){c.reject(d)}return c.promise},authAnonymously:function(a){var b=this._q.defer();try{this._ref.authAnonymously(this._utils.makeNodeResolver(b),a)}catch(c){b.reject(c)}return b.promise},authWithPassword:function(a,b){var c=this._q.defer();try{this._ref.authWithPassword(a,this._utils.makeNodeResolver(c),b)}catch(d){c.reject(d)}return c.promise},authWithOAuthPopup:function(a,b){var c=this._q.defer();try{this._ref.authWithOAuthPopup(a,this._utils.makeNodeResolver(c),b)}catch(d){c.reject(d)}return c.promise},authWithOAuthRedirect:function(a,b){var c=this._q.defer();try{this._ref.authWithOAuthRedirect(a,this._utils.makeNodeResolver(c),b)}catch(d){c.reject(d)}return c.promise},authWithOAuthToken:function(a,b,c){var d=this._q.defer();try{this._ref.authWithOAuthToken(a,b,this._utils.makeNodeResolver(d),c)}catch(e){d.reject(e)}return d.promise},unauth:function(){null!==this.getAuth()&&this._ref.unauth()},onAuth:function(a,b){var c=this,d=this._utils.debounce(a,b,0);return this._ref.onAuth(d),function(){c._ref.offAuth(d)}},getAuth:function(){return this._ref.getAuth()},_routerMethodOnAuthPromise:function(a){var b=this._ref;return this._utils.promise(function(c,d){function e(f){return b.offAuth(e),null!==f?void c(f):a?void d("AUTH_REQUIRED"):void c(null)}b.onAuth(e)})},requireAuth:function(){return this._routerMethodOnAuthPromise(!0)},waitForAuth:function(){return this._routerMethodOnAuthPromise(!1)},createUser:function(a){var b=this._q.defer();if("string"==typeof a)throw new Error("$createUser() expects an object containing 'email' and 'password', but got a string.");try{this._ref.createUser(a,this._utils.makeNodeResolver(b))}catch(c){b.reject(c)}return b.promise},changePassword:function(a){var b=this._q.defer();if("string"==typeof a)throw new Error("$changePassword() expects an object containing 'email', 'oldPassword', and 'newPassword', but got a string.");try{this._ref.changePassword(a,this._utils.makeNodeResolver(b))}catch(c){b.reject(c)}return b.promise},changeEmail:function(a){if("function"!=typeof this._ref.changeEmail)throw new Error("$changeEmail() expects an object containing 'oldEmail', 'newEmail', and 'password', but got a string.");var b=this._q.defer();try{this._ref.changeEmail(a,this._utils.makeNodeResolver(b))}catch(c){b.reject(c)}return b.promise},removeUser:function(a){var b=this._q.defer();if("string"==typeof a)throw new Error("$removeUser() expects an object containing 'email' and 'password', but got a string.");try{this._ref.removeUser(a,this._utils.makeNodeResolver(b))}catch(c){b.reject(c)}return b.promise},resetPassword:function(a){var b=this._q.defer();if("string"==typeof a)throw new Error("$resetPassword() expects an object containing 'email', but got a string.");try{this._ref.resetPassword(a,this._utils.makeNodeResolver(b))}catch(c){b.reject(c)}return b.promise}}}(),function(){"use strict";angular.module("firebase").factory("$firebaseObject",["$parse","$firebaseUtils","$log",function(a,b,c){function d(a){return this instanceof d?(this.$$conf={sync:new f(this,a),ref:a,binding:new e(this),listeners:[]},Object.defineProperty(this,"$$conf",{value:this.$$conf}),this.$id=b.getKey(a.ref()),this.$priority=null,b.applyDefaults(this,this.$$defaults),void this.$$conf.sync.init()):new d(a)}function e(a){this.subs=[],this.scope=null,this.key=null,this.rec=a}function f(a,d){function e(b){n.isDestroyed||(n.isDestroyed=!0,d.off("value",k),a=null,m(b||"destroyed"))}function f(){d.on("value",k,l),d.once("value",function(a){angular.isArray(a.val())&&c.warn("Storing data using array indices in Firebase can result in unexpected behavior. See https://www.firebase.com/docs/web/guide/understanding-data.html#section-arrays-in-firebase for more information. Also note that you probably wanted $firebaseArray and not $firebaseObject."),m(null)},m)}function g(b){h||(h=!0,b?i.reject(b):i.resolve(a))}var h=!1,i=b.defer(),j=b.batch(),k=j(function(b){var c=a.$$updated(b);c&&a.$$notify()}),l=j(a.$$error,a),m=j(g),n={isDestroyed:!1,destroy:e,init:f,ready:function(){return i.promise}};return n}return d.prototype={$save:function(){var a=this,c=a.$ref(),d=b.toJSON(a);return b.doSet(c,d).then(function(){return a.$$notify(),a.$ref()})},$remove:function(){var a=this;return b.trimKeys(a,{}),a.$value=null,b.doRemove(a.$ref()).then(function(){return a.$$notify(),a.$ref()})},$loaded:function(a,b){var c=this.$$conf.sync.ready();return arguments.length&&(c=c.then.call(c,a,b)),c},$ref:function(){return this.$$conf.ref},$bindTo:function(a,b){var c=this;return c.$loaded().then(function(){return c.$$conf.binding.bindTo(a,b)})},$watch:function(a,b){var c=this.$$conf.listeners;return c.push([a,b]),function(){var d=c.findIndex(function(c){return c[0]===a&&c[1]===b});d>-1&&c.splice(d,1)}},$destroy:function(a){var c=this;c.$isDestroyed||(c.$isDestroyed=!0,c.$$conf.sync.destroy(a),c.$$conf.binding.destroy(),b.each(c,function(a,b){delete c[b]}))},$$updated:function(a){var c=b.updateRec(this,a);return b.applyDefaults(this,this.$$defaults),c},$$error:function(a){c.error(a),this.$destroy(a)},$$scopeUpdated:function(a){var c=b.defer();return this.$ref().set(b.toJSON(a),b.makeNodeResolver(c)),c.promise},$$notify:function(){var a=this,b=this.$$conf.listeners.slice();angular.forEach(b,function(b){b[0].call(b[1],{event:"value",key:a.$id})})},forEach:function(a,c){return b.each(this,a,c)}},d.$extend=function(a,c){return 1===arguments.length&&angular.isObject(a)&&(c=a,a=function(){d.apply(this,arguments)}),b.inherit(a,d,c)},e.prototype={assertNotBound:function(a){if(this.scope){var d="Cannot bind to "+a+" because this instance is already bound to "+this.key+"; one binding per instance (call unbind method or create another FirebaseObject instance)";return c.error(d),b.reject(d)}},bindTo:function(c,d){function e(e){function f(a){return angular.equals(a,k)&&a.$priority===k.$priority&&a.$value===k.$value}function g(a){j.assign(c,b.scopeData(a))}function h(){var a=j(c);return[a,a.$priority,a.$value]}var i=!1,j=a(d),k=e.rec;e.scope=c,e.varName=d;var l=b.debounce(function(a){var d=b.scopeData(a);k.$$scopeUpdated(d)["finally"](function(){i=!1,d.hasOwnProperty("$value")||(delete k.$value,delete j(c).$value)})},50,500),m=function(a){a=a[0],f(a)||(i=!0,l(a))},n=function(){i||f(j(c))||g(k)};return g(k),e.subs.push(c.$on("$destroy",e.unbind.bind(e))),e.subs.push(c.$watch(h,m,!0)),e.subs.push(k.$watch(n)),e.unbind.bind(e)}return this.assertNotBound(d)||e(this)},unbind:function(){this.scope&&(angular.forEach(this.subs,function(a){a()}),this.subs=[],this.scope=null,this.key=null)},destroy:function(){this.unbind(),this.rec=null}},d}]),angular.module("firebase").factory("$FirebaseObject",["$log","$firebaseObject",function(a,b){return function(){return a.warn("$FirebaseObject has been renamed. Use $firebaseObject instead."),b.apply(null,arguments)}}])}(),function(){"use strict";angular.module("firebase").factory("$firebase",function(){return function(){throw new Error("$firebase has been removed. You may instantiate $firebaseArray and $firebaseObject directly now. For simple write operations, just use the Firebase ref directly. See the AngularFire 1.0.0 changelog for details: https://www.firebase.com/docs/web/libraries/angular/changelog.html")}})}(),Array.prototype.indexOf||(Array.prototype.indexOf=function(a,b){if(void 0===this||null===this)throw new TypeError("'this' is null or not defined");var c=this.length>>>0;for(b=+b||0,1/0===Math.abs(b)&&(b=0),0>b&&(b+=c,0>b&&(b=0));c>b;b++)if(this[b]===a)return b;return-1}),Function.prototype.bind||(Function.prototype.bind=function(a){if("function"!=typeof this)throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");var b=Array.prototype.slice.call(arguments,1),c=this,d=function(){},e=function(){return c.apply(this instanceof d&&a?this:a,b.concat(Array.prototype.slice.call(arguments)))};return d.prototype=this.prototype,e.prototype=new d,e}),Array.prototype.findIndex||Object.defineProperty(Array.prototype,"findIndex",{enumerable:!1,configurable:!0,writable:!0,value:function(a){if(null==this)throw new TypeError("Array.prototype.find called on null or undefined");if("function"!=typeof a)throw new TypeError("predicate must be a function");for(var b,c=Object(this),d=c.length>>>0,e=arguments[1],f=0;d>f;f++)if(f in c&&(b=c[f],a.call(e,b,f,c)))return f;return-1}}),"function"!=typeof Object.create&&!function(){var a=function(){};Object.create=function(b){if(arguments.length>1)throw new Error("Second argument not supported");if(null===b)throw new Error("Cannot set a null [[Prototype]]");if("object"!=typeof b)throw new TypeError("Argument must be an object");return a.prototype=b,new a}}(),Object.keys||(Object.keys=function(){"use strict";var a=Object.prototype.hasOwnProperty,b=!{toString:null}.propertyIsEnumerable("toString"),c=["toString","toLocaleString","valueOf","hasOwnProperty","isPrototypeOf","propertyIsEnumerable","constructor"],d=c.length;return function(e){if("object"!=typeof e&&("function"!=typeof e||null===e))throw new TypeError("Object.keys called on non-object");var f,g,h=[];for(f in e)a.call(e,f)&&h.push(f);if(b)for(g=0;d>g;g++)a.call(e,c[g])&&h.push(c[g]);return h}}()),"function"!=typeof Object.getPrototypeOf&&(Object.getPrototypeOf="object"==typeof"test".__proto__?function(a){return a.__proto__}:function(a){return a.constructor.prototype}),function(){"use strict";function a(b){if(!angular.isObject(b))return b;var c=angular.isArray(b)?[]:{};return angular.forEach(b,function(b,d){("string"!=typeof d||"$"!==d.charAt(0))&&(c[d]=a(b))}),c}angular.module("firebase").factory("$firebaseConfig",["$firebaseArray","$firebaseObject","$injector",function(a,b,c){return function(d){var e=angular.extend({},d);return"string"==typeof e.objectFactory&&(e.objectFactory=c.get(e.objectFactory)),"string"==typeof e.arrayFactory&&(e.arrayFactory=c.get(e.arrayFactory)),angular.extend({arrayFactory:a,objectFactory:b},e)}}]).factory("$firebaseUtils",["$q","$timeout","firebaseBatchDelay",function(b,c,d){function e(a){function c(a){e.resolve(a)}function d(a){e.reject(a)}if(!angular.isFunction(a))throw new Error("missing resolver function");var e=b.defer();return a(c,d),e.promise}var f={batch:function(a,b){function c(a,b){if("function"!=typeof a)throw new Error("Must provide a function to be batched. Got "+a);return function(){var c=Array.prototype.slice.call(arguments,0);k.push([a,b,c]),e()}}function e(){i&&(i(),i=null),h&&Date.now()-h>b?j||(j=!0,f.compile(g)):(h||(h=Date.now()),i=f.wait(g,a))}function g(){i=null,h=null,j=!1;var a=k.slice(0);k=[],angular.forEach(a,function(a){a[0].apply(a[1],a[2])})}a=d,b||(b=10*a||100);var h,i,j,k=[];return c},debounce:function(a,b,c,d){function e(){j&&(j(),j=null),i&&Date.now()-i>d?l||(l=!0,f.compile(g)):(i||(i=Date.now()),j=f.wait(g,c))}function g(){j=null,i=null,l=!1,a.apply(b,k)}function h(){k=Array.prototype.slice.call(arguments,0),e()}var i,j,k,l;if("number"==typeof b&&(d=c,c=b,b=null),"number"!=typeof c)throw new Error("Must provide a valid integer for wait. Try 0 for a default");if("function"!=typeof a)throw new Error("Must provide a valid function to debounce");return d||(d=10*c||100),h.running=function(){return i>0},h},assertValidRef:function(a,b){if(!angular.isObject(a)||"function"!=typeof a.ref||"function"!=typeof a.ref().transaction)throw new Error(b||"Invalid Firebase reference")},inherit:function(a,b,c){var d=a.prototype;return a.prototype=Object.create(b.prototype),a.prototype.constructor=a,angular.forEach(Object.keys(d),function(b){a.prototype[b]=d[b]}),angular.isObject(c)&&angular.extend(a.prototype,c),a},getPrototypeMethods:function(a,b,c){for(var d={},e=Object.getPrototypeOf({}),f=angular.isFunction(a)&&angular.isObject(a.prototype)?a.prototype:Object.getPrototypeOf(a);f&&f!==e;){for(var g in f)f.hasOwnProperty(g)&&!d.hasOwnProperty(g)&&(d[g]=!0,b.call(c,f[g],g,f));f=Object.getPrototypeOf(f)}},getPublicMethods:function(a,b,c){f.getPrototypeMethods(a,function(a,d){"function"==typeof a&&"_"!==d.charAt(0)&&b.call(c,a,d)})},defer:b.defer,reject:b.reject,resolve:b.when,promise:angular.isFunction(b)?b:e,makeNodeResolver:function(a){return function(b,c){null===b?(arguments.length>2&&(c=Array.prototype.slice.call(arguments,1)),a.resolve(c)):a.reject(b)}},wait:function(a,b){var d=c(a,b||0);return function(){d&&(c.cancel(d),d=null)}},compile:function(a){return c(a||function(){})},deepCopy:function(a){if(!angular.isObject(a))return a;var b=angular.isArray(a)?a.slice():angular.extend({},a);for(var c in b)b.hasOwnProperty(c)&&angular.isObject(b[c])&&(b[c]=f.deepCopy(b[c]));return b},trimKeys:function(a,b){f.each(a,function(c,d){b.hasOwnProperty(d)||delete a[d]})},scopeData:function(a){var b={$id:a.$id,$priority:a.$priority},c=!1;return f.each(a,function(a,d){c=!0,b[d]=f.deepCopy(a)}),!c&&a.hasOwnProperty("$value")&&(b.$value=a.$value),b},updateRec:function(a,b){var c=b.val(),d=angular.extend({},a);return angular.isObject(c)?delete a.$value:(a.$value=c,c={}),f.trimKeys(a,c),angular.extend(a,c),a.$priority=b.getPriority(),!angular.equals(d,a)||d.$value!==a.$value||d.$priority!==a.$priority},applyDefaults:function(a,b){return angular.isObject(b)&&angular.forEach(b,function(b,c){a.hasOwnProperty(c)||(a[c]=b)}),a},dataKeys:function(a){var b=[];return f.each(a,function(a,c){b.push(c)}),b},each:function(a,b,c){if(angular.isObject(a)){for(var d in a)if(a.hasOwnProperty(d)){var e=d.charAt(0);"_"!==e&&"$"!==e&&"."!==e&&b.call(c,a[d],d,a)}}else if(angular.isArray(a))for(var f=0,g=a.length;g>f;f++)b.call(c,a[f],f,a);return a},getKey:function(a){return"function"==typeof a.key?a.key():a.name()},toJSON:function(b){var c;return angular.isObject(b)||(b={$value:b}),angular.isFunction(b.toJSON)?c=b.toJSON():(c={},f.each(b,function(b,d){c[d]=a(b)})),angular.isDefined(b.$value)&&0===Object.keys(c).length&&null!==b.$value&&(c[".value"]=b.$value),angular.isDefined(b.$priority)&&Object.keys(c).length>0&&null!==b.$priority&&(c[".priority"]=b.$priority),angular.forEach(c,function(a,b){if(b.match(/[.$\[\]#\/]/)&&".value"!==b&&".priority"!==b)throw new Error("Invalid key "+b+" (cannot contain .$[]#)");if(angular.isUndefined(a))throw new Error("Key "+b+" was undefined. Cannot pass undefined in JSON. Use null instead.")}),c},doSet:function(a,b){var c=f.defer();if(angular.isFunction(a.set)||!angular.isObject(b))a.set(b,f.makeNodeResolver(c));else{var d=angular.extend({},b);a.once("value",function(b){b.forEach(function(a){d.hasOwnProperty(f.getKey(a))||(d[f.getKey(a)]=null)}),a.ref().update(d,f.makeNodeResolver(c))},function(a){c.reject(a)})}return c.promise},doRemove:function(a){var b=f.defer();return angular.isFunction(a.remove)?a.remove(f.makeNodeResolver(b)):a.once("value",function(c){var d=[];c.forEach(function(a){var c=f.defer();d.push(c.promise),a.ref().remove(f.makeNodeResolver(b))}),f.allPromises(d).then(function(){b.resolve(a)},function(a){b.reject(a)})},function(a){b.reject(a)}),b.promise},VERSION:"1.0.0",batchDelay:d,allPromises:b.all.bind(b)};return f}])}();
/*
 * angular-loading-bar
 *
 * intercepts XHR requests and creates a loading bar.
 * Based on the excellent nprogress work by rstacruz (more info in readme)
 *
 * (c) 2013 Wes Cruver
 * License: MIT
 */


(function() {

'use strict';

// Alias the loading bar for various backwards compatibilities since the project has matured:
angular.module('angular-loading-bar', ['cfp.loadingBarInterceptor']);
angular.module('chieffancypants.loadingBar', ['cfp.loadingBarInterceptor']);


/**
 * loadingBarInterceptor service
 *
 * Registers itself as an Angular interceptor and listens for XHR requests.
 */
angular.module('cfp.loadingBarInterceptor', ['cfp.loadingBar'])
  .config(['$httpProvider', function ($httpProvider) {

    var interceptor = ['$q', '$cacheFactory', '$timeout', '$rootScope', '$log', 'cfpLoadingBar', function ($q, $cacheFactory, $timeout, $rootScope, $log, cfpLoadingBar) {

      /**
       * The total number of requests made
       */
      var reqsTotal = 0;

      /**
       * The number of requests completed (either successfully or not)
       */
      var reqsCompleted = 0;

      /**
       * The amount of time spent fetching before showing the loading bar
       */
      var latencyThreshold = cfpLoadingBar.latencyThreshold;

      /**
       * $timeout handle for latencyThreshold
       */
      var startTimeout;


      /**
       * calls cfpLoadingBar.complete() which removes the
       * loading bar from the DOM.
       */
      function setComplete() {
        $timeout.cancel(startTimeout);
        cfpLoadingBar.complete();
        reqsCompleted = 0;
        reqsTotal = 0;
      }

      /**
       * Determine if the response has already been cached
       * @param  {Object}  config the config option from the request
       * @return {Boolean} retrns true if cached, otherwise false
       */
      function isCached(config) {
        var cache;
        var defaultCache = $cacheFactory.get('$http');
        var defaults = $httpProvider.defaults;

        // Choose the proper cache source. Borrowed from angular: $http service
        if ((config.cache || defaults.cache) && config.cache !== false &&
          (config.method === 'GET' || config.method === 'JSONP')) {
            cache = angular.isObject(config.cache) ? config.cache
              : angular.isObject(defaults.cache) ? defaults.cache
              : defaultCache;
        }

        var cached = cache !== undefined ?
          cache.get(config.url) !== undefined : false;

        if (config.cached !== undefined && cached !== config.cached) {
          return config.cached;
        }
        config.cached = cached;
        return cached;
      }


      return {
        'request': function(config) {
          // Check to make sure this request hasn't already been cached and that
          // the requester didn't explicitly ask us to ignore this request:
          if (!config.ignoreLoadingBar && !isCached(config)) {
            $rootScope.$broadcast('cfpLoadingBar:loading', {url: config.url});
            if (reqsTotal === 0) {
              startTimeout = $timeout(function() {
                cfpLoadingBar.start();
              }, latencyThreshold);
            }
            reqsTotal++;
            cfpLoadingBar.set(reqsCompleted / reqsTotal);
          }
          return config;
        },

        'response': function(response) {
          if (!response || !response.config) {
            $log.error('Broken interceptor detected: Config object not supplied in response:\n https://github.com/chieffancypants/angular-loading-bar/pull/50');
            return response;
          }

          if (!response.config.ignoreLoadingBar && !isCached(response.config)) {
            reqsCompleted++;
            $rootScope.$broadcast('cfpLoadingBar:loaded', {url: response.config.url, result: response});
            if (reqsCompleted >= reqsTotal) {
              setComplete();
            } else {
              cfpLoadingBar.set(reqsCompleted / reqsTotal);
            }
          }
          return response;
        },

        'responseError': function(rejection) {
          if (!rejection || !rejection.config) {
            $log.error('Broken interceptor detected: Config object not supplied in rejection:\n https://github.com/chieffancypants/angular-loading-bar/pull/50');
            return $q.reject(rejection);
          }

          if (!rejection.config.ignoreLoadingBar && !isCached(rejection.config)) {
            reqsCompleted++;
            $rootScope.$broadcast('cfpLoadingBar:loaded', {url: rejection.config.url, result: rejection});
            if (reqsCompleted >= reqsTotal) {
              setComplete();
            } else {
              cfpLoadingBar.set(reqsCompleted / reqsTotal);
            }
          }
          return $q.reject(rejection);
        }
      };
    }];

    $httpProvider.interceptors.push(interceptor);
  }]);


/**
 * Loading Bar
 *
 * This service handles adding and removing the actual element in the DOM.
 * Generally, best practices for DOM manipulation is to take place in a
 * directive, but because the element itself is injected in the DOM only upon
 * XHR requests, and it's likely needed on every view, the best option is to
 * use a service.
 */
angular.module('cfp.loadingBar', [])
  .provider('cfpLoadingBar', function() {

    this.includeSpinner = true;
    this.includeBar = true;
    this.latencyThreshold = 100;
    this.startSize = 0.02;
    this.parentSelector = 'body';
    this.spinnerTemplate = '<div id="loading-bar-spinner"><div class="spinner-icon"></div></div>';
    this.loadingBarTemplate = '<div id="loading-bar"><div class="bar"><div class="peg"></div></div></div>';

    this.$get = ['$injector', '$document', '$timeout', '$rootScope', function ($injector, $document, $timeout, $rootScope) {
      var $animate;
      var $parentSelector = this.parentSelector,
        loadingBarContainer = angular.element(this.loadingBarTemplate),
        loadingBar = loadingBarContainer.find('div').eq(0),
        spinner = angular.element(this.spinnerTemplate);

      var incTimeout,
        completeTimeout,
        started = false,
        status = 0;

      var includeSpinner = this.includeSpinner;
      var includeBar = this.includeBar;
      var startSize = this.startSize;

      /**
       * Inserts the loading bar element into the dom, and sets it to 2%
       */
      function _start() {
        if (!$animate) {
          $animate = $injector.get('$animate');
        }

        var $parent = $document.find($parentSelector).eq(0);
        $timeout.cancel(completeTimeout);

        // do not continually broadcast the started event:
        if (started) {
          return;
        }

        $rootScope.$broadcast('cfpLoadingBar:started');
        started = true;

        if (includeBar) {
          $animate.enter(loadingBarContainer, $parent, angular.element($parent[0].lastChild));
        }

        if (includeSpinner) {
          $animate.enter(spinner, $parent, angular.element($parent[0].lastChild));
        }

        _set(startSize);
      }

      /**
       * Set the loading bar's width to a certain percent.
       *
       * @param n any value between 0 and 1
       */
      function _set(n) {
        if (!started) {
          return;
        }
        var pct = (n * 100) + '%';
        loadingBar.css('width', pct);
        status = n;

        // increment loadingbar to give the illusion that there is always
        // progress but make sure to cancel the previous timeouts so we don't
        // have multiple incs running at the same time.
        $timeout.cancel(incTimeout);
        incTimeout = $timeout(function() {
          _inc();
        }, 250);
      }

      /**
       * Increments the loading bar by a random amount
       * but slows down as it progresses
       */
      function _inc() {
        if (_status() >= 1) {
          return;
        }

        var rnd = 0;

        // TODO: do this mathmatically instead of through conditions

        var stat = _status();
        if (stat >= 0 && stat < 0.25) {
          // Start out between 3 - 6% increments
          rnd = (Math.random() * (5 - 3 + 1) + 3) / 100;
        } else if (stat >= 0.25 && stat < 0.65) {
          // increment between 0 - 3%
          rnd = (Math.random() * 3) / 100;
        } else if (stat >= 0.65 && stat < 0.9) {
          // increment between 0 - 2%
          rnd = (Math.random() * 2) / 100;
        } else if (stat >= 0.9 && stat < 0.99) {
          // finally, increment it .5 %
          rnd = 0.005;
        } else {
          // after 99%, don't increment:
          rnd = 0;
        }

        var pct = _status() + rnd;
        _set(pct);
      }

      function _status() {
        return status;
      }

      function _completeAnimation() {
        status = 0;
        started = false;
      }

      function _complete() {
        if (!$animate) {
          $animate = $injector.get('$animate');
        }

        $rootScope.$broadcast('cfpLoadingBar:completed');
        _set(1);

        $timeout.cancel(completeTimeout);

        // Attempt to aggregate any start/complete calls within 500ms:
        completeTimeout = $timeout(function() {
          var promise = $animate.leave(loadingBarContainer, _completeAnimation);
          if (promise && promise.then) {
            promise.then(_completeAnimation);
          }
          $animate.leave(spinner);
        }, 500);
      }

      return {
        start            : _start,
        set              : _set,
        status           : _status,
        inc              : _inc,
        complete         : _complete,
        includeSpinner   : this.includeSpinner,
        latencyThreshold : this.latencyThreshold,
        parentSelector   : this.parentSelector,
        startSize        : this.startSize
      };


    }];     //
  });       // wtf javascript. srsly
})();       //

/*
 * angular-mm-foundation
 * http://pineconellc.github.io/angular-foundation/
 * Version: 0.5.1 - 2014-11-29
 * License: MIT
 * (c) Pinecone, LLC
 */
angular.module("mm.foundation",["mm.foundation.tpls","mm.foundation.accordion","mm.foundation.alert","mm.foundation.bindHtml","mm.foundation.buttons","mm.foundation.position","mm.foundation.mediaQueries","mm.foundation.dropdownToggle","mm.foundation.interchange","mm.foundation.transition","mm.foundation.modal","mm.foundation.offcanvas","mm.foundation.pagination","mm.foundation.tooltip","mm.foundation.popover","mm.foundation.progressbar","mm.foundation.rating","mm.foundation.tabs","mm.foundation.topbar","mm.foundation.tour","mm.foundation.typeahead"]),angular.module("mm.foundation.tpls",["template/accordion/accordion-group.html","template/accordion/accordion.html","template/alert/alert.html","template/modal/backdrop.html","template/modal/window.html","template/pagination/pager.html","template/pagination/pagination.html","template/tooltip/tooltip-html-unsafe-popup.html","template/tooltip/tooltip-popup.html","template/popover/popover.html","template/progressbar/bar.html","template/progressbar/progress.html","template/progressbar/progressbar.html","template/rating/rating.html","template/tabs/tab.html","template/tabs/tabset.html","template/topbar/has-dropdown.html","template/topbar/toggle-top-bar.html","template/topbar/top-bar-dropdown.html","template/topbar/top-bar-section.html","template/topbar/top-bar.html","template/tour/tour.html","template/typeahead/typeahead-match.html","template/typeahead/typeahead-popup.html"]),angular.module("mm.foundation.accordion",[]).constant("accordionConfig",{closeOthers:!0}).controller("AccordionController",["$scope","$attrs","accordionConfig",function(e,t,n){this.groups=[],this.closeOthers=function(a){var o=angular.isDefined(t.closeOthers)?e.$eval(t.closeOthers):n.closeOthers;o&&angular.forEach(this.groups,function(e){e!==a&&(e.isOpen=!1)})},this.addGroup=function(e){var t=this;this.groups.push(e),e.$on("$destroy",function(){t.removeGroup(e)})},this.removeGroup=function(e){var t=this.groups.indexOf(e);-1!==t&&this.groups.splice(this.groups.indexOf(e),1)}}]).directive("accordion",function(){return{restrict:"EA",controller:"AccordionController",transclude:!0,replace:!1,templateUrl:"template/accordion/accordion.html"}}).directive("accordionGroup",["$parse",function(e){return{require:"^accordion",restrict:"EA",transclude:!0,replace:!0,templateUrl:"template/accordion/accordion-group.html",scope:{heading:"@"},controller:function(){this.setHeading=function(e){this.heading=e}},link:function(t,n,a,o){var i,r;o.addGroup(t),t.isOpen=!1,a.isOpen&&(i=e(a.isOpen),r=i.assign,t.$parent.$watch(i,function(e){t.isOpen=!!e})),t.$watch("isOpen",function(e){e&&o.closeOthers(t),r&&r(t.$parent,e)})}}}]).directive("accordionHeading",function(){return{restrict:"EA",transclude:!0,template:"",replace:!0,require:"^accordionGroup",compile:function(e,t,n){return function(e,t,a,o){o.setHeading(n(e,function(){}))}}}}).directive("accordionTransclude",function(){return{require:"^accordionGroup",link:function(e,t,n,a){e.$watch(function(){return a[n.accordionTransclude]},function(e){e&&(t.html(""),t.append(e))})}}}),angular.module("mm.foundation.alert",[]).controller("AlertController",["$scope","$attrs",function(e,t){e.closeable="close"in t}]).directive("alert",function(){return{restrict:"EA",controller:"AlertController",templateUrl:"template/alert/alert.html",transclude:!0,replace:!0,scope:{type:"=",close:"&"}}}),angular.module("mm.foundation.bindHtml",[]).directive("bindHtmlUnsafe",function(){return function(e,t,n){t.addClass("ng-binding").data("$binding",n.bindHtmlUnsafe),e.$watch(n.bindHtmlUnsafe,function(e){t.html(e||"")})}}),angular.module("mm.foundation.buttons",[]).constant("buttonConfig",{activeClass:"active",toggleEvent:"click"}).controller("ButtonsController",["buttonConfig",function(e){this.activeClass=e.activeClass,this.toggleEvent=e.toggleEvent}]).directive("btnRadio",function(){return{require:["btnRadio","ngModel"],controller:"ButtonsController",link:function(e,t,n,a){var o=a[0],i=a[1];i.$render=function(){t.toggleClass(o.activeClass,angular.equals(i.$modelValue,e.$eval(n.btnRadio)))},t.bind(o.toggleEvent,function(){t.hasClass(o.activeClass)||e.$apply(function(){i.$setViewValue(e.$eval(n.btnRadio)),i.$render()})})}}}).directive("btnCheckbox",function(){return{require:["btnCheckbox","ngModel"],controller:"ButtonsController",link:function(e,t,n,a){function o(){return r(n.btnCheckboxTrue,!0)}function i(){return r(n.btnCheckboxFalse,!1)}function r(t,n){var a=e.$eval(t);return angular.isDefined(a)?a:n}var l=a[0],s=a[1];s.$render=function(){t.toggleClass(l.activeClass,angular.equals(s.$modelValue,o()))},t.bind(l.toggleEvent,function(){e.$apply(function(){s.$setViewValue(t.hasClass(l.activeClass)?i():o()),s.$render()})})}}}),angular.module("mm.foundation.position",[]).factory("$position",["$document","$window",function(e,t){function n(e,n){return e.currentStyle?e.currentStyle[n]:t.getComputedStyle?t.getComputedStyle(e)[n]:e.style[n]}function a(e){return"static"===(n(e,"position")||"static")}var o=function(t){for(var n=e[0],o=t.offsetParent||n;o&&o!==n&&a(o);)o=o.offsetParent;return o||n};return{position:function(t){var n=this.offset(t),a={top:0,left:0},i=o(t[0]);i!=e[0]&&(a=this.offset(angular.element(i)),a.top+=i.clientTop-i.scrollTop,a.left+=i.clientLeft-i.scrollLeft);var r=t[0].getBoundingClientRect();return{width:r.width||t.prop("offsetWidth"),height:r.height||t.prop("offsetHeight"),top:n.top-a.top,left:n.left-a.left}},offset:function(n){var a=n[0].getBoundingClientRect();return{width:a.width||n.prop("offsetWidth"),height:a.height||n.prop("offsetHeight"),top:a.top+(t.pageYOffset||e[0].body.scrollTop||e[0].documentElement.scrollTop),left:a.left+(t.pageXOffset||e[0].body.scrollLeft||e[0].documentElement.scrollLeft)}}}}]),angular.module("mm.foundation.mediaQueries",[]).factory("matchMedia",["$document","$window",function(e,t){return t.matchMedia||function(e){var t,n=e.documentElement,a=n.firstElementChild||n.firstChild,o=e.createElement("body"),i=e.createElement("div");return i.id="mq-test-1",i.style.cssText="position:absolute;top:-100em",o.style.background="none",o.appendChild(i),function(e){return i.innerHTML='&shy;<style media="'+e+'"> #mq-test-1 { width: 42px; }</style>',n.insertBefore(o,a),t=42===i.offsetWidth,n.removeChild(o),{matches:t,media:e}}}(e[0])}]).factory("mediaQueries",["$document","matchMedia",function(e,t){var n=angular.element(e[0].querySelector("head"));n.append('<meta class="foundation-mq-topbar" />'),n.append('<meta class="foundation-mq-small" />'),n.append('<meta class="foundation-mq-medium" />'),n.append('<meta class="foundation-mq-large" />');var a=/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,o={topbar:getComputedStyle(n[0].querySelector("meta.foundation-mq-topbar")).fontFamily.replace(a,""),small:getComputedStyle(n[0].querySelector("meta.foundation-mq-small")).fontFamily.replace(a,""),medium:getComputedStyle(n[0].querySelector("meta.foundation-mq-medium")).fontFamily.replace(a,""),large:getComputedStyle(n[0].querySelector("meta.foundation-mq-large")).fontFamily.replace(a,"")};return{topbarBreakpoint:function(){return!t(o.topbar).matches},small:function(){return t(o.small).matches},medium:function(){return t(o.medium).matches},large:function(){return t(o.large).matches}}}]),angular.module("mm.foundation.dropdownToggle",["mm.foundation.position","mm.foundation.mediaQueries"]).controller("DropdownToggleController",["$scope","$attrs","mediaQueries",function(e,t,n){this.small=function(){return n.small()&&!n.medium()}}]).directive("dropdownToggle",["$document","$window","$location","$position",function(e,t,n,a){var o=null,i=angular.noop;return{restrict:"CA",scope:{dropdownToggle:"@"},controller:"DropdownToggleController",link:function(n,r,l,s){var c=r.parent(),u=angular.element(e[0].querySelector(n.dropdownToggle)),p=function(){return c.hasClass("has-dropdown")},d=function(l){u=angular.element(e[0].querySelector(n.dropdownToggle));var d=r===o;if(l.preventDefault(),l.stopPropagation(),o&&i(),!d&&!r.hasClass("disabled")&&!r.prop("disabled")){u.css("display","block");var m=a.offset(r),f=a.offset(angular.element(u[0].offsetParent)),g=u.prop("offsetWidth"),h={top:m.top-f.top+m.height+"px"};if(s.small())h.left=Math.max((f.width-g)/2,8)+"px",h.position="absolute",h.width="95%",h["max-width"]="none";else{var v=Math.round(m.left-f.left),b=t.innerWidth-g-8;v>b&&(v=b,u.removeClass("left").addClass("right")),h.left=v+"px",h.position=null,h["max-width"]=null}u.css(h),p()&&c.addClass("hover"),o=r,i=function(){e.off("click",i),u.css("display","none"),i=angular.noop,o=null,c.hasClass("hover")&&c.removeClass("hover")},e.on("click",i)}};u&&u.css("display","none"),n.$watch("$location.path",function(){i()}),r.on("click",d),r.on("$destroy",function(){r.off("click",d)})}}}]),angular.module("mm.foundation.interchange",["mm.foundation.mediaQueries"]).factory("interchangeQueries",["$document",function(e){for(var t,n,a={"default":"only screen",landscape:"only screen and (orientation: landscape)",portrait:"only screen and (orientation: portrait)",retina:"only screen and (-webkit-min-device-pixel-ratio: 2),only screen and (min--moz-device-pixel-ratio: 2),only screen and (-o-min-device-pixel-ratio: 2/1),only screen and (min-device-pixel-ratio: 2),only screen and (min-resolution: 192dpi),only screen and (min-resolution: 2dppx)"},o="foundation-mq-",i=["small","medium","large","xlarge","xxlarge"],r=angular.element(e[0].querySelector("head")),l=0;l<i.length;l++)r.append('<meta class="'+o+i[l]+'" />'),t=getComputedStyle(r[0].querySelector("meta."+o+i[l])),n=t.fontFamily.replace(/^[\/\\'"]+|(;\s?})+|[\/\\'"]+$/g,""),a[i[l]]=n;return a}]).factory("interchangeQueriesManager",["interchangeQueries",function(e){return{add:function(t,n){return t&&n&&angular.isString(t)&&angular.isString(n)&&!e[t]?(e[t]=n,!0):!1}}}]).factory("interchangeTools",["$window","matchMedia","interchangeQueries",function(e,t,n){var a=function(e){for(var t,n=e.split(/\[(.*?)\]/),a=n.length,o=/^(.+)\,\ \((.+)\)$/,i={};a--;)n[a].replace(/[\W\d]+/,"").length>4&&(t=o.exec(n[a]),t&&3===t.length&&(i[t[2]]=t[1]));return i},o=function(e){var a,o,i;for(a in e)if(o=n[a]||a,i=t(o),i.matches)return e[a]};return{parseAttribute:a,findCurrentMediaFile:o}}]).directive("interchange",["$window","$rootScope","interchangeTools",function(e,t,n){var a=/[A-Za-z0-9-_]+\.(jpg|jpeg|png|gif|bmp|tiff)\ *,/i;return{restrict:"A",scope:!0,priority:450,compile:function(o,i){return"DIV"!==o[0].nodeName||a.test(i.interchange)||o.html('<ng-include src="currentFile"></ng-include>'),{pre:function(){},post:function(a,o,i){var r,l;switch(l=o&&o[0]&&o[0].nodeName,a.fileMap=n.parseAttribute(i.interchange),l){case"DIV":r=n.findCurrentMediaFile(a.fileMap),a.type=/[A-Za-z0-9-_]+\.(jpg|jpeg|png|gif|bmp|tiff)$/i.test(r)?"background":"include";break;case"IMG":a.type="image";break;default:return}var s=function(e){var i=n.findCurrentMediaFile(a.fileMap);if(!a.currentFile||a.currentFile!==i){switch(a.currentFile=i,a.type){case"image":o.attr("src",a.currentFile);break;case"background":o.css("background-image","url("+a.currentFile+")")}t.$emit("replace",o,a),e&&a.$apply()}};s(),e.addEventListener("resize",s),a.$on("$destroy",function(){e.removeEventListener("resize",s)})}}}}}]),angular.module("mm.foundation.transition",[]).factory("$transition",["$q","$timeout","$rootScope",function(e,t,n){function a(e){for(var t in e)if(void 0!==i.style[t])return e[t]}var o=function(a,i,r){r=r||{};var l=e.defer(),s=o[r.animation?"animationEndEventName":"transitionEndEventName"],c=function(){n.$apply(function(){a.unbind(s,c),l.resolve(a)})};return s&&a.bind(s,c),t(function(){angular.isString(i)?a.addClass(i):angular.isFunction(i)?i(a):angular.isObject(i)&&a.css(i),s||l.resolve(a)}),l.promise.cancel=function(){s&&a.unbind(s,c),l.reject("Transition cancelled")},l.promise},i=document.createElement("trans"),r={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",transition:"transitionend"},l={WebkitTransition:"webkitAnimationEnd",MozTransition:"animationend",OTransition:"oAnimationEnd",transition:"animationend"};return o.transitionEndEventName=a(r),o.animationEndEventName=a(l),o}]),angular.module("mm.foundation.modal",["mm.foundation.transition"]).factory("$$stackedMap",function(){return{createNew:function(){var e=[];return{add:function(t,n){e.push({key:t,value:n})},get:function(t){for(var n=0;n<e.length;n++)if(t==e[n].key)return e[n]},keys:function(){for(var t=[],n=0;n<e.length;n++)t.push(e[n].key);return t},top:function(){return e[e.length-1]},remove:function(t){for(var n=-1,a=0;a<e.length;a++)if(t==e[a].key){n=a;break}return e.splice(n,1)[0]},removeTop:function(){return e.splice(e.length-1,1)[0]},length:function(){return e.length}}}}}).directive("modalBackdrop",["$modalStack","$timeout",function(e,t){return{restrict:"EA",replace:!0,templateUrl:"template/modal/backdrop.html",link:function(n){n.animate=!1,t(function(){n.animate=!0}),n.close=function(t){var n=e.getTop();n&&n.value.backdrop&&"static"!=n.value.backdrop&&t.target===t.currentTarget&&(t.preventDefault(),t.stopPropagation(),e.dismiss(n.key,"backdrop click"))}}}}]).directive("modalWindow",["$modalStack","$timeout",function(e,t){return{restrict:"EA",scope:{index:"@",animate:"="},replace:!0,transclude:!0,templateUrl:"template/modal/window.html",link:function(e,n,a){e.windowClass=a.windowClass||"",t(function(){e.animate=!0,n[0].querySelectorAll("[autofocus]").length>0?n[0].querySelectorAll("[autofocus]")[0].focus():n[0].querySelector("div").focus()})}}}]).factory("$modalStack",["$window","$transition","$timeout","$document","$compile","$rootScope","$$stackedMap",function(e,t,n,a,o,i,r){function l(){for(var e=-1,t=f.keys(),n=0;n<t.length;n++)f.get(t[n]).value.backdrop&&(e=n);return e}function s(e){var t=a.find("body").eq(0),n=f.get(e).value;f.remove(e),u(n.modalDomEl,n.modalScope,300,function(){n.modalScope.$destroy(),t.toggleClass(m,f.length()>0),c()})}function c(){if(p&&-1==l()){var e=d;u(p,d,150,function(){e.$destroy(),e=null}),p=void 0,d=void 0}}function u(e,a,o,i){function r(){r.done||(r.done=!0,e.remove(),i&&i())}a.animate=!1;var l=t.transitionEndEventName;if(l){var s=n(r,o);e.bind(l,function(){n.cancel(s),r(),a.$apply()})}else n(r,0)}var p,d,m="modal-open",f=r.createNew(),g={};return i.$watch(l,function(e){d&&(d.index=e)}),a.bind("keydown",function(e){var t;27===e.which&&(t=f.top(),t&&t.value.keyboard&&i.$apply(function(){g.dismiss(t.key)}))}),g.open=function(t,n){f.add(t,{deferred:n.deferred,modalScope:n.scope,backdrop:n.backdrop,keyboard:n.keyboard});var r=a.find("body").eq(0),s=l();s>=0&&!p&&(d=i.$new(!0),d.index=s,p=o("<div modal-backdrop></div>")(d),r.append(p));var c=angular.element('<div class="reveal-modal" style="z-index:-1""></div>');r.append(c[0]);var u=parseInt(getComputedStyle(c[0]).top)||0;c.remove();var g=e.pageYOffset||0,h=g+u,v=angular.element('<div modal-window style="visibility: visible; top:'+h+'px;"></div>');v.attr("window-class",n.windowClass),v.attr("index",f.length()-1),v.attr("animate","animate"),v.html(n.content);var b=o(v)(n.scope);f.top().value.modalDomEl=b,r.append(b),r.addClass(m)},g.close=function(e,t){var n=f.get(e).value;n&&(n.deferred.resolve(t),s(e))},g.dismiss=function(e,t){var n=f.get(e).value;n&&(n.deferred.reject(t),s(e))},g.dismissAll=function(e){for(var t=this.getTop();t;)this.dismiss(t.key,e),t=this.getTop()},g.getTop=function(){return f.top()},g}]).provider("$modal",function(){var e={options:{backdrop:!0,keyboard:!0},$get:["$injector","$rootScope","$q","$http","$templateCache","$controller","$modalStack",function(t,n,a,o,i,r,l){function s(e){return e.template?a.when(e.template):o.get(e.templateUrl,{cache:i}).then(function(e){return e.data})}function c(e){var n=[];return angular.forEach(e,function(e){(angular.isFunction(e)||angular.isArray(e))&&n.push(a.when(t.invoke(e)))}),n}var u={};return u.open=function(t){var o=a.defer(),i=a.defer(),u={result:o.promise,opened:i.promise,close:function(e){l.close(u,e)},dismiss:function(e){l.dismiss(u,e)}};if(t=angular.extend({},e.options,t),t.resolve=t.resolve||{},!t.template&&!t.templateUrl)throw new Error("One of template or templateUrl options is required.");var p=a.all([s(t)].concat(c(t.resolve)));return p.then(function(e){var a=(t.scope||n).$new();a.$close=u.close,a.$dismiss=u.dismiss;var i,s={},c=1;t.controller&&(s.$scope=a,s.$modalInstance=u,angular.forEach(t.resolve,function(t,n){s[n]=e[c++]}),i=r(t.controller,s)),l.open(u,{scope:a,deferred:o,content:e[0],backdrop:t.backdrop,keyboard:t.keyboard,windowClass:t.windowClass})},function(e){o.reject(e)}),p.then(function(){i.resolve(!0)},function(){i.reject(!1)}),u},u}]};return e}),angular.module("mm.foundation.offcanvas",[]).directive("offCanvasWrap",["$window",function(e){return{scope:{},restrict:"C",link:function(t,n){var a=angular.element(e),o=t.sidebar=n;t.hide=function(){o.removeClass("move-left"),o.removeClass("move-right")},a.bind("resize.body",t.hide),t.$on("$destroy",function(){a.unbind("resize.body",t.hide)})},controller:["$scope",function(e){this.leftToggle=function(){e.sidebar.toggleClass("move-right")},this.rightToggle=function(){e.sidebar.toggleClass("move-left")},this.hide=function(){e.hide()}}]}}]).directive("leftOffCanvasToggle",[function(){return{require:"^offCanvasWrap",restrict:"C",link:function(e,t,n,a){t.on("click",function(){a.leftToggle()})}}}]).directive("rightOffCanvasToggle",[function(){return{require:"^offCanvasWrap",restrict:"C",link:function(e,t,n,a){t.on("click",function(){a.rightToggle()})}}}]).directive("exitOffCanvas",[function(){return{require:"^offCanvasWrap",restrict:"C",link:function(e,t,n,a){t.on("click",function(){a.hide()})}}}]).directive("offCanvasList",[function(){return{require:"^offCanvasWrap",restrict:"C",link:function(e,t,n,a){t.on("click",function(){a.hide()})}}}]),angular.module("mm.foundation.pagination",[]).controller("PaginationController",["$scope","$attrs","$parse","$interpolate",function(e,t,n,a){var o=this,i=t.numPages?n(t.numPages).assign:angular.noop;this.init=function(a){t.itemsPerPage?e.$parent.$watch(n(t.itemsPerPage),function(t){o.itemsPerPage=parseInt(t,10),e.totalPages=o.calculateTotalPages()}):this.itemsPerPage=a},this.noPrevious=function(){return 1===this.page},this.noNext=function(){return this.page===e.totalPages},this.isActive=function(e){return this.page===e},this.calculateTotalPages=function(){var t=this.itemsPerPage<1?1:Math.ceil(e.totalItems/this.itemsPerPage);return Math.max(t||0,1)},this.getAttributeValue=function(t,n,o){return angular.isDefined(t)?o?a(t)(e.$parent):e.$parent.$eval(t):n},this.render=function(){this.page=parseInt(e.page,10)||1,this.page>0&&this.page<=e.totalPages&&(e.pages=this.getPages(this.page,e.totalPages))},e.selectPage=function(t){!o.isActive(t)&&t>0&&t<=e.totalPages&&(e.page=t,e.onSelectPage({page:t}))},e.$watch("page",function(){o.render()}),e.$watch("totalItems",function(){e.totalPages=o.calculateTotalPages()}),e.$watch("totalPages",function(t){i(e.$parent,t),o.page>t?e.selectPage(t):o.render()})}]).constant("paginationConfig",{itemsPerPage:10,boundaryLinks:!1,directionLinks:!0,firstText:"First",previousText:"Previous",nextText:"Next",lastText:"Last",rotate:!0}).directive("pagination",["$parse","paginationConfig",function(e,t){return{restrict:"EA",scope:{page:"=",totalItems:"=",onSelectPage:" &"},controller:"PaginationController",templateUrl:"template/pagination/pagination.html",replace:!0,link:function(n,a,o,i){function r(e,t,n,a){return{number:e,text:t,active:n,disabled:a}}var l,s=i.getAttributeValue(o.boundaryLinks,t.boundaryLinks),c=i.getAttributeValue(o.directionLinks,t.directionLinks),u=i.getAttributeValue(o.firstText,t.firstText,!0),p=i.getAttributeValue(o.previousText,t.previousText,!0),d=i.getAttributeValue(o.nextText,t.nextText,!0),m=i.getAttributeValue(o.lastText,t.lastText,!0),f=i.getAttributeValue(o.rotate,t.rotate);i.init(t.itemsPerPage),o.maxSize&&n.$parent.$watch(e(o.maxSize),function(e){l=parseInt(e,10),i.render()}),i.getPages=function(e,t){var n=[],a=1,o=t,g=angular.isDefined(l)&&t>l;g&&(f?(a=Math.max(e-Math.floor(l/2),1),o=a+l-1,o>t&&(o=t,a=o-l+1)):(a=(Math.ceil(e/l)-1)*l+1,o=Math.min(a+l-1,t)));for(var h=a;o>=h;h++){var v=r(h,h,i.isActive(h),!1);n.push(v)}if(g&&!f){if(a>1){var b=r(a-1,"...",!1,!1);n.unshift(b)}if(t>o){var $=r(o+1,"...",!1,!1);n.push($)}}if(c){var y=r(e-1,p,!1,i.noPrevious());n.unshift(y);var k=r(e+1,d,!1,i.noNext());n.push(k)}if(s){var x=r(1,u,!1,i.noPrevious());n.unshift(x);var w=r(t,m,!1,i.noNext());n.push(w)}return n}}}}]).constant("pagerConfig",{itemsPerPage:10,previousText:"« Previous",nextText:"Next »",align:!0}).directive("pager",["pagerConfig",function(e){return{restrict:"EA",scope:{page:"=",totalItems:"=",onSelectPage:" &"},controller:"PaginationController",templateUrl:"template/pagination/pager.html",replace:!0,link:function(t,n,a,o){function i(e,t,n,a,o){return{number:e,text:t,disabled:n,previous:s&&a,next:s&&o}}var r=o.getAttributeValue(a.previousText,e.previousText,!0),l=o.getAttributeValue(a.nextText,e.nextText,!0),s=o.getAttributeValue(a.align,e.align);o.init(e.itemsPerPage),o.getPages=function(e){return[i(e-1,r,o.noPrevious(),!0,!1),i(e+1,l,o.noNext(),!1,!0)]}}}}]),angular.module("mm.foundation.tooltip",["mm.foundation.position","mm.foundation.bindHtml"]).provider("$tooltip",function(){function e(e){var t=/[A-Z]/g,n="-";return e.replace(t,function(e,t){return(t?n:"")+e.toLowerCase()})}var t={placement:"top",animation:!0,popupDelay:0},n={mouseenter:"mouseleave",click:"click",focus:"blur"},a={};this.options=function(e){angular.extend(a,e)},this.setTriggers=function(e){angular.extend(n,e)},this.$get=["$window","$compile","$timeout","$parse","$document","$position","$interpolate",function(o,i,r,l,s,c,u){return function(o,p,d){function m(e){var t=e||f.trigger||d,a=n[t]||t;return{show:t,hide:a}}var f=angular.extend({},t,a),g=e(o),h=u.startSymbol(),v=u.endSymbol(),b="<div "+g+'-popup title="'+h+"tt_title"+v+'" content="'+h+"tt_content"+v+'" placement="'+h+"tt_placement"+v+'" animation="tt_animation" is-open="tt_isOpen"></div>';return{restrict:"EA",scope:!0,compile:function(){var e=i(b);return function(t,n,a){function i(){t.tt_isOpen?d():u()}function u(){(!T||t.$eval(a[p+"Enable"]))&&(t.tt_popupDelay?(k=r(g,t.tt_popupDelay,!1),k.then(function(e){e()})):g()())}function d(){t.$apply(function(){h()})}function g(){return t.tt_content?(v(),y&&r.cancel(y),$.css({top:0,left:0,display:"block"}),x?s.find("body").append($):n.after($),P(),t.tt_isOpen=!0,t.$digest(),P):angular.noop}function h(){t.tt_isOpen=!1,r.cancel(k),t.tt_animation?y=r(b,500):b()}function v(){$&&b(),$=e(t,function(){}),t.$digest()}function b(){$&&($.remove(),$=null)}var $,y,k,x=angular.isDefined(f.appendToBody)?f.appendToBody:!1,w=m(void 0),C=!1,T=angular.isDefined(a[p+"Enable"]),P=function(){var e,a,o,i;switch(e=x?c.offset(n):c.position(n),a=$.prop("offsetWidth"),o=$.prop("offsetHeight"),t.tt_placement){case"right":i={top:e.top+e.height/2-o/2,left:e.left+e.width+10};break;case"bottom":i={top:e.top+e.height+10,left:e.left};break;case"left":i={top:e.top+e.height/2-o/2,left:e.left-a-10};break;default:i={top:e.top-o-10,left:e.left}}i.top+="px",i.left+="px",$.css(i)};t.tt_isOpen=!1,a.$observe(o,function(e){t.tt_content=e,!e&&t.tt_isOpen&&h()}),a.$observe(p+"Title",function(e){t.tt_title=e}),a[p+"Placement"]=a[p+"Placement"]||null,a.$observe(p+"Placement",function(e){t.tt_placement=angular.isDefined(e)&&e?e:f.placement}),a[p+"PopupDelay"]=a[p+"PopupDelay"]||null,a.$observe(p+"PopupDelay",function(e){var n=parseInt(e,10);t.tt_popupDelay=isNaN(n)?f.popupDelay:n});var S=function(){C&&(angular.isFunction(w.show)?A():(n.unbind(w.show,u),n.unbind(w.hide,d)))},A=function(){};a[p+"Trigger"]=a[p+"Trigger"]||null,a.$observe(p+"Trigger",function(e){S(),A(),w=m(e),angular.isFunction(w.show)?A=t.$watch(function(){return w.show(t,n,a)},function(e){return r(e?g:h)}):w.show===w.hide?n.bind(w.show,i):(n.bind(w.show,u),n.bind(w.hide,d)),C=!0});var O=t.$eval(a[p+"Animation"]);t.tt_animation=angular.isDefined(O)?!!O:f.animation,a.$observe(p+"AppendToBody",function(e){x=angular.isDefined(e)?l(e)(t):x}),x&&t.$on("$locationChangeSuccess",function(){t.tt_isOpen&&h()}),t.$on("$destroy",function(){r.cancel(y),r.cancel(k),S(),A(),b()})}}}}}]}).directive("tooltipPopup",function(){return{restrict:"EA",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-popup.html"}}).directive("tooltip",["$tooltip",function(e){return e("tooltip","tooltip","mouseenter")}]).directive("tooltipHtmlUnsafePopup",function(){return{restrict:"EA",replace:!0,scope:{content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tooltip/tooltip-html-unsafe-popup.html"}}).directive("tooltipHtmlUnsafe",["$tooltip",function(e){return e("tooltipHtmlUnsafe","tooltip","mouseenter")}]),angular.module("mm.foundation.popover",["mm.foundation.tooltip"]).directive("popoverPopup",function(){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/popover/popover.html"}}).directive("popover",["$tooltip",function(e){return e("popover","popover","click")}]),angular.module("mm.foundation.progressbar",["mm.foundation.transition"]).constant("progressConfig",{animate:!0,max:100}).controller("ProgressController",["$scope","$attrs","progressConfig","$transition",function(e,t,n,a){var o=this,i=[],r=angular.isDefined(t.max)?e.$parent.$eval(t.max):n.max,l=angular.isDefined(t.animate)?e.$parent.$eval(t.animate):n.animate;this.addBar=function(e,t){var n=0,a=e.$parent.$index;angular.isDefined(a)&&i[a]&&(n=i[a].value),i.push(e),this.update(t,e.value,n),e.$watch("value",function(e,n){e!==n&&o.update(t,e,n)}),e.$on("$destroy",function(){o.removeBar(e)})},this.update=function(e,t,n){var o=this.getPercentage(t);l?(e.css("width",this.getPercentage(n)+"%"),a(e,{width:o+"%"})):e.css({transition:"none",width:o+"%"})},this.removeBar=function(e){i.splice(i.indexOf(e),1)},this.getPercentage=function(e){return Math.round(100*e/r)}}]).directive("progress",function(){return{restrict:"EA",replace:!0,transclude:!0,controller:"ProgressController",require:"progress",scope:{},template:'<div class="progress" ng-transclude></div>'}}).directive("bar",function(){return{restrict:"EA",replace:!0,transclude:!0,require:"^progress",scope:{value:"=",type:"@"},templateUrl:"template/progressbar/bar.html",link:function(e,t,n,a){a.addBar(e,t)}}}).directive("progressbar",function(){return{restrict:"EA",replace:!0,transclude:!0,controller:"ProgressController",scope:{value:"=",type:"@"},templateUrl:"template/progressbar/progressbar.html",link:function(e,t,n,a){a.addBar(e,angular.element(t.children()[0]))}}}),angular.module("mm.foundation.rating",[]).constant("ratingConfig",{max:5,stateOn:null,stateOff:null}).controller("RatingController",["$scope","$attrs","$parse","ratingConfig",function(e,t,n,a){this.maxRange=angular.isDefined(t.max)?e.$parent.$eval(t.max):a.max,this.stateOn=angular.isDefined(t.stateOn)?e.$parent.$eval(t.stateOn):a.stateOn,this.stateOff=angular.isDefined(t.stateOff)?e.$parent.$eval(t.stateOff):a.stateOff,this.createRateObjects=function(e){for(var t={stateOn:this.stateOn,stateOff:this.stateOff},n=0,a=e.length;a>n;n++)e[n]=angular.extend({index:n},t,e[n]);return e},e.range=this.createRateObjects(angular.isDefined(t.ratingStates)?angular.copy(e.$parent.$eval(t.ratingStates)):new Array(this.maxRange)),e.rate=function(t){e.value===t||e.readonly||(e.value=t)},e.enter=function(t){e.readonly||(e.val=t),e.onHover({value:t})},e.reset=function(){e.val=angular.copy(e.value),e.onLeave()},e.$watch("value",function(t){e.val=t}),e.readonly=!1,t.readonly&&e.$parent.$watch(n(t.readonly),function(t){e.readonly=!!t})}]).directive("rating",function(){return{restrict:"EA",scope:{value:"=",onHover:"&",onLeave:"&"},controller:"RatingController",templateUrl:"template/rating/rating.html",replace:!0}}),angular.module("mm.foundation.tabs",[]).controller("TabsetController",["$scope",function(e){var t=this,n=t.tabs=e.tabs=[];t.select=function(e){angular.forEach(n,function(e){e.active=!1}),e.active=!0},t.addTab=function(e){n.push(e),(1===n.length||e.active)&&t.select(e)},t.removeTab=function(e){var a=n.indexOf(e);if(e.active&&n.length>1){var o=a==n.length-1?a-1:a+1;t.select(n[o])}n.splice(a,1)}}]).directive("tabset",function(){return{restrict:"EA",transclude:!0,replace:!0,scope:{},controller:"TabsetController",templateUrl:"template/tabs/tabset.html",link:function(e,t,n){e.vertical=angular.isDefined(n.vertical)?e.$parent.$eval(n.vertical):!1,e.justified=angular.isDefined(n.justified)?e.$parent.$eval(n.justified):!1,e.type=angular.isDefined(n.type)?e.$parent.$eval(n.type):"tabs"}}}).directive("tab",["$parse",function(e){return{require:"^tabset",restrict:"EA",replace:!0,templateUrl:"template/tabs/tab.html",transclude:!0,scope:{heading:"@",onSelect:"&select",onDeselect:"&deselect"},controller:function(){},compile:function(t,n,a){return function(t,n,o,i){var r,l;o.active?(r=e(o.active),l=r.assign,t.$parent.$watch(r,function(e,n){e!==n&&(t.active=!!e)}),t.active=r(t.$parent)):l=r=angular.noop,t.$watch("active",function(e){angular.isFunction(l)&&(l(t.$parent,e),e?(i.select(t),t.onSelect()):t.onDeselect())}),t.disabled=!1,o.disabled&&t.$parent.$watch(e(o.disabled),function(e){t.disabled=!!e}),t.select=function(){t.disabled||(t.active=!0)},i.addTab(t),t.$on("$destroy",function(){i.removeTab(t)}),t.$transcludeFn=a}}}}]).directive("tabHeadingTransclude",[function(){return{restrict:"A",require:"^tab",link:function(e,t){e.$watch("headingElement",function(e){e&&(t.html(""),t.append(e))})}}}]).directive("tabContentTransclude",function(){function e(e){return e.tagName&&(e.hasAttribute("tab-heading")||e.hasAttribute("data-tab-heading")||"tab-heading"===e.tagName.toLowerCase()||"data-tab-heading"===e.tagName.toLowerCase())}return{restrict:"A",require:"^tabset",link:function(t,n,a){var o=t.$eval(a.tabContentTransclude);o.$transcludeFn(o.$parent,function(t){angular.forEach(t,function(t){e(t)?o.headingElement=t:n.append(t)})})}}}),angular.module("mm.foundation.topbar",["mm.foundation.mediaQueries"]).factory("closest",[function(){return function(e,t){for(var n=function(e,t){for(var n=(e.parentNode||e.document).querySelectorAll(t),a=-1;n[++a]&&n[a]!=e;);return!!n[a]},a=e[0];a;){if(n(a,t))return angular.element(a);a=a.parentElement}return!1}}]).directive("topBar",["$timeout","$compile","$window","$document","mediaQueries",function(e,t,n,a,o){return{scope:{stickyClass:"@",backText:"@",stickyOn:"=",customBackText:"=",isHover:"=",mobileShowParentLink:"=",scrolltop:"="},restrict:"EA",replace:!0,templateUrl:"template/topbar/top-bar.html",transclude:!0,link:function(e,t){var i=e.topbar=t,r=i.parent(),l=angular.element(a[0].querySelector("body")),s=e.isSticky=function(){var t=r.hasClass(e.settings.stickyClass);return t&&"all"===e.settings.stickyOn?!0:t&&o.small()&&"small"===e.settings.stickyOn?!0:t&&o.medium()&&"medium"===e.settings.stickyOn?!0:t&&o.large()&&"large"===e.settings.stickyOn?!0:!1},c=function(){if(e.stickyTopbar&&e.isSticky()){var t=angular.element(a[0].querySelector("."+e.settings.stickyClass)),o=u;n.pageYOffset>o&&!t.hasClass("fixed")?(t.addClass("fixed"),l.css("padding-top",e.originalHeight+"px")):n.pageYOffset<=o&&t.hasClass("fixed")&&(t.removeClass("fixed"),l.css("padding-top",""))}};if(e.toggle=function(t){if(!o.topbarBreakpoint())return!1;var a=void 0===t?!i.hasClass("expanded"):t;a?i.addClass("expanded"):i.removeClass("expanded"),e.settings.scrolltop?!a&&i.hasClass("fixed")?(i.parent().addClass("fixed"),i.removeClass("fixed"),l.css("padding-top",e.originalHeight+"px")):a&&i.parent().hasClass("fixed")&&(i.parent().removeClass("fixed"),i.addClass("fixed"),l.css("padding-top",""),n.scrollTo(0,0)):(s()&&i.parent().addClass("fixed"),i.parent().hasClass("fixed")&&(a?(i.addClass("fixed"),i.parent().addClass("expanded"),l.css("padding-top",e.originalHeight+"px")):(i.removeClass("fixed"),i.parent().removeClass("expanded"),c())))},r.hasClass("fixed")||s()){e.stickyTopbar=!0,e.height=r[0].offsetHeight;
var u=r[0].getBoundingClientRect().top}else e.height=i[0].offsetHeight;e.originalHeight=e.height,e.$watch("height",function(e){e?i.css("height",e+"px"):i.css("height","")});var p=o.topbarBreakpoint();angular.element(n).bind("resize",function(){var t=o.topbarBreakpoint();if(p!==t){p=o.topbarBreakpoint(),i.removeClass("expanded"),i.parent().removeClass("expanded"),e.height="";var n=angular.element(i[0].querySelectorAll("section"));angular.forEach(n,function(e){angular.element(e.querySelectorAll("li.moved")).removeClass("moved")}),e.$apply()}}),angular.element(n).bind("scroll",function(){c(),e.$apply()}),e.$on("$destroy",function(){angular.element(n).unbind("scroll"),angular.element(n).unbind("resize")}),r.hasClass("fixed")&&l.css("padding-top",e.originalHeight+"px")},controller:["$window","$scope","closest",function(t,n,i){n.settings={},n.settings.stickyClass=n.stickyClass||"sticky",n.settings.backText=n.backText||"Back",n.settings.stickyOn=n.stickyOn||"all",n.settings.customBackText=void 0===n.customBackText?!0:n.customBackText,n.settings.isHover=void 0===n.isHover?!0:n.isHover,n.settings.mobileShowParentLink=void 0===n.mobileShowParentLink?!0:n.mobileShowParentLink,n.settings.scrolltop=void 0===n.scrolltop?!0:n.scrolltop,this.settings=n.settings,n.index=0;var r=function(e){var t=e.offsetHeight,n=e.currentStyle||getComputedStyle(e);return t+=parseInt(n.marginTop,10)+parseInt(n.marginBottom,10)},l=[];this.addSection=function(e){l.push(e)},this.removeSection=function(e){var t=l.indexOf(e);t>-1&&l.splice(t,1)};var s=/rtl/i.test(a.find("html").attr("dir"))?"right":"left";n.$watch("index",function(e){for(var t=0;t<l.length;t++)l[t].move(s,e)}),this.toggle=function(e){n.toggle(e);for(var t=0;t<l.length;t++)l[t].reset();n.index=0,n.height="",n.$apply()},this.back=function(t){if(!(n.index<1)&&o.topbarBreakpoint()){var a=angular.element(t.currentTarget),l=i(a,"li.moved"),s=l.parent();n.index=n.index-1,n.height=0===n.index?"":n.originalHeight+r(s[0]),e(function(){l.removeClass("moved")},300)}},this.forward=function(e){if(!o.topbarBreakpoint())return!1;var t=angular.element(e.currentTarget),a=i(t,"li");a.addClass("moved"),n.height=n.originalHeight+r(t.parent()[0].querySelector("ul")),n.index=n.index+1,n.$apply()}}]}}]).directive("toggleTopBar",["closest",function(e){return{scope:{},require:"^topBar",restrict:"A",replace:!0,templateUrl:"template/topbar/toggle-top-bar.html",transclude:!0,link:function(t,n,a,o){n.bind("click",function(t){var n=e(angular.element(t.currentTarget),"li");n.hasClass("back")||n.hasClass("has-dropdown")||o.toggle()}),t.$on("$destroy",function(){n.unbind("click")})}}}]).directive("topBarSection",["$compile","closest",function(e,t){return{scope:{},require:"^topBar",restrict:"EA",replace:!0,templateUrl:"template/topbar/top-bar-section.html",transclude:!0,link:function(e,n,a,o){var i=n;e.reset=function(){angular.element(i[0].querySelectorAll("li.moved")).removeClass("moved")},e.move=function(e,t){i.css("left"===e?{left:-100*t+"%"}:{right:-100*t+"%"})},o.addSection(e),e.$on("$destroy",function(){o.removeSection(e)});var r=i[0].querySelectorAll("li>a");angular.forEach(r,function(n){var a=angular.element(n),i=t(a,"li");i.hasClass("has-dropdown")||i.hasClass("back")||i.hasClass("title")||(a.bind("click",function(){o.toggle(!1)}),e.$on("$destroy",function(){a.bind("click")}))})}}}]).directive("hasDropdown",["mediaQueries",function(e){return{scope:{},require:"^topBar",restrict:"A",templateUrl:"template/topbar/has-dropdown.html",replace:!0,transclude:!0,link:function(t,n,a,o){t.triggerLink=n.children("a")[0];var i=angular.element(t.triggerLink);i.bind("click",function(e){o.forward(e)}),t.$on("$destroy",function(){i.unbind("click")}),n.bind("mouseenter",function(){o.settings.isHover&&!e.topbarBreakpoint()&&n.addClass("not-click")}),n.bind("click",function(){o.settings.isHover||e.topbarBreakpoint()||n.toggleClass("not-click")}),n.bind("mouseleave",function(){n.removeClass("not-click")}),t.$on("$destroy",function(){n.unbind("click"),n.unbind("mouseenter"),n.unbind("mouseleave")})},controller:["$window","$scope",function(e,t){this.triggerLink=t.triggerLink}]}}]).directive("topBarDropdown",["$compile",function(e){return{scope:{},require:["^topBar","^hasDropdown"],restrict:"A",replace:!0,templateUrl:"template/topbar/top-bar-dropdown.html",transclude:!0,link:function(t,n,a,o){var i=o[0],r=o[1],l=angular.element(r.triggerLink),s=l.attr("href");t.linkText=l.text(),t.back=function(e){i.back(e)},t.backText=i.settings.customBackText?i.settings.backText:"&laquo; "+l.html();var c;c=angular.element(i.settings.mobileShowParentLink&&s&&s.length>1?'<li class="title back js-generated"><h5><a href="#" ng-click="back($event);">{{backText}}</a></h5></li><li><a class="parent-link js-generated" href="'+s+'">{{linkText}}</a></li>':'<li class="title back js-generated"><h5><a href="" ng-click="back($event);">{{backText}}</a></h5></li>'),e(c)(t),n.prepend(c)}}}]),angular.module("mm.foundation.tour",["mm.foundation.position","mm.foundation.tooltip"]).service("$tour",["$window",function(e){function t(){return parseInt(e.localStorage.getItem("mm.tour.step"),10)}function n(t){a=t,e.localStorage.setItem("mm.tour.step",t)}var a=t(),o={};this.add=function(e,t){o[e]=t},this.has=function(e){return!!o[e]},this.isActive=function(){return a>0},this.current=function(e){return e?void n(a):a},this.start=function(){n(1)},this.next=function(){n(a+1)},this.end=function(){n(0)}}]).directive("stepTextPopup",["$tour",function(e){return{restrict:"EA",replace:!0,scope:{title:"@",content:"@",placement:"@",animation:"&",isOpen:"&"},templateUrl:"template/tour/tour.html",link:function(t,n){t.isLastStep=function(){return!e.has(e.current()+1)},t.endTour=function(){n.remove(),e.end()},t.nextStep=function(){n.remove(),e.next()}}}}]).directive("stepText",["$position","$tooltip","$tour","$window",function(e,t,n,a){function o(e){var t=e[0].getBoundingClientRect();return t.top>=0&&t.left>=0&&t.bottom<=a.innerHeight-80&&t.right<=a.innerWidth}function i(t,i,r){var l=parseInt(r.stepIndex,10);if(n.isActive()&&l&&(n.add(l,r),l===n.current())){if(!o(i)){var s=e.offset(i);a.scrollTo(0,s.top-a.innerHeight/2)}return!0}return!1}return t("stepText","step",i)}]),angular.module("mm.foundation.typeahead",["mm.foundation.position","mm.foundation.bindHtml"]).factory("typeaheadParser",["$parse",function(e){var t=/^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;return{parse:function(n){var a=n.match(t);if(!a)throw new Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_' but got '"+n+"'.");return{itemName:a[3],source:e(a[4]),viewMapper:e(a[2]||a[1]),modelMapper:e(a[1])}}}}]).directive("typeahead",["$compile","$parse","$q","$timeout","$document","$position","typeaheadParser",function(e,t,n,a,o,i,r){var l=[9,13,27,38,40];return{require:"ngModel",link:function(s,c,u,p){var d,m=s.$eval(u.typeaheadMinLength)||1,f=s.$eval(u.typeaheadWaitMs)||0,g=s.$eval(u.typeaheadEditable)!==!1,h=t(u.typeaheadLoading).assign||angular.noop,v=t(u.typeaheadOnSelect),b=u.typeaheadInputFormatter?t(u.typeaheadInputFormatter):void 0,$=u.typeaheadAppendToBody?t(u.typeaheadAppendToBody):!1,y=t(u.ngModel).assign,k=r.parse(u.typeahead),x=angular.element("<div typeahead-popup></div>");x.attr({matches:"matches",active:"activeIdx",select:"select(activeIdx)",query:"query",position:"position"}),angular.isDefined(u.typeaheadTemplateUrl)&&x.attr("template-url",u.typeaheadTemplateUrl);var w=s.$new();s.$on("$destroy",function(){w.$destroy()});var C=function(){w.matches=[],w.activeIdx=-1},T=function(e){var t={$viewValue:e};h(s,!0),n.when(k.source(s,t)).then(function(n){if(e===p.$viewValue&&d){if(n.length>0){w.activeIdx=0,w.matches.length=0;for(var a=0;a<n.length;a++)t[k.itemName]=n[a],w.matches.push({label:k.viewMapper(w,t),model:n[a]});w.query=e,w.position=$?i.offset(c):i.position(c),w.position.top=w.position.top+c.prop("offsetHeight")}else C();h(s,!1)}},function(){C(),h(s,!1)})};C(),w.query=void 0;var P;p.$parsers.unshift(function(e){return e&&e.length>=m?f>0?(P&&a.cancel(P),P=a(function(){T(e)},f)):T(e):(h(s,!1),C()),g?e:e?void p.$setValidity("editable",!1):(p.$setValidity("editable",!0),e)}),p.$formatters.push(function(e){var t,n,a={};return b?(a.$model=e,b(s,a)):(a[k.itemName]=e,t=k.viewMapper(s,a),a[k.itemName]=void 0,n=k.viewMapper(s,a),t!==n?t:e)}),w.select=function(e){var t,n,a={};a[k.itemName]=n=w.matches[e].model,t=k.modelMapper(s,a),y(s,t),p.$setValidity("editable",!0),v(s,{$item:n,$model:t,$label:k.viewMapper(s,a)}),C(),c[0].focus()},c.bind("keydown",function(e){0!==w.matches.length&&-1!==l.indexOf(e.which)&&(e.preventDefault(),40===e.which?(w.activeIdx=(w.activeIdx+1)%w.matches.length,w.$digest()):38===e.which?(w.activeIdx=(w.activeIdx?w.activeIdx:w.matches.length)-1,w.$digest()):13===e.which||9===e.which?w.$apply(function(){w.select(w.activeIdx)}):27===e.which&&(e.stopPropagation(),C(),w.$digest()))}),c.bind("blur",function(){d=!1}),c.bind("focus",function(){d=!0});var S=function(e){c[0]!==e.target&&(C(),w.$digest())};o.bind("click",S),s.$on("$destroy",function(){o.unbind("click",S)});var A=e(x)(w);$?o.find("body").append(A):c.after(A)}}}]).directive("typeaheadPopup",function(){return{restrict:"EA",scope:{matches:"=",query:"=",active:"=",position:"=",select:"&"},replace:!0,templateUrl:"template/typeahead/typeahead-popup.html",link:function(e,t,n){e.templateUrl=n.templateUrl,e.isOpen=function(){return e.matches.length>0},e.isActive=function(t){return e.active==t},e.selectActive=function(t){e.active=t},e.selectMatch=function(t){e.select({activeIdx:t})}}}}).directive("typeaheadMatch",["$http","$templateCache","$compile","$parse",function(e,t,n,a){return{restrict:"EA",scope:{index:"=",match:"=",query:"="},link:function(o,i,r){var l=a(r.templateUrl)(o.$parent)||"template/typeahead/typeahead-match.html";e.get(l,{cache:t}).success(function(e){i.replaceWith(n(e.trim())(o))})}}}]).filter("typeaheadHighlight",function(){function e(e){return e.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(t,n){return n?t.replace(new RegExp(e(n),"gi"),"<strong>$&</strong>"):t}}),angular.module("template/accordion/accordion-group.html",[]).run(["$templateCache",function(e){e.put("template/accordion/accordion-group.html",'<dd>\n  <a ng-click="isOpen = !isOpen" ng-class="{ active: isOpen }"  accordion-transclude="heading">{{heading}}</a>\n  <div class="content" ng-style="isOpen ? {display: \'block\'} : {}" ng-transclude></div>\n</dd>\n')}]),angular.module("template/accordion/accordion.html",[]).run(["$templateCache",function(e){e.put("template/accordion/accordion.html",'<dl class="accordion" ng-transclude></dl>\n')}]),angular.module("template/alert/alert.html",[]).run(["$templateCache",function(e){e.put("template/alert/alert.html","<div class='alert-box' ng-class='(type || \"\")'>\n  <span ng-transclude></span>\n  <a ng-show='closeable' class='close' ng-click='close()'>&times;</a>\n</div>\n")}]),angular.module("template/modal/backdrop.html",[]).run(["$templateCache",function(e){e.put("template/modal/backdrop.html",'<div class="reveal-modal-bg fade" ng-class="{in: animate}" ng-click="close($event)" style="display: block"></div>\n')}]),angular.module("template/modal/window.html",[]).run(["$templateCache",function(e){e.put("template/modal/window.html",'<div tabindex="-1" class="reveal-modal fade {{ windowClass }}"\n  ng-class="{in: animate}" style="display: block; visibility: visible; top: 0px">\n  <div ng-transclude></div>\n</div>\n')}]),angular.module("template/pagination/pager.html",[]).run(["$templateCache",function(e){e.put("template/pagination/pager.html",'<ul class="pagination">\n  <li ng-repeat="page in pages" class="arrow" ng-class="{unavailable: page.disabled, left: page.previous, right: page.next}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n</ul>\n')}]),angular.module("template/pagination/pagination.html",[]).run(["$templateCache",function(e){e.put("template/pagination/pagination.html",'<ul class="pagination">\n  <li ng-repeat="page in pages" ng-class="{arrow: $first || $last, current: page.active, unavailable: page.disabled}"><a ng-click="selectPage(page.number)">{{page.text}}</a></li>\n</ul>\n')}]),angular.module("template/tooltip/tooltip-html-unsafe-popup.html",[]).run(["$templateCache",function(e){e.put("template/tooltip/tooltip-html-unsafe-popup.html",'<span class="tooltip tip-{{placement}}"\n  ng-class="{ in: isOpen(), fade: animation() }"\n  style="width: auto">\n  <span bind-html-unsafe="content"></span>\n  <span class="nub"></span>\n</span>\n')}]),angular.module("template/tooltip/tooltip-popup.html",[]).run(["$templateCache",function(e){e.put("template/tooltip/tooltip-popup.html",'<span class="tooltip tip-{{placement}}"\n  ng-class="{ in: isOpen(), fade: animation() }"\n  style="width: auto">\n  <span ng-bind="content"></span>\n  <span class="nub"></span>\n</span>\n')}]),angular.module("template/popover/popover.html",[]).run(["$templateCache",function(e){e.put("template/popover/popover.html",'<div class="joyride-tip-guide" ng-class="{ in: isOpen(), fade: animation() }">\n  <span class="joyride-nub" ng-class="{\n    bottom: placement === \'top\',\n    left: placement === \'right\',\n    right: placement === \'left\',\n    top: placement === \'bottom\'\n  }"></span>\n  <div class="joyride-content-wrapper">\n    <h4 ng-bind="title" ng-show="title"></h4>\n    <p ng-bind="content"></p>\n  </div>\n</div>\n')}]),angular.module("template/progressbar/bar.html",[]).run(["$templateCache",function(e){e.put("template/progressbar/bar.html",'<span class="meter" ng-transclude></span>\n')}]),angular.module("template/progressbar/progress.html",[]).run(["$templateCache",function(e){e.put("template/progressbar/progress.html",'<div class="progress" ng-class="type" ng-transclude></div>\n')}]),angular.module("template/progressbar/progressbar.html",[]).run(["$templateCache",function(e){e.put("template/progressbar/progressbar.html",'<div class="progress" ng-class="type">\n  <span class="meter" ng-transclude></span>\n</div>\n')}]),angular.module("template/rating/rating.html",[]).run(["$templateCache",function(e){e.put("template/rating/rating.html",'<span ng-mouseleave="reset()">\n  <i ng-repeat="r in range" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="fa"\n    ng-class="$index < val && (r.stateOn || \'fa-star\') || (r.stateOff || \'fa-star-o\')"></i>\n</span>\n')}]),angular.module("template/tabs/tab.html",[]).run(["$templateCache",function(e){e.put("template/tabs/tab.html",'<dd ng-class="{active: active}">\n  <a ng-click="select()" tab-heading-transclude>{{heading}}</a>\n</dd>\n')}]),angular.module("template/tabs/tabset.html",[]).run(["$templateCache",function(e){e.put("template/tabs/tabset.html",'<div class="tabbable">\n  <dl class="tabs" ng-class="{\'vertical\': vertical}" ng-transclude></dl>\n  <div class="tabs-content" ng-class="{\'vertical\': vertical}">\n    <div class="content" \n      ng-repeat="tab in tabs" \n      ng-class="{active: tab.active}">\n      <div tab-content-transclude="tab"></div>\n    </div>\n  </div>\n</div>\n')}]),angular.module("template/topbar/has-dropdown.html",[]).run(["$templateCache",function(e){e.put("template/topbar/has-dropdown.html",'<li class="has-dropdown" ng-transclude></li>')}]),angular.module("template/topbar/toggle-top-bar.html",[]).run(["$templateCache",function(e){e.put("template/topbar/toggle-top-bar.html",'<li class="toggle-topbar menu-icon" ng-transclude></li>')}]),angular.module("template/topbar/top-bar-dropdown.html",[]).run(["$templateCache",function(e){e.put("template/topbar/top-bar-dropdown.html",'<ul class="dropdown" ng-transclude></ul>')}]),angular.module("template/topbar/top-bar-section.html",[]).run(["$templateCache",function(e){e.put("template/topbar/top-bar-section.html",'<section class="top-bar-section" ng-transclude></section>')}]),angular.module("template/topbar/top-bar.html",[]).run(["$templateCache",function(e){e.put("template/topbar/top-bar.html",'<nav class="top-bar" ng-transclude></nav>')}]),angular.module("template/tour/tour.html",[]).run(["$templateCache",function(e){e.put("template/tour/tour.html",'<div class="joyride-tip-guide" ng-class="{ in: isOpen(), fade: animation() }">\n  <span class="joyride-nub" ng-class="{\n    bottom: placement === \'top\',\n    left: placement === \'right\',\n    right: placement === \'left\',\n    top: placement === \'bottom\'\n  }"></span>\n  <div class="joyride-content-wrapper">\n    <h4 ng-bind="title" ng-show="title"></h4>\n    <p ng-bind="content"></p>\n    <a class="small button joyride-next-tip" ng-show="!isLastStep()" ng-click="nextStep()">Next</a>\n    <a class="small button joyride-next-tip" ng-show="isLastStep()" ng-click="endTour()">End</a>\n    <a class="joyride-close-tip" ng-click="endTour()">&times;</a>\n  </div>\n</div>\n')}]),angular.module("template/typeahead/typeahead-match.html",[]).run(["$templateCache",function(e){e.put("template/typeahead/typeahead-match.html",'<a tabindex="-1" bind-html-unsafe="match.label | typeaheadHighlight:query"></a>')}]),angular.module("template/typeahead/typeahead-popup.html",[]).run(["$templateCache",function(e){e.put("template/typeahead/typeahead-popup.html","<ul class=\"f-dropdown\" ng-style=\"{display: isOpen()&&'block' || 'none', top: position.top+'px', left: position.left+'px'}\">\n"+'    <li ng-repeat="match in matches" ng-class="{active: isActive($index) }" ng-mouseenter="selectActive($index)" ng-click="selectMatch($index)">\n        <div typeahead-match index="$index" match="match" query="query" template-url="templateUrl"></div>\n    </li>\n</ul>\n')}]);
/* 
 * Angular JS Multi Select
 * Creates a dropdown-like button with checkboxes. 
 *
 * Created: Tue, 14 Jan 2014 - 5:18:02 PM
 *
 * Released under the MIT License
 *
 * --------------------------------------------------------------------------------
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Ignatius Steven (https://github.com/isteven)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights 
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions: 
 *
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 * --------------------------------------------------------------------------------
 */

angular.module( 'multi-select', ['ng'] ).directive( 'multiSelect' , [ '$sce', function ( $sce ) {
    return {
        restrict: 
            'AE',

        replace: 
            true,

        scope: 
        {            
            inputModel      : '=',
            outputModel     : '=',
            buttonLabel     : '@',
            selectionMode   : '@',
            itemLabel       : '@',
            tickProperty    : '@',
            disableProperty : '@',
            orientation     : '@',
            maxLabels       : '@',
            isDisabled      : '=',
            directiveId     : '@'
        },

        template: 
            '<span class="multiSelect inlineBlock">' +
                '<button type="button" class="multiSelect button multiSelectButton" ng-click="toggleCheckboxes( $event ); refreshSelectedItems();" ng-bind-html="varButtonLabel">' +
                '</button>' +                
                '<div class="multiSelect checkboxLayer hide">' +
                    '<div class="multiSelect line">' +
                        '<span ng-if="!isDisabled">Select: &nbsp;</span>' + 
                            '<button type="button" ng-click="select( \'all\' )"    class="multiSelect helperButton" ng-if="!isDisabled && selectionMode.toUpperCase() != \'SINGLE\'">All</button> ' +
                            '<button type="button" ng-click="select( \'none\' )"   class="multiSelect helperButton" ng-if="!isDisabled && selectionMode.toUpperCase() != \'SINGLE\'">None</button> ' + 
                            '<button type="button" ng-click="select( \'reset\' )"  class="multiSelect helperButton" ng-if="!isDisabled">Reset</button>' +
                    '</div>' +
                    '<div class="multiSelect line">' + 
                        'Filter: <input class="multiSelect" type="text" ng-model="labelFilter" />' +
                            '&nbsp;<button type="button" class="multiSelect helperButton" ng-click="labelFilter=\'\'">Clear</button>' +
                    '</div>' +
                    '<div ng-repeat="item in inputModel | filter:labelFilter" ng-class="orientation" class="multiSelect multiSelectItem">' +
                        '<div class="multiSelect acol">' +
                            '<div class="multiSelect" ng-show="item[ tickProperty ]">&#10004;</div>' +
                        '</div>' +
                        '<div class="multiSelect acol">' +
                            '<label class="multiSelect" ng-class="{checkboxSelected:item[ tickProperty ]}">' +
                                '<input class="multiSelect checkbox" type="checkbox" ng-disabled="itemIsDisabled( item )" ng-checked="item[ tickProperty ]" ng-click="syncItems( item, $event )" />' +
                                    '<span class="multiSelect" ng-class="{disabled:itemIsDisabled( item )}" ng-bind-html="writeLabel( item, \'itemLabel\' )"></span>' +
                            '</label>&nbsp;&nbsp;' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</span>',

        link: function ( $scope, element, attrs ) {      
            
            $scope.selectedItems    = [];    
            $scope.backUp           = [];
            $scope.varButtonLabel   = '';            

            // Checkbox is ticked
            $scope.syncItems = function( item, e ) {                                                                
                index = $scope.inputModel.indexOf( item );                
                $scope.inputModel[ index ][ $scope.tickProperty ]   = !$scope.inputModel[ index ][ $scope.tickProperty ];
                
                // If it's single selection mode
                if ( attrs.selectionMode && $scope.selectionMode.toUpperCase() === 'SINGLE' ) {
                    $scope.inputModel[ index ][ $scope.tickProperty ] = true;
                    for( i=0; i<$scope.inputModel.length;i++) {
                        if ( i !== index ) {
                            $scope.inputModel[ i ][ $scope.tickProperty ] = false;
                        }
                    }        
                    $scope.toggleCheckboxes( e );
                }

                $scope.refreshSelectedItems();                   
            }     

            // Refresh the button to display the selected items and push into output model if specified
            $scope.refreshSelectedItems = function() {

                $scope.varButtonLabel   = '';
                $scope.selectedItems    = [];
                ctr                     = 0;
                
                angular.forEach( $scope.inputModel, function( value, key ) {
                    if ( typeof value !== 'undefined' ) {                        
                        if ( value[ $scope.tickProperty ] === true || value[ $scope.tickProperty ] === 'true' ) {
                            $scope.selectedItems.push( value );        
                        }
                    }
                });
                                           
                // Push into output model
                if ( typeof attrs.outputModel !== 'undefined' ) {            
                    $scope.outputModel = angular.copy( $scope.selectedItems );                    
                }                                

                // Write label...
                if ( $scope.selectedItems.length === 0 ) {
                    $scope.varButtonLabel = 'None selected';
                }
                else {                
                    var tempMaxLabels = $scope.selectedItems.length;
                    if ( typeof $scope.maxLabels !== 'undefined' && $scope.maxLabels !== '' && $scope.maxLabels !== "0" ) {
                        tempMaxLabels = $scope.maxLabels;
                    }

                    // If max amount of labels displayed..
                    if ( $scope.selectedItems.length > tempMaxLabels ) {
                        $scope.more = true;
                    }
                    else {
                        $scope.more = false;
                    }                
                
                    angular.forEach( $scope.selectedItems, function( value, key ) {
                        if ( typeof value !== 'undefined' ) {                        
                            if ( ctr < tempMaxLabels ) {                            
                                $scope.varButtonLabel += ( $scope.varButtonLabel.length > 0 ? ', ' : '') + $scope.writeLabel( value, 'buttonLabel' );
                            }
                            ctr++;
                        }
                    });                

                    if ( $scope.more === true ) {
                        $scope.varButtonLabel += ', ... (Total: ' + $scope.selectedItems.length + ')';
                    }
                }
                $scope.varButtonLabel = $sce.trustAsHtml( $scope.varButtonLabel + '<span class="multiSelect caret"></span>' );
            }

            // Check if a checkbox is disabled or enabled. It will check the granular control (disableProperty) and global control (isDisabled)
            // Take note that the granular control has higher priority.
            $scope.itemIsDisabled = function( item ) {
                
                if ( item[ $scope.disableProperty ] === true ) {                    
                    return true;
                }
                else {                    
                    if ( $scope.isDisabled === true ) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                
            }

            // A simple function to parse the item label settings
            $scope.writeLabel = function( item, type ) {
                var label = '';
                var temp = $scope[ type ].split( ' ' );                    
                angular.forEach( temp, function( value2, key2 ) {
                    if ( typeof value2 !== 'undefined' ) {                        
                        angular.forEach( item, function( value1, key1 ) {                    
                            if ( key1 == value2 ) {
                                label += ' ' + value1;        
                            }
                        });                    
                    }
                });
                return $sce.trustAsHtml( label );
            }

            // UI operations to show/hide checkboxes
            $scope.toggleCheckboxes = function( e ) {                                                

                if ( e.target ) {                    
                    if ( e.target.tagName.toUpperCase() !== 'BUTTON' && e.target.className.indexOf( 'multiSelectButton' ) < 0 ) {
                        if ( attrs.selectionMode && $scope.selectionMode.toUpperCase() === 'SINGLE' ) {
                            if ( e.target.tagName.toUpperCase() === 'INPUT' )
                            {
                                e = $scope.findUpTag( e.target, 'div', 'checkboxLayer' );
                                e = e.previousSibling;    
                            }
                        }
                        else {
                            e = $scope.findUpTag( e.target, 'button', 'multiSelectButton' );
                        }
                    }
                    else {
                        e = e.target;
                    }
                }                    

                $scope.labelFilter = '';                
              
                // We search them based on the class names
                var multiSelectIndex    = -1;                                
                var checkboxes          = document.querySelectorAll( '.checkboxLayer' );
                var multiSelectButtons  = document.querySelectorAll( '.multiSelectButton' );   

                for( i=0; i < multiSelectButtons.length; i++ ) {
                    if ( e === multiSelectButtons[ i ] ) {                        
                        multiSelectIndex = i;
                        break;
                    }
                }                
                                 
                if ( multiSelectIndex > -1 ) {
                    for( i=0; i < checkboxes.length; i++ ) {
                        if ( i != multiSelectIndex ) {
                            checkboxes[i].className = 'multiSelect checkboxLayer hide';
                        }
                    }                    

                    if ( checkboxes[ multiSelectIndex ].className == 'multiSelect checkboxLayer hide' ) {                    
                        checkboxes[ multiSelectIndex ].className = 'multiSelect checkboxLayer show';
                    }
                    else if ( checkboxes[ multiSelectIndex ].className == 'multiSelect checkboxLayer show' ) {                                    
                        checkboxes[ multiSelectIndex ].className = 'multiSelect checkboxLayer hide';
                    }                
                }
            }

            // Traverse up to find the button tag
            // http://stackoverflow.com/questions/7332179/how-to-recursively-search-all-parentnodes
            $scope.findUpTag = function ( el, tag, className ) {

                while ( el.parentNode ) {
                    el = el.parentNode;      
                    if ( typeof el.tagName !== 'undefined' ) {
                        if ( el.tagName.toUpperCase() === tag.toUpperCase() && el.className.indexOf( className ) > -1 ) {
                            return el;
                        }
                    }
                }
                return null;
            }

            // Select All / None / Reset
            $scope.select = function( type ) {
                var temp = [];
                switch( type.toUpperCase() ) {
                    case 'ALL':
                        angular.forEach( $scope.inputModel, function( value, key ) {
                            if ( typeof value !== 'undefined' && value[ $scope.disableProperty ] !== true ) {                        
                                value[ $scope.tickProperty ] = true;
                            }
                        });                                        
                        break;
                    case 'NONE':
                        angular.forEach( $scope.inputModel, function( value, key ) {
                            if ( typeof value !== 'undefined' && value[ $scope.disableProperty ] !== true ) {                        
                                value[ $scope.tickProperty ] = false;
                            }
                        });                
                        break;      
                    case 'RESET':
                        $scope.inputModel = angular.copy( $scope.backUp );                        
                        break;
                    default:                        
                }
                $scope.refreshSelectedItems();
            }            


            // Generic validation for required attributes
            validate = function() {
                if ( !( 'inputModel' in attrs )) {
                    console.log( 'Multi-select error: input-model is not defined! (ID: ' + $scope.directiveId + ')' );
                }

                if ( !( 'buttonLabel' in attrs )) {
                    console.log( 'Multi-select error: button-label is not defined! (ID: ' + $scope.directiveId + ')' );                
                }            

                if ( !( 'itemLabel' in attrs )) {
                    console.log( 'Multi-select error: item-label is not defined! (ID: ' + $scope.directiveId + ')' );                
                }                            

                if ( !( 'tickProperty' in attrs )) {
                    console.log( 'Multi-select error: tick-property is not defined! (ID: ' + $scope.directiveId + ')' );                
                }            
            }

            // Validate whether the properties specified in the directive attributes are present in the input model
            validateProperties = function( arrProperties, arrObject ) {
                var notThere = false;            
                var missingProperty = '';
                angular.forEach( arrProperties, function( value1, key1 ) {
                    if ( typeof value1 !== 'undefined' ) {                        
                        var keepGoing = true;
                        angular.forEach( arrObject, function( value2, key2 ) {
                            if ( typeof value2 !== 'undefined' && keepGoing ) {                        
                                if (!( value1 in value2 )) {
                                    notThere = true;
                                    keepGoing = false;
                                    missingLabel = value1;
                                }
                            }
                        });                    
                    }
                });    
                if ( notThere === true ) {
                    console.log( 'Multi-select error: property "' + missingLabel + '" is not available in the input model. (Name: ' + $scope.directiveId + ')' );
                }
                
            }

            ///////////////////////
            // Logic starts here
            ///////////////////////               

            validate();
            $scope.refreshSelectedItems();                  

            // Watch for changes in input model (allow dynamic input)
            $scope.$watch( 'inputModel' , function( oldVal, newVal ) {                 
                if ( $scope.inputModel !== 'undefined' ) {
                    validateProperties( $scope.itemLabel.split( ' ' ), $scope.inputModel );
                    validateProperties( new Array( $scope.tickProperty ), $scope.inputModel );
                }
                $scope.backUp = angular.copy( $scope.inputModel );                                                    
                $scope.refreshSelectedItems();                                                 
            });

            // Watch for changes in directive state (disabled or enabled)
            $scope.$watch( 'isDisabled' , function( newVal ) {         
                $scope.isDisabled = newVal;                               
            });

            // Monitor for clicks outside the button element to hide the checkboxes
            angular.element( document ).bind( 'click' , function( e ) {                                
                var checkboxes = document.querySelectorAll( '.checkboxLayer' );     
                if ( e.target.className.indexOf( 'multiSelect' ) === -1 ) {
                    for( i=0; i < checkboxes.length; i++ ) {                                        
                        checkboxes[i].className = 'multiSelect checkboxLayer hide';                        
                    }    
                    e.stopPropagation();
                }                                
            });           
            
            // For IE8, perhaps. Not sure if this is really executed.
            if ( !Array.prototype.indexOf ) {
                Array.prototype.indexOf = function(what, i) {                    
                    i = i || 0;
                    var L = this.length;
                    while (i < L) {
                        if(this[i] === what) return i;
                        ++i;
                    }
                    return -1;
                };
            }
        }   
    }
}]);


'use strict';

angular.module('ngjsColorPicker', [])
    .directive('ngjsColorPicker', function() {
        var defaultColors =  [
            '#1abc9c',
            '#16a085',
            '#2ecc71',
            '#27ae60',
            '#3498db',
            '#2980b9',
            '#9b59b6',
            '#8e44ad',
            '#f1c40f',
            '#f39c12',
            '#e67e22',
            '#d35400',
            '#e74c3c',
            '#c0392b',
            '#ecf0f1',
            '#bdc3c7',
            '#95a5a6',
            '#34495e',
            '#2c3e50',
            '#19b5fe',
            '#89c4f4',
            '#5d8cae',
            '#7a942e',
            '#2abb9b'
        ];
        return {
            scope: {
                selected: '=?',
                customColors: '=?',
                options: '=?',
                gradient: '=?'
            },
            restrict: 'E',
            template: '<ul ng-style="ulCss"><li ng-repeat="color in colors | limitTo: options.total"\
                        ng-class="{\
                        selected: (color===selected),\
                        hRDF: $first&&hzRound,\
                        hRDL: $last&&hzRound,\
                        vRDF: $first&&vertRound,\
                        vRDL: $last&&vertRound,\
                        tlRound: $first&&columnRound,\
                        trRound: $index==(options.columns-1)&&columnRound,\
                        brRound: $last&&columnRound,\
                        blRound: $index==(colors.length-options.columns)&&columnRound\
                        }"\
                        ng-click="pick(color)"\
                        style="background-color:{{color}};"\
                        ng-style="css">\
                        </li></ul>',
            link: function (scope, element, attr) {

                // Priorities
                // 1. Custom colors
                // 2. Random colors
                // 3. Gradient
                // 4. Default colors

                // Set options
                scope.colors = scope.customColors || defaultColors;
                scope.options = scope.options || {};
                scope.options.size = scope.options.size || 20;
                scope.options.columns = scope.options.columns || 0;
                scope.options.randomColors = scope.options.randomColors || 0;
                scope.options.total = scope.options.total || scope.colors.length;
                scope.options.horizontal = (scope.options.hasOwnProperty('horizontal') ? scope.options.horizontal : true);
                scope.options.roundCorners = (scope.options.hasOwnProperty('roundCorners') ? scope.options.roundCorners : false);
                scope.gradient = scope.gradient || null;

                // Contains all css styles for all <li> elements
                scope.css = {};
                // Contains all css styles for the <ul> element
                scope.ulCss = {};
                // Set bar to horizontal/vertical
                scope.css.display = (scope.options.horizontal ? 'inline-block' : 'block');
                // Set size of squares
                scope.css.width = scope.css.height = scope.options.size + 'px';

                // - If uneven columns - no round corners at all
                // - Horizontal or vertical has no effect if columns are not "even"
                if(scope.options.columns > 0){
                    var indexOfPx = scope.css.width.indexOf('p');
                    scope.ulCss.width = '95%'; //scope.options.columns*(parseInt(scope.css.width.substr(0,indexOfPx))) + 'px';
                    scope.ulCss.height = '120px'; //scope.options.size*(scope.colors.length/scope.options.columns) + 'px';
                    scope.css.cssFloat = 'left';
                }

                // Set if rounded corners (horizontal or vertical)
                scope.hzRound = (scope.options.horizontal && scope.options.roundCorners && scope.options.columns === 0);
                scope.vertRound = (!scope.options.horizontal && scope.options.roundCorners && scope.options.columns === 0);

                // Generate random colors
                if(scope.options.randomColors >0 && !scope.customColors){
                    scope.colors = [];
                    var count = scope.options.randomColors;
                    while(count !== 0){
                        scope.colors.push(_randomHexColor());
                        count--;
                    }
                }

                // Generate random hex color
                function _randomHexColor(){
                    return ("#" + (Math.random().toString(16) + '000000').slice(2, 8));
                }

                // Generate gradient colors
                // Example: { count:10, start:'#DFCA4A', step:1 }
                // count (default 10): how many boxes (integer)
                // start: starting color (#08a35c || 08a35c) (must be full hex (6 characters))
                // step (default 1): If you pass in 100 for percent it will make your color pure white.
                // If you pass in 0 for percent, nothing will happen.
                // If you pass in 1 for percent it will add 3 shades to all colors (2.55 shades per 1%, rounded).
                // (positive or negative integer)
                // ----
                // - Max count is 14, cannot produce more colors than that
                if(scope.gradient && !scope.customColors && !scope.options.randomColors){
                    var validHex = _formatToHex(scope.gradient.start);
                    var isOkHex = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(validHex);
                    if(isOkHex){
                        scope.colors = [];
                        count = (scope.gradient.hasOwnProperty('count') ? scope.gradient.count : 10);
                        var interval = (scope.gradient.hasOwnProperty('step') ? scope.gradient.step : 1);
                        while(count !== 0){
                            scope.colors.push(_shadeColor(scope.colors.length === 0 ? validHex : scope.colors[scope.colors.length-1], interval));
                            interval+=scope.gradient.step;
                            count--;

                            // If black or white - stop generating more colors
                            if(scope.colors[scope.colors.length-1].toLowerCase() === '#ffffff' || scope.colors[scope.colors.length-1] === '#000000')
                                count = 0;
                        }
                    }
                }

                // Set if rounded corners (columns)
                var isOkColumn = (scope.colors.length%scope.options.columns === 0);
                scope.columnRound = (scope.options.columns && scope.options.roundCorners && isOkColumn);

                // Transfer input to valid hex
                function _formatToHex(hex){
                    var index = +(hex.charAt(0) === '#');
                    return '#' + hex.substr(index).toLowerCase();
                }

                // Darken or lighten color
                function _shadeColor(color, percent) {
                    var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
                    return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
                }

                // Pick a color from the view
                scope.pick = function (color) {
                    scope.selected = color;
                };

                // Set selection to chosen color or first color
                scope.selected = scope.selected || scope.colors[0];

            }
        };

    });
angular.module("angular-gap", []).directive("gap", function() {
    return {
        restrict: "ECA",
        transclude: !0,
        template: '<div class="angular-gap"></div>'
    };
});
/*
 AngularJS v1.3.10
 (c) 2010-2014 Google, Inc. http://angularjs.org
 License: MIT
*/
(function(N,f,W){'use strict';f.module("ngAnimate",["ng"]).directive("ngAnimateChildren",function(){return function(X,C,g){g=g.ngAnimateChildren;f.isString(g)&&0===g.length?C.data("$$ngAnimateChildren",!0):X.$watch(g,function(f){C.data("$$ngAnimateChildren",!!f)})}}).factory("$$animateReflow",["$$rAF","$document",function(f,C){return function(g){return f(function(){g()})}}]).config(["$provide","$animateProvider",function(X,C){function g(f){for(var n=0;n<f.length;n++){var g=f[n];if(1==g.nodeType)return g}}
function ba(f,n){return g(f)==g(n)}var t=f.noop,n=f.forEach,da=C.$$selectors,aa=f.isArray,ea=f.isString,ga=f.isObject,r={running:!0},u;X.decorator("$animate",["$delegate","$$q","$injector","$sniffer","$rootElement","$$asyncCallback","$rootScope","$document","$templateRequest","$$jqLite",function(O,N,M,Y,y,H,P,W,Z,Q){function R(a,c){var b=a.data("$$ngAnimateState")||{};c&&(b.running=!0,b.structural=!0,a.data("$$ngAnimateState",b));return b.disabled||b.running&&b.structural}function D(a){var c,b=N.defer();
b.promise.$$cancelFn=function(){c&&c()};P.$$postDigest(function(){c=a(function(){b.resolve()})});return b.promise}function I(a){if(ga(a))return a.tempClasses&&ea(a.tempClasses)&&(a.tempClasses=a.tempClasses.split(/\s+/)),a}function S(a,c,b){b=b||{};var d={};n(b,function(e,a){n(a.split(" "),function(a){d[a]=e})});var h=Object.create(null);n((a.attr("class")||"").split(/\s+/),function(e){h[e]=!0});var f=[],l=[];n(c&&c.classes||[],function(e,a){var b=h[a],c=d[a]||{};!1===e?(b||"addClass"==c.event)&&
l.push(a):!0===e&&(b&&"removeClass"!=c.event||f.push(a))});return 0<f.length+l.length&&[f.join(" "),l.join(" ")]}function T(a){if(a){var c=[],b={};a=a.substr(1).split(".");(Y.transitions||Y.animations)&&c.push(M.get(da[""]));for(var d=0;d<a.length;d++){var f=a[d],k=da[f];k&&!b[f]&&(c.push(M.get(k)),b[f]=!0)}return c}}function U(a,c,b,d){function h(e,a){var b=e[a],c=e["before"+a.charAt(0).toUpperCase()+a.substr(1)];if(b||c)return"leave"==a&&(c=b,b=null),u.push({event:a,fn:b}),J.push({event:a,fn:c}),
!0}function k(c,l,w){var E=[];n(c,function(a){a.fn&&E.push(a)});var m=0;n(E,function(c,f){var p=function(){a:{if(l){(l[f]||t)();if(++m<E.length)break a;l=null}w()}};switch(c.event){case "setClass":l.push(c.fn(a,e,A,p,d));break;case "animate":l.push(c.fn(a,b,d.from,d.to,p));break;case "addClass":l.push(c.fn(a,e||b,p,d));break;case "removeClass":l.push(c.fn(a,A||b,p,d));break;default:l.push(c.fn(a,p,d))}});l&&0===l.length&&w()}var l=a[0];if(l){d&&(d.to=d.to||{},d.from=d.from||{});var e,A;aa(b)&&(e=
b[0],A=b[1],e?A?b=e+" "+A:(b=e,c="addClass"):(b=A,c="removeClass"));var w="setClass"==c,E=w||"addClass"==c||"removeClass"==c||"animate"==c,p=a.attr("class")+" "+b;if(x(p)){var ca=t,m=[],J=[],g=t,s=[],u=[],p=(" "+p).replace(/\s+/g,".");n(T(p),function(a){!h(a,c)&&w&&(h(a,"addClass"),h(a,"removeClass"))});return{node:l,event:c,className:b,isClassBased:E,isSetClassOperation:w,applyStyles:function(){d&&a.css(f.extend(d.from||{},d.to||{}))},before:function(a){ca=a;k(J,m,function(){ca=t;a()})},after:function(a){g=
a;k(u,s,function(){g=t;a()})},cancel:function(){m&&(n(m,function(a){(a||t)(!0)}),ca(!0));s&&(n(s,function(a){(a||t)(!0)}),g(!0))}}}}}function G(a,c,b,d,h,k,l,e){function A(e){var l="$animate:"+e;J&&J[l]&&0<J[l].length&&H(function(){b.triggerHandler(l,{event:a,className:c})})}function w(){A("before")}function E(){A("after")}function p(){p.hasBeenRun||(p.hasBeenRun=!0,k())}function g(){if(!g.hasBeenRun){m&&m.applyStyles();g.hasBeenRun=!0;l&&l.tempClasses&&n(l.tempClasses,function(a){u.removeClass(b,
a)});var w=b.data("$$ngAnimateState");w&&(m&&m.isClassBased?B(b,c):(H(function(){var e=b.data("$$ngAnimateState")||{};fa==e.index&&B(b,c,a)}),b.data("$$ngAnimateState",w)));A("close");e()}}var m=U(b,a,c,l);if(!m)return p(),w(),E(),g(),t;a=m.event;c=m.className;var J=f.element._data(m.node),J=J&&J.events;d||(d=h?h.parent():b.parent());if(z(b,d))return p(),w(),E(),g(),t;d=b.data("$$ngAnimateState")||{};var L=d.active||{},s=d.totalActive||0,q=d.last;h=!1;if(0<s){s=[];if(m.isClassBased)"setClass"==q.event?
(s.push(q),B(b,c)):L[c]&&(v=L[c],v.event==a?h=!0:(s.push(v),B(b,c)));else if("leave"==a&&L["ng-leave"])h=!0;else{for(var v in L)s.push(L[v]);d={};B(b,!0)}0<s.length&&n(s,function(a){a.cancel()})}!m.isClassBased||m.isSetClassOperation||"animate"==a||h||(h="addClass"==a==b.hasClass(c));if(h)return p(),w(),E(),A("close"),e(),t;L=d.active||{};s=d.totalActive||0;if("leave"==a)b.one("$destroy",function(a){a=f.element(this);var e=a.data("$$ngAnimateState");e&&(e=e.active["ng-leave"])&&(e.cancel(),B(a,"ng-leave"))});
u.addClass(b,"ng-animate");l&&l.tempClasses&&n(l.tempClasses,function(a){u.addClass(b,a)});var fa=K++;s++;L[c]=m;b.data("$$ngAnimateState",{last:m,active:L,index:fa,totalActive:s});w();m.before(function(e){var l=b.data("$$ngAnimateState");e=e||!l||!l.active[c]||m.isClassBased&&l.active[c].event!=a;p();!0===e?g():(E(),m.after(g))});return m.cancel}function q(a){if(a=g(a))a=f.isFunction(a.getElementsByClassName)?a.getElementsByClassName("ng-animate"):a.querySelectorAll(".ng-animate"),n(a,function(a){a=
f.element(a);(a=a.data("$$ngAnimateState"))&&a.active&&n(a.active,function(a){a.cancel()})})}function B(a,c){if(ba(a,y))r.disabled||(r.running=!1,r.structural=!1);else if(c){var b=a.data("$$ngAnimateState")||{},d=!0===c;!d&&b.active&&b.active[c]&&(b.totalActive--,delete b.active[c]);if(d||!b.totalActive)u.removeClass(a,"ng-animate"),a.removeData("$$ngAnimateState")}}function z(a,c){if(r.disabled)return!0;if(ba(a,y))return r.running;var b,d,g;do{if(0===c.length)break;var k=ba(c,y),l=k?r:c.data("$$ngAnimateState")||
{};if(l.disabled)return!0;k&&(g=!0);!1!==b&&(k=c.data("$$ngAnimateChildren"),f.isDefined(k)&&(b=k));d=d||l.running||l.last&&!l.last.isClassBased}while(c=c.parent());return!g||!b&&d}u=Q;y.data("$$ngAnimateState",r);var $=P.$watch(function(){return Z.totalPendingRequests},function(a,c){0===a&&($(),P.$$postDigest(function(){P.$$postDigest(function(){r.running=!1})}))}),K=0,V=C.classNameFilter(),x=V?function(a){return V.test(a)}:function(){return!0};return{animate:function(a,c,b,d,h){d=d||"ng-inline-animate";
h=I(h)||{};h.from=b?c:null;h.to=b?b:c;return D(function(b){return G("animate",d,f.element(g(a)),null,null,t,h,b)})},enter:function(a,c,b,d){d=I(d);a=f.element(a);c=c&&f.element(c);b=b&&f.element(b);R(a,!0);O.enter(a,c,b);return D(function(h){return G("enter","ng-enter",f.element(g(a)),c,b,t,d,h)})},leave:function(a,c){c=I(c);a=f.element(a);q(a);R(a,!0);return D(function(b){return G("leave","ng-leave",f.element(g(a)),null,null,function(){O.leave(a)},c,b)})},move:function(a,c,b,d){d=I(d);a=f.element(a);
c=c&&f.element(c);b=b&&f.element(b);q(a);R(a,!0);O.move(a,c,b);return D(function(h){return G("move","ng-move",f.element(g(a)),c,b,t,d,h)})},addClass:function(a,c,b){return this.setClass(a,c,[],b)},removeClass:function(a,c,b){return this.setClass(a,[],c,b)},setClass:function(a,c,b,d){d=I(d);a=f.element(a);a=f.element(g(a));if(R(a))return O.$$setClassImmediately(a,c,b,d);var h,k=a.data("$$animateClasses"),l=!!k;k||(k={classes:{}});h=k.classes;c=aa(c)?c:c.split(" ");n(c,function(a){a&&a.length&&(h[a]=
!0)});b=aa(b)?b:b.split(" ");n(b,function(a){a&&a.length&&(h[a]=!1)});if(l)return d&&k.options&&(k.options=f.extend(k.options||{},d)),k.promise;a.data("$$animateClasses",k={classes:h,options:d});return k.promise=D(function(e){var l=a.parent(),b=g(a),c=b.parentNode;if(!c||c.$$NG_REMOVED||b.$$NG_REMOVED)e();else{b=a.data("$$animateClasses");a.removeData("$$animateClasses");var c=a.data("$$ngAnimateState")||{},d=S(a,b,c.active);return d?G("setClass",d,a,l,null,function(){d[0]&&O.$$addClassImmediately(a,
d[0]);d[1]&&O.$$removeClassImmediately(a,d[1])},b.options,e):e()}})},cancel:function(a){a.$$cancelFn()},enabled:function(a,c){switch(arguments.length){case 2:if(a)B(c);else{var b=c.data("$$ngAnimateState")||{};b.disabled=!0;c.data("$$ngAnimateState",b)}break;case 1:r.disabled=!a;break;default:a=!r.disabled}return!!a}}}]);C.register("",["$window","$sniffer","$timeout","$$animateReflow",function(r,C,M,Y){function y(){b||(b=Y(function(){c=[];b=null;x={}}))}function H(a,e){b&&b();c.push(e);b=Y(function(){n(c,
function(a){a()});c=[];b=null;x={}})}function P(a,e){var b=g(a);a=f.element(b);k.push(a);b=Date.now()+e;b<=h||(M.cancel(d),h=b,d=M(function(){X(k);k=[]},e,!1))}function X(a){n(a,function(a){(a=a.data("$$ngAnimateCSS3Data"))&&n(a.closeAnimationFns,function(a){a()})})}function Z(a,e){var b=e?x[e]:null;if(!b){var c=0,d=0,f=0,g=0;n(a,function(a){if(1==a.nodeType){a=r.getComputedStyle(a)||{};c=Math.max(Q(a[z+"Duration"]),c);d=Math.max(Q(a[z+"Delay"]),d);g=Math.max(Q(a[K+"Delay"]),g);var e=Q(a[K+"Duration"]);
0<e&&(e*=parseInt(a[K+"IterationCount"],10)||1);f=Math.max(e,f)}});b={total:0,transitionDelay:d,transitionDuration:c,animationDelay:g,animationDuration:f};e&&(x[e]=b)}return b}function Q(a){var e=0;a=ea(a)?a.split(/\s*,\s*/):[];n(a,function(a){e=Math.max(parseFloat(a)||0,e)});return e}function R(b,e,c,d){b=0<=["ng-enter","ng-leave","ng-move"].indexOf(c);var f,p=e.parent(),h=p.data("$$ngAnimateKey");h||(p.data("$$ngAnimateKey",++a),h=a);f=h+"-"+g(e).getAttribute("class");var p=f+" "+c,h=x[p]?++x[p].total:
0,m={};if(0<h){var n=c+"-stagger",m=f+" "+n;(f=!x[m])&&u.addClass(e,n);m=Z(e,m);f&&u.removeClass(e,n)}u.addClass(e,c);var n=e.data("$$ngAnimateCSS3Data")||{},k=Z(e,p);f=k.transitionDuration;k=k.animationDuration;if(b&&0===f&&0===k)return u.removeClass(e,c),!1;c=d||b&&0<f;b=0<k&&0<m.animationDelay&&0===m.animationDuration;e.data("$$ngAnimateCSS3Data",{stagger:m,cacheKey:p,running:n.running||0,itemIndex:h,blockTransition:c,closeAnimationFns:n.closeAnimationFns||[]});p=g(e);c&&(I(p,!0),d&&e.css(d));
b&&(p.style[K+"PlayState"]="paused");return!0}function D(a,e,b,c,d){function f(){e.off(D,h);u.removeClass(e,k);u.removeClass(e,t);z&&M.cancel(z);G(e,b);var a=g(e),c;for(c in s)a.style.removeProperty(s[c])}function h(a){a.stopPropagation();var b=a.originalEvent||a;a=b.$manualTimeStamp||b.timeStamp||Date.now();b=parseFloat(b.elapsedTime.toFixed(3));Math.max(a-H,0)>=C&&b>=x&&c()}var m=g(e);a=e.data("$$ngAnimateCSS3Data");if(-1!=m.getAttribute("class").indexOf(b)&&a){var k="",t="";n(b.split(" "),function(a,
b){var e=(0<b?" ":"")+a;k+=e+"-active";t+=e+"-pending"});var s=[],q=a.itemIndex,v=a.stagger,r=0;if(0<q){r=0;0<v.transitionDelay&&0===v.transitionDuration&&(r=v.transitionDelay*q);var y=0;0<v.animationDelay&&0===v.animationDuration&&(y=v.animationDelay*q,s.push(B+"animation-play-state"));r=Math.round(100*Math.max(r,y))/100}r||(u.addClass(e,k),a.blockTransition&&I(m,!1));var F=Z(e,a.cacheKey+" "+k),x=Math.max(F.transitionDuration,F.animationDuration);if(0===x)u.removeClass(e,k),G(e,b),c();else{!r&&
d&&0<Object.keys(d).length&&(F.transitionDuration||(e.css("transition",F.animationDuration+"s linear all"),s.push("transition")),e.css(d));var q=Math.max(F.transitionDelay,F.animationDelay),C=1E3*q;0<s.length&&(v=m.getAttribute("style")||"",";"!==v.charAt(v.length-1)&&(v+=";"),m.setAttribute("style",v+" "));var H=Date.now(),D=V+" "+$,q=1E3*(r+1.5*(q+x)),z;0<r&&(u.addClass(e,t),z=M(function(){z=null;0<F.transitionDuration&&I(m,!1);0<F.animationDuration&&(m.style[K+"PlayState"]="");u.addClass(e,k);
u.removeClass(e,t);d&&(0===F.transitionDuration&&e.css("transition",F.animationDuration+"s linear all"),e.css(d),s.push("transition"))},1E3*r,!1));e.on(D,h);a.closeAnimationFns.push(function(){f();c()});a.running++;P(e,q);return f}}else c()}function I(a,b){a.style[z+"Property"]=b?"none":""}function S(a,b,c,d){if(R(a,b,c,d))return function(a){a&&G(b,c)}}function T(a,b,c,d,f){if(b.data("$$ngAnimateCSS3Data"))return D(a,b,c,d,f);G(b,c);d()}function U(a,b,c,d,f){var g=S(a,b,c,f.from);if(g){var h=g;H(b,
function(){h=T(a,b,c,d,f.to)});return function(a){(h||t)(a)}}y();d()}function G(a,b){u.removeClass(a,b);var c=a.data("$$ngAnimateCSS3Data");c&&(c.running&&c.running--,c.running&&0!==c.running||a.removeData("$$ngAnimateCSS3Data"))}function q(a,b){var c="";a=aa(a)?a:a.split(/\s+/);n(a,function(a,d){a&&0<a.length&&(c+=(0<d?" ":"")+a+b)});return c}var B="",z,$,K,V;N.ontransitionend===W&&N.onwebkittransitionend!==W?(B="-webkit-",z="WebkitTransition",$="webkitTransitionEnd transitionend"):(z="transition",
$="transitionend");N.onanimationend===W&&N.onwebkitanimationend!==W?(B="-webkit-",K="WebkitAnimation",V="webkitAnimationEnd animationend"):(K="animation",V="animationend");var x={},a=0,c=[],b,d=null,h=0,k=[];return{animate:function(a,b,c,d,f,g){g=g||{};g.from=c;g.to=d;return U("animate",a,b,f,g)},enter:function(a,b,c){c=c||{};return U("enter",a,"ng-enter",b,c)},leave:function(a,b,c){c=c||{};return U("leave",a,"ng-leave",b,c)},move:function(a,b,c){c=c||{};return U("move",a,"ng-move",b,c)},beforeSetClass:function(a,
b,c,d,f){f=f||{};b=q(c,"-remove")+" "+q(b,"-add");if(f=S("setClass",a,b,f.from))return H(a,d),f;y();d()},beforeAddClass:function(a,b,c,d){d=d||{};if(b=S("addClass",a,q(b,"-add"),d.from))return H(a,c),b;y();c()},beforeRemoveClass:function(a,b,c,d){d=d||{};if(b=S("removeClass",a,q(b,"-remove"),d.from))return H(a,c),b;y();c()},setClass:function(a,b,c,d,f){f=f||{};c=q(c,"-remove");b=q(b,"-add");return T("setClass",a,c+" "+b,d,f.to)},addClass:function(a,b,c,d){d=d||{};return T("addClass",a,q(b,"-add"),
c,d.to)},removeClass:function(a,b,c,d){d=d||{};return T("removeClass",a,q(b,"-remove"),c,d.to)}}}])}])})(window,window.angular);
//# sourceMappingURL=angular-animate.min.js.map